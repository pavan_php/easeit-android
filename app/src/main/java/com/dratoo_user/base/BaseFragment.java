package com.dratoo_user.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.View;
import android.view.WindowManager;

import com.dratoo_user.data.ToastInterface;
import com.dratoo_user.data.ToastMaker;


/**
 * Created by Admin on 2/27/2018.
 */

public abstract class BaseFragment extends Fragment implements ToastInterface {

    public String TAG = this.getClass().getSimpleName();
    private ProgressDialog progressDialog;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
    }

    /**
     * Binds the viewInteractor objects within the fragment.
     *
     * @param view
     */
    protected void bindViews(View view) {
//        ButterKnife.bind(this, view);
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity()
                    .getWindow()
                    .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void pleaseWait(String... args) {
        try {
            okayThanks();
            progressDialog = new ProgressDialog(getActivity());
            if (args.length > 0) {
                progressDialog.setMessage(args[0]);
            } else {
                progressDialog.setMessage("Please wait...");
            }
            progressDialog.setCancelable(false);
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void okayThanks() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void makeToast(int message) {
        ToastMaker.makeToast((BaseActivity) getActivity(), message);
    }

    @Override
    public void makeToast(String message) {
        if (getActivity() != null) {
            ToastMaker.makeToast((BaseActivity) getActivity(), message);
        }
    }
}
