package com.dratoo_user.base;

/**
 * Created by Admin on 2/27/2018.
 */

public interface Presenter <T extends BaseView> {

    void attachView(T view);

    void detachView();

}
