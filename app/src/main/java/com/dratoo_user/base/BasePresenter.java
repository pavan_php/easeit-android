package com.dratoo_user.base;

/**
 * Created by Admin on 2/27/2018.
 */

public abstract class BasePresenter<T extends BaseView> implements Presenter<T> {

    private T view;

    @Override
    public void attachView(T view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        view = null;
    }

    public T getView() {
        confirmViewInteractorAttached();

        return view;
    }

    private void confirmViewInteractorAttached() {
        if (!isViewInteractorAttached()) {
            throw new ViewInteractorNotAttachedException();
        }
    }

    private boolean isViewInteractorAttached() {
        return view != null;
    }

}
