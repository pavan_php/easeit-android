package com.dratoo_user.base;

/**
 * Created by Admin on 2/27/2018.
 */

public abstract  class BaseNetworkPresenter <T extends BaseView> extends BasePresenter<T> {

    @Override
    public void attachView(T viewInteractor) {
        super.attachView(viewInteractor);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

}

