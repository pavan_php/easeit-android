package com.dratoo_user.base;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.dratoo_user.data.ToastInterface;
import com.dratoo_user.data.ToastMaker;


/**
 * Created by Admin on 2/27/2018.
 */

public abstract class BaseActivity extends AppCompatActivity implements ToastInterface {

    private ProgressDialog progressDialog;
    private int orientation;

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        //noinspection WrongConstant
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void setContentView(int layoutResId) {
        super.setContentView(layoutResId);
    }

    /**
     * Method which notifies when the network connectivity changes
     *
     * @param isAvailable true - if connected to network, false - if disconnected from network.
     */
    public abstract void isNetworkAvailable(boolean isAvailable, AlertDialog.Builder dialog);

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    /**
     * /**
     * Sets orientation to portrait only.
     */
    protected void setOrientationPortrait() {
        orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }

    /**
     * Sets orientation to landscape only.
     */
    protected void setOrientationLandscape() {
        orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
    }

    /**
     * Sets orientation to sensor mode.
     */
    protected void setOrientationSensor() {
        orientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR;
    }

    /**
     * Set orientation to any supported one.
     *
     * @param orientation ActivityInfo.ORIENTATION_*
     */
    protected void setOrientation(int orientation) {
        this.orientation = orientation;
    }

  /*  public App getApp() {
        return App.getInstance();
    }*/

    public void pleaseWait(String... args) {
        try {
            okayThanks();
            progressDialog = new ProgressDialog(this);
            if (args.length > 0) {
                progressDialog.setMessage(args[0]);
            } else {
                progressDialog.setMessage("Please wait...");
            }
            progressDialog.setCancelable(false);
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void okayThanks() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void errorMessage(View coordinator, String message) {
        //        Snackbar.make(coordinator, "" + message, Snackbar.LENGTH_LONG)
        //                .setAction("Action", null).show();
        makeToast(message);
    }

    @Override
    public void makeToast(int message) {
        ToastMaker.makeToast(this, message);
    }

    @Override
    public void makeToast(String message) {
        ToastMaker.makeToast(this, message);
    }



}
