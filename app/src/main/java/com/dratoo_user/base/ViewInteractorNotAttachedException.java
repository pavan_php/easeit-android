package com.dratoo_user.base;

/**
 * Created by Admin on 2/27/2018.
 */

public class ViewInteractorNotAttachedException extends RuntimeException {

    public ViewInteractorNotAttachedException() {
        super(
                "Presenter.attachViewViewInteractor(viewInteractor) should be called before"
                        + " accessing presenter methods which uses view interactor object");
    }

}
