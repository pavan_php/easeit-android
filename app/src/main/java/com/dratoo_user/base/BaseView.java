package com.dratoo_user.base;

/**
 * Created by Admin on 2/27/2018.
 */
public interface BaseView {
    void showProgress();

    void hideProgress();
}
