package com.dratoo_user.data;

/**
 * Created by Admin on 2/27/2018.
 */

public interface ToastInterface {
    public void makeToast(int message);
    public void makeToast(String message);
}
