package com.dratoo_user.data;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Window;

public class ProgressUtils {
    private Dialog mDialog;
    private String defaultMsg;

    public ProgressUtils(Context context) {
        if (context != null) {
            mDialog = new Dialog(context);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mDialog.setCancelable(false);
//            mDialog.setContentView(R.layout.animation_loading);
        }
    }

    public void show() {
        try {
            if (mDialog != null && !mDialog.isShowing()) {
                mDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismiss() {
        try {
            if (mDialog != null) {
                Log.e("Nivi", "dismiss2: ");
                mDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}