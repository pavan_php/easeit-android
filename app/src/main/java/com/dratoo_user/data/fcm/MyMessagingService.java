package com.dratoo_user.data.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.dratoo_user.R;
import com.dratoo_user.ui.main.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    private static final int NOTIFICATION_ID = 001;
    PendingIntent pIntent;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "getNotification: " + remoteMessage.getFrom());
            Log.e(TAG, "getMessageId: " + remoteMessage.getMessageId());
            Log.e(TAG, "getTo: " + remoteMessage.getTo());
            Log.e(TAG, "getTtl: " + remoteMessage.getTtl());
            Log.e(TAG, "getTitle: " + remoteMessage.getNotification().getTitle());
            Log.e(TAG, "getTitleLocalizationArgs: " + remoteMessage.getNotification().getTitleLocalizationArgs());

            Log.e(TAG, "remoteMessage.getTag(): " + remoteMessage.getNotification().getTag());

            sendNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
            //setNotification();
        }
    }

    private void sendNotification(String messageBody, String title) {
        Log.e(TAG, "sendNotification: title " + title);

        Intent intent = new Intent(this, MainActivity.class);
        pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notificationManager.getNotificationChannel(String.valueOf(NOTIFICATION_ID));
            if (mChannel == null) {
                mChannel = new NotificationChannel(String.valueOf(NOTIFICATION_ID), getResources().getString(R.string.app_name), importance);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 500});
                notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, String.valueOf(NOTIFICATION_ID))
                    .setContentTitle(title)  // required
                    .setSmallIcon(R.mipmap.ic_launcher) // required
                    .setContentText(messageBody)  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(false)
                    .setContentIntent(pIntent);

            notificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        }

        Notification n = new Notification.Builder(this)
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(messageBody)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .setSound(soundUri).build();

        notificationManager.notify(0, n);
    }
}