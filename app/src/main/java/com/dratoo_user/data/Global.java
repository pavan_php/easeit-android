package com.dratoo_user.data;

import android.app.Activity;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.dratoo_user.R;
import com.google.android.material.snackbar.Snackbar;

public class Global {
    public static final String client_key = "SB-Mid-client-2WnRp8vvkSpukIus";
    public static final String base_url_payment = "http://php.manageprojects.in/easeit/";
    public static final String BASE_URL = "http://php.manageprojects.in/easeit/";
    public static final String IMG_URL = "http://php.manageprojects.in/easeit/public/uploads/";

    public static final String TAXI_AVAILABLE_DRIVERS = "available_drivers";
    public static final String COURIER_AVAILABE_DRIVERS = "courier_available_drivers";
    public static final String CARGO_AVAILABE_DRIVERS = "cargo_available_drivers";
    public static final String MOTO_AVAILABE_DRIVERS = "moto_available_drivers";

    public static final String HOME_FRAGMENT = "home_fragment";
    public static final String SELECTED_SERVICES = "selected_services";
    public static final String OTHER_SERVICES = "other_services";
    public static final String FROM_DESTINATION = "from_drop";
    public static final String DROP_SHORT_ADDRESS = "drop_short_address";
    public static final String PIKUP_SHORT_ADDRESS = "pikup_short_address";
    public static final String FINAL_DROP_ADDRESS = "final_drop_address";
    public static final String FINAL_PICKUP_ADDRESS = "final_pickup_address";
    public static final String DROP_LATLANG = "drop_latlang";
    public static final String PICKUP_LATLANG = "pickup_latlang";
    public static final String PICORDROP = "picordrop";

    public static final String AUTH_ID = "auth_id";
    public static final String TYPE_OF_REGISTER = "type_of_register";
    public static final String AUTH_TOKEN = "auth_token";
    public static final String MOBILE = "mobile";
    public static final String OTP = "otp";
    public static final String TYPE = "type";
    public static final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public static final String CCP = "ccp";
    public static final String LOGIN = "login";
    public static final String PIC_LANG = "PIC_LANG";
    public static final String PIC_LAT = "PIC_LAT";
    public static final String CURRNTRQID = "CURRNTRQID";
    public static final String FIREBASE_URL = "https://dratoo-services.firebaseio.com/current_request/";
    public static final String MOBILE_DRIVER = "MOBILE_DRIVER";
    public static final String REFERENCE = "REFERENCE";
    public static final String REFERENCE_START_TRIP = "REFERENCE_START_TRIP";
    public static final String REFERENCE_END_TRIP = "REFERENCE_END_TRIP";
    public static final String CURRENT_ADDRESS = "CURRENT_ADDRESS";
    public static final String CURRENT_LAT = "CURRENT_LAT";
    public static final String CURRENT_LANG = "CURRENT_LANG";

    public static final String FOOD_STYLE_ID = "FOOD_STYLE_ID";
    public static final String FOOD_NAME = "FOOD_NAME";
    public static final String RESTARANT_ID = "RESTARANT_ID";
    public static final String COMPLETED = "COMPLETED";
    public static final String CARTMODEL = "CARTMODEL";
    public static final String QUANTITY = "QUANTITY";
    public static final String COUNT = "COUNT";
    public static final String PROVIDERID = "PROVIDERID";
    public static final String SENDMODEL = "SENDMODEL";
    public static final String REQUESTID = "REQUESTID";
    public static final String NAME = "NAME";
    public static final String MOBILE_NBR = "MOBILE_NBR";
    public static final String SERVICE_TYPE = "SERVICE_TYPE";
    public static final String REQUESTID_TRACKING = "REQUESTID_TRACKING";
    public static final String SELECT_LOCATION = "SELECT_LOCATION";
    public static final String LOAD_TYPE = "LOAD_TYPE";
    public static final String EMAIL = "EMAIL";
    public static final String FINAL_AMT = "FINAL_AMT";
    public static final String PAYMENT_TYPE = "PAYMENT_TYPE";
    public static final String ENTERONCE = "ENTERONCE";
    public static final String CHECKSTATUS = "CHECKSTATUS";
    public static final String RIDE_RESPONSE = "RIDE_RESPONSE";
    public static final String RESTARANT_NAME = "RESTARANT_NAME";
    public static final String RESTARANT_PIC = "RESTARANT_PIC";
    public static final String ENTERONCE_FOOD = "ENTERONCE_FOOD";
    public static final String DROP_LAT = "DROP_LAT";
    public static final String DROP_LANG = "DROP_LANG";
    public static final String DRIVER_ID = "DRIVER_ID";
    public static final String START_DAY = "START_DAY";
    public static final String TIMING = "TIMING";
    public static final String CATEGORY_ID = "CATEGORY_ID";
    public static final String PROVIDER_DETAILS = "PROVIDER_DETAILS";
    public static final String SEND_TIMING = "SEND_TIMING";
    public static final String CURRENCY = "CURRENCY";
    public static final String ORDER_PREFIX = "ORDER_PREFIX";
    public static final String GET_VALUE = "GET_VALUE";
    public static final String OPENING_CLOSING_TIME = "OPENING_CLOSING_TIME";
    public static final String OPENING_CLOSING_TIME_TWO = "OPENING_CLOSING_TIME_TWO";
    public static final String LAT = "LAT";
    public static final String LANG = "LANG";
    public static final String RESTARANT_ADDRESS = "RESTARANT_ADDRESS";
    public static final String RESTARANT_MOBILE = "RESTARANT_MOBILE";
    public static final String ADDITEMS = "ADDITEMS";
    public static final String FB_EMAIL = "FB_EMAIL";
    public static final String FROM_RIDE = "FORM_RIDE";

    public static void snack_bar(View view, String mMessage, int mDuration, Activity activity) {
        Snackbar mSnackbar;
        mSnackbar = Snackbar.make(view, mMessage, mDuration);
        View mSnackbar_view = mSnackbar.getView();
        mSnackbar_view.setBackgroundColor(activity.getResources().getColor(R.color.blue));
        TextView textView = mSnackbar_view.findViewById(R.id.snackbar_text);
        textView.setTextColor(activity.getResources().getColor(R.color.white));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        } else {
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        mSnackbar.show();
    }
}
