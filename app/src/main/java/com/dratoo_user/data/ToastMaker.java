package com.dratoo_user.data;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Admin on 2/27/2018.
 */

public class ToastMaker {
    public static void makeToast(Context context, int message) {
        Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
    }

    public static void makeToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
