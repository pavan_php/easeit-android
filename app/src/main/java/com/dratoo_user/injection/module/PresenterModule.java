package com.dratoo_user.injection.module;


import com.dratoo_user.ui.ConfirmationRide.presenter.ConfirmationImpl;
import com.dratoo_user.ui.ConfirmationRide.presenter.ConfirmationPresenter;
import com.dratoo_user.ui.choosetimedateslot.presenter.DateTimeImpl;
import com.dratoo_user.ui.choosetimedateslot.presenter.DateTimePresenter;
import com.dratoo_user.ui.confirmationHandyMan.presenter.ConfirmationHandyManPresenter;
import com.dratoo_user.ui.confirmationHandyMan.presenter.ConfirmationHandymanImpl;
import com.dratoo_user.ui.confirmationfood.presenter.ConfirmationDetailsImpl;
import com.dratoo_user.ui.confirmationfood.presenter.ConfirmationDetailsPresenter;
import com.dratoo_user.ui.foodhome.presenter.FoodHomeImpl;
import com.dratoo_user.ui.foodhome.presenter.FoodHomePresenter;
import com.dratoo_user.ui.handymanmaplocation.presenter.HandyManImpl;
import com.dratoo_user.ui.handymanmaplocation.presenter.HandyManPresenter;
import com.dratoo_user.ui.handymanproviderdetails.presenter.HandyManProviderDetailsImpl;
import com.dratoo_user.ui.handymanproviderdetails.presenter.HandyManProviderDetailsPresenter;
import com.dratoo_user.ui.history.presenter.HistoryImpl;
import com.dratoo_user.ui.history.presenter.HistoryPresenter;

import com.dratoo_user.ui.login.presenter.LoginImpl;
import com.dratoo_user.ui.login.presenter.LoginPresenter;
import com.dratoo_user.ui.lookingdriverbooking.presenter.LookingDriverImpl;
import com.dratoo_user.ui.lookingdriverbooking.presenter.LookingDriverPresenter;
import com.dratoo_user.ui.main.presenter.MainImpl;
import com.dratoo_user.ui.main.presenter.MainPresenter;
import com.dratoo_user.ui.ongoingorder.presenter.OnGoingImpl;
import com.dratoo_user.ui.ongoingorder.presenter.OnGoingPresenter;
import com.dratoo_user.ui.ordertracking.presenter.OrderTrackingImpl;
import com.dratoo_user.ui.ordertracking.presenter.OrderTrackingPresenter;
import com.dratoo_user.ui.otpverification.presenter.OtpVerificationImpl;
import com.dratoo_user.ui.otpverification.presenter.OtpVerificationPresenter;
import com.dratoo_user.ui.profile.presenter.ProfileImpl;
import com.dratoo_user.ui.profile.presenter.ProfilePresenter;
import com.dratoo_user.ui.restarantdetails.presenter.RestarantDetailsImpl;
import com.dratoo_user.ui.restarantdetails.presenter.RestarantDetailsPresenter;
import com.dratoo_user.ui.restarnt.presenter.RestarantImpl;
import com.dratoo_user.ui.restarnt.presenter.RestarantPresenter;
import com.dratoo_user.ui.updateprofile.presenter.UpdateProfileImpl;
import com.dratoo_user.ui.updateprofile.presenter.UpdateProfilePresenter;
import com.dratoo_user.ui.welcome.presenter.WelcomeImpl;
import com.dratoo_user.ui.welcome.presenter.WelcomePresenter;

import dagger.Module;
import dagger.Provides;

/**
 *
 */
@Module
public class PresenterModule {

    @Provides
    ConfirmationPresenter providesConfirmationPresenter() {
        return new ConfirmationImpl();
    }

    @Provides
    LoginPresenter providesLoginPresenter() {
        return new LoginImpl();
    }

    @Provides
    OtpVerificationPresenter providesOtpVerificationPresenter() {
        return new OtpVerificationImpl();
    }


    @Provides
    ProfilePresenter providesProfilePresenter() {
        return new ProfileImpl();
    }

    @Provides
    LookingDriverPresenter providesLookingDriverPresenter() {
        return new LookingDriverImpl();
    }

    @Provides
    UpdateProfilePresenter providesUpdateProfilePresenter() {
        return new UpdateProfileImpl();
    }


    @Provides
    FoodHomePresenter providesFoodHomePresenter() {
        return new FoodHomeImpl();
    }

    @Provides
    RestarantPresenter providesRestarantPresenter() {
        return new RestarantImpl();
    }

    @Provides
    RestarantDetailsPresenter providesRestarantDetailsPresenter() {
        return new RestarantDetailsImpl();
    }


    @Provides
    ConfirmationDetailsPresenter providesConfirmationDetailsPresenter() {
        return new ConfirmationDetailsImpl();
    }

    @Provides
    OnGoingPresenter providesOnGoingPresenter() {
        return new OnGoingImpl();
    }


    @Provides
    OrderTrackingPresenter providesOrderTrackingPresenter() {
        return new OrderTrackingImpl();
    }

    @Provides
    HistoryPresenter providesHistoryPresenter() {
        return new HistoryImpl();
    }

    @Provides
    MainPresenter providesMainPresenter() {
        return new MainImpl();
    }

    @Provides
    HandyManPresenter providesHandyManPresenter() {
        return new HandyManImpl();
    }

    @Provides
    ConfirmationHandyManPresenter providesConfirmationHandyManPresenter() {
        return new ConfirmationHandymanImpl();
    }

    @Provides
    HandyManProviderDetailsPresenter providesHandyManProvidersDetailsPresenter() {
        return new HandyManProviderDetailsImpl();
    }

    @Provides
    DateTimePresenter providesDateTimePresenter() {
        return new DateTimeImpl();
    }

    @Provides
    WelcomePresenter providesWelcomePresenter() {
        return new WelcomeImpl();
    }
}
