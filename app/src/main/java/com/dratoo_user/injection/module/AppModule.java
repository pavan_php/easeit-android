package com.dratoo_user.injection.module;

import android.content.Context;


import com.dratoo_user.app.App;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final App application;

    public AppModule(App application) {
        this.application = application;
    }

    @Provides
    App providesApplication() {
        return application;
    }

    @Provides
    Context providesContext() {
        return application;
    }

}
