package com.dratoo_user.injection.component;

import com.dratoo_user.app.App;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.injection.module.AppModule;
import com.dratoo_user.injection.module.CommonModule;
import com.dratoo_user.injection.module.PersistentModule;
import com.dratoo_user.injection.module.PresenterModule;
import com.dratoo_user.ui.ConfirmationRide.ConfirmationActivity;
import com.dratoo_user.ui.ConfirmationRide.presenter.ConfirmationImpl;
import com.dratoo_user.ui.choosetimedateslot.DateTimeActivity;
import com.dratoo_user.ui.choosetimedateslot.presenter.DateTimeImpl;
import com.dratoo_user.ui.confirmationHandyMan.ConfirmationHandyManActivity;
import com.dratoo_user.ui.confirmationHandyMan.presenter.ConfirmationHandymanImpl;
import com.dratoo_user.ui.confirmationfood.ConfirmationDetailsActivity;
import com.dratoo_user.ui.confirmationfood.presenter.ConfirmationDetailsImpl;
import com.dratoo_user.ui.foodhome.FoodHomeActivity;
import com.dratoo_user.ui.foodhome.presenter.FoodHomeImpl;
import com.dratoo_user.ui.handymanmaplocation.HandyManMapLocationActivity;
import com.dratoo_user.ui.handymanmaplocation.presenter.HandyManImpl;
import com.dratoo_user.ui.handymanproviderdetails.HandyManProviderDetailsActivity;
import com.dratoo_user.ui.handymanproviderdetails.presenter.HandyManProviderDetailsImpl;
import com.dratoo_user.ui.history.HistoryActivity;
import com.dratoo_user.ui.history.presenter.HistoryImpl;
import com.dratoo_user.ui.login.LoginActivity;
import com.dratoo_user.ui.login.presenter.LoginImpl;
import com.dratoo_user.ui.lookingdriverbooking.LookingDriverBookingActivity;
import com.dratoo_user.ui.lookingdriverbooking.presenter.LookingDriverImpl;
import com.dratoo_user.ui.main.MainActivity;
import com.dratoo_user.ui.main.presenter.MainImpl;
import com.dratoo_user.ui.ongoingorder.OnGoingFragment;
import com.dratoo_user.ui.ongoingorder.presenter.OnGoingImpl;
import com.dratoo_user.ui.ordertracking.OrderTrackingActivity;
import com.dratoo_user.ui.ordertracking.presenter.OrderTrackingImpl;
import com.dratoo_user.ui.otpverification.OtpVerificationActivity;
import com.dratoo_user.ui.otpverification.presenter.OtpVerificationImpl;
import com.dratoo_user.ui.profile.ProfileActivity;
import com.dratoo_user.ui.profile.presenter.ProfileImpl;
import com.dratoo_user.ui.restarantdetails.RestarantDetailsActivity;
import com.dratoo_user.ui.restarantdetails.presenter.RestarantDetailsImpl;
import com.dratoo_user.ui.restarnt.RestarantActivity;
import com.dratoo_user.ui.restarnt.presenter.RestarantImpl;
import com.dratoo_user.ui.updateprofile.UpdateProfileFragment;
import com.dratoo_user.ui.updateprofile.presenter.UpdateProfileImpl;
import com.dratoo_user.ui.welcome.WelcomeActivity;
import com.dratoo_user.ui.welcome.presenter.WelcomeImpl;

import javax.inject.Singleton;

import dagger.Component;

/**
 *
 */
@Singleton
@Component(modules = {AppModule.class,
        ApiModule.class,
        PersistentModule.class,
        PresenterModule.class,
        CommonModule.class
}
)
public interface AppComponent {


    void inject(App app);


    void inject(ConfirmationImpl confirmation);

    void inject(ConfirmationActivity confirmationActivity);

    void inject(LoginImpl login);

    void inject(LoginActivity loginActivity);

    void inject(OtpVerificationImpl otpVerification);

    void inject(OtpVerificationActivity otpVerificationActivity);

    void inject(ProfileImpl profile);

    void inject(ProfileActivity profileActivity);

    void inject(LookingDriverImpl lookingDriver);

    void inject(LookingDriverBookingActivity lookingDriverBookingActivity);

    void inject(UpdateProfileImpl updateProfile);

    void inject(UpdateProfileFragment updateProfileFragment);

    void inject(FoodHomeImpl foodHome);

    void inject(FoodHomeActivity foodHomeActivity);

    void inject(RestarantImpl restarant);

    void inject(RestarantActivity restarantActivity);

    void inject(RestarantDetailsActivity restarantDetailsActivity);

    void inject(RestarantDetailsImpl restarantDetails);


    void inject(ConfirmationDetailsActivity confirmationDetailsActivity);

    void inject(ConfirmationDetailsImpl confirmationDetailsVIew);

    void inject(OnGoingImpl onGoing);

    void inject(OnGoingFragment onGoingFragment);

    void inject(OrderTrackingActivity orderTrackingActivity);

    void inject(OrderTrackingImpl orderTracking);

    void inject(HistoryImpl history);

    void inject(HistoryActivity historyActivity);

    void inject(MainImpl main);

    void inject(MainActivity mainActivity);

    void inject(HandyManImpl handyMan);

    void inject(HandyManMapLocationActivity hanyManMapLocationActivity);

    void inject(ConfirmationHandyManActivity confirmationHandyManActivity);

    void inject(ConfirmationHandymanImpl confirmationHandyman);

    void inject(HandyManProviderDetailsActivity handyManProviderDetailsActivity);

    void inject(HandyManProviderDetailsImpl handyManProviderDetails);

    void inject(DateTimeActivity dateTimeActivity);

    void inject(DateTimeImpl dateTime);

    void inject(WelcomeActivity welcomeActivity);


    void inject(WelcomeImpl welcome);


}
