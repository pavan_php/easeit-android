package com.dratoo_user.app;

import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.util.Log;

import com.dratoo_user.ui.common.ConnectivityReceiver;

/**
 * Created by Admin on 2/27/2018.
 */
public class App extends Application {
    public static App mInstance;
    private Context context;
    ConnectivityReceiver connectivityReceiver;

    public static App getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        context = getApplicationContext();

        connectivityReceiver = new ConnectivityReceiver();
        IntentFilter filters = new IntentFilter();
//        filters.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        filters.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(connectivityReceiver, filters);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        Log.e("TAG", "setConnectivityListener:mYaPP ");
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}