package com.dratoo_user.api;

import com.dratoo_user.ui.ConfirmationRide.model.AvailableDriverResponse;
import com.dratoo_user.ui.ConfirmationRide.model.CreateRequestResponse;
import com.dratoo_user.ui.ConfirmationRide.model.GetFareResponse;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentScucessResponse;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentSucessRequest;
import com.dratoo_user.ui.ConfirmationRide.model.RouteMap;
import com.dratoo_user.ui.ConfirmationRide.model.TaxiRequest;
import com.dratoo_user.ui.ConfirmationRide.model.VehicleListResponse;
import com.dratoo_user.ui.choosetimedateslot.model.SendTimeDetailsRequest;
import com.dratoo_user.ui.confirmationHandyMan.model.HandyManFareRequest;
import com.dratoo_user.ui.confirmationfood.model.AddMoreItemsRequest;
import com.dratoo_user.ui.confirmationfood.model.FoodConfirmationResponse;
import com.dratoo_user.ui.confirmationfood.model.MartCartResponse;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodConfirmModel;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodConfirmResponse;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodModel;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodResponse;
import com.dratoo_user.ui.foodhome.model.FoodResponse;
import com.dratoo_user.ui.foodhome.model.GroceryProductListResponse;
import com.dratoo_user.ui.foodhome.model.ServiceListResponse;
import com.dratoo_user.ui.handymanproviderdetails.model.HandyManTimingResponse;
import com.dratoo_user.ui.history.model.HistoryResponse;
import com.dratoo_user.ui.main.model.RideResponse;
import com.dratoo_user.ui.ordertracking.model.HandyManRating;
import com.dratoo_user.ui.ordertracking.presenter.JobDetailsResponse;
import com.dratoo_user.ui.restarantdetails.model.FoodModelRequest;
import com.dratoo_user.ui.restarantdetails.model.IncrementDecrementCartRequest;
import com.dratoo_user.ui.restarnt.model.ProviderResponse;
import com.dratoo_user.ui.login.model.RegisterRequest;
import com.dratoo_user.ui.login.model.RegisterResponse;
import com.dratoo_user.ui.lookingdriverbooking.model.CommonResponse;
import com.dratoo_user.ui.lookingdriverbooking.model.FirebaseUser;
import com.dratoo_user.ui.lookingdriverbooking.model.PaynowResponse;
import com.dratoo_user.ui.lookingdriverbooking.presenter.NotificationStartTripResponse;
import com.dratoo_user.ui.map.model.GoogleApiPlacesResponse;
import com.dratoo_user.ui.ongoingorder.model.OrderListResponse;
import com.dratoo_user.ui.ordertracking.model.OrderTrackingResponse;
import com.dratoo_user.ui.otpverification.model.OtpVerificationResponse;
import com.dratoo_user.ui.profile.model.ProfileResponse;
import com.dratoo_user.ui.restarantdetails.model.AddCartRequest;
import com.dratoo_user.ui.restarantdetails.model.FoodItemResponse;
import com.dratoo_user.ui.restarnt.model.RestarantResponse;
import com.dratoo_user.ui.restarnt.model.ServiceProviderResponse;
import com.dratoo_user.ui.restarnt.model.SubServiceResponse;
import com.dratoo_user.ui.updateprofile.model.LogoutResponse;
import com.dratoo_user.ui.updateprofile.model.ProfileUpdateResponse;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Admin on 2/27/2018.
 */
public interface ApiInterface {

    @GET("place/autocomplete/json?sensor=false&key=AIzaSyAPM27uFJwZpxYOBCetPFXfQ7gfWwc6_24")
    Call<GoogleApiPlacesResponse> getGoogleApiPlacesResponse(@Query("input") String string);

    @GET("/maps/api/directions/json?sensor=false&language=pt&key=AIzaSyAPM27uFJwZpxYOBCetPFXfQ7gfWwc6_24")
    Call<RouteMap> getRoute(@Query("origin") String origin, @Query("destination") String dest);

    //taxi get vehicles
    @GET("get_taxi_vehicles")
    Call<VehicleListResponse> getTaxiVehicles();

    @GET("get-available-drivers/{id}")
    Call<AvailableDriverResponse> getDriverDetails(@Path("id") String id);

    @POST("get-fare")
    Call<GetFareResponse> getFareDetails(@Body TaxiRequest taxiRequest);

    @POST("create-request")
    Call<CreateRequestResponse> createRequest(@Body TaxiRequest taxiRequest);

    @GET("{id}.json")
    Call<FirebaseUser> getStatusRide(@Path("id") String id);

    @FormUrlEncoded
    @POST("get-request-details")
    Call<FirebaseDetails> getDriversDetails(@Field("access_by") String accesby, @Field("request_id") String req_id);

    @GET("start_trip/{request_id}")
    Call<NotificationStartTripResponse> getNotification(@Path("request_id") String request_id);

    @FormUrlEncoded
    @POST("ride_complete")
    Call<CommonResponse> rideCompleted(@Field("request_id") String reqId, @Field("status") String status);

    @FormUrlEncoded
    @POST("paynow")
    Call<PaynowResponse> getPaynowDetails(@Field("request_id") String requestId);

    @FormUrlEncoded
    @POST("rate_provider")
    Call<RatingResponse> getRatingResponse(@Field("request_id") String reqId, @Field("rating") String rating, @Field("comments") String comments);


    //courier vechicles
    @GET("get_courier_vehicles")
    Call<VehicleListResponse> getCourierVeicles();

    @GET("courier/get-available-drivers/{id}")
    Call<AvailableDriverResponse> getDriverDetailsCourier(@Path("id") String id);

    @POST("courier/get-fare")
    Call<GetFareResponse> getFareDetailsCourier(@Body TaxiRequest taxiRequest);

    @POST("courier/create-request")
    Call<CreateRequestResponse> createRequestCourier(@Body TaxiRequest taxiRequest);

    @FormUrlEncoded
    @POST("courier/get-request-details")
    Call<FirebaseDetails> getDriversDetailsCourier(@Field("access_by") String accesby, @Field("request_id") String req_id);

    @FormUrlEncoded
    @POST("courier/paynow")
    Call<PaynowResponse> getPaynowCourierDetails(@Field("request_id") String requestId);

    @FormUrlEncoded
    @POST("courier/rate_provider")
    Call<RatingResponse> getRatingResponseCourier(@Field("request_id") String reqId, @Field("rating") String rating, @Field("comments") String comments);

    @FormUrlEncoded
    @POST("courier/ride_complete")
    Call<CommonResponse> rideCompletedCourier(@Field("request_id") String reqId, @Field("status") String status);

    @GET("courier/start_trip/{request_id}")
    Call<NotificationStartTripResponse> getNotificationCourier(@Path("request_id") String request_id);


    // moto vechicles
    @GET("get_moto_vehicles")
    Call<VehicleListResponse> getMotoVeicles();

    @GET("moto/get-available-drivers/{id}")
    Call<AvailableDriverResponse> getDriverDetailsMoto(@Path("id") String id);


    @POST("moto/get-fare")
    Call<GetFareResponse> getFareDetailsMoto(@Body TaxiRequest taxiRequest);

    @POST("moto/create-request")
    Call<CreateRequestResponse> createRequestMoto(@Body TaxiRequest taxiRequest);

    @FormUrlEncoded
    @POST("moto/get-request-details")
    Call<FirebaseDetails> getDriversDetailsMoto(@Field("access_by") String accesby, @Field("request_id") String req_id);

    @FormUrlEncoded
    @POST("moto/paynow")
    Call<PaynowResponse> getPaynowMotoDetails(@Field("request_id") String requestId);

    @FormUrlEncoded
    @POST("moto/rate_provider")
    Call<RatingResponse> getRatingResponseMoto(@Field("request_id") String reqId, @Field("rating") String rating, @Field("comments") String comments);

    @FormUrlEncoded
    @POST("moto/ride_complete")
    Call<CommonResponse> rideCompletedMoto(@Field("request_id") String reqId, @Field("status") String status);

    @GET("moto/start_trip/{request_id}")
    Call<NotificationStartTripResponse> getNotificationMoto(@Path("request_id") String request_id);


    //cargo vechicles
    @GET("get_cargo_vehicles")
    Call<VehicleListResponse> getCargoVeicles();

    @GET("cargo/get-available-drivers/{id}")
    Call<AvailableDriverResponse> getDriverDetailsCargo(@Path("id") String id);

    @POST("cargo/get-fare")
    Call<GetFareResponse> getFareDetailsCargo(@Body TaxiRequest taxiRequest);

    @POST("cargo/create-request")
    Call<CreateRequestResponse> createRequestCargo(@Body TaxiRequest taxiRequest);

    @FormUrlEncoded
    @POST("cargo/get-request-details")
    Call<FirebaseDetails> getDriversDetailsCargo(@Field("access_by") String accesby, @Field("request_id") String req_id);

    @FormUrlEncoded
    @POST("cargo/paynow")
    Call<PaynowResponse> getPaynowCargoDetails(@Field("request_id") String requestId);

    @FormUrlEncoded
    @POST("cargo/rate_provider")
    Call<RatingResponse> getRatingResponseCargo(@Field("request_id") String reqId, @Field("rating") String rating, @Field("comments") String comments);

    @FormUrlEncoded
    @POST("cargo/ride_complete")
    Call<CommonResponse> rideCompletedCargo(@Field("request_id") String reqId, @Field("status") String status);

    @GET("cargo/start_trip/{request_id}")
    Call<NotificationStartTripResponse> getNotificationCargo(@Path("request_id") String request_id);

    //Common
    @GET("get_pictures/{id}")
    Call<JobDetailsResponse> getPictures(@Path("id") String id);

    @POST("submit_rating")
    Call<CommonResponse> sendRating(@Body HandyManRating handyManRating);

    @POST("user_login")
    Call<RegisterResponse> loginOrRegister(@Body RegisterRequest registerRequest);

    @Multipart
    @POST("save_profile")
    Call<ProfileResponse> profileSave(@PartMap HashMap<String, RequestBody> updateMap, @Part MultipartBody.Part profileImage);

    @FormUrlEncoded
    @POST("otp_verify")
    Call<OtpVerificationResponse> otpVerifiy(@Field("phone") String mob, @Field("otp") String otp);

    @GET("get_profile")
    Call<ProfileUpdateResponse> getProfile();

    @Multipart
    @POST("update_profile")
    Call<ProfileUpdateResponse> profileUpdate(@PartMap HashMap<String, RequestBody> updateMap, @Part MultipartBody.Part profileImage);

    @GET("logout")
    Call<LogoutResponse> logout();

    @POST("mt_paynow")
    Call<PaymentScucessResponse> getSucessResultPayment(@Body PaymentSucessRequest paymentSucessRequest);

    @GET("user/get_current_request")
    Call<RideResponse> getRideStatus();

    //food
    @GET("food/style_list")
    Call<FoodResponse> getFoodStyleList();

    @GET("food/provider_list/{style_id}/{lat}/{lng}/{radius}")
    Call<RestarantResponse> getRestarantLists(@Path("style_id") String styleId, @Path("lat") String lat, @Path("lng") String lng, @Path("radius") String radius);

    @GET("food_list/{provider_id}")
    Call<FoodItemResponse> getProductItem(@Path("provider_id") String providerId);

    @POST("add_to_cart")
    Call<FoodItemResponse> addCart(@Body AddCartRequest addCartRequest);

    @POST("increase_cart_quantity")
    Call<FoodItemResponse> getDetailsIncrement(@Body IncrementDecrementCartRequest incrementDecrementCartRequest);

    @POST("reduce_cart_quantity")
    Call<FoodItemResponse> getDetailsDecrement(@Body IncrementDecrementCartRequest incrementDecrementCartRequest);

    @POST("food_cart")
    Call<FoodConfirmationResponse> getConfirmationDetails(@Body FoodModelRequest foodModelRequest);

    @POST("food_checkout")
    Call<PaymentFoodResponse> getPaymentDetails(@Body PaymentFoodModel paymentFoodModel);

    @POST("food_payment")
    Call<PaymentFoodConfirmResponse> getPaymentConfirm(@Body PaymentFoodConfirmModel foodConfirmModel);

    @GET("status_tracking/{id}")
    Call<OrderListResponse> getStatusTracking(@Path("id") String id);

    @GET("service_status/{id}/{request_id}")
    Call<OrderTrackingResponse> getStatus(@Path("id") String id, @Path("request_id") String request_id);

    @GET("service_status/{id}/{request_id}")
    Call<HistoryResponse> getHistory(@Path("id") String id, @Path("request_id") String request_id);

    @POST("food/add_more_item")
    Call<FoodItemResponse> getFoodMoreItems(@Body AddMoreItemsRequest addMoreItemsRequest);

    @POST("mart/add_more_item")
    Call<FoodItemResponse> getMartMoreItems(@Body AddMoreItemsRequest addMoreItemsRequest);

    //handyman
    @GET("handyman/service_list")
    Call<ServiceListResponse> getHandyManServiceList();

    @GET("handyman/provider_list/{serviceId}/{lat}/{lng}/{time}/{date}")
    Call<ServiceProviderResponse> getNearByProviderDetails(@Path("serviceId") String serviceId, @Path("lat") String lat, @Path("lng") String lng, @Path("time") String time, @Path("date") String date);

    @GET("handyman/service_wise_list/{service_id}")
    Call<SubServiceResponse> getSubServiceList(@Path("service_id") String service_id);

    @POST("handyman_cart")
    Call<HandyManTimingResponse> sendTimingDetails(@Body SendTimeDetailsRequest sendTimeDetailsRequest);

    @POST("handyman_checkout")
    Call<CommonResponse> getHandyManFareDetails(@Body HandyManFareRequest handyManFareRequest);

    //  grocery
    @GET("mart/product_list")
    Call<GroceryProductListResponse> getProductList();

    @GET("mart/provider_list/{productId}/{lat}/{lng}/{radius}")
    Call<ProviderResponse> getProviderList(@Path("productId") String productId, @Path("lat") String lat, @Path("lng") String lng, @Path("radius") String radius);

    //same food response then will use that food response class to mart
    @GET("mart_list/{providerId}")
    Call<FoodItemResponse> getDrinksMartList(@Path("providerId") String providerId);

    @POST("mart/add_to_cart")
    Call<FoodItemResponse> addCartMart(@Body AddCartRequest addCartRequest);

    @POST("mart/increase_cart_quantity")
    Call<FoodItemResponse> getDetailsIncrementMart(@Body IncrementDecrementCartRequest incrementDecrementCartRequest);

    @POST("mart/reduce_cart_quantity")
    Call<FoodItemResponse> getDetailsDecrementMart(@Body IncrementDecrementCartRequest incrementDecrementCartRequest);

    @POST("mart_cart")
    Call<MartCartResponse> getMartList(@Body FoodModelRequest foodModelRequest);

    @POST("mart_checkout")
    Call<PaymentFoodResponse> getMarkPaymentDetails(@Body PaymentFoodModel paymentFoodModel);

    @POST("mart_payment")
    Call<PaymentFoodConfirmResponse> getMartPayment(@Body PaymentFoodConfirmModel paymentFoodConfirmModel);
}