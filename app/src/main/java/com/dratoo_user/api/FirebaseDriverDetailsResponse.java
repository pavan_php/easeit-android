package com.dratoo_user.api;

public class FirebaseDriverDetailsResponse {
    public String user_id;
    public String provider_id;
    public String user_name;
    public String user_mobile;
    public String status;
    public String request_id;
    public String payment_type;
    public String price;
    public String tax;
    public String bill_amount;
    public String distance_km;
    public String pickup_lat;
    public String pickup_long;
    public String dest_address;
    public String pickup_address;
    public String created_at;
    public String fname;

    public String getPickup_address() {
        return pickup_address;
    }

    public void setPickup_address(String pickup_address) {
        this.pickup_address = pickup_address;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String lname;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    public String getDest_address() {
        return dest_address;
    }

    public void setDest_address(String dest_address) {
        this.dest_address = dest_address;
    }

    public String getSource_address() {
        return pickup_address;
    }

    public void setSource_address(String source_address) {
        this.pickup_address = source_address;
    }

    public String getVehicle_number() {
        return vehicle_number;
    }

    public void setVehicle_number(String vehicle_number) {
        this.vehicle_number = vehicle_number;
    }

    public String getId_proof() {
        return id_proof;
    }

    public void setId_proof(String id_proof) {
        this.id_proof = id_proof;
    }

    public String des_lat;
    public String des_long;
    public String phone;
    public String vehicle_number;
    public String id_proof;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(String provider_id) {
        this.provider_id = provider_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_mobile() {
        return user_mobile;
    }

    public void setUser_mobile(String user_mobile) {
        this.user_mobile = user_mobile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTax() {
        return tax;
    }

    @Override
    public String toString() {
        return "FirebaseDriverDetailsResponse{" +
                "user_id='" + user_id + '\'' +
                ", provider_id='" + provider_id + '\'' +
                ", user_name='" + user_name + '\'' +
                ", user_mobile='" + user_mobile + '\'' +
                ", status='" + status + '\'' +
                ", request_id='" + request_id + '\'' +
                ", payment_type='" + payment_type + '\'' +
                ", price='" + price + '\'' +
                ", tax='" + tax + '\'' +
                ", bill_amount='" + bill_amount + '\'' +
                ", distance_km='" + distance_km + '\'' +
                ", pickup_lat='" + pickup_lat + '\'' +
                ", pickup_long='" + pickup_long + '\'' +
                ", des_lat='" + des_lat + '\'' +
                ", des_long='" + des_long + '\'' +
                ", phone='" + phone + '\'' +
                ", vehicle_number='" + vehicle_number + '\'' +
                ", id_proof='" + id_proof + '\'' +
                ", authToken='" + authToken + '\'' +
                ", otp='" + otp + '\'' +
                ", otp_status='" + otp_status + '\'' +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", profile_pic='" + profile_pic + '\'' +
                '}';
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getBill_amount() {
        return bill_amount;
    }

    public void setBill_amount(String bill_amount) {
        this.bill_amount = bill_amount;
    }

    public String getDistance_km() {
        return distance_km;
    }

    public void setDistance_km(String distance_km) {
        this.distance_km = distance_km;
    }

    public String getPickup_lat() {
        return pickup_lat;
    }

    public void setPickup_lat(String pickup_lat) {
        this.pickup_lat = pickup_lat;
    }

    public String getPickup_long() {
        return pickup_long;
    }

    public void setPickup_long(String pickup_long) {
        this.pickup_long = pickup_long;
    }

    public String getDes_lat() {
        return des_lat;
    }

    public void setDes_lat(String des_lat) {
        this.des_lat = des_lat;
    }

    public String getDes_long() {
        return des_long;
    }

    public void setDes_long(String des_long) {
        this.des_long = des_long;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getOtp_status() {
        return otp_status;
    }

    public void setOtp_status(String otp_status) {
        this.otp_status = otp_status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String authToken;
    public String otp;
    public String otp_status;
    public String type;
    public String name;
    public String email;
    public String profile_pic;
}