package com.dratoo_user.api;

public class FirebaseDetails {

    public FirebaseDriverDetailsResponse getRequest_data() {
        return request_data;
    }

    @Override
    public String toString() {
        return "FirebaseDetails{" +
                "request_data=" + request_data +
                '}';
    }

    public void setRequest_data(FirebaseDriverDetailsResponse request_data) {
        this.request_data = request_data;
    }

    public FirebaseDriverDetailsResponse request_data;

}
