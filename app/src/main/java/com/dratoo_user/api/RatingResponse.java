package com.dratoo_user.api;

import com.dratoo_user.ui.lookingdriverbooking.model.RatingDetailsResponse;

public class RatingResponse {

    public RatingDetailsResponse getData() {
        return data;
    }

    public void setData(RatingDetailsResponse data) {
        this.data = data;
    }

    public RatingDetailsResponse data;
}
