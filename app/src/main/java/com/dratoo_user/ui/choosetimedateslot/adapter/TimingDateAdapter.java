package com.dratoo_user.ui.choosetimedateslot.adapter;

import android.graphics.drawable.GradientDrawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dratoo_user.R;
import com.dratoo_user.ui.choosetimedateslot.DateTimeActivity;

import java.util.ArrayList;

public class TimingDateAdapter extends RecyclerView.Adapter<TimingDateAdapter.ViewHolder> {


    DateTimeActivity dateTimeActivity;

    ArrayList<String> timingList;

    private int pos;


    public TimingDateAdapter(DateTimeActivity dateTimeActivity, ArrayList<String> timinglayout) {

        this.dateTimeActivity = dateTimeActivity;
        this.timingList = timinglayout;

    }


    @NonNull
    @Override
    public TimingDateAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.timing_layout, viewGroup, false);
        return new ViewHolder(view);


    }

    @Override
    public void onBindViewHolder(@NonNull TimingDateAdapter.ViewHolder viewHolder, int i) {



        if (pos == i) {

            viewHolder.tv_time_slot.setTextColor(dateTimeActivity.getResources().getColor(R.color.colorPrimary));

            GradientDrawable drawable = (GradientDrawable) viewHolder.tv_time_slot.getBackground();
            drawable.setStroke(3, dateTimeActivity.getResources().getColor(R.color.colorPrimary));
            drawable.setColor(dateTimeActivity.getResources().getColor(R.color.alpha_yellow));// set stroke width and stroke color

        } else {

            viewHolder.tv_time_slot.setTextColor(dateTimeActivity.getResources().getColor(R.color.dark_gray));

            GradientDrawable drawable = (GradientDrawable) viewHolder.tv_time_slot.getBackground();
            drawable.setStroke(3, dateTimeActivity.getResources().getColor(R.color.light_gray));
            drawable.setColor(dateTimeActivity.getResources().getColor(R.color.white));// set stroke width and stroke color

        }


        viewHolder.tv_time_slot.setText(timingList.get(i));


    }

    @Override
    public int getItemCount() {

        return timingList.size();

    }

    public void OnItemClicked(int position, boolean val) {


            pos = position;



    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        AppCompatTextView tv_time_slot;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_time_slot = itemView.findViewById(R.id.tv_time_slot);

        }
    }
}
