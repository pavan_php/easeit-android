package com.dratoo_user.ui.choosetimedateslot.presenter;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;

import javax.inject.Inject;
import javax.inject.Named;

public class DateTimeImpl extends BasePresenter<DateTimeView> implements DateTimePresenter {

    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;


    @Override
    public void attachView(DateTimeView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }


}
