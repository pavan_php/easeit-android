package com.dratoo_user.ui.choosetimedateslot.adapter;

import android.graphics.drawable.GradientDrawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.dratoo_user.R;
import com.dratoo_user.ui.choosetimedateslot.DateTimeActivity;

import java.util.ArrayList;

public class DaysAdapter extends RecyclerView.Adapter<DaysAdapter.ViewHolder> {


    DateTimeActivity dateTimeActivity;

    ArrayList<String> timingList;

    private int posDays;

    public DaysAdapter(DateTimeActivity dateTimeActivity, ArrayList<String> daytimeList) {

        this.dateTimeActivity = dateTimeActivity;
        this.timingList = daytimeList;
    }

    @NonNull
    @Override
    public DaysAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.days_items, viewGroup, false);
        return new ViewHolder(view);
    }

    public void OnItemClickedDays(int position, boolean b) {
        this.posDays = position;
    }

    @Override
    public void onBindViewHolder(@NonNull DaysAdapter.ViewHolder viewHolder, int i) {


        String data = timingList.get(i);

        String[] arr = data.split(" ");

        viewHolder.tv_txt_date.setText(arr[0]);
        viewHolder.tv_nbr_txt.setText(arr[1]);

        if (posDays == i) {


            viewHolder.tv_txt_date.setTextColor(dateTimeActivity.getResources().getColor(R.color.colorPrimary));
            viewHolder.tv_nbr_txt.setTextColor(dateTimeActivity.getResources().getColor(R.color.colorPrimary));
            GradientDrawable drawable = (GradientDrawable) viewHolder.whole_layout.getBackground();
            drawable.setStroke(3, dateTimeActivity.getResources().getColor(R.color.colorPrimary));
            drawable.setColor(dateTimeActivity.getResources().getColor(R.color.alpha_yellow));// set stroke width and stroke color

        } else {

            viewHolder.tv_txt_date.setTextColor(dateTimeActivity.getResources().getColor(R.color.dark_gray));
            viewHolder.tv_nbr_txt.setTextColor(dateTimeActivity.getResources().getColor(R.color.dark_gray));
            GradientDrawable drawable = (GradientDrawable) viewHolder.whole_layout.getBackground();
            drawable.setStroke(3, dateTimeActivity.getResources().getColor(R.color.greyish));
            drawable.setColor(dateTimeActivity.getResources().getColor(R.color.white));// set stroke width and stroke color

        }

    }

    @Override
    public int getItemCount() {
        return timingList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        AppCompatTextView tv_nbr_txt;

        @Nullable
        AppCompatTextView tv_txt_date;
        @Nullable
        LinearLayout whole_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_txt_date = itemView.findViewById(R.id.tv_txt_date);
            tv_nbr_txt = itemView.findViewById(R.id.tv_nbr_txt);
            whole_layout = itemView.findViewById(R.id.whole_layout);
        }
    }
}
