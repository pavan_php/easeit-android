package com.dratoo_user.ui.choosetimedateslot;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.choosetimedateslot.adapter.DaysAdapter;
import com.dratoo_user.ui.choosetimedateslot.adapter.TimingDateAdapter;
import com.dratoo_user.ui.choosetimedateslot.presenter.DateTimePresenter;
import com.dratoo_user.ui.choosetimedateslot.presenter.DateTimeView;
import com.dratoo_user.ui.common.ItemClickSupport;
import com.dratoo_user.ui.common.ProgressDialog;
import com.dratoo_user.ui.handymanmaplocation.HandyManMapLocationActivity;
import com.dratoo_user.ui.restarnt.RestarantActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DateTimeActivity extends AppCompatActivity implements DateTimeView {


    @Inject
    DateTimePresenter dateTimePresenter;

    @BindView(R.id.back)
    AppCompatImageView back;
    @BindView(R.id.search)
    LinearLayout search;
    @BindView(R.id.rv_days)
    RecyclerView rvDays;
    @BindView(R.id.rv_times)
    RecyclerView rvTimes;
    @BindView(R.id.btn_next)
    AppCompatButton btnNext;

    @BindView(R.id.tv_empty_timings)
    AppCompatTextView tvEmptyTimings;
    @BindView(R.id.tv_header_one)
    AppCompatTextView tvHeaderOne;
    @BindView(R.id.view_one)
    View viewOne;
    @BindView(R.id.tv_header_two)
    AppCompatTextView tvHeaderTwo;
    @BindView(R.id.view_two)
    View viewTwo;
    @BindView(R.id.parant_layout)
    RelativeLayout parantLayout;
    private TimingDateAdapter adapter;

    ArrayList<String> timinglayout = new ArrayList<>();


    private String date;
    private ArrayList<String> daytimeList = new ArrayList<>();
    private ArrayList<String> formateDate = new ArrayList<>();
    private DaysAdapter daysAdapter;

    ProgressDialog progressDialog;
    private String apiDateValue;
    private String timeApi;
    private boolean clickedDays = false;
    private boolean clickedTime = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_time);
        ButterKnife.bind(this);

        DaggerAppComponent.create().inject(this);
        dateTimePresenter.attachView(this);

        progressDialog = new ProgressDialog(this);

        setUpToolBar();

        getCurrentMonthDays();


    }


    private void setUpToolBar() {


    }

    private void getCurrentMonthDays() {
        Calendar cal = Calendar.getInstance();

        int currentDay = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.DAY_OF_MONTH, currentDay);
        int myMonth = cal.get(Calendar.MONTH);

        while (myMonth == cal.get(Calendar.MONTH)) {

            Log.e("Nive ", "onCreate:monthMaxDays " + cal.getTime());
            changeFormat(cal.getTime());
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(DateTimeActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvDays.setLayoutManager(layoutManager);

        Log.e("Nive", "onCreate:daytimeList " + daytimeList.size());

        daysAdapter = new DaysAdapter(this, daytimeList);
        rvDays.setAdapter(daysAdapter);
        daysAdapter.notifyDataSetChanged();


        ItemClickSupport.addTo(rvDays)
                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                        Log.e("Nive ", "onItemClicked: " + position);
                        daysAdapter.OnItemClickedDays(position, true);
                        daysAdapter.notifyDataSetChanged();
                        getClickedDays(daytimeList.get(position), position);

                    }
                });


        initTimingRecyclerView();


    }

    private void getClickedDays(String string, int position) {

        Log.e("Nive ", "getClickedDays: " + string);

        clickedDays = true;

        apiDateValue = formateDate.get(position);

        PrefConnect.writeString(DateTimeActivity.this, Global.START_DAY, apiDateValue);


        Calendar cal = Calendar.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE dd", Locale.getDefault());

        String str = simpleDateFormat.format(cal.getTime());


        Log.e("Nive ", "getCurrentDate: " + str);

        if (str.equals(string)) {
            timinglayout.clear();

            initTimingRecyclerView();

        } else {

            timinglayout.clear();

            timinglayout.add("8 Am - 10 Am");
            timinglayout.add("10 Am - 12 Pm");
            timinglayout.add("12 Pm - 2 Pm");
            timinglayout.add("2 Pm - 4 Pm");
            timinglayout.add("4 Pm - 6 Pm");
            timinglayout.add("6 Pm - 8 Pm");

            if (timinglayout.size() > 0) {

                rvTimes.setVisibility(View.VISIBLE);
                tvEmptyTimings.setVisibility(View.GONE);
                rvTimes.setLayoutManager(new GridLayoutManager(this, 2));

                adapter = new TimingDateAdapter(this, timinglayout);
                rvTimes.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
                rvTimes.setVisibility(View.GONE);
                tvEmptyTimings.setVisibility(View.VISIBLE);
            }

        }


    }


    //INFO: TODO  day to text and number format
    private void changeFormat(Date time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE dd", Locale.getDefault());

        String str = simpleDateFormat.format(time);

        SimpleDateFormat simpleDateFormatApi = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        String strApi = simpleDateFormatApi.format(time);

        daytimeList.add(str);

        formateDate.add(strApi);


        Log.e("Nive", "changeFormat: " + daytimeList);
        Log.e("Nive", "changeFormat:formateDate " + formateDate);
    }

    private void initTimingRecyclerView() {


        //date output format
        DateFormat dateFormat = new SimpleDateFormat("HH");
        Calendar cal = Calendar.getInstance();
        String timing = dateFormat.format(cal.getTime());
        Log.e("Nive ", "getCurrentTime: " + timing);

        int time = Integer.parseInt(timing);


        if (timing.equals("08") && timing.equals("09")) {
            timinglayout.add("10 Am - 12 Pm");
            timinglayout.add("12 Pm - 2 Pm");
            timinglayout.add("2 Pm - 4 Pm");
            timinglayout.add("4 Pm - 6 Pm");


        } else if (timing.equals("10") && timing.equals("11")) {

            timinglayout.add("12 Pm - 2 Pm");
            timinglayout.add("2 Pm - 4 Pm");
            timinglayout.add("4 Pm - 6 Pm");
        } else if (timing.equals("12") && timing.equals("1")) {

            timinglayout.add("2 Pm - 4 Pm");
            timinglayout.add("4 Pm - 6 Pm");

        } else if (timing.equals("02") || timing.equals("03") || timing.equals("14") || timing.equals("15")) {

            timinglayout.add("4 Pm - 6 Pm");
            timinglayout.add("6 Pm - 8 Pm");

        } else if (timing.equals("04") || timing.equals("05") || timing.equals("16") || timing.equals("17")) {
            timinglayout.add("6 Pm - 8 Pm");
        } else if (timing.equals("06") || timing.equals("07") || timing.equals("18") || timing.equals("19")) {

        } else if (timing.equals("08") || timing.equals("20")) {

        } else if (time >= 20||time>=8) {

        } else {

            timinglayout.add("8 Am - 10 Am");
            timinglayout.add("10 Am - 12 Pm");
            timinglayout.add("12 Pm - 2 Pm");
            timinglayout.add("2 Pm - 4 Pm");
            timinglayout.add("4 Pm - 6 Pm");
            timinglayout.add("6 Pm - 8 Pm");
        }


        if (timinglayout.size() > 0) {

            rvTimes.setVisibility(View.VISIBLE);
            tvEmptyTimings.setVisibility(View.GONE);
            rvTimes.setLayoutManager(new GridLayoutManager(this, 2));

            adapter = new TimingDateAdapter(this, timinglayout);
            rvTimes.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {
            rvTimes.setVisibility(View.GONE);
            tvEmptyTimings.setVisibility(View.VISIBLE);
        }


        ItemClickSupport.addTo(rvTimes)
                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                        Log.e("Nive ", "onItemClicked: " + position);
                        adapter.OnItemClicked(position, true);
                        adapter.notifyDataSetChanged();
                        getClickedTimings(timinglayout.get(position));

                    }
                });


    }

    private void getClickedTimings(String str) {

        Log.e("Nive ", "getClickedTimings: " + str);

        timeApi = str;

        clickedTime = true;

        PrefConnect.writeString(this, Global.TIMING, timeApi);

    }


    @OnClick({R.id.back, R.id.btn_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
//                finish();
                Intent intent = new Intent(DateTimeActivity.this, RestarantActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_next:


                if (tvEmptyTimings.getVisibility() == View.VISIBLE) {

                    Global.snack_bar(parantLayout, "There is no Time Slot for Selected Date! Please Select Someother Date!!!", Snackbar.LENGTH_LONG, DateTimeActivity.this);

                } else {
                    if (clickedDays) {

                    } else {
                        PrefConnect.writeString(DateTimeActivity.this, Global.START_DAY, formateDate.get(0));
                    }

                    if (clickedTime) {

                    } else {
                        PrefConnect.writeString(this, Global.TIMING, timinglayout.get(0));
                    }

                    Intent intentTime = new Intent(DateTimeActivity.this, HandyManMapLocationActivity.class);
                    startActivity(intentTime);


                }


                break;
        }
    }

    @Override
    public void showProgress() {

        progressDialog.show();

    }

    @Override
    public void hideProgress() {

        progressDialog.dismissLoader();

    }

}
