package com.dratoo_user.ui.map.model;

import java.util.ArrayList;

public class GoogleApiPlacesResponse {


    private ArrayList<Predictions> predictions;

    private String status;


    public ArrayList<Predictions> getPredictions() {
        return predictions;
    }

    public void setPredictions(ArrayList<Predictions> predictions) {
        this.predictions = predictions;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [predictions = "+predictions+", status = "+status+"]";
    }
}
