package com.dratoo_user.ui.map.model;

public class Predictions {

    private String description;

    public String getDescription() {
        return description;
    }

    public StructuredFormat getStructured_formatting() {
        return structured_formatting;
    }

    public void setStructured_formatting(StructuredFormat structured_formatting) {
        this.structured_formatting = structured_formatting;
    }

    public StructuredFormat structured_formatting;

    public void setDescription(String description) {
        this.description = description;
    }
}
