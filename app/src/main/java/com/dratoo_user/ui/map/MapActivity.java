package com.dratoo_user.ui.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.dratoo_user.R;
import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.app.App;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.ui.BaseRetrofit;
import com.dratoo_user.ui.ConfirmationRide.ConfirmationActivity;
import com.dratoo_user.ui.common.ConnectivityReceiver;
import com.dratoo_user.ui.common.ItemClickSupport;
import com.dratoo_user.ui.map.adapter.RecyclerPlacesAdapterClass;
import com.dratoo_user.ui.map.model.GoogleApiPlacesResponse;
import com.dratoo_user.ui.map.model.Predictions;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, ConnectivityReceiver.ConnectivityReceiverListener {

    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.search_location_layout)
    LinearLayout searchLocationLayout;
    @BindView(R.id.top_layout)
    FrameLayout topLayout;
    @BindView(R.id.map_dest_img)
    AppCompatImageView mapDestImg;
    @BindView(R.id.tv_search_slide)
    AppCompatEditText tvSearch;
    @BindView(R.id.map_search)
    AppCompatImageView mapSearch;
    @BindView(R.id.dragView)
    FrameLayout dragView;
    @BindView(R.id.sliding_layout)
    SlidingUpPanelLayout slidingLayout;
    @BindView(R.id.selec_via_map_layout)
    LinearLayout selecViaMapLayout;
    @BindView(R.id.below_layout)
    LinearLayout belowLayout;
    @BindView(R.id.tv_select_map)
    AppCompatTextView tvSelectMap;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.list_auto_complete)
    RecyclerView listAutoComplete;
    @BindView(R.id.img_place_holder)
    AppCompatImageView imgPlaceHolder;
    @BindView(R.id.tv_drop_address)
    AppCompatTextView tvDropAddress;
    @BindView(R.id.destination_layout)
    LinearLayout destinationLayout;
    @BindView(R.id.tv_short_address)
    AppCompatTextView tvShortAddress;
    @BindView(R.id.btn_set_destination)
    AppCompatButton btnSetDestination;
    @BindView(R.id.select_pickup_layout)
    LinearLayout selectPickupLayout;
    @BindView(R.id.img_place_holder_pickup)
    AppCompatImageView imgPlaceHolderPickup;
    @BindView(R.id.tv_short_address_pickup)
    AppCompatTextView tvShortAddressPickup;
    @BindView(R.id.tv_drop_address_pickup)
    AppCompatTextView tvDropAddressPickup;
    @BindView(R.id.select_destination_layout)
    LinearLayout selectDestinationLayout;
    @BindView(R.id.btn_set_pickup)
    AppCompatButton btnSetPickup;
    @BindView(R.id.pickup_layout)
    LinearLayout pickupLayout;
    @BindView(R.id.img_search_des)
    AppCompatImageView imgSearchDes;
    @BindView(R.id.img_search)
    AppCompatImageView imgSearch;
    @BindView(R.id.bubble_loader)
    AVLoadingIndicatorView bubbleLoader;
    @BindView(R.id.tv_header)
    AppCompatTextView tvHeader;
    @BindView(R.id.sothree_bottom_layout)
    LinearLayout sothreeBottomLayout;
    @BindView(R.id.center_frame_drop)
    FrameLayout centerFrameDrop;
    @BindView(R.id.center_frame_pickup)
    FrameLayout centerFramePickup;
    @BindView(R.id.select_location_layout)
    LinearLayout selectLocationLayout;
    @BindView(R.id.parant_layout)
    RelativeLayout parantLayout;
    private GoogleMap mMap;


    private boolean locationReceiveed = false;
    private Geocoder geocoder;
    private String zipcode, locality;
    private String city, state, country, locationCurrent, locationAdd;


    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private Location mLastLocation;


    LatLng pickupLatLang;

    SupportMapFragment HomemapFragment;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 101;
    public static final int REQUEST_CHECK_SETTINGS = 100;


    SlidingUpPanelLayout.PanelState panelState;

    ArrayList<Predictions> predictionsArrayList = new ArrayList<>();

    RecyclerPlacesAdapterClass mAdapter;
    private ApiInterface apiInterface;

    private String strLocation = "";

    private LatLng latLang;
    private double longitude;
    private double latitude;

    private Animation animShow, animHide;


    CameraUpdate center;
    CameraUpdate zoom;
    private MenuItem item;

    AVLoadingIndicatorView bubble_loader;
    private LatLng trackLatLangDrop;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);

        bubble_loader = findViewById(R.id.bubble_loader);

        bubble_loader.smoothToShow();


        BaseRetrofit baseRetrofit = new BaseRetrofit(this);

        apiInterface = BaseRetrofit.getGoogleApi().create(ApiInterface.class);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.map_back);

        //initialize drop as a first
        PrefConnect.writeString(MapActivity.this, Global.PICORDROP, "DROP");


        slidingLayout.setTouchEnabled(false);

        animShow = AnimationUtils.loadAnimation(this, R.anim.popup_show);
        animHide = AnimationUtils.loadAnimation(this, R.anim.popup_hide);


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(1000)
                .setFastestInterval(1000);

        PrefConnect.writeBoolean(MapActivity.this, Global.FROM_DESTINATION, false);

        HomemapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        HomemapFragment.getMapAsync(this);


        //get Current Location

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult != null) {


                    super.onLocationResult(locationResult);
                    mLastLocation = locationResult.getLastLocation();

                    Log.e("Nive ", "onLocationResult:Map " + mLastLocation.getLatitude());


                    //this method handler use to certain time get started the funtion (layout should be hide)

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            // this function used for (callback) call lot of time use boolean function to avoid setting marker lot of time only one time set marker and get latlang

                            if (locationReceiveed) {
                                searchLocationLayout.setVisibility(View.GONE);

                            } else {
                                bubble_loader.hide();
                                searchLocationLayout.setVisibility(View.VISIBLE);

                                if (mLastLocation != null) {

                                    // pickupLatLang

                                    pickupLatLang = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                                    setMarker(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
                                    getAddress(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), "Current");
                                }
                            }


                        }
                    }, 2000);
                } else {
                    Log.e("Nive ", "onLocationResult:Null " + locationResult);
                }
            }
        };

        //detect what stage slidinguppanel layout is there


        slidingLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {

                Log.e("Nive ", "onPanelStateChanged:Call ");

                if (SlidingUpPanelLayout.PanelState.EXPANDED == slidingLayout.getPanelState()) {

                    Log.e("Nive", "onPanelStateChanged: ");

                    selecViaMapLayout.setVisibility(View.VISIBLE);
                    view.setVisibility(View.VISIBLE);


                } else {

                    selecViaMapLayout.setVisibility(View.GONE);
                    view.setVisibility(View.GONE);
                    listAutoComplete.setVisibility(View.GONE);


                    Log.e("Nive", "onPanelStateChanged:else ");

                }


            }
        });


        tvSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() == 0) {
                    predictionsArrayList.clear();
                    if (mAdapter != null)
                        mAdapter.notifyDataSetChanged();
                } else {
                    getTypedLocation(s.toString());

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ItemClickSupport.addTo(listAutoComplete).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                slidingLayout.setVisibility(View.GONE);
                destinationLayout.startAnimation(animShow);

                Log.e("Nive ", "onItemClicked: " + predictionsArrayList.get(position).getDescription());
                if (!predictionsArrayList.get(position).getDescription().equalsIgnoreCase(null)) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    strLocation = predictionsArrayList.get(position).getDescription();
                    tvSearch.setText(strLocation);
                    latLang = getLocationFromAddress(MapActivity.this, strLocation);

                    if (latLang != null) {
                        try {


                            if (PrefConnect.readString(MapActivity.this, Global.PICORDROP, "").equals("DROP")) {


                                latitude = latLang.latitude;
                                longitude = latLang.longitude;
                                Log.e("Nive ", "onItemClicked:Drop ");

                                PrefConnect.writeString(MapActivity.this, Global.DROP_LATLANG, String.valueOf(latLang));

                                destinationLayout.setVisibility(View.VISIBLE);
                                tvSearch.setText(strLocation);

                                getAddress(latLang, "AutoComplete");

                                tvShortAddress.setText(predictionsArrayList.get(position).getStructured_formatting().getMain_text());
                                tvDropAddress.setText(predictionsArrayList.get(position).getDescription());
                                PrefConnect.writeString(MapActivity.this, Global.DROP_SHORT_ADDRESS, tvShortAddress.getText().toString());


                                mMap.clear();
                                mMap.addMarker(new MarkerOptions().position(new LatLng(latLang.latitude, latLang.longitude)).icon(BitmapDescriptorFactory.fromResource(R.drawable.drop_location)));
                                center = CameraUpdateFactory.newLatLng(new LatLng(latLang.latitude, latLang.longitude));
                                zoom = CameraUpdateFactory.zoomTo(16);
                                mMap.moveCamera(center);
                                mMap.animateCamera(zoom);

                            } else {
                                PrefConnect.writeString(MapActivity.this, Global.PICKUP_LATLANG, String.valueOf(latLang));
                                latitude = latLang.latitude;
                                longitude = latLang.longitude;
                                Log.e("Nive ", "onItemClicked:pickup ");


                                pickupLatLang = new LatLng(latitude, longitude);


                                tvShortAddressPickup.setText(predictionsArrayList.get(position).getStructured_formatting().getMain_text());
                                tvDropAddressPickup.setText(predictionsArrayList.get(position).getDescription());
                                PrefConnect.writeString(MapActivity.this, Global.PIKUP_SHORT_ADDRESS, tvShortAddressPickup.getText().toString());
                                slidingLayout.setVisibility(View.GONE);
                                pickupLayout.startAnimation(animShow);
                                pickupLayout.setVisibility(View.VISIBLE);
                                tvSearch.setText(strLocation);

                                getAddress(latLang, "AutoComplete");

                                mMap.clear();
                                mMap.addMarker(new MarkerOptions().position(new LatLng(latLang.latitude, latLang.longitude)).icon(BitmapDescriptorFactory.fromResource(R.drawable.pick_pin)));
                                center = CameraUpdateFactory.newLatLng(new LatLng(latLang.latitude, latLang.longitude));
                                zoom = CameraUpdateFactory.zoomTo(16);
                                mMap.moveCamera(center);
                                mMap.animateCamera(zoom);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }


                }
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {

        String message;

        if (isConnected) {
            message = getResources().getString(R.string.connected_to_internet);

        } else {
            message = getResources().getString(R.string.not_connected_to_internet);

        }

        Global.snack_bar(parantLayout, message, Snackbar.LENGTH_LONG, MapActivity.this);

    }


    private void getTypedLocation(String str) {
        listAutoComplete.setVisibility(View.VISIBLE);
        Call<GoogleApiPlacesResponse> call = apiInterface.getGoogleApiPlacesResponse(str);
        call.enqueue(new Callback<GoogleApiPlacesResponse>() {
            @Override
            public void onResponse(Call<GoogleApiPlacesResponse> call, Response<GoogleApiPlacesResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body().getStatus().equals("OK")) {
                        predictionsArrayList = response.body().getPredictions();
                        mAdapter = new RecyclerPlacesAdapterClass(MapActivity.this, predictionsArrayList);
                        listAutoComplete.setLayoutManager(new LinearLayoutManager(MapActivity.this));
                        listAutoComplete.setAdapter(mAdapter);


                    }

                }
            }

            @Override
            public void onFailure(Call<GoogleApiPlacesResponse> call, Throwable t) {
                Log.e("Nive ", "onFailure: " + t.toString());

            }
        });

    }


    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Log.e("Nive ", "getLocationFromAddress: " + strAddress);
        geocoder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;
        try {
// May throw an IOException
            address = geocoder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude());
            Log.e("Nive ", "getLocationFromAddress:LatLang " + p1);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return p1;
    }


    private void getAddress(LatLng LatLng, String str) {
        geocoder = new Geocoder(this, Locale.getDefault());

        Log.e("Nive ", "getAddress: " + LatLng);

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(LatLng.latitude, LatLng.longitude, 1);


            if (addresses.get(0).getPostalCode() != null) {
                zipcode = addresses.get(0).getPostalCode();
                Log.e("Nive ", "getPlaceInfo:ZIP " + zipcode);

            }
            if (addresses.get(0).getLocality() != null) {
                locality = addresses.get(0).getLocality();
                Log.e("Nive ", "getPlaceInfo:ZIP " + locality);

            }

            if (addresses.get(0).getLocality() != null) {
                city = addresses.get(0).getLocality();
                Log.e("Nive ", "getPlaceInfo:city " + city);
            }

            if (addresses.get(0).getAdminArea() != null) {
                state = addresses.get(0).getAdminArea();
                Log.e("Nive ", "getPlaceInfo:state " + state);
            }

            if (addresses.get(0).getCountryName() != null) {
                country = addresses.get(0).getCountryName();
                Log.e("Nive ", "getPlaceInfo:country " + country);
            }

            if (str.equals("Current")) {
                PrefConnect.writeString(MapActivity.this, Global.PICKUP_LATLANG, String.valueOf(LatLng));
                PrefConnect.writeString(MapActivity.this, Global.PIKUP_SHORT_ADDRESS, locality);
                locationCurrent = city.concat(",").concat(state).concat(",").concat(country).concat(",").concat(zipcode);
            } else {

                if (PrefConnect.readString(MapActivity.this, Global.PICORDROP, "").equals("DROP")) {

                    locationAdd = city.concat(",").concat(state).concat(",").concat(country).concat(",").concat(zipcode);
                } else {

                    locationAdd = city.concat(",").concat(state).concat(",").concat(country).concat(",").concat(zipcode);

                }

            }

            Log.e("Nive ", "getAddress:final " + locationAdd);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setMarker(LatLng latLng) {
        locationReceiveed = true;


        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,
                14));

        searchLocationLayout.setVisibility(View.GONE);

        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);


        slidingLayout.setVisibility(View.VISIBLE);


    }


    public void checkLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // Asking user if explanation is needed
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);

                    Log.e("Nive ", "checkLocationPermission:ACCESS_FINE_LOCATION ");
                } else {
                    Log.e("Nive ", "checkLocationPermission:MY_PERMISSIONS_REQUEST_LOCATION ");

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }
            } else {
                checkGps();
            }
        } else {
            checkGps();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
// If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        checkGps();

                        mLocationCallback = new LocationCallback() {
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                if (locationResult != null) {
                                    super.onLocationResult(locationResult);
                                    mLastLocation = locationResult.getLastLocation();
                                    Log.e("Nive ", "onLocationResult: " + mLastLocation);

                                    //this method handler use to certain time get started the funtion (layout should be hide)

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.e("Nive ", "run: " + locationReceiveed);


                                            if (locationReceiveed) {
                                                searchLocationLayout.setVisibility(View.GONE);


                                            } else {
                                                searchLocationLayout.setVisibility(View.VISIBLE);

                                                if (mLastLocation != null) {
                                                    pickupLatLang = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                                                    setMarker(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
                                                    getAddress(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), "Current");
                                                }
                                            }


                                        }
                                    }, 2000);
                                }
                            }
                        };

                    }

                } else {
// Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

        }
    }


    private void checkGps() {
        LocationSettingsRequest.Builder settingsBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        settingsBuilder.setAlwaysShow(true);

        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(this).checkLocationSettings(settingsBuilder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    setLocalUpdateRequest();
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(
                                        MapActivity.this,
                                        REQUEST_CHECK_SETTINGS);

                                Log.e("Nive ", "onComplete: gps");
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void setLocalUpdateRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback, Looper.myLooper());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.location, menu);
        item = menu.findItem(R.id.current_location_icon);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;
            case R.id.current_location_icon:
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        App.getInstance().setConnectivityListener(this);

        checkLocationPermission();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        mMap.setMyLocationEnabled(false);
        GoogleMapOptions options = new GoogleMapOptions().liteMode(true);


        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {


                trackLatLangDrop = mMap.getCameraPosition().target;

                if (centerFrameDrop.getVisibility() == View.VISIBLE) {
                    getAddressFromSelectViaMap(trackLatLangDrop.latitude, trackLatLangDrop.longitude, "drop");

                } else {

                    getAddressFromSelectViaMap(trackLatLangDrop.latitude, trackLatLangDrop.longitude, "pick");
                }

            }
        });

    }

    private void getAddressFromSelectViaMap(double latitude, double longitude, String str) {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);

            if (addressList != null && addressList.size() > 0) {
                Address obj = addressList.get(0);
                String add = obj.getAddressLine(0);
                String locality = obj.getLocality();

                if (str.equals("drop")) {

                    PrefConnect.writeString(MapActivity.this, Global.DROP_SHORT_ADDRESS, locality);

                    LatLng latLng = new LatLng(latitude, longitude);

                    PrefConnect.writeString(MapActivity.this, Global.DROP_LATLANG, String.valueOf(latLng));

                    tvDropAddress.setText(add);


                    tvShortAddress.setText(PrefConnect.readString(MapActivity.this, Global.DROP_SHORT_ADDRESS, ""));


                } else {

                    PrefConnect.writeString(MapActivity.this, Global.PIKUP_SHORT_ADDRESS, locality);

                    LatLng latLng = new LatLng(latitude, longitude);
                    PrefConnect.writeString(MapActivity.this, Global.PICKUP_LATLANG, String.valueOf(latLng));

                    tvDropAddressPickup.setText(add);

                    if (locality != null) {

                        tvShortAddressPickup.setText(PrefConnect.readString(MapActivity.this, Global.PIKUP_SHORT_ADDRESS, ""));
                    } else {
                        tvShortAddressPickup.setText("Un Named City");

                    }


                }

                Log.e("Nive ", "getAddressFromSelectViaMap: " + add);

            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @OnClick({R.id.select_location_layout, R.id.tv_search_slide, R.id.tv_select_map, R.id.below_layout, R.id.btn_set_pickup, R.id.btn_set_destination, R.id.select_destination_layout, R.id.select_pickup_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.select_location_layout:

                Log.e("Nive ", "onViewClicked:Toplayout ");
                if (slidingLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

                } else {

                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                }
                break;

            case R.id.tv_select_map:
                if (PrefConnect.readString(MapActivity.this, Global.PICORDROP, "").equals("DROP")) {


                    slidingLayout.setVisibility(View.GONE);
                    mMap.clear();
                    destinationLayout.startAnimation(animShow);
                    destinationLayout.setVisibility(View.VISIBLE);
                    pickupLayout.setVisibility(View.GONE);
                    searchLocationLayout.setVisibility(View.GONE);
                    centerFrameDrop.setVisibility(View.VISIBLE);
                    centerFramePickup.setVisibility(View.GONE);

                } else {


                    slidingLayout.setVisibility(View.GONE);
                    mMap.clear();
                    centerFrameDrop.setVisibility(View.GONE);
                    centerFramePickup.setVisibility(View.VISIBLE);

                    destinationLayout.setVisibility(View.GONE);
                    pickupLayout.startAnimation(animShow);

                    pickupLayout.setVisibility(View.VISIBLE);
                    searchLocationLayout.setVisibility(View.GONE);
                }

                break;
            case R.id.btn_set_pickup:


                if (centerFramePickup.getVisibility() == View.VISIBLE) {
                    centerFramePickup.setVisibility(View.GONE);
                }

                PrefConnect.writeString(MapActivity.this, Global.FINAL_PICKUP_ADDRESS, tvDropAddressPickup.getText().toString());

                Intent intent = new Intent(MapActivity.this, ConfirmationActivity.class);
                startActivity(intent);

                break;
            case R.id.btn_set_destination:

                if (centerFrameDrop.getVisibility() == View.VISIBLE) {
                    centerFrameDrop.setVisibility(View.GONE);
                }

                PrefConnect.writeString(MapActivity.this, Global.FINAL_DROP_ADDRESS, tvDropAddress.getText().toString());
                tvShortAddressPickup.setText(PrefConnect.readString(MapActivity.this, Global.PIKUP_SHORT_ADDRESS, ""));
                tvDropAddressPickup.setText(locationAdd);
                destinationLayout.setVisibility(View.GONE);
                pickupLayout.startAnimation(animShow);
                pickupLayout.setVisibility(View.VISIBLE);

                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(pickupLatLang).icon(BitmapDescriptorFactory.fromResource(R.drawable.pick_pin)));
                center = CameraUpdateFactory.newLatLng(pickupLatLang);
                zoom = CameraUpdateFactory.zoomTo(16);
                mMap.moveCamera(center);
                mMap.animateCamera(zoom);

                break;
            case R.id.tv_search_slide:
                tvSearch.setText("");
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                break;
            case R.id.select_destination_layout:
                centerFrameDrop.setVisibility(View.GONE);
                centerFramePickup.setVisibility(View.GONE);


                tvHeader.setText(getResources().getString(R.string.str_destination));
                slidingLayout.setVisibility(View.VISIBLE);
                PrefConnect.writeString(MapActivity.this, Global.PICORDROP, "DROP");
                Log.e("Nive ", "onViewClicked:Pikup " + PrefConnect.readString(MapActivity.this, Global.DROP_SHORT_ADDRESS, ""));
                tvSearch.setText(PrefConnect.readString(MapActivity.this, Global.DROP_SHORT_ADDRESS, ""));
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                break;

            case R.id.select_pickup_layout:

                centerFrameDrop.setVisibility(View.GONE);
                centerFramePickup.setVisibility(View.GONE);


                tvHeader.setText(getResources().getString(R.string.str_pickup));
                PrefConnect.writeString(MapActivity.this, Global.PICORDROP, "PIC");
                slidingLayout.setVisibility(View.VISIBLE);
                Log.e("Nive ", "onViewClicked:Pikup " + PrefConnect.readString(MapActivity.this, Global.PIKUP_SHORT_ADDRESS, ""));
                tvSearch.setText(PrefConnect.readString(MapActivity.this, Global.PIKUP_SHORT_ADDRESS, ""));
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                break;


        }
    }


}
