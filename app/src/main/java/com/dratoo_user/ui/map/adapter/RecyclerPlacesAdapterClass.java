package com.dratoo_user.ui.map.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.dratoo_user.R;
import com.dratoo_user.ui.map.model.Predictions;

import java.util.ArrayList;

/**
 * Created by krishnaprakash on 30/5/16.
 */
public class RecyclerPlacesAdapterClass extends RecyclerView.Adapter<RecyclerPlacesAdapterClass.ViewHolder> {
    Context mContext;
    private ArrayList<Predictions> mLocationList = new ArrayList<>();
    private LayoutInflater inflater;

    public RecyclerPlacesAdapterClass(Context context, ArrayList<Predictions> mLocationList) {
        this.mContext = context;
        this.mLocationList = mLocationList;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_search, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.tv_address.setText(mLocationList.get(position).getDescription());
        holder.tv_header.setText(mLocationList.get(position).getStructured_formatting().getMain_text());

    }

    @Override
    public int getItemCount() {
        return mLocationList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public AppCompatTextView tv_header, tv_address, tv_km;

        AppCompatImageView img_type;

        LinearLayout whole_layout;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_header = itemView.findViewById(R.id.tv_header);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_km = itemView.findViewById(R.id.tv_km);
            img_type = itemView.findViewById(R.id.img_type);
            whole_layout = itemView.findViewById(R.id.whole_layout);
        }
    }
}
