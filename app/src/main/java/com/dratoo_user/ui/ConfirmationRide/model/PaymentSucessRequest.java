package com.dratoo_user.ui.ConfirmationRide.model;

public class PaymentSucessRequest {

    private String fraud_status;
    private String request_id;
    private String service_type;
    private String pay_type;

    public String getPay_type() {
        return pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    private String status_message;

    private String transaction_id;

    private String approval_code;

    private String point_balance;

    private String transaction_status;

    private String status_code;

    private String eci;

    private String gross_amount;

    private String point_redeem_amount;

    private String bank;

    private String payment_type;

    private String secure_token;

    private String masked_card;

    private String transaction_time;

    private String currency;

    private String order_id;

    @Override
    public String toString() {
        return "PaymentSucessRequest{" +
                "fraud_status='" + fraud_status + '\'' +
                ", status_message='" + status_message + '\'' +
                ", transaction_id='" + transaction_id + '\'' +
                ", approval_code='" + approval_code + '\'' +
                ", point_balance='" + point_balance + '\'' +
                ", transaction_status='" + transaction_status + '\'' +
                ", status_code='" + status_code + '\'' +
                ", eci='" + eci + '\'' +
                ", gross_amount='" + gross_amount + '\'' +
                ", point_redeem_amount='" + point_redeem_amount + '\'' +
                ", bank='" + bank + '\'' +
                ", payment_type='" + payment_type + '\'' +
                ", secure_token='" + secure_token + '\'' +
                ", masked_card='" + masked_card + '\'' +
                ", transaction_time='" + transaction_time + '\'' +
                ", currency='" + currency + '\'' +
                ", order_id='" + order_id + '\'' +
                ", finish_redirect_url='" + finish_redirect_url + '\'' +
                '}';
    }

    private String finish_redirect_url;

    public String getFraud_status() {
        return fraud_status;
    }

    public void setFraud_status(String fraud_status) {
        this.fraud_status = fraud_status;
    }

    public String getStatus_message() {
        return status_message;
    }

    public void setStatus_message(String status_message) {
        this.status_message = status_message;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getApproval_code() {
        return approval_code;
    }

    public void setApproval_code(String approval_code) {
        this.approval_code = approval_code;
    }

    public String getPoint_balance() {
        return point_balance;
    }

    public void setPoint_balance(String point_balance) {
        this.point_balance = point_balance;
    }

    public String getTransaction_status() {
        return transaction_status;
    }

    public void setTransaction_status(String transaction_status) {
        this.transaction_status = transaction_status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getEci() {
        return eci;
    }

    public void setEci(String eci) {
        this.eci = eci;
    }

    public String getGross_amount() {
        return gross_amount;
    }

    public void setGross_amount(String gross_amount) {
        this.gross_amount = gross_amount;
    }

    public String getPoint_redeem_amount() {
        return point_redeem_amount;
    }

    public void setPoint_redeem_amount(String point_redeem_amount) {
        this.point_redeem_amount = point_redeem_amount;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getSecure_token() {
        return secure_token;
    }

    public void setSecure_token(String secure_token) {
        this.secure_token = secure_token;
    }

    public String getMasked_card() {
        return masked_card;
    }

    public void setMasked_card(String masked_card) {
        this.masked_card = masked_card;
    }

    public String getTransaction_time() {
        return transaction_time;
    }

    public void setTransaction_time(String transaction_time) {
        this.transaction_time = transaction_time;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getFinish_redirect_url() {
        return finish_redirect_url;
    }

    public void setFinish_redirect_url(String finish_redirect_url) {
        this.finish_redirect_url = finish_redirect_url;
    }
}
