package com.dratoo_user.ui.ConfirmationRide.model;

import java.util.ArrayList;

public class VehicleDetails {

    public ArrayList<VehicleLists> result;

    public String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<VehicleLists> getResult() {
        return result;
    }

    public void setResult(ArrayList<VehicleLists> result) {
        this.result = result;
    }


}
