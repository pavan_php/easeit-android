package com.dratoo_user.ui.ConfirmationRide.presenter;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.ConfirmationRide.model.AvailableDriverResponse;
import com.dratoo_user.ui.ConfirmationRide.model.CreateRequestResponse;
import com.dratoo_user.ui.ConfirmationRide.model.GetFareResponse;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentScucessResponse;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentSucessRequest;
import com.dratoo_user.ui.ConfirmationRide.model.TaxiRequest;
import com.dratoo_user.ui.ConfirmationRide.model.VehicleListResponse;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmationImpl extends BasePresenter<ConfirmationView> implements ConfirmationPresenter {
    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;


    @Override
    public void attachView(ConfirmationView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void getVehicles(String service_type) {

        Call<VehicleListResponse> call = null;

        if (service_type.equals("4")) {
            call = apiInterface.getTaxiVehicles();
        } else if (service_type.equals("5")) {
            call = apiInterface.getCourierVeicles();
        } else if (service_type.equals("6")) {
            call = apiInterface.getCargoVeicles();
        } else if (service_type.equals("7")) {
            call = apiInterface.getMotoVeicles();
        }


        call.enqueue(new Callback<VehicleListResponse>() {
            @Override
            public void onResponse(@NotNull Call<VehicleListResponse> call, @NotNull Response<VehicleListResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body().getData().getStatus().equals("Success")) {

                        getView().getVehicles(response.body());
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<VehicleListResponse> call, @NotNull Throwable t) {

            }
        });

    }

    @Override
    public void getDriverLatLang(String id, String service_type) {

        Call<AvailableDriverResponse> call = null;
        getView().showProgress();

        if (service_type.equals("4")) {
            call = apiInterface.getDriverDetails(id);
        } else if (service_type.equals("5")) {
            call = apiInterface.getDriverDetailsCourier(id);
        } else if (service_type.equals("6")) {
            call = apiInterface.getDriverDetailsCargo(id);
        } else if (service_type.equals("7")) {
            call = apiInterface.getDriverDetailsMoto(id);
        }


        call.enqueue(new Callback<AvailableDriverResponse>() {
            @Override
            public void onResponse(@NotNull Call<AvailableDriverResponse> call,@NotNull  Response<AvailableDriverResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("failed")) {
                        getView().hideProgress();
                        getView().failure("No Provider Available");
                    } else {
                        getView().hideProgress();
                        getView().getSucessLatLang(response.body());
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<AvailableDriverResponse> call, @NotNull Throwable t) {
                getView().hideProgress();
            }
        });
    }

    @Override
    public void getFare(TaxiRequest taxiRequest, String service_type) {

        Call<GetFareResponse> call = null;

        if (service_type.equals("4")) {
            call = apiInterface.getFareDetails(taxiRequest);
        } else if (service_type.equals("5")) {
            call = apiInterface.getFareDetailsCourier(taxiRequest);
        } else if (service_type.equals("6")) {
            call = apiInterface.getFareDetailsCargo(taxiRequest);
        } else if (service_type.equals("7")) {
            call = apiInterface.getFareDetailsMoto(taxiRequest);
        }

        call.enqueue(new Callback<GetFareResponse>() {
            @Override
            public void onResponse(Call<GetFareResponse> call, Response<GetFareResponse> response) {

                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("true")) {
                        getView().getFare(response.body().getRide_fare());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetFareResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void createRequest(TaxiRequest taxiRequest, String service_type) {

        getView().showProgress();

        Call<CreateRequestResponse> call = null;

        if (service_type.equals("4")) {

            call = apiInterface.createRequest(taxiRequest);

        } else if (service_type.equals("5")) {

            call = apiInterface.createRequestCourier(taxiRequest);

        } else if (service_type.equals("6")) {

            call = apiInterface.createRequestCargo(taxiRequest);

        } else if (service_type.equals("7")) {

            call = apiInterface.createRequestMoto(taxiRequest);

        }

        call.enqueue(new Callback<CreateRequestResponse>() {
            @Override
            public void onResponse(Call<CreateRequestResponse> call, Response<CreateRequestResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    if (response.body().getStatus().equals("true")) {
                        getView().createRequestSucess(response.body());
                    }
                } else {
                    getView().hideProgress();
                }
            }

            @Override
            public void onFailure(Call<CreateRequestResponse> call, Throwable t) {

                getView().hideProgress();

            }
        });

    }

    @Override
    public void sendSucessRequest(PaymentSucessRequest paymentSucessRequest) {
        Call<PaymentScucessResponse> call = apiInterface.getSucessResultPayment(paymentSucessRequest);
        call.enqueue(new Callback<PaymentScucessResponse>() {
            @Override
            public void onResponse(Call<PaymentScucessResponse> call, Response<PaymentScucessResponse> response) {
                if (response.isSuccessful()) {
                    getView().paymentSuces();
                }
            }

            @Override
            public void onFailure(Call<PaymentScucessResponse> call, Throwable t) {

            }
        });
    }
}
