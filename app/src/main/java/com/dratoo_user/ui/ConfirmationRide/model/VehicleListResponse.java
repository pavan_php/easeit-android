package com.dratoo_user.ui.ConfirmationRide.model;

public class VehicleListResponse {

    public VehicleDetails data;

    public VehicleDetails getData() {
        return data;
    }

    public void setData(VehicleDetails data) {
        this.data = data;
    }
}
