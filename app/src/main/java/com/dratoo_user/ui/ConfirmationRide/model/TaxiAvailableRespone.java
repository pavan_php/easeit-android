package com.dratoo_user.ui.ConfirmationRide.model;

/**
 * Created by ${Krishnaprakash} on 03-04-2018.
 */

public class TaxiAvailableRespone {


    @Override
    public String toString() {
        return "TaxiAvailableRespone{" +
                "driver_id='" + driver_id + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                '}';
    }

    public String driver_id;

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double lat;
    public Double lng;
}
