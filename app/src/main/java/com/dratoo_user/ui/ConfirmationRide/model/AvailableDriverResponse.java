package com.dratoo_user.ui.ConfirmationRide.model;

import java.util.ArrayList;

/**
 * Created by ${Krishnaprakash} on 03-04-2018.
 */

public class AvailableDriverResponse {
    @Override
    public String toString() {
        return "AvailableDriverResponse{" +
                "data=" + data +
                '}';
    }

    public ArrayList<TaxiAvailableRespone> getData() {
        return data;
    }

    public void setData(ArrayList<TaxiAvailableRespone> data) {
        this.data = data;
    }

    public ArrayList<TaxiAvailableRespone> data;

    public String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
