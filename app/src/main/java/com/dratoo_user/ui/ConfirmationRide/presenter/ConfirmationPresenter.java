package com.dratoo_user.ui.ConfirmationRide.presenter;

import com.dratoo_user.base.Presenter;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentSucessRequest;
import com.dratoo_user.ui.ConfirmationRide.model.TaxiRequest;

public interface ConfirmationPresenter extends Presenter<ConfirmationView> {

    void getVehicles(String service_type);

    void getDriverLatLang(String id, String service_type);

    void getFare(TaxiRequest taxiRequest, String service_type);

    void createRequest(TaxiRequest taxiRequest, String service_type);

    void sendSucessRequest(PaymentSucessRequest paymentSucessRequest);


}
