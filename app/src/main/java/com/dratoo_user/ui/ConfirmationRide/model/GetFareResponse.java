package com.dratoo_user.ui.ConfirmationRide.model;

public class GetFareResponse {

    public String status;
    public String ride_fare;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRide_fare() {
        return ride_fare;
    }

    public void setRide_fare(String ride_fare) {
        this.ride_fare = ride_fare;
    }
}
