package com.dratoo_user.ui.ConfirmationRide.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.ConfirmationRide.model.AvailableDriverResponse;
import com.dratoo_user.ui.ConfirmationRide.model.CreateRequestResponse;
import com.dratoo_user.ui.ConfirmationRide.model.VehicleListResponse;

public interface ConfirmationView extends BaseView {

    void getVehicles(VehicleListResponse vehicleListResponse);

    void getSucessLatLang(AvailableDriverResponse availableDriverResponse);

    void failure(String msg);

    void getFare(String fare);

    void createRequestSucess(CreateRequestResponse createRequestResponse);

    void paymentSuces();
}
