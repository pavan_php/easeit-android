package com.dratoo_user.ui.ConfirmationRide.model;

/**
 * Created by ${Krishnaprakash} on 02-04-2018.
 */

public class TaxiRequest {

    public String source_lat;
    public String source_lng;
    public String dest_lat;
    public String dest_address;
    public String source_address;

    public String load_type;

    public String getLoad_type() {
        return load_type;
    }

    public void setLoad_type(String load_type) {
        this.load_type = load_type;
    }

    public String getVehicle_type() {
        return vehicle_type;
    }

    public void setVehicle_type(String vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    public String vehicle_type;

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String payment_type;

    public String getDest_address() {
        return dest_address;
    }

    public void setDest_address(String dest_address) {
        this.dest_address = dest_address;
    }

    public String getSource_address() {
        return source_address;
    }

    public void setSource_address(String source_address) {
        this.source_address = source_address;
    }

    public String dest_lng;

    public String getSource_lat() {
        return source_lat;
    }

    public void setSource_lat(String source_lat) {
        this.source_lat = source_lat;
    }

    public String getSource_lng() {
        return source_lng;
    }

    public void setSource_lng(String source_lng) {
        this.source_lng = source_lng;
    }

    public String getDest_lat() {
        return dest_lat;
    }

    public void setDest_lat(String dest_lat) {
        this.dest_lat = dest_lat;
    }

    public String getDest_lng() {
        return dest_lng;
    }

    public void setDest_lng(String dest_lng) {
        this.dest_lng = dest_lng;
    }

    public String getTaxi_type() {
        return taxi_type;
    }

    public void setTaxi_type(String taxi_type) {
        this.taxi_type = taxi_type;
    }

    public String taxi_type;


}
