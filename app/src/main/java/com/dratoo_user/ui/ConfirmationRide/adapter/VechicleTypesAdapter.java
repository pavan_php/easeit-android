package com.dratoo_user.ui.ConfirmationRide.adapter;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.ui.ConfirmationRide.ConfirmationActivity;
import com.dratoo_user.ui.ConfirmationRide.model.VehicleLists;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class VechicleTypesAdapter extends RecyclerView.Adapter<VechicleTypesAdapter.ViewHolder> {


    ConfirmationActivity confirmationActivity;

    ArrayList<VehicleLists> vehicleLists = new ArrayList<>();

    String service_type;

    public VechicleTypesAdapter(ConfirmationActivity confirmationActivity, ArrayList<VehicleLists> arrayList, String service_type) {

        this.confirmationActivity = confirmationActivity;
        this.vehicleLists = arrayList;
        this.service_type = service_type;

    }

    @NonNull
    @Override
    public VechicleTypesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vehicle_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VechicleTypesAdapter.ViewHolder viewHolder, final int position) {
        try {
            Picasso.get().load(Global.IMG_URL + vehicleLists.get(position).getVehicle_image()).fit().into(viewHolder.img_place_holder);
        } catch (Exception e) {
            e.printStackTrace();
        }

        viewHolder.tv_vehicle_name.setText(vehicleLists.get(position).getVehicle_type());

        viewHolder.whole_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmationActivity.getVehiclesDetails(vehicleLists.get(position));
            }
        });

        if (service_type.equals("4")) {
            viewHolder.tv_seats.setVisibility(View.VISIBLE);

            viewHolder.tv_max_weight.setVisibility(View.GONE);
            viewHolder.tv_rate_per_km.setVisibility(View.GONE);
            viewHolder.tv_unload_rate_per_km.setVisibility(View.GONE);

            viewHolder.tv_seats.setText(vehicleLists.get(position).getMax_seats() + " " + "People Seats");

        } else if (service_type.equals("5")) {

            viewHolder.tv_seats.setVisibility(View.GONE);
            viewHolder.tv_max_weight.setVisibility(View.VISIBLE);
            viewHolder.tv_rate_per_km.setVisibility(View.VISIBLE);
            viewHolder.tv_unload_rate_per_km.setVisibility(View.GONE);

            viewHolder.tv_max_weight.setText(vehicleLists.get(position).getMax_weight() + " " + "Kg");
            viewHolder.tv_rate_per_km.setText("Rate Per Km " + " " + PrefConnect.readString(confirmationActivity, Global.CURRENCY, "") + vehicleLists.get(position).getRate_per_kilometer());

        } else if (service_type.equals("6")) {
            viewHolder.tv_seats.setVisibility(View.GONE);
            viewHolder.tv_max_weight.setVisibility(View.VISIBLE);
            viewHolder.tv_rate_per_km.setVisibility(View.VISIBLE);
            viewHolder.tv_unload_rate_per_km.setVisibility(View.VISIBLE);

            viewHolder.tv_max_weight.setText(vehicleLists.get(position).getVolume() + " " + "Kg");
            viewHolder.tv_rate_per_km.setText("Rate Per Km Load " + " " +PrefConnect.readString(confirmationActivity, Global.CURRENCY, "") + vehicleLists.get(position).getLoad_rate_per_kilometer());
            viewHolder.tv_unload_rate_per_km.setText("Rate Per Km UnLoad " + " " +PrefConnect.readString(confirmationActivity, Global.CURRENCY, "") + vehicleLists.get(position).getUnload_rate_per_kilometer());

        } else if (service_type.equals("7")) {
            viewHolder.tv_seats.setVisibility(View.GONE);

            viewHolder.tv_max_weight.setVisibility(View.GONE);
            viewHolder.tv_rate_per_km.setVisibility(View.VISIBLE);
            viewHolder.tv_unload_rate_per_km.setVisibility(View.GONE);

            viewHolder.tv_rate_per_km.setText("Rate Per Kilometer" + " : " + vehicleLists.get(position).getRate_per_kilometer() + " " + "Km");

//            viewHolder.tv_seats.setText(vehicleLists.get(position).getMax_seats() + " " + "People Seats");
        }

    }

    @Override
    public int getItemCount() {
        return vehicleLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView img_place_holder;

        AppCompatTextView tv_vehicle_name, tv_seats, tv_rate_per_km, tv_unload_rate_per_km, tv_max_weight;

        RelativeLayout whole_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_place_holder = itemView.findViewById(R.id.img_place_holder);
            tv_vehicle_name = itemView.findViewById(R.id.tv_vehicle_name);
            tv_rate_per_km = itemView.findViewById(R.id.tv_rate_per_km);
            tv_max_weight = itemView.findViewById(R.id.tv_max_weight);
            tv_unload_rate_per_km = itemView.findViewById(R.id.tv_unload_rate_per_km);
            tv_seats = itemView.findViewById(R.id.tv_seats);
            whole_layout = itemView.findViewById(R.id.whole_layout);
        }
    }
}
