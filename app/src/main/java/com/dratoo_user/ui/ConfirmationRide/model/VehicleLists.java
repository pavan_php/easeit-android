package com.dratoo_user.ui.ConfirmationRide.model;

public class VehicleLists {


    public String vehicle_id;
    public String vehicle_name;
    public String volume;
    public String max_weight;
    public String rate_per_kilometer;
    public String vehicle_image;
    public String max_seats;
    public String min_fare;
    public String min_km;

    public String getMax_seats() {
        return max_seats;
    }

    public void setMax_seats(String max_seats) {
        this.max_seats = max_seats;
    }

    public String getMin_fare() {
        return min_fare;
    }

    public void setMin_fare(String min_fare) {
        this.min_fare = min_fare;
    }

    public String getMin_km() {
        return min_km;
    }

    public void setMin_km(String min_km) {
        this.min_km = min_km;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getVehicle_type() {
        return vehicle_type;
    }

    public void setVehicle_type(String vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    public String tax;
    public String vehicle_type;

    public String getVehicle_image() {
        return vehicle_image;
    }

    public void setVehicle_image(String vehicle_image) {
        this.vehicle_image = vehicle_image;
    }

    public String load_rate_per_kilometer;

    public boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getLoad_rate_per_kilometer() {
        return load_rate_per_kilometer;
    }

    public void setLoad_rate_per_kilometer(String load_rate_per_kilometer) {
        this.load_rate_per_kilometer = load_rate_per_kilometer;
    }

    public String getUnload_rate_per_kilometer() {
        return unload_rate_per_kilometer;
    }

    public void setUnload_rate_per_kilometer(String unload_rate_per_kilometer) {
        this.unload_rate_per_kilometer = unload_rate_per_kilometer;
    }

    public String unload_rate_per_kilometer;


    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getVehicle_name() {
        return vehicle_name;
    }

    public void setVehicle_name(String vehicle_name) {
        this.vehicle_name = vehicle_name;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getMax_weight() {
        return max_weight;
    }

    public void setMax_weight(String max_weight) {
        this.max_weight = max_weight;
    }

    public String getRate_per_kilometer() {
        return rate_per_kilometer;
    }

    public void setRate_per_kilometer(String rate_per_kilometer) {
        this.rate_per_kilometer = rate_per_kilometer;
    }

}
