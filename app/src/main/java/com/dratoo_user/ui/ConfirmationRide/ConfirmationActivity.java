package com.dratoo_user.ui.ConfirmationRide;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.maps.android.PolyUtil;
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.Constants;
import com.midtrans.sdk.corekit.core.LocalDataHandler;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme;
import com.midtrans.sdk.corekit.models.BankType;
import com.midtrans.sdk.corekit.models.TransactionResponse;
import com.midtrans.sdk.corekit.models.UserAddress;
import com.midtrans.sdk.corekit.models.UserDetail;
import com.midtrans.sdk.corekit.models.snap.CreditCard;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.app.App;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.BaseRetrofit;
import com.dratoo_user.ui.ConfirmationRide.adapter.VechicleTypesAdapter;
import com.dratoo_user.ui.ConfirmationRide.model.AvailableDriverResponse;
import com.dratoo_user.ui.ConfirmationRide.model.CreateRequestResponse;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentSucessRequest;
import com.dratoo_user.ui.ConfirmationRide.model.RouteMap;
import com.dratoo_user.ui.ConfirmationRide.model.TaxiRequest;
import com.dratoo_user.ui.ConfirmationRide.model.VehicleListResponse;
import com.dratoo_user.ui.ConfirmationRide.model.VehicleLists;
import com.dratoo_user.ui.ConfirmationRide.presenter.ConfirmationPresenter;
import com.dratoo_user.ui.ConfirmationRide.presenter.ConfirmationView;
import com.dratoo_user.ui.common.ConnectivityReceiver;
import com.dratoo_user.ui.common.ProgressDialog;
import com.dratoo_user.ui.lookingdriverbooking.LookingDriverBookingActivity;
import com.dratoo_user.ui.main.MainActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmationActivity extends AppCompatActivity implements OnMapReadyCallback, ConfirmationView, TransactionFinishedCallback, ConnectivityReceiver.ConnectivityReceiverListener {


    @Inject
    ConfirmationPresenter presenter;

    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.img_place_holder_cab)
    CircleImageView imgPlaceHolderCab;
    @BindView(R.id.type)
    AppCompatTextView type;
    @BindView(R.id.select_ride_layout)
    LinearLayout selectRideLayout;
    @BindView(R.id.tv_person)
    AppCompatTextView tvPerson;
    @BindView(R.id.img_three_dots)
    AppCompatImageView imgThreeDots;
    @BindView(R.id.tv_short_address_pickup)
    AppCompatTextView tvShortAddressPickup;
    @BindView(R.id.tv_drop_address_pickup)
    AppCompatTextView tvDropAddressPickup;
    @BindView(R.id.select_pickup_layout)
    LinearLayout selectPickupLayout;
    @BindView(R.id.tv_short_address_des)
    AppCompatTextView tvShortAddressDes;
    @BindView(R.id.tv_drop_address_des)
    AppCompatTextView tvDropAddressDes;
    @BindView(R.id.select_destination_layout)
    LinearLayout selectDestinationLayout;
    @BindView(R.id.img_place_holder_cash)
    AppCompatImageView imgPlaceHolderCash;
    @BindView(R.id.select_cash_layout)
    LinearLayout selectCashLayout;
    @BindView(R.id.tv_amt)
    AppCompatTextView tvAmt;
    @BindView(R.id.tv_amt_final)
    AppCompatTextView tvAmtFinal;
    @BindView(R.id.btn_order)
    AppCompatButton btn_order;
    @BindView(R.id.bottom_layout)
    FrameLayout bottomLayout;
    @BindView(R.id.second_sliding_view)
    RelativeLayout secondSlidingView;
    @BindView(R.id.top_layout)
    LinearLayout topLayout;
    @BindView(R.id.map_dest_img)
    AppCompatImageView mapDestImg;
    @BindView(R.id.tv_search_slide)
    AppCompatEditText tvSearch;
    @BindView(R.id.map_search)
    AppCompatImageView mapSearch;
    @BindView(R.id.tv_select_map)
    AppCompatTextView tvSelectMap;
    @BindView(R.id.selec_via_map_layout)
    LinearLayout selecViaMapLayout;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.list_auto_complete)
    RecyclerView listAutoComplete;
    @BindView(R.id.below_layout)
    LinearLayout belowLayout;

    @BindView(R.id.dragView)
    FrameLayout dragView;
    @BindView(R.id.sliding_layout)
    SlidingUpPanelLayout slidingLayout;
    @BindView(R.id.vehicle_layout)
    RelativeLayout vehicleLayout;
    @BindView(R.id.cash_layout)
    RelativeLayout cashLayout;
    @BindView(R.id.tv_header)
    AppCompatTextView tvHeader;
    @BindView(R.id.parant_layout)
    CoordinatorLayout parantLayout;
    @BindView(R.id.select_location_layout)
    LinearLayout selectLocationLayout;
    @BindView(R.id.sothree_bottom_layout)
    LinearLayout sothreeBottomLayout;
    @BindView(R.id.tv_pyament_type)
    AppCompatTextView tvPyamentType;
    private BottomSheetDialog mBottomSheetDialog, mBottomSheetDialogVehicle;
    private View bottomSheet, bottomSheetVehicle;

    private Animation animShow, animHide;
    ProgressBar progressBar;
    Polyline line;
    List<RouteMap.Routes> routesDetails = new ArrayList<RouteMap.Routes>();
    List<LatLng> route = new ArrayList<>();

    private ApiInterface apiInterface;
    private Marker mCurrLocationMarker, mDeliveryMarker;

    private GoogleMap mMap;

    LatLng pickupLatLang, dropLatLang;

    String newOrigin, newDestination;
    private VechicleTypesAdapter adapter;


    RecyclerView vechileList;

    public ArrayList<VehicleLists> arrayList = new ArrayList<>();

    ProgressDialog progressDialog;

    private ArrayList<LatLng> latlngs = new ArrayList<>();


    String picLat, picLang, dropLat, droplang;

    String vehicleId;
    private boolean fareApiSucess = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        presenter.attachView(this);
        animShow = AnimationUtils.loadAnimation(this, R.anim.popup_show);
        animHide = AnimationUtils.loadAnimation(this, R.anim.popup_hide);
        bottomLayout.startAnimation(animShow);
        bottomLayout.setVisibility(View.VISIBLE);

        progressDialog = new ProgressDialog(this);

        setUpToolbar();

        apiInterface = BaseRetrofit.getGoogleApi().create(ApiInterface.class);
        tvDropAddressPickup.setText(PrefConnect.readString(this, Global.PIKUP_SHORT_ADDRESS, ""));
        tvDropAddressDes.setText(PrefConnect.readString(this, Global.DROP_SHORT_ADDRESS, ""));

        initMap();

        drawRoute();

        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("4")) {


            tvPerson.setVisibility(View.VISIBLE);
        } else {
            tvPerson.setVisibility(View.GONE);

        }


        // by default payment type is cash
        PrefConnect.writeString(ConfirmationActivity.this, Global.PAYMENT_TYPE, "1");

        //initialize payment
        initMidtransSdk();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {

        String message;

        if (isConnected) {
            message = getResources().getString(R.string.connected_to_internet);

        } else {
            message = getResources().getString(R.string.not_connected_to_internet);

        }

        Global.snack_bar(parantLayout, message, Snackbar.LENGTH_LONG, ConfirmationActivity.this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        App.getInstance().setConnectivityListener(this);

    }

    public void getVehiclesDetails(VehicleLists vehicleLists) {

        try {
            Picasso.get().load(Global.IMG_URL + vehicleLists.getVehicle_image()).fit().into(imgPlaceHolderCab);
        } catch (Exception e) {
            e.printStackTrace();
        }
        type.setText(vehicleLists.getVehicle_type());

        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("4")) {

            tvPerson.setText(vehicleLists.getMax_seats() + " " + "Person");
            tvPerson.setClickable(false);
        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("5") || PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("7")) {

            tvPerson.setVisibility(View.GONE);
            tvPerson.setClickable(false);

        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("6")) {

            tvPerson.setVisibility(View.VISIBLE);
            tvPerson.setText("Change Load Type");
            tvPerson.setClickable(true);

        }


        if (vehicleLists.getVehicle_id() != null) {

            vehicleId = vehicleLists.getVehicle_id();

            presenter.getDriverLatLang(vehicleLists.getVehicle_id(), PrefConnect.readString(this, Global.SERVICE_TYPE, ""));
        }


    }

    private void setUpToolbar() {
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.map_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
    }


    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    private void drawRoute() {

        String val = PrefConnect.readString(this, Global.PICKUP_LATLANG, "");

        Log.e("Nive ", "drawRoute: " + val);

        String origin = PrefConnect.readString(this, Global.PICKUP_LATLANG, "").replace("lat/lng: ", " ");
        String destination = PrefConnect.readString(this, Global.DROP_LATLANG, "").replace("lat/lng: ", " ");

        Log.e("Nive ", "drawRoute:origin " + origin);
        Log.e("Nive ", "drawRoute:destination " + destination);

        if (origin != null) {
            String arr[] = origin.split(",");
            String newLat = null, newLang = null;

            if (arr[0].contains("(")) {
                newLat = arr[0].replace("(", " ");
            }

            if (arr[1].contains(")")) {
                newLang = arr[1].replace(")", " ");
            }


            picLat = newLat;
            picLang = newLang;

            newOrigin = newLat + "," + newLang;

            pickupLatLang = new LatLng(Double.valueOf(newLat), Double.valueOf(newLang));
        }


        if (destination != null) {
            String arr[] = destination.split(",");
            String newLat = null, newLang = null;

            try {
                if (arr[0].contains("(")) {
                    newLat = arr[0].replace("(", " ");
                }

                if (arr[1].contains(")")) {
                    newLang = arr[1].replace(")", " ");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            dropLat = newLat;
            droplang = newLang;


            newDestination = newLat + "," + newLang;

            dropLatLang = new LatLng(Double.valueOf(newLat), Double.valueOf(newLang));
        }


        Call<RouteMap> call = apiInterface.getRoute(newOrigin, newDestination);
        call.enqueue(new Callback<RouteMap>() {
            @Override
            public void onResponse(Call<RouteMap> call, Response<RouteMap> response) {
                if (response.body().getStatus().equalsIgnoreCase("OK")) {

                        /*confirmRoute.setVisibility(View.GONE);
                        requestConfirm.setVisibility(View.VISIBLE);
*/

                    if (line != null) {
                        line.remove();
                    }

                    if (mCurrLocationMarker != null) {
                        mCurrLocationMarker.remove();
                    }

                    if (mDeliveryMarker != null) {
                        mDeliveryMarker.remove();
                    }

                    mCurrLocationMarker = mMap.addMarker(new MarkerOptions().position(pickupLatLang)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.pick_pin))
                            .draggable(false));

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pickupLatLang,
                            16));


                    mDeliveryMarker = mMap.addMarker(new MarkerOptions().position(dropLatLang)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.drop_location))
                            .draggable(false));

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dropLatLang,
                            16));

                    routesDetails.clear();
                    route.clear();

                    routesDetails.addAll(response.body().getRoutes());
                    route.addAll(PolyUtil.decode(routesDetails.get(0).getOverview_polyline().getPoints()));
                    line = mMap.addPolyline(new PolylineOptions()
                            .width(8)
                            .color(getResources().getColor(R.color.splash_bg)));
                    line.setPoints(route);

                }
            }

            @Override
            public void onFailure(Call<RouteMap> call, Throwable t) {

            }
        });

    }


    public void initBottomSheet() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        bottomSheet = this.getLayoutInflater().inflate(R.layout.bottomsheet_payments, null);
        mBottomSheetDialog.setContentView(bottomSheet);

        AppCompatTextView title = (AppCompatTextView) mBottomSheetDialog.findViewById(R.id.title);
        AppCompatImageView img_close = mBottomSheetDialog.findViewById(R.id.img_close);
        LinearLayout halo_pay_layout = mBottomSheetDialog.findViewById(R.id.halo_pay_layout);
        LinearLayout cash_layout = mBottomSheetDialog.findViewById(R.id.cash_layout);

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        cash_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefConnect.writeString(ConfirmationActivity.this, Global.PAYMENT_TYPE, "1");
                mBottomSheetDialog.dismiss();
                tvPyamentType.setText("CASH");
            }
        });


        halo_pay_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  INFO: TODO

                tvPyamentType.setText("CARD");
                PrefConnect.writeString(ConfirmationActivity.this, Global.PAYMENT_TYPE, "2");
                mBottomSheetDialog.dismiss();

            }
        });


        mBottomSheetDialog.show();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

    }

    @OnClick({R.id.vehicle_layout, R.id.cash_layout, R.id.tv_person, R.id.btn_order, R.id.select_destination_layout, R.id.select_pickup_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.vehicle_layout:
                initBottomSheetVehicle();
                break;
            case R.id.cash_layout:
                if (fareApiSucess) {
//                    initBottomSheet();
                } else {
                    Global.snack_bar(parantLayout, "Select Vehicle", Snackbar.LENGTH_LONG, ConfirmationActivity.this);

                }

                break;
            case R.id.select_destination_layout:

                break;
            case R.id.select_pickup_layout:

                break;
            case R.id.tv_person:

                initBottomSheetVehicle();
                break;
            case R.id.btn_order:

                if (type.getText().toString().equals("SELECT VEHICLE")) {
                    Global.snack_bar(parantLayout, "Select Vehicle!!!", Snackbar.LENGTH_LONG, ConfirmationActivity.this);
                } else if (latlngs.size() == 0) {
                    Global.snack_bar(parantLayout, "No Provider Available Here!!!", Snackbar.LENGTH_LONG, ConfirmationActivity.this);
                } else if (btn_order.getText().toString().equals(getResources().getString(R.string.order_go_ride))) {
                    Global.snack_bar(parantLayout, "Select Vehicle!!!", Snackbar.LENGTH_LONG, ConfirmationActivity.this);
                } else {

                    createRequestApi();

                }

                break;
        }
    }

    private void createRequestApi() {
        TaxiRequest taxiRequest = new TaxiRequest();
        taxiRequest.setDest_lng(droplang);
        taxiRequest.setDest_lat(dropLat);
        taxiRequest.setSource_lat(picLat);
        taxiRequest.setSource_lng(picLang);
        taxiRequest.setSource_address(PrefConnect.readString(this, Global.FINAL_PICKUP_ADDRESS, ""));
        taxiRequest.setDest_address(PrefConnect.readString(this, Global.FINAL_DROP_ADDRESS, ""));

        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("4")) {
            taxiRequest.setTaxi_type(vehicleId);
        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("5")) {
            taxiRequest.setVehicle_type(vehicleId);
        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("7")) {
            taxiRequest.setVehicle_type(vehicleId);
        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("6")) {
            taxiRequest.setVehicle_type(vehicleId);
            taxiRequest.setLoad_type(PrefConnect.readString(this, Global.LOAD_TYPE, ""));
        }

        taxiRequest.setPayment_type(PrefConnect.readString(this, Global.PAYMENT_TYPE, ""));

        presenter.createRequest(taxiRequest, PrefConnect.readString(this, Global.SERVICE_TYPE, ""));
    }

    private void initBottomSheetVehicle() {

        mBottomSheetDialogVehicle = new BottomSheetDialog(this);
        bottomSheetVehicle = this.getLayoutInflater().inflate(R.layout.bottomsheet_vehicle_list, null);
        mBottomSheetDialogVehicle.setContentView(bottomSheetVehicle);

        AppCompatTextView title = (AppCompatTextView) mBottomSheetDialogVehicle.findViewById(R.id.title);
        progressBar = mBottomSheetDialogVehicle.findViewById(R.id.progressBar);
        AppCompatImageView img_close = (AppCompatImageView) mBottomSheetDialogVehicle.findViewById(R.id.img_close);
        AppCompatButton btn_load_type = mBottomSheetDialogVehicle.findViewById(R.id.btn_load_type);
        AppCompatButton btn_unload_type = mBottomSheetDialogVehicle.findViewById(R.id.btn_unload_type);
        final LinearLayout cargo_loadtype_layout = mBottomSheetDialogVehicle.findViewById(R.id.cargo_loadtype_layout);
        final FrameLayout recyclerView_frame_layout = mBottomSheetDialogVehicle.findViewById(R.id.recyclerView_frame_layout);

        vechileList = mBottomSheetDialogVehicle.findViewById(R.id.rv_vehicle_list);

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialogVehicle.dismiss();
            }
        });


        mBottomSheetDialogVehicle.show();

        btn_load_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recyclerView_frame_layout.setVisibility(View.VISIBLE);
                cargo_loadtype_layout.setVisibility(View.GONE);
                PrefConnect.writeString(ConfirmationActivity.this, Global.LOAD_TYPE, "1");


            }
        });

        btn_unload_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView_frame_layout.setVisibility(View.VISIBLE);
                cargo_loadtype_layout.setVisibility(View.GONE);
                PrefConnect.writeString(ConfirmationActivity.this, Global.LOAD_TYPE, "2");

            }
        });

        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("6")) {

            recyclerView_frame_layout.setVisibility(View.GONE);
            cargo_loadtype_layout.setVisibility(View.VISIBLE);

        } else {

            recyclerView_frame_layout.setVisibility(View.VISIBLE);
            cargo_loadtype_layout.setVisibility(View.GONE);

        }

        presenter.getVehicles(PrefConnect.readString(ConfirmationActivity.this, Global.SERVICE_TYPE, ""));


        progressBar.setVisibility(View.VISIBLE);


    }

    @Override
    public void getVehicles(VehicleListResponse vehicleListResponse) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(ConfirmationActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        vechileList.setLayoutManager(layoutManager);
        arrayList = vehicleListResponse.getData().getResult();
        adapter = new VechicleTypesAdapter(this, arrayList, PrefConnect.readString(this, Global.SERVICE_TYPE, ""));
        vechileList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void getSucessLatLang(AvailableDriverResponse availableDriverResponse) {

        if (mBottomSheetDialogVehicle != null) {
            mBottomSheetDialogVehicle.dismiss();
        }

        for (int i = 0; i < availableDriverResponse.getData().size(); i++) {


            latlngs.add(new LatLng(availableDriverResponse.getData().get(i).getLat(), availableDriverResponse.getData().get(i).getLng()));


        }

        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("7")) {
            for (int i = 0; i < latlngs.size(); i++) {


                mMap.addMarker(new MarkerOptions().position(latlngs.get(i)).icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_icon)));


            }
        } else {
            for (int i = 0; i < latlngs.size(); i++) {


                mMap.addMarker(new MarkerOptions().position(latlngs.get(i)).icon(BitmapDescriptorFactory.fromResource(R.drawable.car_map)));


            }
        }


        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pickupLatLang,
                14));


        btn_order.setText("REQUEST" + " " + type.getText().toString());


        callFareApi();


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ConfirmationActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private void callFareApi() {

        TaxiRequest taxiRequest = new TaxiRequest();
        taxiRequest.setSource_lat(picLat);
        taxiRequest.setSource_lng(picLang);
        taxiRequest.setDest_lat(dropLat);
        taxiRequest.setDest_lng(droplang);
        taxiRequest.setTaxi_type(vehicleId);
        taxiRequest.setSource_address(PrefConnect.readString(this, Global.FINAL_PICKUP_ADDRESS, ""));
        taxiRequest.setDest_address(PrefConnect.readString(this, Global.FINAL_DROP_ADDRESS, ""));


        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("6")) {

            if (PrefConnect.readString(ConfirmationActivity.this, Global.LOAD_TYPE, "") != null) {

                taxiRequest.setLoad_type(PrefConnect.readString(ConfirmationActivity.this, Global.LOAD_TYPE, ""));

                presenter.getFare(taxiRequest, PrefConnect.readString(this, Global.SERVICE_TYPE, ""));

            } else {
                Global.snack_bar(parantLayout, "Select Load Type!!!", Snackbar.LENGTH_LONG, ConfirmationActivity.this);
            }

        } else {
            presenter.getFare(taxiRequest, PrefConnect.readString(this, Global.SERVICE_TYPE, ""));
        }


    }

    @Override
    public void failure(String msg) {
        if (mBottomSheetDialogVehicle != null) {

            mBottomSheetDialogVehicle.dismiss();
        }

        type.setText("SELECT VEHICLE");
        Picasso.get().load(R.drawable.ic_cab).fit().into(imgPlaceHolderCab);
        btn_order.setText(getResources().getString(R.string.order_go_ride));
        btn_order.setBackground(getResources().getDrawable(R.drawable.white_bg_round));
        btn_order.setTextColor(getResources().getColor(R.color.black));

        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("4")) {


            tvPerson.setVisibility(View.VISIBLE);

            tvPerson.setText("0 Person");
        } else {
            tvPerson.setVisibility(View.GONE);

        }

        mMap.clear();
        drawRoute();
        Global.snack_bar(parantLayout, msg, Snackbar.LENGTH_LONG, ConfirmationActivity.this);
    }

    @Override
    public void getFare(String fare) {

        if (mBottomSheetDialogVehicle != null) {

            mBottomSheetDialogVehicle.dismiss();
        }

        tvAmt.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + fare);

        fareApiSucess = true;

        PrefConnect.writeString(this, Global.FINAL_AMT, fare);

        btn_order.setBackground(getResources().getDrawable(R.drawable.green_bg));
        btn_order.setTextColor(getResources().getColor(R.color.white));


    }

    @Override
    public void createRequestSucess(CreateRequestResponse response) {
        PrefConnect.writeString(ConfirmationActivity.this, Global.CURRNTRQID, response.getRequest_id());
        Intent intent = new Intent(this, LookingDriverBookingActivity.class);
        intent.putExtra(Global.PIC_LAT, picLat);
        intent.putExtra(Global.PIC_LANG, picLang);
        intent.putExtra(Global.DROP_LAT, dropLat);
        intent.putExtra(Global.DROP_LANG, droplang);
        startActivity(intent);
        PrefConnect.writeString(this, Global.LOAD_TYPE, "");

    }

    @Override
    public void paymentSuces() {

    }

    @Override
    public void showProgress() {

        progressDialog.show();

    }

    @Override
    public void hideProgress() {
        progressDialog.dismissLoader();

    }

    @Override
    public void onTransactionFinished(TransactionResult result) {
        if (result.getResponse() != null) {
            switch (result.getStatus()) {
                case TransactionResult.STATUS_SUCCESS:

                    Log.e("Nive ", "onTransactionFinished: " + new Gson().toJson(result.getResponse()));
                    callPaymentSucessApi(result.getResponse());
                    break;
                case TransactionResult.STATUS_PENDING:
                    Toast.makeText(this, "Transaction Pending. ID: " + result.getResponse().getTransactionId(), Toast.LENGTH_LONG).show();
                    break;
                case TransactionResult.STATUS_FAILED:
                    Toast.makeText(this, "Transaction Failed. ID: " + result.getResponse().getTransactionId() + ". Message: " + result.getResponse().getStatusMessage(), Toast.LENGTH_LONG).show();
                    break;
            }
            result.getResponse().getValidationMessages();
        } else if (result.isTransactionCanceled()) {
            Toast.makeText(this, "Transaction Canceled", Toast.LENGTH_LONG).show();
        } else {
            if (result.getStatus().equalsIgnoreCase(TransactionResult.STATUS_INVALID)) {
                Toast.makeText(this, "Transaction Invalid", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Transaction Finished with failure.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void callPaymentSucessApi(TransactionResponse response) {

        PaymentSucessRequest paymentSucessRequest = new PaymentSucessRequest();
        paymentSucessRequest.setApproval_code(response.getApprovalCode());
        paymentSucessRequest.setBank(response.getBank());
        paymentSucessRequest.setCurrency(response.getCurrency());
        paymentSucessRequest.setEci(response.getEci());
        paymentSucessRequest.setFinish_redirect_url(response.getFinishRedirectUrl());
        paymentSucessRequest.setFraud_status(response.getFraudStatus());
        paymentSucessRequest.setPayment_type(response.getPaymentType());
        paymentSucessRequest.setGross_amount(response.getGrossAmount());
        paymentSucessRequest.setOrder_id(response.getOrderId());
        paymentSucessRequest.setMasked_card(response.getMaskedCard());
        paymentSucessRequest.setPoint_redeem_amount(response.getPointBalanceAmount());
        paymentSucessRequest.setPayment_type(response.getPaymentType());
        paymentSucessRequest.setStatus_message(response.getStatusMessage());
        paymentSucessRequest.setTransaction_id(response.getTransactionId());


        presenter.sendSucessRequest(paymentSucessRequest);
    }


    private TransactionRequest initTransactionRequest() {
        // Create new Transaction Request
        TransactionRequest transactionRequestNew = new
                TransactionRequest(System.currentTimeMillis() + "", Double.parseDouble(PrefConnect.readString(this, Global.FINAL_AMT, "")));

        Log.e("Nive ", "initTransactionRequest:FinalAmt " + Double.parseDouble(PrefConnect.readString(this, Global.FINAL_AMT, "")));
        Log.e("Nive ", "initTransactionRequest:FinalAmt " + PrefConnect.readString(this, Global.EMAIL, ""));

        // Set user details
        UserDetail userDetail = new UserDetail();
        userDetail.setUserFullName(PrefConnect.readString(ConfirmationActivity.this, Global.NAME, ""));
        userDetail.setEmail(PrefConnect.readString(ConfirmationActivity.this, Global.EMAIL, ""));
        userDetail.setPhoneNumber(PrefConnect.readString(ConfirmationActivity.this, Global.MOBILE_NBR, ""));
        userDetail.setUserId(PrefConnect.readString(this, Global.AUTH_ID, ""));

        ArrayList<UserAddress> userAddresses = new ArrayList<>();
        UserAddress userAddress = new UserAddress();
       // userAddress.setAddress("indonesia");
      //  userAddress.setCity("jakarta");
      //  userAddress.setCountry("IDN");
      //  userAddress.setZipcode("86554");
        userAddress.setAddressType(Constants.ADDRESS_TYPE_BOTH);
        userAddresses.add(userAddress);

        userDetail.setUserAddresses(userAddresses);
        LocalDataHandler.saveObject("user_details", userDetail);

        Log.e("Nive ", "initTransactionRequest:userDetail " + new Gson().toJson(userDetail));

        // Create creditcard options for payment
        CreditCard creditCard = new CreditCard();
        creditCard.setSaveCard(false); // when using one/two click set to true and if normal set to false
        creditCard.setAuthentication(CreditCard.AUTHENTICATION_TYPE_3DS);
        creditCard.setBank(BankType.BCA); //set spesific acquiring bank
        transactionRequestNew.setCreditCard(creditCard);

        return transactionRequestNew;
    }


    private void initMidtransSdk() {
        SdkUIFlowBuilder.init()
                .setClientKey(Global.client_key) // client_key is mandatory
                .setContext(this) // context is mandatory
                .setTransactionFinishedCallback(this) // set transaction finish callback (sdk callback)
                .setMerchantBaseUrl(Global.base_url_payment) //set merchant url
                .enableLog(true) // enable sdk log
                .setColorTheme(new CustomColorTheme("#FFE51255", "#B61548", "#FFE51255")) // will replace theme on snap theme on MAP
                .buildSDK();
    }
}
