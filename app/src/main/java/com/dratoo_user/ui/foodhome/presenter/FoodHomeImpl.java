package com.dratoo_user.ui.foodhome.presenter;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.foodhome.model.FoodResponse;
import com.dratoo_user.ui.foodhome.model.GroceryProductListResponse;
import com.dratoo_user.ui.foodhome.model.ServiceListResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FoodHomeImpl extends BasePresenter<FoodHomeView> implements FoodHomePresenter {


    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;


    @Override
    public void attachView(FoodHomeView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void getFoodList() {


        Call<FoodResponse> call = apiInterface.getFoodStyleList();

        call.enqueue(new Callback<FoodResponse>() {
            @Override
            public void onResponse(Call<FoodResponse> call, Response<FoodResponse> response) {


                if (response.isSuccessful()) {

                    getView().foodSucess(response.body());
                }

            }

            @Override
            public void onFailure(Call<FoodResponse> call, Throwable t) {

            }
        });

    }



    @Override
    public void getProductList() {
        Call<GroceryProductListResponse> call = apiInterface.getProductList();
        call.enqueue(new Callback<GroceryProductListResponse>() {
            @Override
            public void onResponse(retrofit2.Call<GroceryProductListResponse> call, Response<GroceryProductListResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().sucessGroceryStyleList(response.body());
                } else {
                    getView().hideProgress();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<GroceryProductListResponse> call, Throwable t) {
                getView().hideProgress();


            }
        });

    }

    @Override
    public void getServiceList() {

        Call<ServiceListResponse> call = apiInterface.getHandyManServiceList();
        call.enqueue(new Callback<ServiceListResponse>() {
            @Override
            public void onResponse(Call<ServiceListResponse> call, Response<ServiceListResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().sucessServiceLists(response.body());
                } else {
                    getView().hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ServiceListResponse> call, Throwable t) {
                getView().hideProgress();
            }
        });


    }

}
