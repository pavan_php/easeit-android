package com.dratoo_user.ui.foodhome.model;

import java.util.ArrayList;

/**
 * Created by ${Krishnaprakash} on 15-03-2018.
 */

public class FoodResponse {
    public ArrayList<FoodList> getData() {
        return data;
    }

    public void setData(ArrayList<FoodList> data) {
        this.data = data;
    }

    public ArrayList<FoodList> data;
}
