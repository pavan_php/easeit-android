package com.dratoo_user.ui.foodhome.model;

import java.util.ArrayList;

public class GroceryProductListResponse {

    public ArrayList<ProductList> data;

    public ArrayList<ProductList> getData() {
        return data;
    }

    public void setData(ArrayList<ProductList> data) {
        this.data = data;
    }
}
