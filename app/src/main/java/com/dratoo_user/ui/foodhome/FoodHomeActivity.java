package com.dratoo_user.ui.foodhome;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.dratoo_user.R;
import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.app.App;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.BaseRetrofit;
import com.dratoo_user.ui.common.ConnectivityReceiver;
import com.dratoo_user.ui.common.ItemClickSupport;
import com.dratoo_user.ui.common.SearchLocationActivity;
import com.dratoo_user.ui.foodhome.adapter.FoodStyleAdapter;
import com.dratoo_user.ui.foodhome.adapter.HandyManAdapter;
import com.dratoo_user.ui.foodhome.model.FoodList;
import com.dratoo_user.ui.foodhome.model.FoodResponse;
import com.dratoo_user.ui.foodhome.model.GroceryProductListResponse;
import com.dratoo_user.ui.foodhome.model.ProductList;
import com.dratoo_user.ui.foodhome.model.ServiceList;
import com.dratoo_user.ui.foodhome.model.ServiceListResponse;
import com.dratoo_user.ui.foodhome.presenter.FoodHomePresenter;
import com.dratoo_user.ui.foodhome.presenter.FoodHomeView;
import com.dratoo_user.ui.main.MainActivity;
import com.dratoo_user.ui.map.adapter.RecyclerPlacesAdapterClass;
import com.dratoo_user.ui.map.model.GoogleApiPlacesResponse;
import com.dratoo_user.ui.map.model.Predictions;
import com.dratoo_user.utils.AutoScrollViewPager;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FoodHomeActivity extends AppCompatActivity implements LocationListener, OnMapReadyCallback, FoodHomeView, ConnectivityReceiver.ConnectivityReceiverListener {
    @Inject
    FoodHomePresenter presenter;
    @BindView(R.id.img_back)
    AppCompatImageView imgBack;
    @BindView(R.id.tv_location)
    AppCompatTextView tvLocation;
    @BindView(R.id.img_cart)
    AppCompatImageView imgCart;
    @BindView(R.id.img_heart)
    ImageView imgHeart;
    @BindView(R.id.top_layout)
    LinearLayout topLayout;
    @BindView(R.id.map_search)
    AppCompatImageView mapSearch;
    @BindView(R.id.tv_search)
    AppCompatEditText tvSearch;
    @BindView(R.id.viewpager)
    AutoScrollViewPager viewpager;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.tv_see_all)
    AppCompatTextView tvSeeAll;
    @BindView(R.id.rv_food_style_list)
    RecyclerView rvFoodStyleList;
    @BindView(R.id.tv_current_address)
    AppCompatTextView tvCurrentAddress;
    @BindView(R.id.text_message)
    LinearLayout textMessage;
    @BindView(R.id.image_arrow)
    ImageView imageArrow;
    @BindView(R.id.location_layout_anim)
    FrameLayout locationLayoutAnim;

    @BindView(R.id.btn_got_it)
    AppCompatButton btnGotIt;
    @BindView(R.id.choose_location)
    LinearLayout chooseLocation;
    @BindView(R.id.parant_layout)
    LinearLayout parantLayout;
    @BindView(R.id.tv_header)
    AppCompatTextView tvHeader;

    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private Location mLastLocation;

    SupportMapFragment HomemapFragment;
    private Geocoder geocoder;
    private String zipcode, locality, city, state, country, currentAddres;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 101;
    public static final int REQUEST_CHECK_SETTINGS = 100;
    private boolean setText = false;
    private ArrayList<FoodList> foodList;
    private FoodStyleAdapter foodAdapter;
    private ArrayList<ProductList> groceryList;
    private Animation sgAnimation;
    private BottomSheetDialog mBottomSheetDialog;
    private View bottomSheet;
    private String from_location;
    ArrayList<Predictions> predictionsArrayList = new ArrayList<>();

    RecyclerPlacesAdapterClass mAdapter;

    RecyclerView list_auto_complete;

    private ApiInterface apiInterface;

    AppCompatEditText tv_search_slide;


    private String strLocation = "";
    private LatLng latLang;
    private Dialog cuisinesDialog;
    private ArrayList<ServiceList> serviceList;
    private HandyManAdapter handyManAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_home);
        ButterKnife.bind(this);

        DaggerAppComponent.create().inject(this);
        presenter.attachView(this);

        apiInterface = BaseRetrofit.getGoogleApi().create(ApiInterface.class);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        PrefConnect.writeBoolean(FoodHomeActivity.this, Global.FROM_DESTINATION, false);

        HomemapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        HomemapFragment.getMapAsync(this);

        //Intent intent = new Intent(FoodHomeActivity.this, SearchLocationActivity.class);
        // startActivity(intent);

        if (getIntent() != null) {
            if (getIntent().getStringExtra(Global.SELECT_LOCATION) != null) {
                from_location = getIntent().getStringExtra(Global.SELECT_LOCATION);
            }
        }

        if (from_location != null) {
            Log.e("Nive ", "onCreate:from_location " + from_location);
            setText = true;
            tvCurrentAddress.setText(PrefConnect.readString(FoodHomeActivity.this, Global.CURRENT_ADDRESS, ""));
        } else {
            setText = false;
        }

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult != null) {
                    super.onLocationResult(locationResult);
                    mLastLocation = locationResult.getLastLocation();

                    Log.e("Nive ", "onLocationResult: " + mLastLocation);
                    if (mLastLocation != null) {
                        LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                        // setText boolean used to choose map via address purpose to use setText boolean
                        if (!setText) {
                            Log.e("FoodHome ", "setText: ");

                            PrefConnect.writeString(FoodHomeActivity.this, Global.CURRENT_LAT, String.valueOf(mLastLocation.getLatitude()));
                            PrefConnect.writeString(FoodHomeActivity.this, Global.CURRENT_LANG, String.valueOf(mLastLocation.getLongitude()));

                            // get Address and latlang from current location
                            getAddress(latLng);
                        }
                    }
                }
            }

        };

        setViewPager();

        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("1")) {

            //food
            if (!PrefConnect.readBoolean(FoodHomeActivity.this, Global.ENTERONCE_FOOD, false)) {
                sgAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);
                locationLayoutAnim.startAnimation(sgAnimation);
                locationLayoutAnim.setVisibility(View.VISIBLE);
            }

            tvHeader.setText(getResources().getString(R.string.choose_from_cuisines));

            presenter.getFoodList();
        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("2")) {
            locationLayoutAnim.setVisibility(View.GONE);
            //mart
            presenter.getProductList();

            tvHeader.setText(getResources().getString(R.string.choose_from_cuisines));

        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("3")) {
            //handyman
            tvHeader.setText(getResources().getString(R.string.choose_from_services));

            locationLayoutAnim.setVisibility(View.GONE);
            presenter.getServiceList();
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;

        if (isConnected) {
            message = getResources().getString(R.string.connected_to_internet);
        } else {
            message = getResources().getString(R.string.not_connected_to_internet);
        }

        Global.snack_bar(parantLayout, message, Snackbar.LENGTH_LONG, FoodHomeActivity.this);

    }

    public void initBottomSheet() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        bottomSheet = this.getLayoutInflater().inflate(R.layout.bottom_search_layout, null);
        mBottomSheetDialog.setContentView(bottomSheet);


        RelativeLayout parant_layout = mBottomSheetDialog.findViewById(R.id.parant_layout);
        tv_search_slide = mBottomSheetDialog.findViewById(R.id.tv_search_slide);
        LinearLayout selec_via_map_layout = mBottomSheetDialog.findViewById(R.id.selec_via_map_layout);
        SlidingUpPanelLayout sliding_layout = mBottomSheetDialog.findViewById(R.id.sliding_layout);

        list_auto_complete = mBottomSheetDialog.findViewById(R.id.list_auto_complete);

        AppCompatImageView food_destination = mBottomSheetDialog.findViewById(R.id.food_destination);

        food_destination.setImageResource(R.drawable.destination_img);

        sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

        sliding_layout.setTouchEnabled(false);

        selec_via_map_layout.setOnClickListener(v -> {
            FoodHomeActivity.this.finish();
            Intent intent = new Intent(FoodHomeActivity.this, SearchLocationActivity.class);
            startActivity(intent);
        });

        tv_search_slide.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() == 0) {
                    predictionsArrayList.clear();
                    if (mAdapter != null)
                        mAdapter.notifyDataSetChanged();
                } else {
                    getTypedLocation(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ItemClickSupport.addTo(list_auto_complete).setOnItemClickListener((recyclerView, position, v) -> {
            Log.e("Nive ", "onItemClicked: " + predictionsArrayList.get(position).getDescription());
            if (!predictionsArrayList.get(position).getDescription().equalsIgnoreCase(null)) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                strLocation = predictionsArrayList.get(position).getDescription();
                tvCurrentAddress.setText(strLocation);
                latLang = getLocationFromAddress(FoodHomeActivity.this, strLocation);

                Log.e("Nive ", "onItemClicked: " + latLang);
                // use only search adapter(i.e) replace Adapter class
                getAddressList(latLang);
            }
        });
        //mBottomSheetDialog.show();
    }

    private void getTypedLocation(String str) {
        list_auto_complete.setVisibility(View.VISIBLE);
        Call<GoogleApiPlacesResponse> call = apiInterface.getGoogleApiPlacesResponse(str);
        call.enqueue(new Callback<GoogleApiPlacesResponse>() {
            @Override
            public void onResponse(@NotNull Call<GoogleApiPlacesResponse> call, @NotNull Response<GoogleApiPlacesResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("OK")) {
                        predictionsArrayList = response.body().getPredictions();
                        Log.e("Nive ", "onResponse: " + predictionsArrayList.size());
                        mAdapter = new RecyclerPlacesAdapterClass(FoodHomeActivity.this, predictionsArrayList);
                        list_auto_complete.setLayoutManager(new LinearLayoutManager(FoodHomeActivity.this));
                        list_auto_complete.setAdapter(mAdapter);
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<GoogleApiPlacesResponse> call, @NotNull Throwable t) {
                Log.e("Nive ", "onFailure: " + t.toString());
            }
        });
    }

    private void getAddressList(LatLng latLang) {
        geocoder = new Geocoder(this, Locale.getDefault());

        PrefConnect.writeString(FoodHomeActivity.this, Global.CURRENT_LAT, String.valueOf(latLang.latitude));
        PrefConnect.writeString(FoodHomeActivity.this, Global.CURRENT_LANG, String.valueOf(latLang.longitude));

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latLang.latitude, latLang.longitude, 1);


            if (addresses.get(0).getPostalCode() != null) {
                zipcode = addresses.get(0).getPostalCode();
                Log.e("Nive ", "getPlaceInfo:ZIP " + zipcode);

            }
            if (addresses.get(0).getLocality() != null) {
                locality = addresses.get(0).getLocality();
                Log.e("Nive ", "getPlaceInfo:ZIP " + locality);

            }

            if (addresses.get(0).getLocality() != null) {
                city = addresses.get(0).getLocality();
                Log.e("Nive ", "getPlaceInfo:city " + city);
            }

            if (addresses.get(0).getAdminArea() != null) {
                state = addresses.get(0).getAdminArea();
                Log.e("Nive ", "getPlaceInfo:state " + state);
            }

            if (addresses.get(0).getCountryName() != null) {
                country = addresses.get(0).getCountryName();
                Log.e("Nive ", "getPlaceInfo:country " + country);
            }


            currentAddres = city.concat(",").concat(state).concat(",").concat(country).concat(",").concat(zipcode);

            Log.e("Nive ", "getAddressList: " + currentAddres);

            tvCurrentAddress.setText(currentAddres);

            PrefConnect.writeString(FoodHomeActivity.this, Global.CURRENT_ADDRESS, currentAddres);

            if (mBottomSheetDialog != null) {
                mBottomSheetDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Log.e("Nive ", "getLocationFromAddress: " + strAddress);
        geocoder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;
        try {
// May throw an IOException
            address = geocoder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude());
            Log.e("Nive ", "getLocationFromAddress:LatLang " + p1);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return p1;
    }


    @Override
    protected void onStart() {
        App.getInstance().setConnectivityListener(this);

        checkLocationPermission();
        super.onStart();
    }

    public void checkLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // Asking user if explanation is needed
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);

                    Log.e("Nive ", "checkLocationPermission:ACCESS_FINE_LOCATION ");
                } else {
                    Log.e("Nive ", "checkLocationPermission:MY_PERMISSIONS_REQUEST_LOCATION ");

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }
            } else {
                checkGps();
            }
        } else {
            checkGps();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NotNull String[] permissions, @NotNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
// If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        checkGps();

                        mLocationCallback = new LocationCallback() {
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                if (locationResult != null) {
                                    super.onLocationResult(locationResult);
                                    mLastLocation = locationResult.getLastLocation();
                                    Log.e("Nive ", "onLocationResult: " + mLastLocation);

                                    if (mLastLocation != null) {

                                        LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

                                        if (!setText) {

                                            Log.e("FoodHome ", "setText: ");

                                            PrefConnect.writeString(FoodHomeActivity.this, Global.CURRENT_LAT, String.valueOf(mLastLocation.getLatitude()));
                                            PrefConnect.writeString(FoodHomeActivity.this, Global.CURRENT_LANG, String.valueOf(mLastLocation.getLongitude()));


                                            getAddress(latLng);
                                        }

                                    }
                                }
                            }
                        };

                    }

                } else {
// Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

        }
    }

    private void checkGps() {
        LocationSettingsRequest.Builder settingsBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        settingsBuilder.setAlwaysShow(true);

        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(this).checkLocationSettings(settingsBuilder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    setLocalUpdateRequest();
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(
                                        FoodHomeActivity.this,
                                        REQUEST_CHECK_SETTINGS);

                                Log.e("Nive ", "onComplete: gps");
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void setLocalUpdateRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback, Looper.myLooper());
    }

    private void setViewPager() {
        try {
            ArrayList<String> list = new ArrayList<>();
            list.add("http://php.manageprojects.in/easeit/uploads/PHOTO-1.jpg");
            list.add("http://php.manageprojects.in/easeit/uploads/PHOTO-2.jpg");
            list.add("http://php.manageprojects.in/easeit/uploads/PHOTO-3.jpg");
            final ViewPagerAdapter mViewPagerAdapter = new ViewPagerAdapter(FoodHomeActivity.this, list);
            viewpager.setAdapter(mViewPagerAdapter);
            viewpager.startAutoScroll(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(this, "PERMISSION DENIED", Toast.LENGTH_LONG).show();
            return;
        }
        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));

        if (location != null) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(90)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            getAddressFromSelectViaMap(location.getLatitude(), location.getLongitude());
            // Toast.makeText(this, location.toString(), Toast.LENGTH_LONG).show();
            // getAddress(new LatLng(location.getLatitude(), location.getLongitude()));
        }
    }

    private void getAddressFromSelectViaMap(double latitude, double longitude) {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);

            if (addressList != null && addressList.size() > 0) {
                Address obj = addressList.get(0);
                String locality = obj.getLocality();

                Log.e("Nive ", "getAddressFromSelectViaMap: " + obj.getAddressLine(0));
                PrefConnect.writeString(FoodHomeActivity.this, Global.CURRENT_ADDRESS, String.valueOf(obj.getAddressLine(0)));
                PrefConnect.writeString(FoodHomeActivity.this, Global.SELECT_LOCATION, "1");
                PrefConnect.writeString(FoodHomeActivity.this, Global.PIKUP_SHORT_ADDRESS, String.valueOf(obj.getAddressLine(0)));
                PrefConnect.writeString(FoodHomeActivity.this, Global.FINAL_PICKUP_ADDRESS, String.valueOf(obj.getAddressLine(0)));


                if (locality != null) {
                    tvCurrentAddress.setText(locality);
                } else {

                    tvCurrentAddress.setText("Un Named City");
                }
                tvCurrentAddress.setText(String.valueOf(obj.getAddressLine(0)));

                //Toast.makeText(this, locality.toString(), Toast.LENGTH_LONG).show();

                PrefConnect.writeString(FoodHomeActivity.this, Global.CURRENT_LAT, String.valueOf(latitude));
                PrefConnect.writeString(FoodHomeActivity.this, Global.CURRENT_LANG, String.valueOf(longitude));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getAddress(LatLng LatLng) {
        setText = true;
        geocoder = new Geocoder(this, Locale.getDefault());

        Log.e("Nive ", "getAddress: " + LatLng);

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(LatLng.latitude, LatLng.longitude, 1);


            if (addresses.get(0).getPostalCode() != null) {
                zipcode = addresses.get(0).getPostalCode();
                Log.e("Nive ", "getPlaceInfo:ZIP " + zipcode);
            }
            if (addresses.get(0).getLocality() != null) {
                locality = addresses.get(0).getLocality();
                Log.e("Nive ", "getPlaceInfo:ZIP " + locality);
            }

            if (addresses.get(0).getLocality() != null) {
                city = addresses.get(0).getLocality();
                Log.e("Nive ", "getPlaceInfo:city " + city);
            }

            if (addresses.get(0).getAdminArea() != null) {
                state = addresses.get(0).getAdminArea();
                Log.e("Nive ", "getPlaceInfo:state " + state);
            }

            if (addresses.get(0).getCountryName() != null) {
                country = addresses.get(0).getCountryName();
                Log.e("Nive ", "getPlaceInfo:country " + country);
            }

            currentAddres = city.concat(",").concat(state).concat(",").concat(country).concat(",").concat(zipcode);

            tvCurrentAddress.setText(currentAddres);

            Toast.makeText(this, currentAddres, Toast.LENGTH_LONG).show();

            PrefConnect.writeString(FoodHomeActivity.this, Global.CURRENT_ADDRESS, currentAddres);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void foodSucess(FoodResponse body) {
        if (body.getData() != null) {
            rvFoodStyleList.setLayoutManager(new GridLayoutManager(this, 3));
            foodList = body.getData();
            foodAdapter = new FoodStyleAdapter(this, foodList, "1", "1");
            rvFoodStyleList.setAdapter(foodAdapter);
            foodAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void sucess(String msg) {

    }

    @Override
    public void sucessGroceryStyleList(GroceryProductListResponse data) {
        if (data.getData() != null) {
            rvFoodStyleList.setLayoutManager(new GridLayoutManager(this, 3));
            groceryList = data.getData();
            foodAdapter = new FoodStyleAdapter(this, groceryList, "1");
            rvFoodStyleList.setAdapter(foodAdapter);
            foodAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void sucessServiceLists(ServiceListResponse body) {
        if (body.getData() != null && body.getData().size() > 0) {
            rvFoodStyleList.setLayoutManager(new GridLayoutManager(this, 3));
            serviceList = body.getData();
            handyManAdapter = new HandyManAdapter(this, serviceList, "1");
            rvFoodStyleList.setAdapter(handyManAdapter);
            handyManAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    public class ViewPagerAdapter extends PagerAdapter {
        Context context;
        LayoutInflater layoutInflater;
        ArrayList<String> list;

        ViewPagerAdapter(FoodHomeActivity foodHomeActivity, ArrayList<String> list) {
            this.context = foodHomeActivity;
            this.list = list;
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, final int position) {
            View itemView = layoutInflater.inflate(R.layout.item_slider, container, false);
            AppCompatImageView imageView = itemView.findViewById(R.id.iv_image);
            try {
                Picasso.get().load(list.get(position)).into(imageView);
            } catch (Exception e) {
                e.printStackTrace();
            }
            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((RelativeLayout) object);
        }
    }

    @OnClick({R.id.img_back, R.id.tv_location, R.id.choose_location, R.id.img_cart, R.id.img_heart, R.id.tv_see_all, R.id.btn_got_it})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_location:
                break;
            case R.id.img_cart:
                break;
            case R.id.img_heart:
                break;
            case R.id.tv_see_all:
                showAllCuisinesDialog();
                break;
            case R.id.choose_location:
                initBottomSheet();
//                slidingLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_got_it:
                PrefConnect.writeBoolean(FoodHomeActivity.this, Global.ENTERONCE_FOOD, true);
                locationLayoutAnim.clearAnimation();
                locationLayoutAnim.setVisibility(View.GONE);
                break;
        }
    }

    private void showAllCuisinesDialog() {
        cuisinesDialog = new Dialog(this, R.style.SlideTheme);
        cuisinesDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cuisinesDialog.setCancelable(false);
        cuisinesDialog.setContentView(R.layout.cousiness_bottom_layout);
        Window window = cuisinesDialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        ShimmerRecyclerView rv_food_style_list_dialog = cuisinesDialog.findViewById(R.id.rv_food_style_list_dialog);

        AppCompatImageView back = cuisinesDialog.findViewById(R.id.back);
        AppCompatTextView tv_food_item = cuisinesDialog.findViewById(R.id.tv_food_item);

        back.setOnClickListener(v -> cuisinesDialog.dismiss());

        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("1")) {
            tv_food_item.setText(getResources().getString(R.string.cuisines));
            rv_food_style_list_dialog.setLayoutManager(new GridLayoutManager(this, 3));
            foodAdapter = new FoodStyleAdapter(this, foodList, "1", "0");
            rv_food_style_list_dialog.setAdapter(foodAdapter);
            foodAdapter.notifyDataSetChanged();
        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("2")) {
            tv_food_item.setText(getResources().getString(R.string.cuisines));

            rv_food_style_list_dialog.setLayoutManager(new GridLayoutManager(this, 3));
            foodAdapter = new FoodStyleAdapter(this, groceryList, "0");
            rv_food_style_list_dialog.setAdapter(foodAdapter);
            foodAdapter.notifyDataSetChanged();

        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("3")) {
            tv_food_item.setText(getResources().getString(R.string.services));

            rv_food_style_list_dialog.setLayoutManager(new GridLayoutManager(this, 3));
            handyManAdapter = new HandyManAdapter(this, serviceList, "0");
            rv_food_style_list_dialog.setAdapter(handyManAdapter);
            handyManAdapter.notifyDataSetChanged();
        }
        cuisinesDialog.show();
    }
}