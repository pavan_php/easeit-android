package com.dratoo_user.ui.foodhome.model;

import java.util.ArrayList;

public class ServiceListResponse {

    public ArrayList<ServiceList> data;

    public ArrayList<ServiceList> getData() {
        return data;
    }

    public void setData(ArrayList<ServiceList> data) {
        this.data = data;
    }
}
