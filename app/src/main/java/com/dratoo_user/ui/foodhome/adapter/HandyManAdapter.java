package com.dratoo_user.ui.foodhome.adapter;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.ui.foodhome.FoodHomeActivity;
import com.dratoo_user.ui.foodhome.model.ServiceList;
import com.dratoo_user.ui.restarnt.RestarantActivity;

import java.util.ArrayList;

public class HandyManAdapter extends RecyclerView.Adapter<HandyManAdapter.ViewHolder> {
    private FoodHomeActivity foodHomeActivity;
    private ArrayList<ServiceList> handyManList;
    private String dialog;

    public HandyManAdapter(FoodHomeActivity foodHomeActivity, ArrayList<ServiceList> serviceList, String s) {
        this.foodHomeActivity = foodHomeActivity;
        this.handyManList = serviceList;
        this.dialog = s;
    }

    @NonNull
    @Override
    public HandyManAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.food_style_items, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HandyManAdapter.ViewHolder viewHolder, final int position) {

        if (dialog.equals("0")) {

            viewHolder.items_card_view_dialog.setVisibility(View.VISIBLE);
            viewHolder.items_card_view.setVisibility(View.GONE);

            viewHolder.tv_name_dialog.setText(handyManList.get(position).getService_name());

            try {
                Picasso.get().load(Global.IMG_URL + handyManList.get(position).getImage()).placeholder(foodHomeActivity.getResources().getDrawable(R.drawable.place_holder)).fit().into(viewHolder.img_food_dialog);
            } catch (Exception e) {
                e.printStackTrace();
            }


            viewHolder.items_card_view_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(foodHomeActivity, RestarantActivity.class);
                    PrefConnect.writeString(foodHomeActivity, Global.FOOD_NAME, handyManList.get(position).getService_name());


                    PrefConnect.writeString(foodHomeActivity, Global.FOOD_STYLE_ID, handyManList.get(position).getService_id());

                    intent.putExtra(Global.FOOD_NAME, handyManList.get(position).getService_name());

                    foodHomeActivity.startActivity(intent);
                }
            });

        } else {

            viewHolder.items_card_view_dialog.setVisibility(View.GONE);
            viewHolder.items_card_view.setVisibility(View.VISIBLE);


            viewHolder.tv_food_name.setText(handyManList.get(position).getService_name());

            try {
                Picasso.get().load(Global.IMG_URL + handyManList.get(position).getImage()).placeholder(foodHomeActivity.getResources().getDrawable(R.drawable.place_holder)).fit().into(viewHolder.img_food);
            } catch (Exception e) {
                e.printStackTrace();
            }

            viewHolder.items_card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(foodHomeActivity, RestarantActivity.class);
                    PrefConnect.writeString(foodHomeActivity, Global.FOOD_NAME, handyManList.get(position).getService_name());


                    PrefConnect.writeString(foodHomeActivity, Global.FOOD_STYLE_ID, handyManList.get(position).getService_id());

                    intent.putExtra(Global.FOOD_NAME, handyManList.get(position).getService_name());

                    foodHomeActivity.startActivity(intent);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return handyManList == null ? 0 : handyManList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView img_food;
        AppCompatImageView img_food_dialog;

        AppCompatTextView tv_food_name;
        AppCompatTextView tv_name_dialog;

        CardView items_card_view;
        CardView items_card_view_dialog;

        LinearLayout bottom_layout_text;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_food = itemView.findViewById(R.id.img_food);
            img_food_dialog = itemView.findViewById(R.id.img_food_dialog);
            tv_name_dialog = itemView.findViewById(R.id.tv_name_dialog);
            tv_food_name = itemView.findViewById(R.id.tv_food_name);
            items_card_view = itemView.findViewById(R.id.items_card_view);
            items_card_view_dialog = itemView.findViewById(R.id.items_card_view_dialog);
            bottom_layout_text = itemView.findViewById(R.id.bottom_layout_text);
        }
    }
}
