package com.dratoo_user.ui.foodhome.adapter;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.ui.foodhome.FoodHomeActivity;
import com.dratoo_user.ui.foodhome.model.FoodList;
import com.dratoo_user.ui.foodhome.model.ProductList;
import com.dratoo_user.ui.restarnt.RestarantActivity;

import java.util.ArrayList;

public class FoodStyleAdapter extends RecyclerView.Adapter<FoodStyleAdapter.ViewHolder> {

    ArrayList<ProductList> groceryList;
    FoodHomeActivity foodHomeActivity;

    ArrayList<FoodList> foodLists;

    String service_type;

    String dialog;

    public FoodStyleAdapter(FoodHomeActivity foodHomeActivity, ArrayList<FoodList> foodList, String service_type, String dialog) {

        this.foodHomeActivity = foodHomeActivity;

        this.service_type = service_type;
        this.foodLists = foodList;
        this.dialog = dialog;

    }

    public FoodStyleAdapter(FoodHomeActivity foodHomeActivity, ArrayList<ProductList> groceryList, String dialog) {
        this.foodHomeActivity = foodHomeActivity;

        this.groceryList = groceryList;

        this.dialog = dialog;


    }

    @NonNull
    @Override
    public FoodStyleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.food_style_items, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodStyleAdapter.ViewHolder viewHolder, final int position) {


        viewHolder.setIsRecyclable(false);

        if (service_type != null) {


            if (dialog.equals("0")) {

                viewHolder.items_card_view_dialog.setVisibility(View.VISIBLE);
                viewHolder.items_card_view.setVisibility(View.GONE);

                viewHolder.tv_name_dialog.setText(foodLists.get(position).getStyle());

                try {
                    Picasso.get().load(Global.IMG_URL + foodLists.get(position).getImage()).placeholder(foodHomeActivity.getResources().getDrawable(R.drawable.place_holder)).fit().into(viewHolder.img_food_dialog);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                viewHolder.items_card_view_dialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(foodHomeActivity, RestarantActivity.class);
                        if (service_type != null) {
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_NAME, foodLists.get(position).getStyle());
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_STYLE_ID, foodLists.get(position).getId());

                            intent.putExtra(Global.FOOD_NAME, foodLists.get(position).getStyle());
                        } else {
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_STYLE_ID, groceryList.get(position).getId());

                            intent.putExtra(Global.FOOD_NAME, groceryList.get(position).getProduct());
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_NAME, groceryList.get(position).getProduct());
                        }
                        foodHomeActivity.startActivity(intent);
                    }
                });

            } else {

                viewHolder.items_card_view_dialog.setVisibility(View.GONE);
                viewHolder.items_card_view.setVisibility(View.VISIBLE);


                viewHolder.tv_food_name.setText(foodLists.get(position).getStyle());

                try {
                    Picasso.get().load(Global.IMG_URL + foodLists.get(position).getImage()).placeholder(foodHomeActivity.getResources().getDrawable(R.drawable.place_holder)).fit().into(viewHolder.img_food);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                viewHolder.items_card_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(foodHomeActivity, RestarantActivity.class);
                        if (service_type != null) {
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_NAME, foodLists.get(position).getStyle());

                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_STYLE_ID, foodLists.get(position).getId());

                            intent.putExtra(Global.FOOD_NAME, foodLists.get(position).getStyle());
                        } else {
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_STYLE_ID, groceryList.get(position).getId());

                            intent.putExtra(Global.FOOD_NAME, groceryList.get(position).getProduct());
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_NAME, groceryList.get(position).getProduct());

                        }
                        foodHomeActivity.startActivity(intent);
                    }
                });

            }


        } else {

            if (dialog.equals("0")) {
                viewHolder.tv_name_dialog.setText(groceryList.get(position).getProduct());

                viewHolder.items_card_view_dialog.setVisibility(View.VISIBLE);
                viewHolder.items_card_view.setVisibility(View.GONE);

                try {
                    Picasso.get().load(Global.IMG_URL + groceryList.get(position).getImage()).placeholder(foodHomeActivity.getResources().getDrawable(R.drawable.place_holder)).fit().into(viewHolder.img_food_dialog);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                viewHolder.items_card_view_dialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(foodHomeActivity, RestarantActivity.class);
                        if (service_type != null) {
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_NAME, foodLists.get(position).getStyle());
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_STYLE_ID, foodLists.get(position).getId());

                            intent.putExtra(Global.FOOD_NAME, foodLists.get(position).getStyle());
                        } else {
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_STYLE_ID, groceryList.get(position).getId());

                            intent.putExtra(Global.FOOD_NAME, groceryList.get(position).getProduct());
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_NAME, groceryList.get(position).getProduct());

                        }
                        foodHomeActivity.startActivity(intent);
                    }
                });


            } else {

                viewHolder.items_card_view_dialog.setVisibility(View.GONE);
                viewHolder.items_card_view.setVisibility(View.VISIBLE);


                viewHolder.tv_food_name.setText(groceryList.get(position).getProduct());

                try {
                    Picasso.get().load(Global.IMG_URL + groceryList.get(position).getImage()).placeholder(foodHomeActivity.getResources().getDrawable(R.drawable.place_holder)).fit().into(viewHolder.img_food);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                viewHolder.items_card_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(foodHomeActivity, RestarantActivity.class);
                        if (service_type != null) {
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_STYLE_ID, foodLists.get(position).getId());

                            intent.putExtra(Global.FOOD_NAME, foodLists.get(position).getStyle());
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_NAME, foodLists.get(position).getStyle());

                        } else {
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_STYLE_ID, groceryList.get(position).getId());

                            intent.putExtra(Global.FOOD_NAME, groceryList.get(position).getProduct());
                            PrefConnect.writeString(foodHomeActivity, Global.FOOD_NAME, groceryList.get(position).getProduct());

                        }
                        foodHomeActivity.startActivity(intent);
                    }
                });

            }


        }


    }

    @Override
    public int getItemCount() {

        if (service_type != null) {
            return foodLists.size();
        } else {
            return groceryList.size();
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        AppCompatImageView img_food;
        AppCompatImageView img_food_dialog;

        AppCompatTextView tv_food_name;
        AppCompatTextView tv_name_dialog;

        CardView items_card_view;
        CardView items_card_view_dialog;

        LinearLayout bottom_layout_text;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_food = itemView.findViewById(R.id.img_food);
            img_food_dialog = itemView.findViewById(R.id.img_food_dialog);
            tv_name_dialog = itemView.findViewById(R.id.tv_name_dialog);
            tv_food_name = itemView.findViewById(R.id.tv_food_name);
            items_card_view = itemView.findViewById(R.id.items_card_view);
            items_card_view_dialog = itemView.findViewById(R.id.items_card_view_dialog);
            bottom_layout_text = itemView.findViewById(R.id.bottom_layout_text);
        }
    }
}
