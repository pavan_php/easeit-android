package com.dratoo_user.ui.foodhome.presenter;


import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.foodhome.model.FoodResponse;
import com.dratoo_user.ui.foodhome.model.GroceryProductListResponse;
import com.dratoo_user.ui.foodhome.model.ServiceListResponse;

public interface FoodHomeView extends BaseView {

    void foodSucess(FoodResponse body);

    void sucess(String msg);


    void sucessGroceryStyleList(GroceryProductListResponse data);

    void sucessServiceLists(ServiceListResponse body);


}
