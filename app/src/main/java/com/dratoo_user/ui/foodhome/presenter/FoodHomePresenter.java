package com.dratoo_user.ui.foodhome.presenter;

import com.dratoo_user.base.Presenter;

public interface FoodHomePresenter extends Presenter<FoodHomeView> {


    void getFoodList();


    void getProductList();

    void getServiceList();


}
