package com.dratoo_user.ui.handymanmaplocation.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.restarnt.model.ServiceProviderResponse;

public interface HandyManView extends BaseView {

    void getProviderList(ServiceProviderResponse serviceProviderResponse);
}
