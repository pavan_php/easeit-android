package com.dratoo_user.ui.handymanmaplocation.presenter;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.restarnt.model.ServiceProviderResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HandyManImpl extends BasePresenter<HandyManView> implements HandyManPresenter {

    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;


    @Override
    public void attachView(HandyManView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void getNearByProviderList(String service_id, String lat, String lang, String timing, String startDay) {

        getView().showProgress();
        Call<ServiceProviderResponse> call = apiInterface.getNearByProviderDetails(service_id, lat, lang, timing, startDay);

        call.enqueue(new Callback<ServiceProviderResponse>() {
            @Override
            public void onResponse(Call<ServiceProviderResponse> call, Response<ServiceProviderResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().getProviderList(response.body());
                }
            }

            @Override
            public void onFailure(Call<ServiceProviderResponse> call, Throwable t) {

            }
        });

    }
}
