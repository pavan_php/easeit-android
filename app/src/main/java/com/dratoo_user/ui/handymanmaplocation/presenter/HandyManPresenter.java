package com.dratoo_user.ui.handymanmaplocation.presenter;

import com.dratoo_user.base.Presenter;

public interface HandyManPresenter extends Presenter<HandyManView> {

    void getNearByProviderList(String service_id,String lat,String lang,String timing,String startDay);
}
