package com.dratoo_user.ui.handymanmaplocation.adapter;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.ui.handymanmaplocation.HandyManMapLocationActivity;
import com.dratoo_user.ui.handymanproviderdetails.HandyManProviderDetailsActivity;
import com.dratoo_user.ui.restarnt.model.ServiceProviderDetails;

import java.text.DecimalFormat;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class HandyManProvidersAdapter extends RecyclerView.Adapter<HandyManProvidersAdapter.ViewHolder> {


    HandyManMapLocationActivity handyManMapLocationActivity;
    ArrayList<ServiceProviderDetails> serviceProviderDetails;

    public HandyManProvidersAdapter(HandyManMapLocationActivity handyManMapLocationActivity, ArrayList<ServiceProviderDetails> data) {

        this.handyManMapLocationActivity = handyManMapLocationActivity;
        this.serviceProviderDetails = data;

    }

    @NonNull
    @Override
    public HandyManProvidersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.handyman_provider_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HandyManProvidersAdapter.ViewHolder viewHolder, final int position) {

        try {
            Picasso.get().load(Global.IMG_URL + serviceProviderDetails.get(position).getProfile_image()).fit().into(viewHolder.img_provider);
        } catch (Exception e) {
            e.printStackTrace();
        }


        DecimalFormat precision = new DecimalFormat("0.00");

        double val = Double.parseDouble(serviceProviderDetails.get(position).getRating());

        String conversion = precision.format(val);
        viewHolder.tv_rating.setText(conversion);


        viewHolder.tv_provider_name.setText(serviceProviderDetails.get(position).getFname()+" "+serviceProviderDetails.get(position).getLname());
        viewHolder.tv_distance.setText("Experience " + " : " + serviceProviderDetails.get(position).getExperiance() + " " + "Year(s)");
        viewHolder.tv_amt_hr.setText(PrefConnect.readString(handyManMapLocationActivity, Global.CURRENCY, "") + " " + serviceProviderDetails.get(position).getRate_per_hour() + " " + "/ hr");

        viewHolder.provider_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(handyManMapLocationActivity, HandyManProviderDetailsActivity.class);
                intent.putExtra(Global.PROVIDER_DETAILS, serviceProviderDetails.get(position));
                handyManMapLocationActivity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return serviceProviderDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView img_provider;

        AppCompatTextView tv_provider_name, tv_distance, tv_amt_hr, tv_rating;

        LinearLayout provider_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_provider = itemView.findViewById(R.id.img_provider);
            tv_provider_name = itemView.findViewById(R.id.tv_provider_name);
            tv_distance = itemView.findViewById(R.id.tv_distance);
            tv_amt_hr = itemView.findViewById(R.id.tv_amt_hr);
            tv_rating = itemView.findViewById(R.id.tv_rating);
            provider_layout = itemView.findViewById(R.id.provider_layout);
        }
    }
}
