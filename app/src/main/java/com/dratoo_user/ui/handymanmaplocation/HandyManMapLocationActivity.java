package com.dratoo_user.ui.handymanmaplocation;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.choosetimedateslot.DateTimeActivity;
import com.dratoo_user.ui.common.ProgressDialog;
import com.dratoo_user.ui.handymanmaplocation.adapter.HandyManProvidersAdapter;
import com.dratoo_user.ui.handymanmaplocation.presenter.HandyManPresenter;
import com.dratoo_user.ui.handymanmaplocation.presenter.HandyManView;
import com.dratoo_user.ui.restarnt.model.ServiceProviderDetails;
import com.dratoo_user.ui.restarnt.model.ServiceProviderResponse;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HandyManMapLocationActivity extends AppCompatActivity implements HandyManView, OnMapReadyCallback {


    @Inject
    HandyManPresenter presenter;

    ProgressDialog progressDialog;
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;

    @BindView(R.id.rv_workers)
    RecyclerView rvWorkers;
    @BindView(R.id.dragView)
    FrameLayout dragView;
    @BindView(R.id.sliding_layout)
    SlidingUpPanelLayout slidingLayout;
    @BindView(R.id.top_layout)
    LinearLayout topLayout;
    @BindView(R.id.parant_layout)
    RelativeLayout parantLayout;
    private HandyManProvidersAdapter adapter;
    private SupportMapFragment HomemapFragment;
    private ArrayList<LatLng> latlngs = new ArrayList<>();
    private GoogleMap mMap;

    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private Location mLastLocation;


    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 101;
    public static final int REQUEST_CHECK_SETTINGS = 100;

    LatLng latLng;
    private Marker mCurrLocationMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hany_man_map_location);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        presenter.attachView(this);
        progressDialog = new ProgressDialog(this);

        HomemapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        HomemapFragment.getMapAsync(this);


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.map_back);

        slidingLayout.setTouchEnabled(false);


        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult != null) {
                    super.onLocationResult(locationResult);
                    mLastLocation = locationResult.getLastLocation();
                    Log.e("Nive ", "onLocationResult: " + mLastLocation);
                    if (mLastLocation != null) {
                        latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(latLng);
                        markerOptions.title("Current Position");
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pick_pin));
                        mCurrLocationMarker = mMap.addMarker(markerOptions);

                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,
                                14));


                    }

                }

            }

        };


        callNearByProvidersApi();


    }


    @Override
    protected void onStart() {
        checkLocationPermission();
        super.onStart();
    }


    public void checkLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // Asking user if explanation is needed
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);

                    Log.e("Nive ", "checkLocationPermission:ACCESS_FINE_LOCATION ");
                } else {
                    Log.e("Nive ", "checkLocationPermission:MY_PERMISSIONS_REQUEST_LOCATION ");

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }
            } else {
                checkGps();
            }
        } else {
            checkGps();
        }
    }


    private void checkGps() {
        LocationSettingsRequest.Builder settingsBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        settingsBuilder.setAlwaysShow(true);

        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(this).checkLocationSettings(settingsBuilder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    setLocalUpdateRequest();
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(
                                        HandyManMapLocationActivity.this,
                                        REQUEST_CHECK_SETTINGS);

                                Log.e("Nive ", "onComplete: gps");
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void setLocalUpdateRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback, Looper.myLooper());
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
// If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        checkGps();

                        mLocationCallback = new LocationCallback() {
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                if (locationResult != null) {
                                    super.onLocationResult(locationResult);
                                    mLastLocation = locationResult.getLastLocation();
                                    Log.e("Nive ", "onLocationResult: " + mLastLocation);

                                    if (mLastLocation != null) {

                                        latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());


                                    }
                                }
                            }
                        };

                    }

                } else {
// Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

        }
    }


    private void callNearByProvidersApi() {


        Log.e("Nive ", "callNearByProvidersApi: " + PrefConnect.readString(this, Global.FOOD_STYLE_ID, ""));

        presenter.getNearByProviderList(PrefConnect.readString(this, Global.CATEGORY_ID, ""), PrefConnect.readString(this, Global.CURRENT_LAT, ""), PrefConnect.readString(this, Global.CURRENT_LANG, ""), PrefConnect.readString(this, Global.TIMING, ""), PrefConnect.readString(this, Global.START_DAY, ""));

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                Intent intent = new Intent(HandyManMapLocationActivity.this, DateTimeActivity.class);
                startActivity(intent);
                break;
            case R.id.current_location_icon:
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void callProvidersList(ArrayList<ServiceProviderDetails> data) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvWorkers.setLayoutManager(layoutManager);

        adapter = new HandyManProvidersAdapter(this, data);
        rvWorkers.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }

    @Override
    public void showProgress() {

        progressDialog.show();

    }

    @Override
    public void hideProgress() {

        progressDialog.dismissLoader();

    }

    @OnClick(R.id.top_layout)
    public void onViewClicked() {

        if (slidingLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

        } else {

            slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


    }

    @Override
    public void getProviderList(ServiceProviderResponse serviceProviderResponse) {


        if (serviceProviderResponse.getMessage().equals("false")) {
            Global.snack_bar(parantLayout, "No Workers Available!!!", Snackbar.LENGTH_LONG, HandyManMapLocationActivity.this);
        } else {
            if (serviceProviderResponse.getData().size() > 0) {

                slidingLayout.setVisibility(View.VISIBLE);

                for (int i = 0; i < serviceProviderResponse.getData().size(); i++) {


                    latlngs.add(new LatLng(serviceProviderResponse.getData().get(i).getResult().getLat(), serviceProviderResponse.getData().get(i).getResult().getLng()));


                }

                Log.e("Nive ", "getProviderList: " + latlngs.size());

                for (int i = 0; i < latlngs.size(); i++) {


                    mMap.addMarker(new MarkerOptions().position(latlngs.get(i)).icon(BitmapDescriptorFactory.fromResource(R.drawable.confirm_drop_place_holder)));


                }

                callProvidersList(serviceProviderResponse.getData());


            }

        }


    }
}
