package com.dratoo_user.ui.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.dratoo_user.R;

import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class LocationBottomFragment extends BottomSheetDialogFragment {
    Activity activity;
    onLocationFetch onLocationFetch;
    private View view;

    public LocationBottomFragment(Activity activity, onLocationFetch onLocationFetch) {
        this.activity =activity;
        this.onLocationFetch = onLocationFetch;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_services_bottom_sheet_dialog, container, false);
        ButterKnife.bind(this, view);
       /* setData();
        setServiceRecycler();
        setOtherServiceRecycler();*/
        return view;
    }
}
