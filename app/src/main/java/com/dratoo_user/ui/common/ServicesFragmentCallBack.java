package com.dratoo_user.ui.common;

public interface ServicesFragmentCallBack {

    void onServicesFragmentDismiss();
}
