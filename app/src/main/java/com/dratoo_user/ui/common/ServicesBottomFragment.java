package com.dratoo_user.ui.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dratoo_user.R;
import com.dratoo_user.data.ToastMaker;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dratoo_user.data.Global.OTHER_SERVICES;
import static com.dratoo_user.data.Global.SELECTED_SERVICES;


@SuppressLint("ValidFragment")
public class ServicesBottomFragment extends BottomSheetDialogFragment implements EditServiceCallback {

    View view;
    Activity activity;
    List<ServiceDetails> serviceDetailsList = new ArrayList<>();
    List<Drawable> drawableList = new ArrayList<>();
    List<String> serviceNameList = new ArrayList<>();

    List<ServiceDetails> otherServicesList = new ArrayList<>();
    List<Drawable> otherDrawableList = new ArrayList<>();
    List<String> otherserviceNameList = new ArrayList<>();

    ServicesFragmentCallBack servicesFragmentCallBack;
    @BindView(R.id.check_edt)
    AppCompatCheckBox checkEdt;
    @BindView(R.id.selected_service_recycler)
    RecyclerView selectedServiceRecycler;
    @BindView(R.id.other_service_recycler)
    RecyclerView otherServiceRecycler;


    @SuppressLint("ValidFragment")
    public ServicesBottomFragment(Activity activity, ServiceOnClick servicesFragmentCallBack) {
        this.activity = activity;
        this.servicesFragmentCallBack = (ServicesFragmentCallBack) servicesFragmentCallBack;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_services_bottom_sheet_dialog, container, false);
        ButterKnife.bind(this, view);
        setData();
        setServiceRecycler();
        setOtherServiceRecycler();
        return view;
    }

    private void setServiceRecycler() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 4);
        selectedServiceRecycler.setLayoutManager(gridLayoutManager);
        selectedServiceRecycler.setNestedScrollingEnabled(false);
        EditServicesAdapter servicesAdapter = new EditServicesAdapter(activity, serviceDetailsList, otherServicesList, this, SELECTED_SERVICES);
        selectedServiceRecycler.setAdapter(servicesAdapter);
    }

    private void setOtherServiceRecycler() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 4);
        otherServiceRecycler.setLayoutManager(gridLayoutManager);
        otherServiceRecycler.setNestedScrollingEnabled(false);
        EditServicesAdapter servicesAdapter = new EditServicesAdapter(activity, serviceDetailsList, otherServicesList, this, OTHER_SERVICES);
        otherServiceRecycler.setAdapter(servicesAdapter);
    }

    private void setData() {
        drawableList.add(activity.getResources().getDrawable(R.drawable.ic_bike));
        drawableList.add(activity.getResources().getDrawable(R.drawable.ic_car));
        drawableList.add(activity.getResources().getDrawable(R.drawable.ic_cab));
        drawableList.add(activity.getResources().getDrawable(R.drawable.ic_food));
        drawableList.add(activity.getResources().getDrawable(R.drawable.ic_courier));
        drawableList.add(activity.getResources().getDrawable(R.drawable.ic_grocery));
        drawableList.add(activity.getResources().getDrawable(R.drawable.ic_cargo));
        drawableList.add(activity.getResources().getDrawable(R.drawable.ic_more));

        otherDrawableList.add(activity.getResources().getDrawable(R.drawable.ic_bike));
        otherDrawableList.add(activity.getResources().getDrawable(R.drawable.ic_car));
        otherDrawableList.add(activity.getResources().getDrawable(R.drawable.ic_cab));
        otherDrawableList.add(activity.getResources().getDrawable(R.drawable.ic_food));
        otherDrawableList.add(activity.getResources().getDrawable(R.drawable.ic_courier));
        otherDrawableList.add(activity.getResources().getDrawable(R.drawable.ic_grocery));
        otherDrawableList.add(activity.getResources().getDrawable(R.drawable.ic_cargo));
        otherDrawableList.add(activity.getResources().getDrawable(R.drawable.ic_bike));
        otherDrawableList.add(activity.getResources().getDrawable(R.drawable.ic_car));
        otherDrawableList.add(activity.getResources().getDrawable(R.drawable.ic_cab));
        otherDrawableList.add(activity.getResources().getDrawable(R.drawable.ic_food));
        otherDrawableList.add(activity.getResources().getDrawable(R.drawable.ic_courier));
        otherDrawableList.add(activity.getResources().getDrawable(R.drawable.ic_grocery));
        otherDrawableList.add(activity.getResources().getDrawable(R.drawable.ic_cargo));

        serviceNameList.add("Drato-ride");
        serviceNameList.add("Drato-Car");
        serviceNameList.add("Drato-bluebird");
        serviceNameList.add("Drato-food");
        serviceNameList.add("Drato-courier");
        serviceNameList.add("Drato-daily");
        serviceNameList.add("Drato-cargo");
        serviceNameList.add("More");

        otherserviceNameList.add("Drato-ride");
        otherserviceNameList.add("Drato-car");
        otherserviceNameList.add("Drato-bluebird");
        otherserviceNameList.add("Drato-food");
        otherserviceNameList.add("Drato-courier");
        otherserviceNameList.add("Drato-daily");
        otherserviceNameList.add("Drato-cargo");
        otherserviceNameList.add("Drato-ride");
        otherserviceNameList.add("Drato-car");
        otherserviceNameList.add("Drato-bluebird");
        otherserviceNameList.add("Drato-food");
        otherserviceNameList.add("Drato-courier");
        otherserviceNameList.add("Drato-daily");
        otherserviceNameList.add("Drato-cargo");

        for (int i = 0; i < serviceNameList.size(); i++) {
            ServiceDetails serviceDetails = new ServiceDetails();
            serviceDetails.setSeriveimage(drawableList.get(i));
            serviceDetails.setSerivename(serviceNameList.get(i));
            serviceDetailsList.add(serviceDetails);
        }
        for (int i = 0; i < otherserviceNameList.size(); i++) {
            ServiceDetails serviceDetails = new ServiceDetails();
            serviceDetails.setSeriveimage(otherDrawableList.get(i));
            serviceDetails.setSerivename(otherserviceNameList.get(i));
            otherServicesList.add(serviceDetails);
        }
    }


    @Override
    public void onClickEditService(int position, String serviceType) {
        if (serviceType.equals(OTHER_SERVICES)) {
            serviceDetailsList.add(serviceDetailsList.size() - 1, otherServicesList.get(position));
            otherServicesList.remove(position);
            setRefreshRecycler();
        } else if (serviceType.equals(SELECTED_SERVICES)) {
            otherServicesList.add(serviceDetailsList.get(position));
            serviceDetailsList.remove(position);
            setRefreshRecycler();
        }


    }

    private void setRefreshRecycler() {
        if (otherServiceRecycler.getAdapter() != null) {
            otherServiceRecycler.getAdapter().notifyDataSetChanged();
        }
        if (selectedServiceRecycler.getAdapter() != null) {
            selectedServiceRecycler.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void onAddRestrict(String message) {
        ToastMaker.makeToast(activity, message);
    }

    private void refreshRecycler() {

    }
}
