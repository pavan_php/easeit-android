package com.dratoo_user.ui.common;

import android.graphics.drawable.Drawable;

public class ServiceDetails {

    private String serivename;
    private Drawable seriveimage;

    public String getSerivename() {
        return serivename;
    }

    public void setSerivename(String serivename) {
        this.serivename = serivename;
    }

    public Drawable getSeriveimage() {
        return seriveimage;
    }

    public void setSeriveimage(Drawable seriveimage) {
        this.seriveimage = seriveimage;
    }
}
