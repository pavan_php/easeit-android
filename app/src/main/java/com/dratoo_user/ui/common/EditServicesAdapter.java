package com.dratoo_user.ui.common;

import android.app.Activity;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.dratoo_user.R;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.VIBRATOR_SERVICE;
import static com.dratoo_user.data.Global.OTHER_SERVICES;
import static com.dratoo_user.data.Global.SELECTED_SERVICES;


public class EditServicesAdapter extends RecyclerView.Adapter<EditServicesAdapter.ViewHolder> {
    String TAG = "EditServicesAdapter";
    private final Vibrator vibrator;
    Activity activity;
    List<ServiceDetails> serviceDetailsList;
    EditServiceCallback editServiceCallback;
    List<ServiceDetails> finalServicesList;
    String serviceType;
    Animation shakeAnimation;
    public EditServicesAdapter(Activity activity, List<ServiceDetails> serviceDetailsList, List<ServiceDetails> otherServicesList, EditServiceCallback editServiceCallback, String serviceType) {

        Log.e(TAG, "EditServicesAdapter:serviceDetailsList "+serviceDetailsList.size());
        Log.e(TAG, "EditServicesAdapter:otherServicesList "+otherServicesList.size());

        this.activity = activity;

        this.serviceDetailsList = serviceDetailsList;

        if (serviceType.equals(SELECTED_SERVICES)) {
            this.finalServicesList = serviceDetailsList;
        } else if (serviceType.equals(OTHER_SERVICES)) {
            this.finalServicesList = otherServicesList;
        }

        this.editServiceCallback = editServiceCallback;
        this.serviceType = serviceType;
        vibrator = (Vibrator) activity.getSystemService(VIBRATOR_SERVICE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.services_adapter, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (serviceType.equals(SELECTED_SERVICES)) {
            if(finalServicesList.get(position).getSerivename().equalsIgnoreCase("more")){
                holder.imgEditBtn.setImageResource(android.R.color.transparent);
            }else{
                holder.imgEditBtn.setImageResource(R.drawable.ic_minus);
            }
        } else if (serviceType.equals(OTHER_SERVICES)) {
            holder.imgEditBtn.setImageResource(R.drawable.ic_plus);
        }

        holder.imgService.setImageDrawable(finalServicesList.get(position).getSeriveimage());
        holder.txtServiceName.setText(finalServicesList.get(position).getSerivename());

    }


    @Override
    public int getItemCount() {

        return finalServicesList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {
        @BindView(R.id.img_service)
        AppCompatImageView imgService;
        @BindView(R.id.txt_service_name)
        AppCompatTextView txtServiceName;
        @BindView(R.id.img_edit_btn)
        AppCompatImageView imgEditBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            shakeAnimation = AnimationUtils.loadAnimation(activity, R.anim.shake);
            imgEditBtn.setVisibility(View.VISIBLE);
            imgService.setOnLongClickListener(this);

        }

        @Override
        public boolean onLongClick(View v) {
            setVibrate();

            if (serviceType.equals(OTHER_SERVICES)) {
                if (serviceDetailsList.size() < 8) {
                    editServiceCallback.onClickEditService(getAdapterPosition(), serviceType);
                } else {
                    editServiceCallback.onAddRestrict("Only 8 services are allowed");
                }
            }else if (serviceType.equals(SELECTED_SERVICES)) {
                if(!finalServicesList.get(getAdapterPosition()).getSerivename().equalsIgnoreCase("more")){
                    editServiceCallback.onClickEditService(getAdapterPosition(), serviceType);
                }
            }

            return true;
        }

        private void setVibrate() {
            //vibrate the phone on longPress success on adding the service
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrator.vibrate(VibrationEffect.createOneShot(35, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                //deprecated in API 26
                vibrator.vibrate(100);
            }

            //this shake animation is used to display that the service is not added by shaking the imageview
            imgService.startAnimation(shakeAnimation);
        }
    }
}
