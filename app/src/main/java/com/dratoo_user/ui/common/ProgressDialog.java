package com.dratoo_user.ui.common;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.dratoo_user.R;

public class ProgressDialog {
    private Dialog showLoaderDialog;
    private ProgressBar progress_bar;

    public ProgressDialog(Context context) {
        showLoaderDialog = new Dialog(context);
        showLoaderDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        showLoaderDialog.setCancelable(false);
        showLoaderDialog.setContentView(R.layout.dialog_loader);

        progress_bar = showLoaderDialog.findViewById(R.id.progress_bar);
        Window window = showLoaderDialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
    }

    public void show() {
        try {
            if (showLoaderDialog != null && !showLoaderDialog.isShowing()) {

                showLoaderDialog.show();
                progress_bar.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissLoader() {
        try {
            if (showLoaderDialog != null) {
                showLoaderDialog.dismiss();
                progress_bar.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}