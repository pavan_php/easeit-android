package com.dratoo_user.ui.common;

public interface EditServiceCallback {

    void onClickEditService(int position, String serviceType);
    void onAddRestrict(String message);
}
