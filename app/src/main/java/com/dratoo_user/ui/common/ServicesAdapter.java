package com.dratoo_user.ui.common;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dratoo_user.R;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {
    Activity activity;
    List<ServiceDetails> serviceDetailsList;
    ServiceOnClick serviceOnClick;


    public ServicesAdapter(Activity activity, List<ServiceDetails> serviceDetailsList, ServiceOnClick serviceOnClick) {
        this.activity = activity;
        this.serviceDetailsList = serviceDetailsList;
        this.serviceOnClick = serviceOnClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.services_adapter, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.imgService.setImageDrawable(serviceDetailsList.get(position).getSeriveimage());
        holder.txtServiceName.setText(serviceDetailsList.get(position).getSerivename());

    }

    @Override
    public int getItemCount() {
        return serviceDetailsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.img_service)
        AppCompatImageView imgService;
        @BindView(R.id.txt_service_name)
        AppCompatTextView txtServiceName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imgService.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.img_service:
                    if(txtServiceName.getText().toString().equalsIgnoreCase("more")){
                        serviceOnClick.onServiceClick();
                    }
                    break;
            }
        }
    }
}
