package com.dratoo_user.ui.common;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.ui.foodhome.FoodHomeActivity;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchLocationActivity extends AppCompatActivity implements LocationListener, OnMapReadyCallback {


    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.center_frame_drop)
    FrameLayout centerFrameDrop;
    @BindView(R.id.top_layout)
    FrameLayout topLayout;
    @BindView(R.id.img_place_holder)
    AppCompatImageView imgPlaceHolder;
    @BindView(R.id.tv_short_address_drop)
    AppCompatTextView tvShortAddressDrop;
    @BindView(R.id.tv_full_address_drop)
    AppCompatTextView tvFullAddressDrop;
    @BindView(R.id.select_destination_layout)
    LinearLayout selectDestinationLayout;
    @BindView(R.id.img_search_des)
    AppCompatImageView imgSearchDes;
    @BindView(R.id.btn_set_destination)
    AppCompatButton btnSetDestination;
    @BindView(R.id.destination_layout)
    LinearLayout destinationLayout;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private Location mLastLocation;

    SupportMapFragment HomemapFragment;
    private Geocoder geocoder;
    private String zipcode, locality, city, state, country, currentAddres;

    private boolean setText = false;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 101;
    public static final int REQUEST_CHECK_SETTINGS = 100;
    private GoogleMap mMap;
    private Animation animShow;
    private LatLng trackLatLangDrop;

    private LocationRequest locationRequest;
    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_location);
        ButterKnife.bind(this);

        animShow = AnimationUtils.loadAnimation(this, R.anim.popup_show);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(1000)
                .setFastestInterval(1000);

        HomemapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        HomemapFragment.getMapAsync(this);


        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult != null) {
                    super.onLocationResult(locationResult);
                    mLastLocation = locationResult.getLastLocation();
                    Log.e("Nive ", "onLocationResult: " + mLastLocation);
                    if (mLastLocation != null) {
                        LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

                        if (!setText) {

                            Log.e("SearchLocation", "setText: ");

                            PrefConnect.writeString(SearchLocationActivity.this, Global.CURRENT_LAT, String.valueOf(mLastLocation.getLatitude()));
                            PrefConnect.writeString(SearchLocationActivity.this, Global.CURRENT_LANG, String.valueOf(mLastLocation.getLongitude()));


                            getAddress(latLng);
                        }
                    }

                }

            }

        };


    }

    @Override
    protected void onStart() {
        checkLocationPermission();
        super.onStart();
    }


    @Override
    public void onLocationChanged(Location location) {

       double latitude = location.getLatitude();
       double longitude = location.getLongitude();


        //Intent intent = new Intent(SearchLocationActivity.this, FoodHomeActivity.class);
        //intent.putExtra(Global.SELECT_LOCATION, "1");
        //startActivity(intent);

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }



    @Override
    public void onProviderDisabled(String s) {

    }


    public void checkLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // Asking user if explanation is needed
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);

                    Log.e("Nive ", "checkLocationPermission:ACCESS_FINE_LOCATION ");
                } else {
                    Log.e("Nive ", "checkLocationPermission:MY_PERMISSIONS_REQUEST_LOCATION ");

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }
            } else {
                checkGps();
            }
        } else {
            checkGps();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
// If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        checkGps();

                        mLocationCallback = new LocationCallback() {
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                if (locationResult != null) {
                                    super.onLocationResult(locationResult);
                                    mLastLocation = locationResult.getLastLocation();
                                    Log.e("Nive ", "onLocationResult: " + mLastLocation);

                                    if (mLastLocation != null) {

                                        LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

                                        if (!setText) {

                                            PrefConnect.writeString(SearchLocationActivity.this, Global.CURRENT_LAT, String.valueOf(mLastLocation.getLatitude()));
                                            PrefConnect.writeString(SearchLocationActivity.this, Global.CURRENT_LANG, String.valueOf(mLastLocation.getLongitude()));


                                            getAddress(latLng);
                                        }

                                    }
                                }
                            }
                        };

                    }

                } else {
// Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

        }
    }


    private void checkGps() {
        LocationSettingsRequest.Builder settingsBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        settingsBuilder.setAlwaysShow(true);

        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(this).checkLocationSettings(settingsBuilder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    setLocalUpdateRequest();
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(
                                        SearchLocationActivity.this,
                                        REQUEST_CHECK_SETTINGS);

                                Log.e("Nive ", "onComplete: gps");
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void setLocalUpdateRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback, Looper.myLooper());
    }


    private void getAddress(LatLng LatLng) {


        geocoder = new Geocoder(this, Locale.getDefault());

        setText = true;

        Log.e("Nive ", "getAddress:Print " + LatLng);

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(LatLng.latitude, LatLng.longitude, 1);


            if (addresses.get(0).getPostalCode() != null) {
                zipcode = addresses.get(0).getPostalCode();
                Log.e("Nive ", "getPlaceInfo:ZIP " + zipcode);

            }
            if (addresses.get(0).getLocality() != null) {
                locality = addresses.get(0).getLocality();
                Log.e("Nive ", "getPlaceInfo:ZIP " + locality);

            }

            if (addresses.get(0).getLocality() != null) {
                city = addresses.get(0).getLocality();
                Log.e("Nive ", "getPlaceInfo:city " + city);
            }

            if (addresses.get(0).getAdminArea() != null) {
                state = addresses.get(0).getAdminArea();
                Log.e("Nive ", "getPlaceInfo:state " + state);
            }

            if (addresses.get(0).getCountryName() != null) {
                country = addresses.get(0).getCountryName();
                Log.e("Nive ", "getPlaceInfo:country " + country);
            }


            currentAddres = locality.concat(",").concat(city).concat(",").concat(state).concat(",").concat(country).concat(",").concat(zipcode);

            Log.e("Nive", "getAddress:currentAddres " + currentAddres);

            PrefConnect.writeString(SearchLocationActivity.this, Global.CURRENT_ADDRESS, currentAddres);


            PrefConnect.writeString(SearchLocationActivity.this, Global.CURRENT_LAT, String.valueOf(LatLng.latitude));
            PrefConnect.writeString(SearchLocationActivity.this, Global.CURRENT_LANG, String.valueOf(LatLng.longitude));

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng,
                    14));

            centerFrameDrop.setVisibility(View.VISIBLE);

            destinationLayout.startAnimation(animShow);
            destinationLayout.setVisibility(View.VISIBLE);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
        if (location != null)
        {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(90)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            getAddressFromSelectViaMap(location.getLatitude(), location.getLongitude());


        }



    }


    private void getAddressFromSelectViaMap(double latitude, double longitude) {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);

            if (addressList != null && addressList.size() > 0) {
                Address obj = addressList.get(0);
                String locality = obj.getLocality();


                Log.e("Nive ", "getAddressFromSelectViaMap: " + obj.getAddressLine(0));
                PrefConnect.writeString(SearchLocationActivity.this, Global.CURRENT_ADDRESS, String.valueOf(obj.getAddressLine(0)));

                if (locality != null) {
                    tvShortAddressDrop.setText(locality);
                } else {

                    tvShortAddressDrop.setText("Un Named City");
                }
                tvFullAddressDrop.setText(String.valueOf(obj.getAddressLine(0)));

                Toast.makeText(this, locality.toString(), Toast.LENGTH_LONG).show();

                destinationLayout.startAnimation(animShow);
                destinationLayout.setVisibility(View.VISIBLE);

                PrefConnect.writeString(SearchLocationActivity.this, Global.CURRENT_LAT, String.valueOf(latitude));
                PrefConnect.writeString(SearchLocationActivity.this, Global.CURRENT_LANG, String.valueOf(longitude));





            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.btn_set_destination)
    public void onViewClicked() {

        Log.e("Nive ", "onViewClicked: " + PrefConnect.readString(SearchLocationActivity.this, Global.CURRENT_ADDRESS, ""));
        //onBackPressed();

            /*
        Intent intent = new Intent(this, FoodHomeActivity.class);
        intent.putExtra(Global.SELECT_LOCATION, "1");
        startActivity(intent);
     */
        SearchLocationActivity.this.finish();
        Intent intent = new Intent(SearchLocationActivity.this, FoodHomeActivity.class);
        intent.putExtra(Global.SELECT_LOCATION, "1");
        startActivity(intent);
        this.finish();



    }
}
