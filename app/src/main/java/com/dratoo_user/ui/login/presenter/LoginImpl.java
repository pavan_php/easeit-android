package com.dratoo_user.ui.login.presenter;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.login.model.RegisterRequest;
import com.dratoo_user.ui.login.model.RegisterResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginImpl extends BasePresenter<LoginView> implements LoginPresenter {

    @Inject
    @Named(ApiModule.DEFAULT)
    ApiInterface apiInterface;

    @Override
    public void attachView(LoginView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void loginOrRegister(RegisterRequest registerRequest) {

        getView().showProgress();

        Call<RegisterResponse> call = apiInterface.loginOrRegister(registerRequest);

        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if (response.isSuccessful()) {

                    getView().hideProgress();

                    if (response.body().getData().getStatusCode().equals("1")) {
                        getView().sucess(response.body());
                    } else {

                        getView().failure("Something went wrong!!!");

                    }


                } else {
                    getView().hideProgress();
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {

                getView().hideProgress();

            }
        });

    }
}
