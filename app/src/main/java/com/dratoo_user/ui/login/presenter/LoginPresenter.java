package com.dratoo_user.ui.login.presenter;

import com.dratoo_user.base.Presenter;
import com.dratoo_user.ui.login.model.RegisterRequest;

public interface LoginPresenter extends Presenter<LoginView> {

    void loginOrRegister(RegisterRequest registerRequest);


}
