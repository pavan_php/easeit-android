package com.dratoo_user.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;

import com.dratoo_user.R;
import com.dratoo_user.app.App;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.common.ConnectivityReceiver;
import com.dratoo_user.ui.common.ProgressDialog;
import com.dratoo_user.ui.login.model.RegisterRequest;
import com.dratoo_user.ui.login.model.RegisterResponse;
import com.dratoo_user.ui.login.presenter.LoginPresenter;
import com.dratoo_user.ui.login.presenter.LoginView;
import com.dratoo_user.ui.otpverification.OtpVerificationActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hbb20.CountryCodePicker;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginView, ConnectivityReceiver.ConnectivityReceiverListener {
    @Inject
    LoginPresenter presenter;
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.et_mob_nbr)
    AppCompatEditText etMobNbr;
    @BindView(R.id.btn_continue)
    AppCompatButton btnContinue;
    @BindView(R.id.parant_layout)
    RelativeLayout parantLayout;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        presenter.attachView(this);
        progressDialog = new ProgressDialog(this);
        setUpToolbar();

        etMobNbr.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                btnContinue.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            }
        });
    }

    private void setUpToolbar() {
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.back_arrow);
        mToolbar.setNavigationOnClickListener(v -> finish());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.common, menu);
        menu.findItem(R.id.action_help);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_continue)
    public void onViewClicked() {
        if (etMobNbr.getText().toString().isEmpty()) {
            Global.snack_bar(parantLayout, "Fill the Mobile Number", Snackbar.LENGTH_LONG, LoginActivity.this);
        } else if (!(etMobNbr.getText().length() > 6) || !(etMobNbr.getText().length() < 14)) {
            Global.snack_bar(parantLayout, "Fill the Valid Mobile Number", Snackbar.LENGTH_LONG, LoginActivity.this);
        } else {
            final RegisterRequest registerRequest = new RegisterRequest();

            registerRequest.setCountry_code(ccp.getSelectedCountryCodeWithPlus());
            registerRequest.setPhone(etMobNbr.getText().toString());
            registerRequest.setType("0");
            registerRequest.setDevice_token(FirebaseInstanceId.getInstance().getToken());

            presenter.loginOrRegister(registerRequest);
        }
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismissLoader();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        if (isConnected) {
            message = getResources().getString(R.string.connected_to_internet);
        } else {
            message = getResources().getString(R.string.not_connected_to_internet);
        }
        Global.snack_bar(parantLayout, message, Snackbar.LENGTH_LONG, LoginActivity.this);
    }

    @Override
    public void sucess(RegisterResponse registerResponse) {
        Global.snack_bar(parantLayout, "You have registered Sucessfully", Snackbar.LENGTH_LONG, LoginActivity.this);

        PrefConnect.writeString(LoginActivity.this, Global.AUTH_ID, registerResponse.getData().getAuthId());

        PrefConnect.writeString(LoginActivity.this, Global.CURRENCY, registerResponse.getData().getCurrency());
        PrefConnect.writeString(LoginActivity.this, Global.ORDER_PREFIX, registerResponse.getData().getOrder_prefix());

        PrefConnect.writeString(LoginActivity.this, Global.TYPE_OF_REGISTER, registerResponse.getData().getType());
        PrefConnect.writeString(LoginActivity.this, Global.AUTH_TOKEN, registerResponse.getData().getAuthToken());

        Intent intentReg = new Intent(LoginActivity.this, OtpVerificationActivity.class);
        intentReg.putExtra(Global.MOBILE, registerResponse.getData().getPhone());
        intentReg.putExtra(Global.OTP, registerResponse.getData().getOtp());
        startActivity(intentReg);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    @Override
    protected void onStart() {
        super.onStart();
        App.getInstance().setConnectivityListener(this);
    }

    @Override
    public void failure(String msg) {
        Global.snack_bar(parantLayout, msg, Snackbar.LENGTH_LONG, LoginActivity.this);
    }
}