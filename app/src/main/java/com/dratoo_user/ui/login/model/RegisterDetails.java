package com.dratoo_user.ui.login.model;

public class RegisterDetails {

    public String phone;
    public String authToken;
    public String otp;
    public String type;
    public String authId;
    public String statusCode;
    public String currency;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String order_prefix;
    public String email;

    public String getIs_user_exist() {
        return is_user_exist;
    }

    public void setIs_user_exist(String is_user_exist) {
        this.is_user_exist = is_user_exist;
    }

    public String is_user_exist;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOrder_prefix() {
        return order_prefix;
    }

    public void setOrder_prefix(String order_prefix) {
        this.order_prefix = order_prefix;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String message;
}
