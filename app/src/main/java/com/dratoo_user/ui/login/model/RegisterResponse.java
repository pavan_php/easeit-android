package com.dratoo_user.ui.login.model;

public class RegisterResponse {

    public RegisterDetails data;

    public RegisterDetails getData() {
        return data;
    }

    public void setData(RegisterDetails data) {
        this.data = data;
    }
}
