package com.dratoo_user.ui.login.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.login.model.RegisterResponse;

public interface LoginView extends BaseView {

    void sucess(RegisterResponse registerResponse);

    void failure(String msg);
}
