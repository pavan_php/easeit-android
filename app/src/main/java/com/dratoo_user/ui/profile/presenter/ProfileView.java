package com.dratoo_user.ui.profile.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.profile.model.ProfileResponse;

public interface ProfileView extends BaseView {

    void profileSucess(ProfileResponse profileResponse);

    void failure(String msg);
}
