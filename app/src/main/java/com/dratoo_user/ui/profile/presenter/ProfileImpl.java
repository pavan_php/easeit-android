package com.dratoo_user.ui.profile.presenter;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.profile.model.ProfileResponse;

import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Named;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileImpl extends BasePresenter<ProfileView> implements ProfilePresenter {

    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;


    @Override
    public void attachView(ProfileView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void profileSave(HashMap<String, RequestBody> updateMap, MultipartBody.Part profilePhoto) {
        getView().showProgress();

        Call<ProfileResponse> call = apiInterface.profileSave(updateMap, profilePhoto);

        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    if (response.body().getData().getStatus().equals("1")) {
                        getView().profileSucess(response.body());
                    } else {
                        getView().hideProgress();
                        getView().failure(response.body().getData().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                getView().hideProgress();
            }
        });
    }
}
