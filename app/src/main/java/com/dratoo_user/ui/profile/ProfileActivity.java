package com.dratoo_user.ui.profile;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.dratoo_user.R;
import com.dratoo_user.data.FilePath;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.common.ProgressDialog;
import com.dratoo_user.ui.main.MainActivity;
import com.dratoo_user.ui.profile.model.ProfileResponse;
import com.dratoo_user.ui.profile.presenter.ProfilePresenter;
import com.dratoo_user.ui.profile.presenter.ProfileView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.hbb20.CountryCodePicker;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ProfileActivity extends AppCompatActivity implements ProfileView {
    @Inject
    ProfilePresenter profilePresenter;
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.desc_fill)
    AppCompatTextView descFill;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.btn_confirm)
    AppCompatButton btnConfirm;
    @BindView(R.id.img_profile)
    CircleImageView imgProfile;
    @BindView(R.id.et_name)
    AppCompatEditText etName;
    @BindView(R.id.et_email)
    AppCompatEditText etEmail;
    @BindView(R.id.et_mob_nbr)
    AppCompatEditText etMobNbr;
    @BindView(R.id.parant_layout)
    RelativeLayout parantLayout;
    private String mobNbr, type, email;
    private BottomSheetDialog mBottomSheetDialog;
    private View bottomSheet;
    private int SELECT_FILE = 1;
    public static final int PERMISSION_REQUEST = 000;
    private File cameraFile;
    private Uri uriCam = null;
    private Uri profileURI;
    private File avatarFile;
    private ProgressDialog progressDialog;
    private Uri resultUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        profilePresenter.attachView(this);
        setUpToolbar();
        progressDialog = new ProgressDialog(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        initBottomSheet();

        if (getIntent() != null) {
            Intent intent = getIntent();
            mobNbr = intent.getStringExtra(Global.MOBILE);
            type = intent.getStringExtra(Global.TYPE);
            email = intent.getStringExtra(Global.FB_EMAIL);

        }

        if (type != null) {

            if (type.equals("0")) {

                if (mobNbr != null) {
                    Log.e("Nive ", "onCreate: " + mobNbr);
                    etMobNbr.setText(mobNbr);

                }
            } else {

                if (email != null) {
                    etEmail.setText(email);
                }

            }

        }
    }

    public boolean isCameraPermissionEnabled() {

        return ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    public boolean isStoragePermissionEnabled() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            ArrayList<String> permissionList = new ArrayList<>();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
                permissionList.add(Manifest.permission.CAMERA);
            }

            if (permissionList.size() > 0) {
                String[] permissionArray = new String[permissionList.size()];
                permissionList.toArray(permissionArray);
                this.requestPermissions(permissionArray, PERMISSION_REQUEST);
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    public void initBottomSheet() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        bottomSheet = this.getLayoutInflater().inflate(R.layout.bottom_sheet_select_images, null);
        mBottomSheetDialog.setContentView(bottomSheet);

        AppCompatTextView tv_gallery = mBottomSheetDialog.findViewById(R.id.tv_gallery);
        AppCompatTextView tv_camera = mBottomSheetDialog.findViewById(R.id.tv_camera);

        tv_camera.setOnClickListener(v -> {
            if (isCameraPermissionEnabled() && isStoragePermissionEnabled()) {
                Calendar cal = Calendar.getInstance();
                cameraFile = new File(Environment.getExternalStorageDirectory(),
                        (cal.getTimeInMillis() + ".jpg"));

                if (!cameraFile.exists()) {
                    try {
                        cameraFile.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    cameraFile.delete();
                    try {
                        cameraFile.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                uriCam = Uri.fromFile(cameraFile);
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                i.putExtra(MediaStore.EXTRA_OUTPUT, uriCam);
                startActivityForResult(i, 0);

                mBottomSheetDialog.dismiss();
            } else {
                checkPermissions();
            }
        });

        tv_gallery.setOnClickListener(v -> {
            if (isCameraPermissionEnabled() && isStoragePermissionEnabled()) {
                galleryIntent();
                mBottomSheetDialog.dismiss();
            } else {
                checkPermissions();
            }
        });
    }

    private void galleryIntent() {


        Intent i1 = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i1, SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    profileURI = uriCam;
                    String filePath = getRealPathFromURI(profileURI);
                    File file = new File(filePath);
                    profileURI = Uri.fromFile(file);

                    // start cropping activity for pre-acquired image saved on the device
                    CropImage.activity(profileURI)
                            .start(this);

                }
                break;

            case 1:
                if (resultCode == RESULT_OK) {

                    Uri uri = data.getData();
                    profileURI = uri;
                    String filePath = getRealPathFromURI(profileURI);
                    File file = new File(filePath);
                    profileURI = Uri.fromFile(file);

                    // start cropping activity for pre-acquired image saved on the device
                    CropImage.activity(profileURI)
                            .start(this);


                }
                break;

            case 203:
                if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (resultCode == RESULT_OK) {
                        resultUri = result.getUri();

                        try {
                            Picasso.get().load(resultUri).fit().into(imgProfile);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                        Exception error = result.getError();
                    }

                }
                break;


        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null,
                null, null, null);

        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.common, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpToolbar() {
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.back_arrow);
        mToolbar.setNavigationOnClickListener(v -> finish());
    }

    @OnClick({R.id.img_profile, R.id.btn_confirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_profile:
                mBottomSheetDialog.show();
                break;
            case R.id.btn_confirm:

                if (etName.getText().toString().isEmpty()) {
                    Global.snack_bar(parantLayout, "Enter Name !!!", Snackbar.LENGTH_LONG, ProfileActivity.this);
                } else if (etEmail.getText().toString().isEmpty()) {

                    Global.snack_bar(parantLayout, "Enter Email !!!", Snackbar.LENGTH_LONG, ProfileActivity.this);
                } else if (etMobNbr.getText().toString().isEmpty()) {
                    Global.snack_bar(parantLayout, "Enter MobileNumber !!!", Snackbar.LENGTH_LONG, ProfileActivity.this);
                } else {
                    if (validate()) {
                        HashMap<String, RequestBody> updateMap = new HashMap();
                        HashMap<String, MultipartBody.Part> imageMap = new HashMap();
                        updateMap.put("email", createPartFromString(etEmail.getText().toString()));
                        updateMap.put("phone", createPartFromString(etMobNbr.getText().toString()));
                        updateMap.put("name", createPartFromString(etName.getText().toString()));


                        if (resultUri != null) {

                            avatarFile = FilePath.getPath(this, resultUri);
                        }

                        if (avatarFile != null) {
                            RequestBody requestFile =
                                    RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
                            MultipartBody.Part body =
                                    MultipartBody.Part.createFormData(
                                            "profile_pic", avatarFile.getName(), requestFile);
                            imageMap.put("profile_pic", body);
                        }

                        //add social login
                        if (type != null) {
                            if (type.equals("0")) {
                                updateMap.put("type", createPartFromString("0"));
                            } else {
                                updateMap.put("type", createPartFromString("1"));
                            }
                            profilePresenter.profileSave(updateMap, imageMap.get("profile_pic"));
                        }
                    }
                }
                break;
        }
    }

    private boolean validate() {
        if (etName.getText().toString().isEmpty()) {
            Global.snack_bar(parantLayout, "Enter Name !!!", Snackbar.LENGTH_LONG, ProfileActivity.this);
            return false;
        } else if (etMobNbr.getText().toString().isEmpty()) {
            Global.snack_bar(parantLayout, "Enter Mobile Number !!!", Snackbar.LENGTH_LONG, ProfileActivity.this);
            return false;
        } else if (etEmail.getText().toString().isEmpty()) {
            Global.snack_bar(parantLayout, "Enter Email !!!", Snackbar.LENGTH_LONG, ProfileActivity.this);
            return false;

        } else if (!etEmail.getText().toString().trim().matches(Global.EMAIL_PATTERN)) {
            Global.snack_bar(parantLayout, "Enter Valid Email !!!", Snackbar.LENGTH_LONG, ProfileActivity.this);
            return false;
        }
        btnConfirm.setBackgroundColor(getResources().getColor(R.color.dark_yellow));
        return true;
    }

    @NonNull
    public RequestBody createPartFromString(String descriptionString) {
        String assign = descriptionString;
        Log.e("assign", assign);
        return RequestBody.create(MultipartBody.FORM, descriptionString);
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismissLoader();
    }

    @Override
    public void profileSucess(ProfileResponse profileResponse) {
        PrefConnect.writeString(ProfileActivity.this, Global.AUTH_ID, profileResponse.getData().getAuthId());
        PrefConnect.writeString(ProfileActivity.this, Global.AUTH_TOKEN, profileResponse.getData().getAuthToken());

        PrefConnect.writeString(ProfileActivity.this, Global.NAME, profileResponse.getData().getName());
        PrefConnect.writeString(ProfileActivity.this, Global.MOBILE_NBR, profileResponse.getData().getPhone());

        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void failure(String msg) {
        Global.snack_bar(parantLayout, msg
                , Snackbar.LENGTH_LONG, ProfileActivity.this);

    }
}