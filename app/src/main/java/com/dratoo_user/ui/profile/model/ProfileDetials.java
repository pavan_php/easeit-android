package com.dratoo_user.ui.profile.model;

public class ProfileDetials {

    public String status;
    public String message;
    public String authId;
    public String authToken;
    public String is_user_exist;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getIs_user_exist() {
        return is_user_exist;
    }

    public void setIs_user_exist(String is_user_exist) {
        this.is_user_exist = is_user_exist;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String name;
    public String phone;
}
