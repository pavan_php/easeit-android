package com.dratoo_user.ui.profile.presenter;

import com.dratoo_user.base.Presenter;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface ProfilePresenter extends Presenter<ProfileView> {

    void profileSave(HashMap<String, RequestBody> updateMap, MultipartBody.Part profilePhoto);
}
