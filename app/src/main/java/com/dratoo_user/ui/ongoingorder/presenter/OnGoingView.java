package com.dratoo_user.ui.ongoingorder.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.ongoingorder.model.OrderListResponse;

public interface OnGoingView extends BaseView {

    void orderGetDetails(OrderListResponse body);
}
