package com.dratoo_user.ui.ongoingorder.adapter;

import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.ui.history.HistoryActivity;
import com.dratoo_user.ui.ongoingorder.OnGoingFragment;
import com.dratoo_user.ui.ongoingorder.model.OrderList;
import com.dratoo_user.ui.ordertracking.OrderTrackingActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class OngoingAdapter extends RecyclerView.Adapter<OngoingAdapter.ViewHolder> {


    ArrayList<OrderList> orderListArrayList;

    OnGoingFragment onGoingFragment;
    private String date1;

    public OngoingAdapter(OnGoingFragment onGoingFragment, ArrayList<OrderList> orderLists) {

        this.onGoingFragment = onGoingFragment;

        this.orderListArrayList = orderLists;

    }

    @NonNull
    @Override
    public OngoingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ongoingorder_items, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OngoingAdapter.ViewHolder viewHolder, final int position) {


        viewHolder.tv_status.setText(PrefConnect.readString(onGoingFragment.getActivity(), Global.CURRENCY, "") + " " + orderListArrayList.get(position).getBill_amount());


        if (PrefConnect.readString(onGoingFragment.getActivity(), Global.SERVICE_TYPE, "").equals("4") || PrefConnect.readString(onGoingFragment.getActivity(), Global.SERVICE_TYPE, "").equals("5") || PrefConnect.readString(onGoingFragment.getActivity(), Global.SERVICE_TYPE, "").equals("6") || PrefConnect.readString(onGoingFragment.getActivity(), Global.SERVICE_TYPE, "").equals("7")) {
            try {
                Picasso.get().load(Global.IMG_URL + orderListArrayList.get(position).getId_proof()).fit().into(viewHolder.order_image);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (orderListArrayList.get(position).getRide_status().equals("1")) {
                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.trip_accept));

            } else if (orderListArrayList.get(position).getRide_status().equals("2")) {
                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.trip_started));

            } else if (orderListArrayList.get(position).getRide_status().equals("3")) {

                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.trip_completed));
            } else if (orderListArrayList.get(position).getRide_status().equals("4")) {

                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.trip_canelled));
            } else {
                viewHolder.tv_trip_status.setVisibility(View.GONE);
            }

            viewHolder.tv_place.setText(orderListArrayList.get(position).getPickup_address());
            viewHolder.tv_place_drop.setTextColor(onGoingFragment.getActivity().getResources().getColor(R.color.black));
            viewHolder.tv_place_drop.setText(orderListArrayList.get(position).getDest_address());


            if (orderListArrayList.get(position).getPayment_type().equals("1")) {
                viewHolder.btn_payment_type.setText("CASH");

            } else {
                viewHolder.btn_payment_type.setText("CARD");
            }


            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd , yyyy");

            try {

                date1 = sdf.format(new SimpleDateFormat("yyyy-M-dd").parse(orderListArrayList.get(position).getCreated_at()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            viewHolder.tv_time.setText(date1);


        } else if (PrefConnect.readString(onGoingFragment.getActivity(), Global.SERVICE_TYPE, "").equals("3")) {


            viewHolder.tv_place_drop.setTextColor(onGoingFragment.getActivity().getResources().getColor(R.color.greyish));


            if (orderListArrayList.get(position).getService_status().equals("1") || orderListArrayList.get(position).getService_status().equals("10")) {
                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.order_placed));

            } else if (orderListArrayList.get(position).getService_status().equals("2")) {
                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.booking_accepted));

            } else if (orderListArrayList.get(position).getService_status().equals("3")) {

                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.completed));
            } else if (orderListArrayList.get(position).getService_status().equals("4")) {

                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.reject_by_restarant));
            } else if (orderListArrayList.get(position).getService_status().equals("5")) {

                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.started_job));
            } else if (orderListArrayList.get(position).getService_status().equals("6")) {

                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.worker_on_the_way));
            } else if (orderListArrayList.get(position).getService_status().equals("7")) {

                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.job_completed));
            } else {
                viewHolder.tv_trip_status.setVisibility(View.GONE);
            }


            viewHolder.tv_place_drop.setText(orderListArrayList.get(position).getDelivery_address());
            viewHolder.tv_place.setText(orderListArrayList.get(position).getProvider_admin());


            if (orderListArrayList.get(position).getPayment_type() != null) {
                if (orderListArrayList.get(position).getPayment_type().equals("1")) {
                    viewHolder.btn_payment_type.setText("CASH");

                } else {
                    viewHolder.btn_payment_type.setText("CARD");
                }
            }


            if (PrefConnect.readString(onGoingFragment.getActivity(), Global.SERVICE_TYPE, "").equals("3")) {
                try {
                    Picasso.get().load(Global.IMG_URL + orderListArrayList.get(position).getImage()).fit().into(viewHolder.order_image);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd , yyyy");

            try {

                date1 = sdf.format(new SimpleDateFormat("yyyy-M-dd").parse(orderListArrayList.get(position).getStart_date()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            viewHolder.tv_time.setText(date1);


        } else {


            viewHolder.tv_place_drop.setTextColor(onGoingFragment.getActivity().getResources().getColor(R.color.greyish));

            if (orderListArrayList.get(position).getService_status().equals("0") || orderListArrayList.get(position).getService_status().equals("10")) {
                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.order_placed));

            } else if (orderListArrayList.get(position).getService_status().equals("1")) {
                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.booking_accepted));

            } else if (orderListArrayList.get(position).getService_status().equals("2")) {
                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.waiting_delivery));

            } else if (orderListArrayList.get(position).getService_status().equals("3")) {

                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.completed));
            } else if (orderListArrayList.get(position).getService_status().equals("4")) {
                if (PrefConnect.readString(onGoingFragment.getActivity(), Global.SERVICE_TYPE, "").equals("2")) {
                    viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.reject_by_shop));

                } else {

                    viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.reject_by_restarant));
                }
            } else if (orderListArrayList.get(position).getService_status().equals("5")) {

                viewHolder.tv_trip_status.setText(onGoingFragment.getResources().getString(R.string.started_job));
            } else {
                viewHolder.tv_trip_status.setVisibility(View.GONE);
            }


            viewHolder.tv_place_drop.setText(orderListArrayList.get(position).getDelivery_address());
            viewHolder.tv_place.setText(orderListArrayList.get(position).getProvider_admin());

            if (orderListArrayList.get(position).getPayment_type().equals("1")) {
                viewHolder.btn_payment_type.setText("CASH");

            } else {
                viewHolder.btn_payment_type.setText("CARD");
            }


            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd , yyyy");

            try {

                date1 = sdf.format(new SimpleDateFormat("yyyy-M-dd").parse(orderListArrayList.get(position).getCreated_at()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            viewHolder.tv_time.setText(date1);


            if (PrefConnect.readString(onGoingFragment.getActivity(), Global.SERVICE_TYPE, "").equals("3")) {
                try {
                    Picasso.get().load(Global.IMG_URL + orderListArrayList.get(position).getImage()).fit().into(viewHolder.order_image);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {


                try {
                    Picasso.get().load(Global.IMG_URL + orderListArrayList.get(position).getProfile_pic()).fit().into(viewHolder.order_image);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }


        viewHolder.whole_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PrefConnect.readString(onGoingFragment.getActivity(), Global.SERVICE_TYPE, "").equals("4") || PrefConnect.readString(onGoingFragment.getActivity(), Global.SERVICE_TYPE, "").equals("5") || PrefConnect.readString(onGoingFragment.getActivity(), Global.SERVICE_TYPE, "").equals("6") || PrefConnect.readString(onGoingFragment.getActivity(), Global.SERVICE_TYPE, "").equals("7")) {
                    Intent intent = new Intent(onGoingFragment.getActivity(), HistoryActivity.class);
                    intent.putExtra(Global.REQUESTID_TRACKING, orderListArrayList.get(position).getRequest_id());
                    onGoingFragment.startActivity(intent);
                } else {
                    Intent intent = new Intent(onGoingFragment.getActivity(), OrderTrackingActivity.class);
                    intent.putExtra(Global.REQUESTID_TRACKING, orderListArrayList.get(position).getRequest_id());
                    onGoingFragment.startActivity(intent);
                }

            }
        });

        viewHolder.call_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                dialIntent.setData(Uri.parse("tel:" + orderListArrayList.get(position).getAdmin_phone()));
                onGoingFragment.startActivity(dialIntent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return orderListArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout layout_child;
        LinearLayout call_layout;

        AppCompatTextView tv_place;
        AppCompatTextView tv_status;
        AppCompatTextView tv_time;
        AppCompatTextView tv_place_drop;
        AppCompatTextView tv_trip_status;

        AppCompatButton btn_payment_type;

        CircleImageView order_image;

        LinearLayout whole_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            layout_child = itemView.findViewById(R.id.layout_child);
            whole_layout = itemView.findViewById(R.id.whole_layout);
            call_layout = itemView.findViewById(R.id.call_layout);
            tv_place = itemView.findViewById(R.id.tv_place);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_place_drop = itemView.findViewById(R.id.tv_place_drop);
            tv_trip_status = itemView.findViewById(R.id.tv_trip_status);
            order_image = itemView.findViewById(R.id.order_image);
            btn_payment_type = itemView.findViewById(R.id.btn_payment_type);
        }
    }
}
