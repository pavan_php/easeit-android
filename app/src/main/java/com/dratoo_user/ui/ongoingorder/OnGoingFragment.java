package com.dratoo_user.ui.ongoingorder;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.dratoo_user.R;
import com.dratoo_user.app.App;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.data.ProgressUtils;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.common.ConnectivityReceiver;
import com.dratoo_user.ui.ongoingorder.adapter.OngoingAdapter;
import com.dratoo_user.ui.ongoingorder.model.OrderList;
import com.dratoo_user.ui.ongoingorder.model.OrderListResponse;
import com.dratoo_user.ui.ongoingorder.presenter.OnGoingPresenter;
import com.dratoo_user.ui.ongoingorder.presenter.OnGoingView;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class OnGoingFragment extends Fragment implements OnGoingView, ConnectivityReceiver.ConnectivityReceiverListener {
    @Inject
    OnGoingPresenter presenter;
    @BindView(R.id.final_cart_details_layout)
    CardView finalCartDetailsLayout;
    @BindView(R.id.rv_ongoing_list)
    ShimmerRecyclerView rvOngoingList;
    Unbinder unbinder;
    @BindView(R.id.parant_layout)
    LinearLayout parantLayout;
    @BindView(R.id.taxi_layout)
    LinearLayout taxiLayout;
    @BindView(R.id.courier_layout)
    LinearLayout courierLayout;
    @BindView(R.id.carga_layout)
    LinearLayout cargaLayout;
    @BindView(R.id.food_layout)
    LinearLayout foodLayout;
    @BindView(R.id.mart_layout)
    LinearLayout martLayout;
    @BindView(R.id.handy_layout)
    LinearLayout handyLayout;
    @BindView(R.id.dummy_lay)
    LinearLayout dummyLay;
    @BindView(R.id.top_layout)
    LinearLayout topLayout;
    @BindView(R.id.main_layout)
    LinearLayout mainLayout;
    @BindView(R.id.empty_layout)
    LinearLayout emptyLayout;
    private OngoingAdapter adapter;

    OrderListResponse orderListResponse;
    private ArrayList<OrderList> responseListOrderArrayList;

    Activity activity;

    ProgressUtils progressUtils;
    private String typeOfService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View layout = inflater.inflate(R.layout.fragment_on_going, container, false);

        unbinder = ButterKnife.bind(this, layout);

        DaggerAppComponent.create().inject(this);
        presenter.attachView(this);

        activity = getActivity();
        progressUtils = new ProgressUtils(activity);


        if (getArguments() != null) {

            typeOfService = getArguments().getString(Global.GET_VALUE);
        }

        if (typeOfService != null) {
            presenter.getList(typeOfService);
        }


        return layout;
    }

    private void callListApi() {


        if (PrefConnect.readString(activity, Global.SERVICE_TYPE, "") != null) {
            presenter.getList(PrefConnect.readString(activity, Global.SERVICE_TYPE, ""));

        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {

        String message;

        if (isConnected) {
            message = getResources().getString(R.string.connected_to_internet);

        } else {
            message = getResources().getString(R.string.not_connected_to_internet);

        }

        Global.snack_bar(parantLayout, message, Snackbar.LENGTH_LONG, activity);

    }

    @Override
    public void onStart() {
        super.onStart();
        App.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {
        progressUtils.show();

    }

    @Override
    public void hideProgress() {

        progressUtils.dismiss();

    }

    @Override
    public void orderGetDetails(OrderListResponse body) {
        this.orderListResponse = body;


        if (body.getData().getStatus().equalsIgnoreCase("failed")) {
            Global.snack_bar(parantLayout, body.getData().getMessage(), Snackbar.LENGTH_LONG, activity);
            rvOngoingList.setVisibility(View.GONE);
            mainLayout.setVisibility(View.GONE);
            emptyLayout.setVisibility(View.VISIBLE);
        } else {


            rvOngoingList.setVisibility(View.VISIBLE);

            LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvOngoingList.setLayoutManager(layoutManager);
/*
            final LayoutAnimationController controller =
                    AnimationUtils.loadLayoutAnimation(activity, R.anim.layout_fall_down);

            rvOngoingList.setLayoutAnimation(controller);*/

            responseListOrderArrayList = body.getData().getResult();

            if (responseListOrderArrayList.size() > 0) {
                rvOngoingList.setVisibility(View.VISIBLE);
                mainLayout.setVisibility(View.GONE);
                emptyLayout.setVisibility(View.GONE);
                adapter = new OngoingAdapter(this, responseListOrderArrayList);
                rvOngoingList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {

            }


        }

    }

    @OnClick({R.id.taxi_layout, R.id.courier_layout, R.id.carga_layout, R.id.food_layout, R.id.mart_layout, R.id.handy_layout, R.id.dummy_lay, R.id.moto_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.taxi_layout:
                PrefConnect.writeString(activity, Global.SERVICE_TYPE, "4");
                callListApi();
                break;
            case R.id.courier_layout:
                PrefConnect.writeString(activity, Global.SERVICE_TYPE, "5");
                callListApi();
                break;
            case R.id.carga_layout:
                PrefConnect.writeString(activity, Global.SERVICE_TYPE, "6");
                callListApi();
                break;
            case R.id.food_layout:
                PrefConnect.writeString(activity, Global.SERVICE_TYPE, "1");
                callListApi();
                break;
            case R.id.mart_layout:
                PrefConnect.writeString(activity, Global.SERVICE_TYPE, "2");
                callListApi();
                break;
            case R.id.handy_layout:
                PrefConnect.writeString(activity, Global.SERVICE_TYPE, "3");
                callListApi();
                break;
            case R.id.dummy_lay:
                break;
            case R.id.moto_layout:
                PrefConnect.writeString(activity, Global.SERVICE_TYPE, "7");
                callListApi();
                break;
        }
    }
}
