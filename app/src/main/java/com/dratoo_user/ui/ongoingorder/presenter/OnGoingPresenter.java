package com.dratoo_user.ui.ongoingorder.presenter;

import com.dratoo_user.base.Presenter;

public interface OnGoingPresenter extends Presenter<OnGoingView> {

    void getList(String requestId);

}
