package com.dratoo_user.ui.ongoingorder.model;

public class OrderListResponse {

    public OrderTrackingResponseListDetails getData() {
        return data;
    }

    public void setData(OrderTrackingResponseListDetails data) {
        this.data = data;
    }

    public OrderTrackingResponseListDetails data;

}
