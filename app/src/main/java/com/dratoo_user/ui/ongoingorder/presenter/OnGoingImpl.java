package com.dratoo_user.ui.ongoingorder.presenter;

import android.util.Log;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.ongoingorder.model.OrderListResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class OnGoingImpl extends BasePresenter<OnGoingView> implements OnGoingPresenter {

    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;

    @Override
    public void attachView(OnGoingView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void getList(String requestId) {
//        getView().showProgress();
        Call<OrderListResponse> call = apiInterface.getStatusTracking(requestId);
        call.enqueue(new Callback<OrderListResponse>() {
            @Override
            public void onResponse(Call<OrderListResponse> call, Response<OrderListResponse> response) {
                if (response.isSuccessful()) {
//                    getView().hideProgress();
                    getView().orderGetDetails(response.body());
                }
            }

            @Override
            public void onFailure(Call<OrderListResponse> call, Throwable t) {
//                getView().hideProgress();
                Log.e(TAG, "onFailure: Handy " + t.toString());
            }
        });

    }
}
