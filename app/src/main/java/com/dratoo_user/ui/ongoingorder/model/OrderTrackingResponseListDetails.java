package com.dratoo_user.ui.ongoingorder.model;

import java.util.ArrayList;

public class OrderTrackingResponseListDetails {

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String status;
    public String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<OrderList> getResult() {
        return result;
    }

    public void setResult(ArrayList<OrderList> result) {
        this.result = result;
    }

    public ArrayList<OrderList> result;

}
