package com.dratoo_user.ui.confirmationHandyMan.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.handymanproviderdetails.model.HandyManTimingResponse;

public interface ConfirmationHandyManView extends BaseView {

    void getSucessHandyManFare();

    void sucessSendTimings(HandyManTimingResponse body);


    void paynowSucess();

}
