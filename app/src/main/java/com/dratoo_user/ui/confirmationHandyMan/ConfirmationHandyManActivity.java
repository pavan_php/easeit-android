package com.dratoo_user.ui.confirmationHandyMan;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.Constants;
import com.midtrans.sdk.corekit.core.LocalDataHandler;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme;
import com.midtrans.sdk.corekit.models.BankType;
import com.midtrans.sdk.corekit.models.TransactionResponse;
import com.midtrans.sdk.corekit.models.UserAddress;
import com.midtrans.sdk.corekit.models.UserDetail;
import com.midtrans.sdk.corekit.models.snap.CreditCard;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;
import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentSucessRequest;
import com.dratoo_user.ui.choosetimedateslot.model.SendTimeDetailsRequest;
import com.dratoo_user.ui.common.ProgressDialog;
import com.dratoo_user.ui.confirmationHandyMan.model.HandyManFareRequest;
import com.dratoo_user.ui.confirmationHandyMan.presenter.ConfirmationHandyManPresenter;
import com.dratoo_user.ui.confirmationHandyMan.presenter.ConfirmationHandyManView;
import com.dratoo_user.ui.handymanproviderdetails.model.HandyManTimingResponse;
import com.dratoo_user.ui.ordertracking.OrderTrackingActivity;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ConfirmationHandyManActivity extends AppCompatActivity implements ConfirmationHandyManView, TransactionFinishedCallback {


    @Inject
    ConfirmationHandyManPresenter presenter;

    ProgressDialog progressDialog;
    @BindView(R.id.back)
    AppCompatImageView back;
    @BindView(R.id.tv_title)
    AppCompatTextView tvTitle;
    @BindView(R.id.top_layout)
    LinearLayout topLayout;
    @BindView(R.id.left_layout)
    LinearLayout leftLayout;
    @BindView(R.id.tv_timing)
    AppCompatTextView tvTiming;
    @BindView(R.id.tv_date)
    AppCompatTextView tvDate;
    @BindView(R.id.right_layout)
    LinearLayout rightLayout;
    @BindView(R.id.left_layout_location)
    LinearLayout leftLayoutLocation;
    @BindView(R.id.tv_location)
    AppCompatTextView tvLocation;
    @BindView(R.id.right_layout_location)
    LinearLayout rightLayoutLocation;
    @BindView(R.id.img_provider)
    CircleImageView imgProvider;
    @BindView(R.id.tv_provider_name)
    AppCompatTextView tv_provider_name;
    @BindView(R.id.tv_distance)
    AppCompatTextView tvDistance;
    @BindView(R.id.tv_rating)
    AppCompatTextView tvRating;
    @BindView(R.id.other_layout)
    LinearLayout otherLayout;
    @BindView(R.id.technician_layout)
    LinearLayout technicianLayout;
    @BindView(R.id.tv_booking_amt)
    AppCompatTextView tvBookingAmt;
    @BindView(R.id.tv_professional_fee)
    AppCompatTextView tvProfessionalFee;
    @BindView(R.id.tv_total_payment)
    AppCompatTextView tvTotalPayment;
    @BindView(R.id.btn_confirm)
    AppCompatButton btnConfirm;
    @BindView(R.id.bottom_card_view)
    CardView bottomCardView;
    @BindView(R.id.iv_cash)
    AppCompatImageView ivCash;
    @BindView(R.id.tv_payment_type)
    AppCompatTextView tvPaymentType;
    @BindView(R.id.tv_total_amt)
    AppCompatTextView tvTotalAmt;
    @BindView(R.id.tv_vertical_dot)
    AppCompatTextView tvVerticalDot;
    @BindView(R.id.tv_pay)
    AppCompatTextView tvPay;
    @BindView(R.id.select_payment_type)
    LinearLayout selectPaymentType;
    @BindView(R.id.parant_layout)
    RelativeLayout parantLayout;
    private Dialog dialog;
    private String id;
    private HandyManTimingResponse body;
    private String request_id;
    private String date;
    private BottomSheetDialog mBottomSheetDialog;
    private View bottomSheet;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation_handy_man);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        presenter.attachView(this);

        progressDialog = new ProgressDialog(this);


        if (getIntent() != null) {
            Intent intent = getIntent();

            id = intent.getStringExtra(Global.SEND_TIMING);
        }

        if (id != null) {

            showFareDetails(id);
        }

        // at defaulut payment type is 1 cash


        PrefConnect.writeString(ConfirmationHandyManActivity.this, Global.PAYMENT_TYPE, "1");

        //initialize payment
        initMidtransSdk();


    }


    private void initMidtransSdk() {


        SdkUIFlowBuilder.init()
                .setClientKey(Global.client_key) // client_key is mandatory
                .setContext(this) // context is mandatory
                .setTransactionFinishedCallback(this) // set transaction finish callback (sdk callback)
                .setMerchantBaseUrl(Global.base_url_payment) //set merchant url
                .enableLog(true) // enable sdk log
                .setColorTheme(new CustomColorTheme("#eea500", "#e5e6ea", "#4c64b5")) // will replace theme on snap theme on MAP
                .buildSDK();
    }


    private TransactionRequest initTransactionRequest() {
        // Create new Transaction Request
        TransactionRequest transactionRequestNew = new
                TransactionRequest(System.currentTimeMillis() + "", Double.parseDouble(PrefConnect.readString(this, Global.FINAL_AMT, "")));

        Log.e("Nive ", "initTransactionRequest:FinalAmt " + Double.parseDouble(PrefConnect.readString(this, Global.FINAL_AMT, "")));
        Log.e("Nive ", "initTransactionRequest:FinalAmt " + PrefConnect.readString(this, Global.EMAIL, ""));

        // Set user details
        UserDetail userDetail = new UserDetail();
        userDetail.setUserFullName(PrefConnect.readString(ConfirmationHandyManActivity.this, Global.NAME, ""));
        userDetail.setEmail(PrefConnect.readString(ConfirmationHandyManActivity.this, Global.EMAIL, ""));
        userDetail.setPhoneNumber(PrefConnect.readString(ConfirmationHandyManActivity.this, Global.MOBILE_NBR, ""));
        userDetail.setUserId(PrefConnect.readString(this, Global.AUTH_ID, ""));

        ArrayList<UserAddress> userAddresses = new ArrayList<>();
        UserAddress userAddress = new UserAddress();
        userAddress.setAddress("indonesia");
        userAddress.setCity("jakarta");
        userAddress.setCountry("IDN");
        userAddress.setZipcode("86554");
        userAddress.setAddressType(Constants.ADDRESS_TYPE_BOTH);
        userAddresses.add(userAddress);

        userDetail.setUserAddresses(userAddresses);
        LocalDataHandler.saveObject("user_details", userDetail);

        Log.e("Nive ", "initTransactionRequest:userDetail " + new Gson().toJson(userDetail));


        // Create creditcard options for payment
        CreditCard creditCard = new CreditCard();
        creditCard.setSaveCard(false); // when using one/two click set to true and if normal set to false
        creditCard.setAuthentication(CreditCard.AUTHENTICATION_TYPE_3DS);
        creditCard.setBank(BankType.BCA); //set spesific acquiring bank
        transactionRequestNew.setCreditCard(creditCard);

        return transactionRequestNew;
    }


    private void showFareDetails(String id) {
        SendTimeDetailsRequest sendTimeDetailsRequest = new SendTimeDetailsRequest();
        sendTimeDetailsRequest.setProvider_id(id);
        sendTimeDetailsRequest.setCategory_id(PrefConnect.readString(this, Global.CATEGORY_ID, ""));
        sendTimeDetailsRequest.setService_id(PrefConnect.readString(this, Global.FOOD_STYLE_ID, ""));
        sendTimeDetailsRequest.setStart_date(PrefConnect.readString(this, Global.START_DAY, ""));
        sendTimeDetailsRequest.setWorking_hour(PrefConnect.readString(this, Global.TIMING, ""));
        presenter.sendTimings(sendTimeDetailsRequest);

    }


    private void callHandyManFare() {

        HandyManFareRequest handyManFareRequest = new HandyManFareRequest();
        handyManFareRequest.setDelivery_location(PrefConnect.readString(ConfirmationHandyManActivity.this, Global.CURRENT_ADDRESS, ""));
        handyManFareRequest.setRequest_id(request_id);
        handyManFareRequest.setPayment_type(PrefConnect.readString(this, Global.PAYMENT_TYPE, ""));
        presenter.calculateFare(handyManFareRequest);

    }

    @Override
    public void showProgress() {
        progressDialog.show();

    }

    @Override
    public void hideProgress() {
        progressDialog.dismissLoader();

    }


    private void showDialogConfirmation() {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.handyman_confirmation_dialog);
        Window window = dialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);

        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        AppCompatButton btn_ok = dialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ConfirmationHandyManActivity.this, OrderTrackingActivity.class);
                intent.putExtra(Global.REQUESTID_TRACKING, PrefConnect.readString(ConfirmationHandyManActivity.this, Global.REQUESTID, ""));
                startActivity(intent);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);


        dialog.show();


    }

    @Override
    public void getSucessHandyManFare() {

        showDialogConfirmation();


    }

    @Override
    public void sucessSendTimings(HandyManTimingResponse body) {


        this.body = body;

        request_id = body.getRequest_id();

        tvTiming.setText(body.getWorking_hour());

        tvLocation.setText(PrefConnect.readString(ConfirmationHandyManActivity.this, Global.CURRENT_ADDRESS, ""));
        PrefConnect.writeString(ConfirmationHandyManActivity.this, Global.REQUESTID, body.getRequest_id());

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE , MMM dd yyyy ");

        try {

            date = sdf.format(new SimpleDateFormat("yyyy-MM-dd").parse(body.getStart_date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        tvDate.setText(date);


        DecimalFormat precision = new DecimalFormat("0.00");

        double val = Double.parseDouble(body.getData().getRating());

        String conversion = precision.format(val);


        tvRating.setText(conversion);

        Picasso.get().load(Global.IMG_URL + body.getData().getProfile_image()).fit().into(imgProvider);
        tv_provider_name.setText(body.getData().getFname() + " " + body.getData().getLname());
        tvDistance.setText("Experience" + " : " + body.getData().getExperiance() + " " + "Year(s)");

        tvBookingAmt.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getService_fare());
        tvProfessionalFee.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getTax());
        tvTotalPayment.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getBill_amount());


        tvTotalAmt.setText(getResources().getString(R.string.tot_fare) + " : " + PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getBill_amount());


        PrefConnect.writeString(this, Global.FINAL_AMT, body.getBill_amount());

    }

    @Override
    public void paynowSucess() {

        selectPaymentType.setClickable(false);

        Global.snack_bar(parantLayout, "Payment Completed!!!", Snackbar.LENGTH_LONG, ConfirmationHandyManActivity.this);


    }

    @OnClick({R.id.back, R.id.btn_confirm, R.id.select_payment_type})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                /*Intent intent = new Intent(ConfirmationHandyManActivity.this, HandyManProviderDetailsActivity.class);
                startActivity(intent);*/
                finish();
                break;
            case R.id.btn_confirm:
                callHandyManFare();
                break;
            case R.id.select_payment_type:
//                initBottomSheet();
                break;
        }
    }


    public void initBottomSheet() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        bottomSheet = this.getLayoutInflater().inflate(R.layout.bottomsheet_payments, null);
        mBottomSheetDialog.setContentView(bottomSheet);

        AppCompatTextView title = (AppCompatTextView) mBottomSheetDialog.findViewById(R.id.title);
        AppCompatTextView tv_header = (AppCompatTextView) mBottomSheetDialog.findViewById(R.id.tv_header);
        AppCompatImageView img_close = mBottomSheetDialog.findViewById(R.id.img_close);
        LinearLayout halo_pay_layout = mBottomSheetDialog.findViewById(R.id.halo_pay_layout);
        LinearLayout cash_layout = mBottomSheetDialog.findViewById(R.id.cash_layout);

        tv_header.setText(getResources().getString(R.string.select_payment));

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        cash_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefConnect.writeString(ConfirmationHandyManActivity.this, Global.PAYMENT_TYPE, "1");
                mBottomSheetDialog.dismiss();
                tvPaymentType.setText(getResources().getString(R.string.cash));
            }
        });


        halo_pay_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  INFO: TODO

                tvPaymentType.setText(getResources().getString(R.string.card));
                PrefConnect.writeString(ConfirmationHandyManActivity.this, Global.PAYMENT_TYPE, "2");
                mBottomSheetDialog.dismiss();


              /*  // initiate payment

                MidtransSDK.getInstance().setTransactionRequest(initTransactionRequest());
                MidtransSDK.getInstance().startPaymentUiFlow(ConfirmationHandyManActivity.this);
*/
            }
        });


        mBottomSheetDialog.show();

    }

    @Override
    public void onTransactionFinished(TransactionResult result) {

        if (result.getResponse() != null) {
            switch (result.getStatus()) {
                case TransactionResult.STATUS_SUCCESS:

                    Log.e("Nive ", "onTransactionFinished: " + new Gson().toJson(result.getResponse()));
                    callPaymentSucessApi(result.getResponse());
                    break;
                case TransactionResult.STATUS_PENDING:
                    Toast.makeText(this, "Transaction Pending. ID: " + result.getResponse().getTransactionId(), Toast.LENGTH_LONG).show();
                    break;
                case TransactionResult.STATUS_FAILED:
                    Toast.makeText(this, "Transaction Failed. ID: " + result.getResponse().getTransactionId() + ". Message: " + result.getResponse().getStatusMessage(), Toast.LENGTH_LONG).show();
                    break;
            }
            result.getResponse().getValidationMessages();
        } else if (result.isTransactionCanceled()) {
            Toast.makeText(this, "Transaction Canceled", Toast.LENGTH_LONG).show();
        } else {
            if (result.getStatus().equalsIgnoreCase(TransactionResult.STATUS_INVALID)) {
                Toast.makeText(this, "Transaction Invalid", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Transaction Finished with failure.", Toast.LENGTH_LONG).show();
            }
        }


    }


    private void callPaymentSucessApi(TransactionResponse response) {

        PaymentSucessRequest paymentSucessRequest = new PaymentSucessRequest();
        paymentSucessRequest.setApproval_code(response.getApprovalCode());
        paymentSucessRequest.setBank(response.getBank());
        paymentSucessRequest.setCurrency(response.getCurrency());
        paymentSucessRequest.setEci(response.getEci());
        paymentSucessRequest.setFinish_redirect_url(response.getFinishRedirectUrl());
        paymentSucessRequest.setFraud_status(response.getFraudStatus());
        paymentSucessRequest.setPayment_type(response.getPaymentType());
        paymentSucessRequest.setGross_amount(response.getGrossAmount());
        paymentSucessRequest.setOrder_id(response.getOrderId());
        paymentSucessRequest.setMasked_card(response.getMaskedCard());
        paymentSucessRequest.setPoint_redeem_amount(response.getPointBalanceAmount());
        paymentSucessRequest.setPayment_type(response.getPaymentType());
        paymentSucessRequest.setStatus_message(response.getStatusMessage());
        paymentSucessRequest.setTransaction_id(response.getTransactionId());
        paymentSucessRequest.setRequest_id(PrefConnect.readString(ConfirmationHandyManActivity.this, Global.REQUESTID, ""));
        paymentSucessRequest.setPay_type("2");
        paymentSucessRequest.setService_type(PrefConnect.readString(this, Global.SERVICE_TYPE, ""));


        presenter.sendSucessRequest(paymentSucessRequest);
    }
}
