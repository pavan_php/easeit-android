package com.dratoo_user.ui.confirmationHandyMan.presenter;

import com.dratoo_user.base.Presenter;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentSucessRequest;
import com.dratoo_user.ui.choosetimedateslot.model.SendTimeDetailsRequest;
import com.dratoo_user.ui.confirmationHandyMan.model.HandyManFareRequest;

public interface ConfirmationHandyManPresenter extends Presenter<ConfirmationHandyManView> {

    void calculateFare(HandyManFareRequest handyManFareRequest);


    void sendTimings(SendTimeDetailsRequest sendTimeDetailsRequest);

    void sendSucessRequest(PaymentSucessRequest paymentSucessRequest);

}
