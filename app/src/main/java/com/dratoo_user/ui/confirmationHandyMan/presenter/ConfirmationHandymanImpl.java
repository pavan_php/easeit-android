package com.dratoo_user.ui.confirmationHandyMan.presenter;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentScucessResponse;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentSucessRequest;
import com.dratoo_user.ui.choosetimedateslot.model.SendTimeDetailsRequest;
import com.dratoo_user.ui.confirmationHandyMan.model.HandyManFareRequest;
import com.dratoo_user.ui.handymanproviderdetails.model.HandyManTimingResponse;
import com.dratoo_user.ui.lookingdriverbooking.model.CommonResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmationHandymanImpl extends BasePresenter<ConfirmationHandyManView> implements ConfirmationHandyManPresenter {

    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;

    @Override
    public void attachView(ConfirmationHandyManView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }


    @Override
    public void sendSucessRequest(PaymentSucessRequest paymentSucessRequest) {
        Call<PaymentScucessResponse> call = apiInterface.getSucessResultPayment(paymentSucessRequest);
        call.enqueue(new Callback<PaymentScucessResponse>() {
            @Override
            public void onResponse(Call<PaymentScucessResponse> call, Response<PaymentScucessResponse> response) {
                if (response.isSuccessful()) {
                    getView().paynowSucess();
                }
            }

            @Override
            public void onFailure(Call<PaymentScucessResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void calculateFare(HandyManFareRequest handyManFareRequest) {

        getView().showProgress();

        Call<CommonResponse> call = apiInterface.getHandyManFareDetails(handyManFareRequest);

        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().getSucessHandyManFare();
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                getView().hideProgress();
            }
        });


    }


    @Override
    public void sendTimings(SendTimeDetailsRequest sendTimeDetailsRequest) {
        getView().showProgress();
        Call<HandyManTimingResponse> call = apiInterface.sendTimingDetails(sendTimeDetailsRequest);

        call.enqueue(new Callback<HandyManTimingResponse>() {
            @Override
            public void onResponse(Call<HandyManTimingResponse> call, Response<HandyManTimingResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().sucessSendTimings(response.body());
                }
            }

            @Override
            public void onFailure(Call<HandyManTimingResponse> call, Throwable t) {

                getView().hideProgress();


            }
        });
    }
}
