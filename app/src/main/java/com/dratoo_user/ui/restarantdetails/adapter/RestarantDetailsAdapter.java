package com.dratoo_user.ui.restarantdetails.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.ui.restarantdetails.RestarantDetailsActivity;
import com.dratoo_user.ui.restarantdetails.model.FoodDetails;

import java.util.ArrayList;

public class RestarantDetailsAdapter extends RecyclerView.Adapter<RestarantDetailsAdapter.ViewHolder> {

    private int headerViewType = 0, foodType = 1;

    public ArrayList<FoodDetails> foodDetailsArrayList = new ArrayList<>();

    public ArrayList<FoodDetails> food;

    RestarantDetailsActivity restarantDetailsActivity;

    public boolean checkCount = false;


    public RestarantDetailsAdapter(ArrayList<FoodDetails> food, RestarantDetailsActivity restarantDetailsActivity) {


        Log.e("Nive ", "RestarantDetailsAdapter: " + food.size());

        this.food = foodDetailsArrayList;
        this.restarantDetailsActivity = restarantDetailsActivity;

        for (int i = 0; i < food.size(); i++) {

            FoodDetails foodDetails = new FoodDetails();
            foodDetails.setCategory_id(food.get(i).getCategory_id());
            foodDetails.setCategory(food.get(i).getCategory());
            foodDetails.setImage(food.get(i).getImage());
            foodDetails.setViewType(food.get(i).getViewType());
            foodDetails.setProvider_admin_id(food.get(i).getProvider_admin_id());


            foodDetailsArrayList.add(foodDetails);

            for (int j = 0; j < food.get(i).getProduct().size(); j++) {
                FoodDetails foodItemProductDetailsList = new FoodDetails();

                foodItemProductDetailsList.setItem_id(food.get(i).getProduct().get(j).getItem_id());
                foodItemProductDetailsList.setItem_name(food.get(i).getProduct().get(j).getItem_name());
                foodItemProductDetailsList.setImage(food.get(i).getProduct().get(j).getImage());
                foodItemProductDetailsList.setPrice(food.get(i).getProduct().get(j).getPrice());
                foodItemProductDetailsList.setViewType(foodType);
                foodItemProductDetailsList.setQuantity(food.get(i).getProduct().get(j).getQuantity());
                foodItemProductDetailsList.setDescription(food.get(i).getProduct().get(j).getDescription());
                foodItemProductDetailsList.setTax(food.get(i).getProduct().get(j).getTax());
                foodDetailsArrayList.add(foodItemProductDetailsList);

            }


        }


    }

    @NonNull
    @Override
    public RestarantDetailsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {


        Log.e("Nive ", "onCreateViewHolder: " + viewType);

        if (viewType == headerViewType) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.header_title_items, viewGroup, false);
            return new ViewHolder(view);
        } else if (viewType == foodType) {

            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.restarant_individual_content_items, viewGroup, false);
            return new ViewHolder(view);
        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");


    }


    @Override
    public void onBindViewHolder(@NonNull final RestarantDetailsAdapter.ViewHolder viewHolder, final int position) {


        final FoodDetails mlist = foodDetailsArrayList.get(position);

        if (mlist.getViewType() == headerViewType) {

            viewHolder.header.setText(mlist.getCategory());

        } else if (mlist.getViewType() == foodType) {


            if (mlist.getQuantity().equals("0")) {
                viewHolder.add_card_view_items.setVisibility(View.GONE);
                viewHolder.btn_add.setVisibility(View.VISIBLE);

            } else {
                viewHolder.tv_cart_count.setText(mlist.getQuantity());
                viewHolder.add_card_view_items.setVisibility(View.VISIBLE);
                viewHolder.btn_add.setVisibility(View.GONE);

            }


            try {
                Picasso.get().load(Global.IMG_URL + mlist.getImage()).placeholder(restarantDetailsActivity.getResources().getDrawable(R.drawable.place_holder)).fit().into(viewHolder.img_food);
            } catch (Exception e) {
                e.printStackTrace();
            }


            viewHolder.tv_restarant_name.setText(mlist.getItem_name());

            viewHolder.tv_description.setText(mlist.getDescription());

            viewHolder.tv_price.setText(PrefConnect.readString(restarantDetailsActivity, Global.CURRENCY, "")+" "+mlist.getPrice());

            viewHolder.btn_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    viewHolder.btn_add.setVisibility(View.GONE);

                    viewHolder.add_card_view_items.setVisibility(View.VISIBLE);

                    restarantDetailsActivity.getFoodId(mlist.getItem_name(), mlist.getItem_id(), mlist.getPrice(), mlist.getImage(), mlist.getTax(), mlist.getProvider_admin_id());

                }
            });


            viewHolder.iv_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = Integer.valueOf(viewHolder.tv_cart_count.getText().toString());


                    if (count > 0) {
                        checkCount = true;
                        count--;
                        // holder.txtCartCount.setText(String.valueOf(count));
                        PrefConnect.writeString(restarantDetailsActivity, Global.QUANTITY, String.valueOf(count));

                        PrefConnect.writeString(restarantDetailsActivity, Global.COUNT, viewHolder.tv_cart_count.getText().toString());

                        restarantDetailsActivity.getItemID(mlist.getItem_id(), viewHolder.tv_cart_count.getText().toString(), 2, viewHolder.getAdapterPosition());

                    } else {

                    }
                }
            });


            viewHolder.iv_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.e("Nive ", "onClick:Plus ");

                    int count = Integer.valueOf(viewHolder.tv_cart_count.getText().toString());

                    if (count >= 0) {
                        checkCount = true;
                        count++;
                        PrefConnect.writeString(restarantDetailsActivity, Global.QUANTITY, String.valueOf(count));

                        PrefConnect.writeString(restarantDetailsActivity, Global.COUNT, viewHolder.tv_cart_count.getText().toString());


                        restarantDetailsActivity.getItemID(mlist.getItem_id(), viewHolder.tv_cart_count.getText().toString(), 1, viewHolder.getAdapterPosition());


                    }


                }
            });


        }


    }


    @Override
    public int getItemViewType(int position) {

        return foodDetailsArrayList.get(position).getViewType();
    }

    @Override
    public int getItemCount() {

        return foodDetailsArrayList.size();


    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        @Nullable
        AppCompatImageView img_food;

        @Nullable
        AppCompatTextView tv_restarant_name;

        @Nullable
        AppCompatTextView tv_description;

        @Nullable
        AppCompatTextView header;


        @Nullable
        AppCompatTextView tv_price;

        @Nullable
        AppCompatTextView tv_cart_count;

        @Nullable
        AppCompatButton btn_add;

        @Nullable
        AppCompatImageView iv_edit_cart;

        @Nullable
        AppCompatImageView iv_minus;

        @Nullable
        AppCompatImageView iv_plus;

        @Nullable
        CardView add_card_view_items;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_food = itemView.findViewById(R.id.img_food);
            tv_restarant_name = itemView.findViewById(R.id.tv_restarant_name);
            tv_description = itemView.findViewById(R.id.tv_description);
            tv_price = itemView.findViewById(R.id.tv_price);
            btn_add = itemView.findViewById(R.id.btn_add);
            iv_edit_cart = itemView.findViewById(R.id.iv_edit_cart);
            add_card_view_items = itemView.findViewById(R.id.add_card_view_items);
            iv_minus = itemView.findViewById(R.id.iv_minus);
            iv_plus = itemView.findViewById(R.id.iv_plus);
            tv_cart_count = itemView.findViewById(R.id.tv_cart_count);
            header = itemView.findViewById(R.id.header);
        }
    }
}
