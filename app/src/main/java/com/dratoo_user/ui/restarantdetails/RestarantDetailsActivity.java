package com.dratoo_user.ui.restarantdetails;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.app.App;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.common.ConnectivityReceiver;
import com.dratoo_user.ui.common.ProgressDialog;
import com.dratoo_user.ui.confirmationfood.ConfirmationDetailsActivity;
import com.dratoo_user.ui.confirmationfood.model.AddMoreItemsRequest;
import com.dratoo_user.ui.restarantdetails.adapter.RestarantDetailsAdapter;
import com.dratoo_user.ui.restarantdetails.model.AddCartArrRequst;
import com.dratoo_user.ui.restarantdetails.model.AddCartRequest;
import com.dratoo_user.ui.restarantdetails.model.AddCartResponse;
import com.dratoo_user.ui.restarantdetails.model.FoodDetails;
import com.dratoo_user.ui.restarantdetails.model.FoodItemProductDetails;
import com.dratoo_user.ui.restarantdetails.model.FoodItemResponse;
import com.dratoo_user.ui.restarantdetails.model.FoodModelRequest;
import com.dratoo_user.ui.restarantdetails.model.IncrementDecrementCartRequest;
import com.dratoo_user.ui.restarantdetails.model.ModelRequest;
import com.dratoo_user.ui.restarantdetails.presenter.RestarantDetailsPresenter;
import com.dratoo_user.ui.restarantdetails.presenter.RestarantDetailsView;
import com.dratoo_user.ui.restarnt.RestarantActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RestarantDetailsActivity extends AppCompatActivity implements RestarantDetailsView, ConnectivityReceiver.ConnectivityReceiverListener {


    @Inject
    RestarantDetailsPresenter presenter;

    @BindView(R.id.iv_restarant)
    AppCompatImageView ivRestarant;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.tv_restarant_name)
    AppCompatTextView tvRestarantName;
    @BindView(R.id.tv_km)
    AppCompatTextView tvKm;
    @BindView(R.id.iv_timer)
    AppCompatImageView ivTimer;
    @BindView(R.id.tv_open_time)
    AppCompatTextView tvOpenTime;
    @BindView(R.id.rv_cart_item)
    RecyclerView rvCartItem;
    @BindView(R.id.final_cart_details_layout)
    CardView finalCartDetailsLayout;
    @BindView(R.id.tv_total_items)
    AppCompatTextView tvTotalItems;
    @BindView(R.id.parant_layout)
    RelativeLayout parantLayout;
    @BindView(R.id.map_layout)
    CardView mapLayout;
    @BindView(R.id.tv_amt)
    AppCompatTextView tvAmt;
    @BindView(R.id.tv_restarant_name_card)
    AppCompatTextView tvRestarantNameCard;
    @BindView(R.id.tv_no_items)
    AppCompatTextView tvNoItems;
    private MenuItem item;

    String restarantId;
    private ArrayList<FoodDetails> cartItemList = new ArrayList<>();
    private RestarantDetailsAdapter cartItemListAdapter;

    private Animation animShow;


    private AddCartRequest addCartRequest;
    ArrayList<AddCartArrRequst> addCartArrRequstArrayList = new ArrayList<>();

    ArrayList<FoodItemProductDetails> foodItemProductDetailsList = new ArrayList<>();
    private AddCartResponse addCartResponse;


    AddCartArrRequst addCartArrRequst;

    ArrayList<ModelRequest> modelRequestsList = new ArrayList<>();

    FoodModelRequest foodModelRequest;

    ProgressDialog progressDialog;

    FoodItemResponse foodItemResponse;
    private boolean addOrSubract = false;

    ModelRequest modelRequest;

    int val = 0;

    String timing, distance;

    String lat, lang;
    private Marker mCurrLocationMarker;
    private Marker mRestarantLocMarker;
    private BottomSheetDialog mBottomSheetDialog;
    private View bottomSheet;
    private String addItems;
    private boolean enterAdd = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restarant_details);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        presenter.attachView(this);
        setUpToolbar();

        progressDialog = new ProgressDialog(this);

        if (getIntent() != null) {
            Intent intent = getIntent();

            timing = intent.getStringExtra(Global.OPENING_CLOSING_TIME);
            distance = intent.getStringExtra(Global.OPENING_CLOSING_TIME_TWO);
            lat = intent.getStringExtra(Global.LAT);
            lang = intent.getStringExtra(Global.LANG);

            addItems = intent.getStringExtra(Global.ADDITEMS);

            Log.e("Nive ", "onCreate: " + lat);
            Log.e("Nive ", "AddItems: " + addItems);
        }


        if (addItems != null) {


            AddMoreItemsRequest addMoreItemsRequest = new AddMoreItemsRequest();

            addMoreItemsRequest.setProvider_admin_id(PrefConnect.readString(RestarantDetailsActivity.this, Global.PROVIDERID, ""));
            addMoreItemsRequest.setService_type(PrefConnect.readString(RestarantDetailsActivity.this, Global.SERVICE_TYPE, ""));

            if (addItems.equals("1")) {
                presenter.getAddMoreItemsFood(addMoreItemsRequest);
            } else {

                presenter.getAddMoreItemsMart(addMoreItemsRequest);
            }


        } else {
            if (PrefConnect.readString(this, Global.SERVICE_TYPE, "") != null) {


                //service type 1 --> food

                if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("1")) {
                    if (PrefConnect.readString(this, Global.RESTARANT_ID, "") != null) {
                        presenter.getRestarantItems(PrefConnect.readString(this, Global.RESTARANT_ID, ""));

                    }
                } else {

                    // 2 --> mart

                    if (PrefConnect.readString(this, Global.RESTARANT_ID, "") != null) {
                        presenter.getDrinksMartList(PrefConnect.readString(this, Global.RESTARANT_ID, ""));

                    }
                }
            }

        }


        if (timing != null || distance != null) {
            tvOpenTime.setText("Until" + " " + timing + " " + "today");
            tvKm.setText(distance);
        }

        animShow = AnimationUtils.loadAnimation(this, R.anim.slow_animation);


        tvRestarantName.setText(PrefConnect.readString(this, Global.RESTARANT_NAME, ""));
        tvRestarantNameCard.setText(PrefConnect.readString(this, Global.RESTARANT_NAME, ""));

        try {
            Picasso.get().load(Global.IMG_URL + PrefConnect.readString(this, Global.RESTARANT_PIC, "")).fit().into(ivRestarant);
        } catch (Exception e) {
            e.printStackTrace();
        }


        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                    // If collapsed, then do this


                    mapLayout.setVisibility(View.GONE);
                    Log.e("Nive ", "onOffsetChanged: Collapse");


                } else if (verticalOffset == 0) {
                    // If expanded, then do this
                    mapLayout.setVisibility(View.VISIBLE);

                    Log.e("Nive ", "onOffsetChanged: Expand");


                } else {
                    // Somewhere in between
                    // Do according to your requirement
                }
            }
        });


    }

    public void showMapDialog() {

        final Dialog mapDialog = new Dialog(this, R.style.SlideTheme);
        mapDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mapDialog.setCancelable(false);
        mapDialog.setContentView(R.layout.restarant_map_dialog);
        Window window = mapDialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        Toolbar toolbar = mapDialog.findViewById(R.id.toolbar);
        final LinearLayout bottom_layout = mapDialog.findViewById(R.id.bottom_layout);

        AppCompatTextView tv_name = mapDialog.findViewById(R.id.tv_name);
        AppCompatTextView tv_address = mapDialog.findViewById(R.id.tv_address);
        AppCompatImageView iv_yellow_call = mapDialog.findViewById(R.id.iv_yellow_call);

        MapView mapFragment = mapDialog.findViewById(R.id.map_dialog);
        MapsInitializer.initialize(this);

        mapFragment.onCreate(mapDialog.onSaveInstanceState());
        mapFragment.onResume();// needed to get the map to display immediately

        tv_name.setText(PrefConnect.readString(this, Global.RESTARANT_NAME, ""));
        tv_address.setText(PrefConnect.readString(this, Global.RESTARANT_ADDRESS, ""));

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                LatLng pickupLatLang = new LatLng(Double.parseDouble(PrefConnect.readString(RestarantDetailsActivity.this, Global.CURRENT_LAT, "")), Double.parseDouble(PrefConnect.readString(RestarantDetailsActivity.this, Global.CURRENT_LANG, "")));


                if (mCurrLocationMarker != null) {

                    mCurrLocationMarker.remove();
                }

                mCurrLocationMarker = googleMap.addMarker(new MarkerOptions().position(pickupLatLang)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pick_pin))
                        .draggable(false));

                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pickupLatLang,
                        14));

                if (mRestarantLocMarker != null) {
                    mRestarantLocMarker.remove();
                }

                LatLng latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lang));

                mRestarantLocMarker = googleMap.addMarker(new MarkerOptions().position(latLng)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.drop_location))
                        .draggable(false));

                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,
                        14));


            }
        });


        iv_yellow_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                dialIntent.setData(Uri.parse("tel:" + PrefConnect.readString(RestarantDetailsActivity.this, Global.RESTARANT_MOBILE, "")));
                startActivity(dialIntent);
            }
        });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                bottom_layout.startAnimation(animShow);
                bottom_layout.setVisibility(View.VISIBLE);

            }
        }, 1000);


        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.map_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapDialog.dismiss();


            }
        });


        mapDialog.show();


    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {

        String message;

        if (isConnected) {
            message = getResources().getString(R.string.connected_to_internet);

        } else {
            message = getResources().getString(R.string.not_connected_to_internet);

        }

        Global.snack_bar(parantLayout, message, Snackbar.LENGTH_LONG, RestarantDetailsActivity.this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        App.getInstance().setConnectivityListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.trans_search, menu);
        item = menu.findItem(R.id.search);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.current_location_icon:
                break;
            case R.id.action_home:
                supportFinishAfterTransition();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.transparant_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                supportFinishAfterTransition();
                Intent intent = new Intent(RestarantDetailsActivity.this, RestarantActivity.class);
                startActivity(intent);

            }
        });
    }


    @Override
    public void getProductList(FoodItemResponse body) {


        if (body.getData() != null) {
            this.foodItemResponse = body;
            PrefConnect.writeString(RestarantDetailsActivity.this, Global.PROVIDERID, body.getProvider_admin_id());


            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvCartItem.setLayoutManager(layoutManager);
            cartItemList = body.getData();

            Log.e("Nive ", "getProductList: " + cartItemList.size());

            if (cartItemList.size() > 0) {

                tvNoItems.setVisibility(View.GONE);
                rvCartItem.setVisibility(View.VISIBLE);

                if (cartItemList.get(0).getProduct().size() == 0) {

                } else {

                    cartItemListAdapter = new RestarantDetailsAdapter(cartItemList, this);
                    rvCartItem.setAdapter(cartItemListAdapter);
                    cartItemListAdapter.notifyDataSetChanged();
                }
            } else {
                tvNoItems.setVisibility(View.VISIBLE);
                rvCartItem.setVisibility(View.GONE);
            }


        }


    }

    @Override
    public void incrementSucess(FoodItemResponse body) {
        if (body != null) {
            this.foodItemResponse = body;
            Log.e("Nive ", "onViewClicked:Increment " + body.getData());
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            cartItemList = body.getData();

            tvTotalItems.setText(body.getTotal_qty() + " " + getResources().getString(R.string.items));
            tvAmt.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getTotal_amt());


            if (body.getTotal_qty().equals("0")) {

                finalCartDetailsLayout.setVisibility(View.GONE);
            } else {
                finalCartDetailsLayout.setVisibility(View.VISIBLE);

            }

            rvCartItem.setLayoutManager(layoutManager);
            cartItemListAdapter = new RestarantDetailsAdapter(cartItemList, this);
            rvCartItem.setAdapter(cartItemListAdapter);

            cartItemListAdapter.notifyDataSetChanged();


        }
    }

    @Override
    public void decrementSucess(FoodItemResponse body) {

        if (body != null) {

            Log.e("Nive ", "onViewClicked:Decrement " + body.getData());

            this.foodItemResponse = body;
            tvTotalItems.setText(body.getTotal_qty() + " " + "Items");
            tvAmt.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getTotal_amt());

            if (body.getTotal_qty().equals("0")) {

                finalCartDetailsLayout.setVisibility(View.GONE);
            } else {
                finalCartDetailsLayout.setVisibility(View.VISIBLE);

            }

            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
           /* final LayoutAnimationController controller =
                    AnimationUtils.loadLayoutAnimation(this, R.anim.layout_fall_down);

            rvCartItem.setLayoutAnimation(controller);
*/
            cartItemList = body.getData();
            rvCartItem.setLayoutManager(layoutManager);
            cartItemListAdapter = new RestarantDetailsAdapter(cartItemList, this);
            rvCartItem.setAdapter(cartItemListAdapter);

            cartItemListAdapter.notifyDataSetChanged();

        }

    }

    @Override
    public void sucessAddcart(FoodItemResponse body) {
        this.foodItemResponse = body;
        tvTotalItems.setText(body.getTotal_qty() + " " + "Items");
        tvAmt.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getTotal_amt());


    }

    public void getItemID(String item_id, String quantity, int type, int position) {


        addOrSubract = true;

        IncrementDecrementCartRequest incrementDecrementCartRequest = new IncrementDecrementCartRequest();

        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("1")) {

            incrementDecrementCartRequest.setService_type("1");
        } else {
            incrementDecrementCartRequest.setService_type("2");


        }
        incrementDecrementCartRequest.setItem_id(item_id);
        incrementDecrementCartRequest.setProvider_admin_id(PrefConnect.readString(RestarantDetailsActivity.this, Global.PROVIDERID, ""));


        if (type == 1) {


            presenter.incrementDetails(incrementDecrementCartRequest, PrefConnect.readString(this, Global.SERVICE_TYPE, ""));

        } else {

            presenter.decrementDetails(incrementDecrementCartRequest, PrefConnect.readString(this, Global.SERVICE_TYPE, ""));

        }


    }

    public void getFoodId(String item_name, String item_id, String price, String image, String tax, String provider_id) {

        Log.e("Nive", "getFoodId: item_id" + item_id);
        Log.e("Nive", "getFoodId: addCartArrRequstArrayList" + addCartArrRequstArrayList.size());


        enterAdd = true;

        addCartArrRequstArrayList.clear();

        addCartArrRequst = new AddCartArrRequst();
        addCartArrRequst.setItem_id(item_id);
        addCartArrRequstArrayList.add(addCartArrRequst);
        addCartRequest = new AddCartRequest();
        addCartRequest.setData(addCartArrRequstArrayList);

        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("1")) {

            addCartRequest.setService_type("1");
        } else {
            addCartRequest.setService_type("2");

        }

        addCartRequest.setProvider_admin_id(PrefConnect.readString(this, Global.PROVIDERID, ""));


        presenter.addCart(addCartRequest, PrefConnect.readString(this, Global.SERVICE_TYPE, ""));

        finalCartDetailsLayout.startAnimation(animShow);
        finalCartDetailsLayout.setVisibility(View.VISIBLE);
        tvTotalItems.setText(addCartArrRequstArrayList.size() + " " + "Items");


    }


    @Override
    public void showProgress() {

        progressDialog.show();

    }

    @Override
    public void hideProgress() {

        progressDialog.dismissLoader();

    }


    @OnClick({R.id.map_layout, R.id.final_cart_details_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.map_layout:
                showMapDialog();

                break;
            case R.id.final_cart_details_layout:

                checkDetails();

//                actionsOnAddItems();
                break;
        }
    }

    private void checkDetails() {
        if (foodItemResponse.getData() != null) {

            for (int i = 0; i < foodItemResponse.getData().size(); i++) {


                for (int j = 0; j < foodItemResponse.getData().get(i).getProduct().size(); j++) {

                    ModelRequest modelRequest = new ModelRequest();

                    if (foodItemResponse.getData().get(i).getProduct().get(j).getQuantity().equals("0")) {


                        Log.e("Nive ", "onViewClicked:Response " + foodItemResponse.getData().get(i).getProduct().get(j).getQuantity().equals("0"));

                    } else {
                        modelRequest.setQuantity(foodItemResponse.getData().get(i).getProduct().get(j).getQuantity());
                        modelRequest.setItem_id(foodItemResponse.getData().get(i).getProduct().get(j).getItem_id());
                        modelRequestsList.add(modelRequest);

                    }


                }


                foodModelRequest = new FoodModelRequest();
                foodModelRequest.setData(modelRequestsList);
                foodModelRequest.setProvider_admin_id(PrefConnect.readString(RestarantDetailsActivity.this, Global.PROVIDERID, ""));

            }

        }


        String count = tvTotalItems.getText().toString();

        String arr[] = count.split(" ");

        int totalItems = 0;
        if (arr[0] != null) {

            totalItems = Integer.parseInt(arr[0]);
        }


        Log.e("Nive ", "checkDetails:totalItems " + totalItems);

        if (totalItems >= 1) {

            Intent intent = new Intent(RestarantDetailsActivity.this, ConfirmationDetailsActivity.class);
            intent.putExtra(Global.SENDMODEL, foodModelRequest);
            startActivity(intent);
        } else {
            Global.snack_bar(parantLayout, "Please Select Items!!!", Snackbar.LENGTH_LONG, RestarantDetailsActivity.this);

        }
    }

    private void actionsOnAddItems() {
        if (addOrSubract) {
            if (foodItemResponse.getData() != null) {

                for (int i = 0; i < foodItemResponse.getData().size(); i++) {


                    for (int j = 0; j < foodItemResponse.getData().get(i).getProduct().size(); j++) {

                        ModelRequest modelRequest = new ModelRequest();

                        if (foodItemResponse.getData().get(i).getProduct().get(j).getQuantity().equals("0")) {


                            Log.e("Nive ", "onViewClicked:Response " + foodItemResponse.getData().get(i).getProduct().get(j).getQuantity().equals("0"));

                        } else {
                            modelRequest.setQuantity(foodItemResponse.getData().get(i).getProduct().get(j).getQuantity());
                            modelRequest.setItem_id(foodItemResponse.getData().get(i).getProduct().get(j).getItem_id());
                            modelRequestsList.add(modelRequest);

                        }


                    }


                    foodModelRequest = new FoodModelRequest();
                    foodModelRequest.setData(modelRequestsList);
                    foodModelRequest.setProvider_admin_id(PrefConnect.readString(RestarantDetailsActivity.this, Global.PROVIDERID, ""));

                }

            }
        } else {


            for (int i = 0; i < addCartResponse.getData().size(); i++) {


                Log.e("Nive ", "onViewClicked:addCartResponse " + addCartResponse.getData().size());

                modelRequest = new ModelRequest();

                modelRequest.setQuantity(addCartResponse.getData().get(i).getQuantity());
                modelRequest.setItem_id(addCartResponse.getData().get(i).getItem_id());
                modelRequestsList.add(modelRequest);

                foodModelRequest = new FoodModelRequest();
                foodModelRequest.setData(modelRequestsList);
                foodModelRequest.setProvider_admin_id(PrefConnect.readString(RestarantDetailsActivity.this, Global.PROVIDERID, ""));

            }


        }


        String count = tvTotalItems.getText().toString();

        String arr[] = count.split(" ");

        int totalItems = 0;
        if (arr[0] != null) {

            totalItems = Integer.parseInt(arr[0]);
        }


        if (totalItems >= 1) {

            Intent intent = new Intent(RestarantDetailsActivity.this, ConfirmationDetailsActivity.class);
            intent.putExtra(Global.SENDMODEL, foodModelRequest);
            startActivity(intent);
        } else {
            Global.snack_bar(parantLayout, "Please Select Items!!!", Snackbar.LENGTH_LONG, RestarantDetailsActivity.this);

        }

    }
}
