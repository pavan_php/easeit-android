package com.dratoo_user.ui.restarantdetails.model;

import java.io.Serializable;

public class ListDetails implements Serializable {

    public String id;
    public String quantity;
    public String user_id;
    public String item_id;
    public String provider_admin_id;

    public int ViewType;

    public int getViewType() {
        return ViewType;
    }

    public void setViewType(int viewType) {
        ViewType = viewType;
    }

    @Override
    public String toString() {
        return "ListDetails{" +
                "id='" + id + '\'' +
                ", quantity='" + quantity + '\'' +
                ", user_id='" + user_id + '\'' +
                ", item_id='" + item_id + '\'' +
                ", provider_admin_id='" + provider_admin_id + '\'' +
                ", category_id='" + category_id + '\'' +
                ", image='" + image + '\'' +
                ", item_name='" + item_name + '\'' +
                ", price='" + price + '\'' +
                ", tax='" + tax + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getProvider_admin_id() {
        return provider_admin_id;
    }

    public void setProvider_admin_id(String provider_admin_id) {
        this.provider_admin_id = provider_admin_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String category_id;
    public String image;
    public String item_name;
    public String price;
    public String tax;


    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

}
