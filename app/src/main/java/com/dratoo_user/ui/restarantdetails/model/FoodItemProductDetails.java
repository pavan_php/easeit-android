package com.dratoo_user.ui.restarantdetails.model;

import java.io.Serializable;

/**
 * Created by ${Krishnaprakash} on 22-03-2018.
 */

public class FoodItemProductDetails implements Serializable {

    public String item_id;
    public String item_name;
    public String image;
    public String description;
    public String quantity;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProvider_admin_id() {
        return provider_admin_id;
    }

    public void setProvider_admin_id(String provider_admin_id) {
        this.provider_admin_id = provider_admin_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public int ViewType;
    public String price;
    public String tax;

    public String provider_admin_id;

    @Override
    public String toString() {
        return "FoodItemProductDetails{" +
                "item_id='" + item_id + '\'' +
                ", item_name='" + item_name + '\'' +
                ", image='" + image + '\'' +
                ", description='" + description + '\'' +
                ", quantity='" + quantity + '\'' +
                ", ViewType=" + ViewType +
                ", price='" + price + '\'' +
                ", tax='" + tax + '\'' +
                ", provider_admin_id='" + provider_admin_id + '\'' +
                '}';
    }

    public int getViewTYpe() {
        return ViewType;
    }

    public void setViewType(int viewTYpe) {
        this.ViewType = viewTYpe;
    }


    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }


}
