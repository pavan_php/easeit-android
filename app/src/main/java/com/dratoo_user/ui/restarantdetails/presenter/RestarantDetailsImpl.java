package com.dratoo_user.ui.restarantdetails.presenter;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;

import com.dratoo_user.ui.confirmationfood.model.AddMoreItemsRequest;
import com.dratoo_user.ui.restarantdetails.model.AddCartRequest;
import com.dratoo_user.ui.restarantdetails.model.FoodItemResponse;
import com.dratoo_user.ui.restarantdetails.model.IncrementDecrementCartRequest;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestarantDetailsImpl extends BasePresenter<RestarantDetailsView> implements RestarantDetailsPresenter {


    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;

    @Override
    public void attachView(RestarantDetailsView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void getRestarantItems(String id) {
        getView().showProgress();
        Call<FoodItemResponse> call = apiInterface.getProductItem(id);

        call.enqueue(new Callback<FoodItemResponse>() {
            @Override
            public void onResponse(Call<FoodItemResponse> call, Response<FoodItemResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().getProductList(response.body());
                }
            }

            @Override
            public void onFailure(Call<FoodItemResponse> call, Throwable t) {
                getView().hideProgress();
            }
        });

    }

    @Override
    public void addCart(AddCartRequest addCartRequest, String service_type) {

        Call<FoodItemResponse> call = null;

        getView().showProgress();

        if (service_type != null) {
            if (service_type.equals("1")) {
                call = apiInterface.addCart(addCartRequest);

            } else {
                call = apiInterface.addCartMart(addCartRequest);
            }
        }

        call.enqueue(new Callback<FoodItemResponse>() {
            @Override
            public void onResponse(Call<FoodItemResponse> call, Response<FoodItemResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().sucessAddcart(response.body());


                }
            }

            @Override
            public void onFailure(Call<FoodItemResponse> call, Throwable t) {
                getView().hideProgress();
            }
        });

    }


    @Override
    public void incrementDetails(IncrementDecrementCartRequest
                                         incrementDecrementCartRequest, String service_type) {
        Call<FoodItemResponse> call = null;

        getView().showProgress();

        if (service_type != null) {
            if (service_type.equals("1")) {
                call = apiInterface.getDetailsIncrement(incrementDecrementCartRequest);

            } else {
                call = apiInterface.getDetailsIncrementMart(incrementDecrementCartRequest);
            }
        }

        call.enqueue(new Callback<FoodItemResponse>() {
            @Override
            public void onResponse(Call<FoodItemResponse> call, Response<FoodItemResponse> response) {
                getView().hideProgress();
                if (response.isSuccessful()) {
                    getView().incrementSucess(response.body());
                }
            }

            @Override
            public void onFailure(Call<FoodItemResponse> call, Throwable t) {
                getView().hideProgress();

            }
        });


    }

    @Override
    public void decrementDetails(IncrementDecrementCartRequest
                                         incrementDecrementCartRequest, String service_type) {

        getView().showProgress();

        Call<FoodItemResponse> call = null;

        if (service_type != null) {
            if (service_type.equals("1")) {

                call = apiInterface.getDetailsDecrement(incrementDecrementCartRequest);
            } else {

                call = apiInterface.getDetailsDecrementMart(incrementDecrementCartRequest);


            }
        }


        call.enqueue(new Callback<FoodItemResponse>() {
            @Override
            public void onResponse(Call<FoodItemResponse> call, Response<FoodItemResponse> response) {
                getView().hideProgress();
                if (response.isSuccessful()) {
                    getView().decrementSucess(response.body());
                }
            }

            @Override
            public void onFailure(Call<FoodItemResponse> call, Throwable t) {
                getView().hideProgress();

            }
        });


    }

    @Override
    public void getDrinksMartList(String providerId) {
        getView().showProgress();
        Call<FoodItemResponse> call = apiInterface.getDrinksMartList(providerId);
        call.enqueue(new Callback<FoodItemResponse>() {
            @Override
            public void onResponse(Call<FoodItemResponse> call, Response<FoodItemResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().getProductList(response.body());
                } else {
                    getView().hideProgress();
                }
            }

            @Override
            public void onFailure(Call<FoodItemResponse> call, Throwable t) {
                getView().hideProgress();
            }
        });
    }

    @Override
    public void getAddMoreItemsFood(AddMoreItemsRequest addMoreItemsRequest) {

        getView().showProgress();
        Call<FoodItemResponse> call = apiInterface.getFoodMoreItems(addMoreItemsRequest);
        call.enqueue(new Callback<FoodItemResponse>() {
            @Override
            public void onResponse(Call<FoodItemResponse> call, Response<FoodItemResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().getProductList(response.body());
                } else {
                    getView().hideProgress();
                }
            }

            @Override
            public void onFailure(Call<FoodItemResponse> call, Throwable t) {
                getView().hideProgress();
            }
        });

    }

    @Override
    public void getAddMoreItemsMart(AddMoreItemsRequest addMoreItemsRequest) {

        getView().showProgress();
        Call<FoodItemResponse> call = apiInterface.getMartMoreItems(addMoreItemsRequest);
        call.enqueue(new Callback<FoodItemResponse>() {
            @Override
            public void onResponse(Call<FoodItemResponse> call, Response<FoodItemResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().getProductList(response.body());
                } else {
                    getView().hideProgress();
                }
            }

            @Override
            public void onFailure(Call<FoodItemResponse> call, Throwable t) {
                getView().hideProgress();
            }
        });

    }
}
