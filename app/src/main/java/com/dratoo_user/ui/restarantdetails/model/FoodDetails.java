package com.dratoo_user.ui.restarantdetails.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ${Krishnaprakash} on 22-03-2018.
 */

public class FoodDetails implements Serializable {

    public String category_id;
    public String category;


    public String item_id;
    public String item_name;
    public String description;
    public String quantity;

    public String price;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProvider_admin_id() {
        return provider_admin_id;
    }

    public void setProvider_admin_id(String provider_admin_id) {
        this.provider_admin_id = provider_admin_id;
    }

    public String tax;

    public String provider_admin_id;

    public int  ViewType;

    public String image;
    public ArrayList<FoodItemProductDetails> product;



    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    @Override
    public String toString() {
        return "FoodDetails{" +
                "category_id='" + category_id + '\'' +
                ", category='" + category + '\'' +
                ", item_id='" + item_id + '\'' +
                ", item_name='" + item_name + '\'' +
                ", price='" + price + '\'' +
                ", tax='" + tax + '\'' +
                ", viewType=" + ViewType +
                ", image='" + image + '\'' +
                ", product=" + product +
                '}';
    }

    public int getViewType() {
        return ViewType;
    }

    public void setViewType(int viewType) {
        this.ViewType = viewType;
    }



    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<FoodItemProductDetails> getProduct() {
        return product;
    }

    public void setProduct(ArrayList<FoodItemProductDetails> product) {
        this.product = product;
    }


}
