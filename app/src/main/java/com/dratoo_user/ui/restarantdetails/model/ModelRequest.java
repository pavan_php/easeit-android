package com.dratoo_user.ui.restarantdetails.model;

import java.io.Serializable;

public class ModelRequest implements Serializable {

    public String item_id;

    public String getItem_id() {
        return item_id;
    }

    @Override
    public String toString() {
        return "ModelRequest{" +
                "item_id='" + item_id + '\'' +
                ", quantity='" + quantity + '\'' +
                '}';
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String quantity;

}
