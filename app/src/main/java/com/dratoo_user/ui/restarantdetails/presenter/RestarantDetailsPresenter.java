package com.dratoo_user.ui.restarantdetails.presenter;

import com.dratoo_user.base.Presenter;
import com.dratoo_user.ui.confirmationfood.model.AddMoreItemsRequest;
import com.dratoo_user.ui.restarantdetails.model.AddCartRequest;
import com.dratoo_user.ui.restarantdetails.model.IncrementDecrementCartRequest;

public interface RestarantDetailsPresenter extends Presenter<RestarantDetailsView> {

    void getRestarantItems(String id);


    void addCart(AddCartRequest addCartRequest, String service_type);

    void incrementDetails(IncrementDecrementCartRequest incrementDecrementCartRequest, String service_type);

    void decrementDetails(IncrementDecrementCartRequest incrementDecrementCartRequest, String service_type);

    void getDrinksMartList(String providerId);

    void getAddMoreItemsFood(AddMoreItemsRequest addMoreItemsRequest);

    void getAddMoreItemsMart(AddMoreItemsRequest addMoreItemsRequest);

}
