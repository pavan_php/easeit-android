package com.dratoo_user.ui.restarantdetails.model;

import java.io.Serializable;
import java.util.ArrayList;

public class FoodModelRequest implements Serializable {

    public ArrayList<ModelRequest> getData() {
        return data;
    }

    public void setData(ArrayList<ModelRequest> modelRequestArrayList) {
        this.data = modelRequestArrayList;
    }

    public ArrayList<ModelRequest> data;

    @Override
    public String toString() {
        return "FoodModelRequest{" +
                "data=" + data +
                ", provider_admin_id='" + provider_admin_id + '\'' +
                '}';
    }

    public String getProvider_admin_id() {
        return provider_admin_id;
    }

    public void setProvider_admin_id(String provider_admin_id) {
        this.provider_admin_id = provider_admin_id;
    }

    public String provider_admin_id;

    public String removed;

    public String getRemoved() {
        return removed;
    }

    public void setRemoved(String removed) {
        this.removed = removed;
    }

}
