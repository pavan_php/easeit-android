package com.dratoo_user.ui.restarantdetails.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.restarantdetails.model.FoodItemResponse;

public interface RestarantDetailsView extends BaseView {

    void getProductList(FoodItemResponse body);

    void incrementSucess(FoodItemResponse body);

    void decrementSucess(FoodItemResponse body);

    void sucessAddcart(FoodItemResponse body);


}
