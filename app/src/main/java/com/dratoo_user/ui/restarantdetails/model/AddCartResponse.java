package com.dratoo_user.ui.restarantdetails.model;

import java.io.Serializable;
import java.util.ArrayList;

public class AddCartResponse implements Serializable {

    public ArrayList<ListDetails> getData() {
        return data;
    }

    public void setData(ArrayList<ListDetails> data) {
        this.data = data;
    }

    public ArrayList<ListDetails> data;

    public String total_qty;
    public String total_amt;

    public String getTotal_amt() {
        return total_amt;
    }

    public void setTotal_amt(String total_amt) {
        this.total_amt = total_amt;
    }

    public String getTotal_qty() {
        return total_qty;
    }

    public void setTotal_qty(String total_qty) {
        this.total_qty = total_qty;
    }

}
