package com.dratoo_user.ui.restarantdetails.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ${Krishnaprakash} on 05-04-2018.
 */

public class AddCartRequest implements Serializable {
    public ArrayList<AddCartArrRequst> getData() {
        return data;
    }

    public void setData(ArrayList<AddCartArrRequst> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "AddCartRequest{" +
                "data=" + data +
                ", service_type='" + service_type + '\'' +
                '}';
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public ArrayList<AddCartArrRequst> data;

    public String service_type;

    public String getProvider_admin_id() {
        return provider_admin_id;
    }

    public void setProvider_admin_id(String provider_admin_id) {
        this.provider_admin_id = provider_admin_id;
    }

    public String provider_admin_id;

}
