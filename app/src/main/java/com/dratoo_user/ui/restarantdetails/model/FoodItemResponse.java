package com.dratoo_user.ui.restarantdetails.model;

import java.util.ArrayList;

/**
 * Created by ${Krishnaprakash} on 22-03-2018.
 */

public class FoodItemResponse {


    public ArrayList<FoodDetails> getData() {
        return data;
    }

    public void setData(ArrayList<FoodDetails> data) {
        this.data = data;
    }

    public ArrayList<FoodDetails> data;

    public String getProvider_admin_id() {
        return provider_admin_id;
    }

    public void setProvider_admin_id(String provider_admin_id) {
        this.provider_admin_id = provider_admin_id;
    }

    public String provider_admin_id;
    public String total_qty;
    public String total_amt;

    public String getTotal_amt() {
        return total_amt;
    }

    public void setTotal_amt(String total_amt) {
        this.total_amt = total_amt;
    }

    public String getTotal_qty() {
        return total_qty;
    }

    public void setTotal_qty(String total_qty) {
        this.total_qty = total_qty;
    }
}
