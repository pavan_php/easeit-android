package com.dratoo_user.ui.restarantdetails.model;

import java.io.Serializable;

/**
 * Created by ${Krishnaprakash} on 05-04-2018.
 */

public class AddCartArrRequst implements Serializable {

    public String item_id;

    @Override
    public String toString() {
        return "AddCartArrRequst{" +
                "item_id='" + item_id + '\'' +
                '}';
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }
}
