package com.dratoo_user.ui.updateprofile.model;

public class ProfileUpdateResponse {

    public ProfileUpdate getData() {
        return data;
    }

    public void setData(ProfileUpdate data) {
        this.data = data;
    }

    public ProfileUpdate data;


}
