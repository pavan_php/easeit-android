package com.dratoo_user.ui.updateprofile.presenter;

import com.dratoo_user.base.Presenter;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface UpdateProfilePresenter extends Presenter<UpdateProfileView> {

    void getProfile();

    void logout();

    void profileUpdate(HashMap<String, RequestBody> updateMap, MultipartBody.Part profilePhoto);
}
