package com.dratoo_user.ui.updateprofile.presenter;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.updateprofile.model.LogoutResponse;
import com.dratoo_user.ui.updateprofile.model.ProfileUpdateResponse;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Named;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfileImpl extends BasePresenter<UpdateProfileView> implements UpdateProfilePresenter {
    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;

    @Override
    public void attachView(UpdateProfileView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void getProfile() {
        getView().showProgress();
        Call<ProfileUpdateResponse> call = apiInterface.getProfile();
        call.enqueue(new Callback<ProfileUpdateResponse>() {
            @Override
            public void onResponse(@NotNull Call<ProfileUpdateResponse> call, @NotNull Response<ProfileUpdateResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().sucessGetProfile(response.body());
                }
            }

            @Override
            public void onFailure(@NotNull Call<ProfileUpdateResponse> call, @NotNull Throwable t) {
                getView().hideProgress();

            }
        });
    }

    @Override
    public void logout() {
        getView().showProgress();
        Call<LogoutResponse> call = apiInterface.logout();
        call.enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(@NotNull Call<LogoutResponse> call, @NotNull Response<LogoutResponse> response) {
                getView().hideProgress();
                if (response.isSuccessful()) {
                    getView().logoutSucess();
                }
            }

            @Override
            public void onFailure(@NotNull Call<LogoutResponse> call, @NotNull Throwable t) {
                getView().hideProgress();
            }
        });


    }

    @Override
    public void profileUpdate(HashMap<String, RequestBody> updateMap, MultipartBody.Part profilePhoto) {

        getView().showProgress();
        Call<ProfileUpdateResponse> call = apiInterface.profileUpdate(updateMap, profilePhoto);
        call.enqueue(new Callback<ProfileUpdateResponse>() {
            @Override
            public void onResponse(@NotNull Call<ProfileUpdateResponse> call, @NotNull Response<ProfileUpdateResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().updateProfileSucess();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ProfileUpdateResponse> call, @NotNull Throwable t) {
                getView().hideProgress();

            }
        });
    }
}