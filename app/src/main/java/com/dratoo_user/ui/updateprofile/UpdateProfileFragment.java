package com.dratoo_user.ui.updateprofile;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.dratoo_user.R;
import com.dratoo_user.data.FilePath;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.common.ProgressDialog;
import com.dratoo_user.ui.updateprofile.model.ProfileUpdateResponse;
import com.dratoo_user.ui.updateprofile.presenter.UpdateProfilePresenter;
import com.dratoo_user.ui.updateprofile.presenter.UpdateProfileView;
import com.dratoo_user.ui.welcome.WelcomeActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;


public class UpdateProfileFragment extends Fragment implements UpdateProfileView {


    private static final int PERMISSION_REQUEST = 000;
    private static final int SELECT_FILE = 1;

    @Inject
    UpdateProfilePresenter presenter;
    ProgressDialog progressDialog;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_name)
    AppCompatTextView tvName;
    @BindView(R.id.tv_edit)
    AppCompatTextView tvEdit;
    @BindView(R.id.tv_mail)
    AppCompatTextView tvMail;
    @BindView(R.id.tv_mob_nbr)
    AppCompatTextView tvMobNbr;
    @BindView(R.id.btn_with_facebook)
    AppCompatButton btnWithFacebook;
    Unbinder unbinder;
    private Dialog dialog;
    private BottomSheetDialog mBottomSheetDialog;
    private View bottomSheet;

    private File cameraFile;

    Uri uriCam = null;
    private Uri profileURI;
    private File avatarFile;

    AppCompatEditText et_name, et_email, et_mobile;

    CircleImageView img_profile;
    private Uri resultUri;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_update_profile, container, false);

        DaggerAppComponent.create().inject(this);
        presenter.attachView(this);


        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        progressDialog = new ProgressDialog(getActivity());

        unbinder = ButterKnife.bind(this, layout);

        initBottomSheet();

        presenter.getProfile();
        return layout;
    }


    private void galleryIntent() {


        Intent i1 = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i1, SELECT_FILE);
    }

    public boolean isCameraPermissionEnabled() {

        return ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }


    public boolean isStoragePermissionEnabled() {
        return ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            ArrayList<String> permissionList = new ArrayList<>();
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
                permissionList.add(Manifest.permission.CAMERA);
            }

            if (permissionList.size() > 0) {
                String[] permissionArray = new String[permissionList.size()];
                permissionList.toArray(permissionArray);
                getActivity().requestPermissions(permissionArray, PERMISSION_REQUEST);
                return false;
            } else {
                return true;
            }
        }
        return true;
    }


    public void initBottomSheet() {
        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        bottomSheet = getActivity().getLayoutInflater().inflate(R.layout.bottom_sheet_select_images, null);
        mBottomSheetDialog.setContentView(bottomSheet);

        AppCompatTextView tv_gallery = (AppCompatTextView) mBottomSheetDialog.findViewById(R.id.tv_gallery);
        AppCompatTextView tv_camera = (AppCompatTextView) mBottomSheetDialog.findViewById(R.id.tv_camera);


        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCameraPermissionEnabled() && isStoragePermissionEnabled()) {
                    Calendar cal = Calendar.getInstance();
                    cameraFile = new File(Environment.getExternalStorageDirectory(),
                            (cal.getTimeInMillis() + ".jpg"));


                    if (!cameraFile.exists()) {
                        try {
                            cameraFile.createNewFile();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } else {

                        cameraFile.delete();
                        try {
                            cameraFile.createNewFile();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                    uriCam = Uri.fromFile(cameraFile);
                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    i.putExtra(MediaStore.EXTRA_OUTPUT, uriCam);
                    startActivityForResult(i, 0);

                    mBottomSheetDialog.dismiss();
                } else {
                    checkPermissions();
                }

            }
        });

        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCameraPermissionEnabled() && isStoragePermissionEnabled()) {
                    galleryIntent();
                    mBottomSheetDialog.dismiss();
                } else {
                    checkPermissions();
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    profileURI = uriCam;
                    String filePath = getRealPathFromURI(profileURI);
                    File file = new File(filePath);
                    profileURI = Uri.fromFile(file);

                    // start cropping activity for pre-acquired image saved on the device
                    CropImage.activity(profileURI)
                            .start(getActivity(), UpdateProfileFragment.this);


                }
                break;

            case 1:
                if (resultCode == RESULT_OK) {

                    Uri uri = data.getData();
                    profileURI = uri;
                    String filePath = getRealPathFromURI(profileURI);
                    File file = new File(filePath);
                    profileURI = Uri.fromFile(file);

                    // start cropping activity for pre-acquired image saved on the device
                    CropImage.activity(profileURI)
                            .start(getActivity(), UpdateProfileFragment.this);

                }
                break;

            case 203:
                if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (resultCode == RESULT_OK) {
                        resultUri = result.getUri();

                        try {
                            Picasso.get().load(resultUri).fit().into(img_profile);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                        Exception error = result.getError();
                    }

                }
                break;


        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getActivity().getContentResolver().query(contentURI, null,
                null, null, null);

        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    public void showProgress() {
        progressDialog.show();

    }

    @Override
    public void hideProgress() {

        progressDialog.dismissLoader();

    }

    @Override
    public void sucessGetProfile(ProfileUpdateResponse profileUpdateResponse) {

        if (profileUpdateResponse != null) {

            tvName.setText(profileUpdateResponse.getData().getName());
            tvMail.setText(profileUpdateResponse.getData().getEmail());
            tvMobNbr.setText(profileUpdateResponse.getData().getPhone());


            PrefConnect.writeString(getActivity(), Global.NAME, profileUpdateResponse.getData().getName());
            PrefConnect.writeString(getActivity(), Global.MOBILE_NBR, profileUpdateResponse.getData().getPhone());
            PrefConnect.writeString(getActivity(), Global.EMAIL, profileUpdateResponse.getData().getEmail());


            Log.e("Nive ", "sucessGetProfile: " + profileUpdateResponse.getData().getPhone());

            if (dialog != null) {
                et_name.setText(profileUpdateResponse.getData().getName());
                et_email.setText(profileUpdateResponse.getData().getEmail());
                et_mobile.setText(profileUpdateResponse.getData().getPhone());

                try {
                    Picasso.get().load(Global.IMG_URL + profileUpdateResponse.getData().getProfile_pic()).fit().error(R.drawable.default_profile).into(img_profile);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


        }

    }

    @Override
    public void updateProfileSucess() {
        if (dialog != null) {
            dialog.dismiss();
            presenter.getProfile();
        }
    }

    @Override
    public void logoutSucess() {

        PrefConnect.writeString(getActivity(), Global.LOGIN, "");
        PrefConnect.writeBoolean(getActivity(), Global.ENTERONCE, false);
        PrefConnect.writeBoolean(getActivity(), Global.ENTERONCE_FOOD, false);
        Intent intent = new Intent(getActivity(), WelcomeActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.tv_edit, R.id.btn_with_facebook, R.id.logout_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_edit:
                showEditDialog();
                break;
            case R.id.btn_with_facebook:
                Snackbar snack = Snackbar.make(view.getRootView(), "Not Enabled", Snackbar.LENGTH_LONG);
                View x = snack.getView();
                FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)x.getLayoutParams();
                params.gravity = Gravity.TOP;
                x.setLayoutParams(params);
                snack.show();
                break;
            case R.id.logout_layout:
                presenter.logout();
                break;
        }
    }

    private void showEditDialog() {
        dialog = new Dialog(getActivity(), R.style.SlideTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.bottom_edit_profile);
        Window window = dialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        AppCompatTextView tv_save = dialog.findViewById(R.id.tv_save);
        LinearLayout name_click_layout = dialog.findViewById(R.id.name_click_layout);
        LinearLayout email_click_layout = dialog.findViewById(R.id.email_click_layout);
        LinearLayout mobile_click_layout = dialog.findViewById(R.id.mobile_click_layout);
        img_profile = dialog.findViewById(R.id.img_profile);
        final LinearLayout mobileLyaout = dialog.findViewById(R.id.mobileLyaout);
        final LinearLayout emailLayout = dialog.findViewById(R.id.emailLayout);
        final LinearLayout nameLayout = dialog.findViewById(R.id.nameLayout);
        AppCompatImageView img_back = dialog.findViewById(R.id.img_back);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        presenter.getProfile();

        et_name = dialog.findViewById(R.id.et_name);
        et_email = dialog.findViewById(R.id.et_email);
        et_mobile = dialog.findViewById(R.id.et_mobile);

        name_click_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameLayout.setVisibility(View.VISIBLE);
                mobileLyaout.setVisibility(View.GONE);
                emailLayout.setVisibility(View.GONE);
            }
        });

        email_click_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nameLayout.setVisibility(View.GONE);
                mobileLyaout.setVisibility(View.GONE);
                emailLayout.setVisibility(View.VISIBLE);

            }
        });

        mobile_click_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameLayout.setVisibility(View.GONE);
//                mobileLyaout.setVisibility(View.VISIBLE);
                emailLayout.setVisibility(View.GONE);
            }
        });

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.show();
            }
        });


        tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_email.getText().toString().isEmpty()) {

                } else if (et_name.getText().toString().isEmpty()) {

                } else {

                    final HashMap<String, RequestBody> updateMap = new HashMap();
                    final HashMap<String, MultipartBody.Part> imageMap = new HashMap();
                    updateMap.put("email", createPartFromString(et_email.getText().toString()));
                    updateMap.put("name", createPartFromString(et_name.getText().toString()));


                    if (resultUri != null) {
                        avatarFile = FilePath.getPath(getActivity(), resultUri);
                    }

                    if (avatarFile != null) {
                        RequestBody requestFile =
                                RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
                        MultipartBody.Part body =
                                MultipartBody.Part.createFormData(
                                        "profile_pic", avatarFile.getName(), requestFile);
                        imageMap.put("profile_pic", body);
                    }
                    presenter.profileUpdate(updateMap, imageMap.get("profile_pic"));
                }
            }
        });

        dialog.show();
    }

    @NonNull
    public RequestBody createPartFromString(String descriptionString) {

        String assign = descriptionString;
        Log.e("assign", assign);
        return RequestBody.create(MultipartBody.FORM, descriptionString);
    }
}
