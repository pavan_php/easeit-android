package com.dratoo_user.ui.updateprofile.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.updateprofile.model.ProfileUpdateResponse;

public interface UpdateProfileView extends BaseView {

    void sucessGetProfile(ProfileUpdateResponse profileUpdateResponse);

    void updateProfileSucess();

    void logoutSucess();
}
