package com.dratoo_user.ui.restarnt.presenter;

import com.dratoo_user.base.Presenter;

public interface RestarantPresenter extends Presenter<RestarantView> {

    void RestarantList(String styleId, String lat, String lng, String radius);

    void ProviderList(String styleId, String lat, String lng, String radius);

    void getSubServiceList(String service_id);
}
