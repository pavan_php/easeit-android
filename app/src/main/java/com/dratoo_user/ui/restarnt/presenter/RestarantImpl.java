package com.dratoo_user.ui.restarnt.presenter;

import android.util.Log;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.restarnt.model.ProviderResponse;
import com.dratoo_user.ui.restarnt.model.RestarantResponse;
import com.dratoo_user.ui.restarnt.model.SubServiceResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestarantImpl extends BasePresenter<RestarantView> implements RestarantPresenter {


    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;

    @Override
    public void attachView(RestarantView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void RestarantList(String styleId, String lat, String lng, String radius) {

        Call<RestarantResponse> call = apiInterface.getRestarantLists(styleId, lat, lng, radius);

        call.enqueue(new Callback<RestarantResponse>() {
            @Override
            public void onResponse(Call<RestarantResponse> call, Response<RestarantResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().getRestarantLists(response.body());
                }
            }

            @Override
            public void onFailure(Call<RestarantResponse> call, Throwable t) {

                Log.e("Nive ", "onFailure:Food "+t.toString() );

            }
        });

    }

    @Override
    public void ProviderList(String styleId, String lat, String lng, String radius) {

        Call<ProviderResponse> call = apiInterface.getProviderList(styleId, lat, lng, radius);

        call.enqueue(new Callback<ProviderResponse>() {
            @Override
            public void onResponse(Call<ProviderResponse> call, Response<ProviderResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().getProviderList(response.body());
                }
            }

            @Override
            public void onFailure(Call<ProviderResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void getSubServiceList(String service_id) {

        Call<SubServiceResponse> call = apiInterface.getSubServiceList(service_id);

        call.enqueue(new Callback<SubServiceResponse>() {
            @Override
            public void onResponse(Call<SubServiceResponse> call, Response<SubServiceResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().subServicesList(response.body());
                }
            }

            @Override
            public void onFailure(Call<SubServiceResponse> call, Throwable t) {

            }
        });

    }
}
