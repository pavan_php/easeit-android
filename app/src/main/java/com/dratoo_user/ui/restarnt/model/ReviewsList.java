package com.dratoo_user.ui.restarnt.model;

import java.io.Serializable;

public class ReviewsList implements Serializable {

    public String profile_pic;
    public String worker_rating;
    public String user_comments;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String name;

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getWorker_rating() {
        return worker_rating;
    }

    public void setWorker_rating(String worker_rating) {
        this.worker_rating = worker_rating;
    }

    public String getUser_comments() {
        return user_comments;
    }

    public void setUser_comments(String user_comments) {
        this.user_comments = user_comments;
    }
}
