package com.dratoo_user.ui.restarnt.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ServiceProviderDetails implements Serializable {

    public String id;
    public String provider_admin_id;
    public String service_id;
    public String name;
    public String phone;
    public String email;
    public String profile_image;
    public String experiance;

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public ArrayList<ReviewsList> reviews;

    public ArrayList<ReviewsList> getReviews() {
        return reviews;
    }

    public void setReviews(ArrayList<ReviewsList> reviews) {
        this.reviews = reviews;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String summary;

    public LatLangProviderDetails getResult() {
        return result;
    }

    public void setResult(LatLangProviderDetails result) {
        this.result = result;
    }

    public String rate_per_hour;
    public String working_time;
    public String rating;
    public String fname;

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String lname;
    public String status;
    public String created_at;
    public String updated_at;
    public boolean isSelected;


    public LatLangProviderDetails result;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String ServiceName;
    public String spinnerPerHour;
    public String spinnerWorkingHrs;
    public String spinnerTime;
    public String spinnerTimeVal;
    public String spinnerDate;
    public String spinnerMonth;
    public String spinnerYear;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvider_admin_id() {
        return provider_admin_id;
    }

    public void setProvider_admin_id(String provider_admin_id) {
        this.provider_admin_id = provider_admin_id;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public String toString() {
        return "BuilderDetails{" +
                "id='" + id + '\'' +
                ", provider_admin_id='" + provider_admin_id + '\'' +
                ", service_id='" + service_id + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", experiance='" + experiance + '\'' +
                ", rate_per_hour='" + rate_per_hour + '\'' +
                ", working_time='" + working_time + '\'' +
                ", rating='" + rating + '\'' +
                ", status='" + status + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", isSelected=" + isSelected +
                ", ServiceName='" + ServiceName + '\'' +
                ", spinnerPerHour='" + spinnerPerHour + '\'' +
                ", spinnerWorkingHrs='" + spinnerWorkingHrs + '\'' +
                ", spinnerTime='" + spinnerTime + '\'' +
                ", spinnerTimeVal='" + spinnerTimeVal + '\'' +
                ", spinnerDate='" + spinnerDate + '\'' +
                ", spinnerMonth='" + spinnerMonth + '\'' +
                ", spinnerYear='" + spinnerYear + '\'' +
                '}';
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getExperiance() {
        return experiance;
    }

    public void setExperiance(String experiance) {
        this.experiance = experiance;
    }

    public String getRate_per_hour() {
        return rate_per_hour;
    }

    public void setRate_per_hour(String rate_per_hour) {
        this.rate_per_hour = rate_per_hour;
    }

    public String getWorking_time() {
        return working_time;
    }

    public void setWorking_time(String working_time) {
        this.working_time = working_time;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getSpinnerPerHour() {
        return spinnerPerHour;
    }

    public void setSpinnerPerHour(String spinnerPerHour) {
        this.spinnerPerHour = spinnerPerHour;
    }

    public String getSpinnerWorkingHrs() {
        return spinnerWorkingHrs;
    }

    public void setSpinnerWorkingHrs(String spinnerWorkingHrs) {
        this.spinnerWorkingHrs = spinnerWorkingHrs;
    }

    public String getSpinnerTime() {
        return spinnerTime;
    }

    public void setSpinnerTime(String spinnerTime) {
        this.spinnerTime = spinnerTime;
    }

    public String getSpinnerTimeVal() {
        return spinnerTimeVal;
    }

    public void setSpinnerTimeVal(String spinnerTimeVal) {
        this.spinnerTimeVal = spinnerTimeVal;
    }

    public String getSpinnerDate() {
        return spinnerDate;
    }

    public void setSpinnerDate(String spinnerDate) {
        this.spinnerDate = spinnerDate;
    }

    public String getSpinnerMonth() {
        return spinnerMonth;
    }

    public void setSpinnerMonth(String spinnerMonth) {
        this.spinnerMonth = spinnerMonth;
    }

    public String getSpinnerYear() {
        return spinnerYear;
    }

    public void setSpinnerYear(String spinnerYear) {
        this.spinnerYear = spinnerYear;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }
}
