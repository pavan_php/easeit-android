package com.dratoo_user.ui.restarnt.model;

import java.util.ArrayList;

public class SubServiceResponse {

    public ArrayList<SubServiceList> data;

    public ArrayList<SubServiceList> getData() {
        return data;
    }

    public void setData(ArrayList<SubServiceList> data) {
        this.data = data;
    }
}
