package com.dratoo_user.ui.restarnt.model;

import java.util.ArrayList;

public class ServiceProviderResponse {

    public ArrayList<ServiceProviderDetails> data;
    public ArrayList<ServiceProviderList> data1;

    public String service_name;
    public String message;

    public ArrayList<ServiceProviderList> getData1() {
        return data1;
    }

    public void setData1(ArrayList<ServiceProviderList> data1) {
        this.data1 = data1;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ServiceProviderDetails> getData() {
        return data;
    }

    public void setData(ArrayList<ServiceProviderDetails> data) {
        this.data = data;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }
}
