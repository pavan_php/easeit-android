package com.dratoo_user.ui.restarnt.model;

import java.util.ArrayList;

public class RestarantResponse {

    public ArrayList<RestarantDetailsList> getData() {
        return data;
    }

    public void setData(ArrayList<RestarantDetailsList> data) {
        this.data = data;
    }

    public ArrayList<RestarantDetailsList> data;
}
