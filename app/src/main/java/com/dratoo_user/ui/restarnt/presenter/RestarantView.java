package com.dratoo_user.ui.restarnt.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.restarnt.model.ProviderResponse;
import com.dratoo_user.ui.restarnt.model.RestarantResponse;
import com.dratoo_user.ui.restarnt.model.SubServiceResponse;

public interface RestarantView extends BaseView {

    void getRestarantLists(RestarantResponse body);

    void sucess(String msg);

    void getProviderList(ProviderResponse body);

    void subServicesList(SubServiceResponse body);
}
