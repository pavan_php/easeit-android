package com.dratoo_user.ui.restarnt;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.dratoo_user.R;
import com.dratoo_user.app.App;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.common.ConnectivityReceiver;
import com.dratoo_user.ui.foodhome.FoodHomeActivity;
import com.dratoo_user.ui.restarnt.adapter.RestarantAdapter;
import com.dratoo_user.ui.restarnt.model.ProviderListDetails;
import com.dratoo_user.ui.restarnt.model.ProviderResponse;
import com.dratoo_user.ui.restarnt.model.RestarantDetailsList;
import com.dratoo_user.ui.restarnt.model.RestarantResponse;
import com.dratoo_user.ui.restarnt.model.SubServiceList;
import com.dratoo_user.ui.restarnt.model.SubServiceResponse;
import com.dratoo_user.ui.restarnt.presenter.RestarantPresenter;
import com.dratoo_user.ui.restarnt.presenter.RestarantView;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RestarantActivity extends AppCompatActivity implements RestarantView, ConnectivityReceiver.ConnectivityReceiverListener {
    @Inject
    RestarantPresenter presenter;
    @BindView(R.id.back)
    AppCompatImageView back;
    @BindView(R.id.tv_food_item)
    AppCompatTextView tvFoodItem;
    @BindView(R.id.search)
    LinearLayout search;
    @BindView(R.id.map_search)
    AppCompatImageView mapSearch;
    @BindView(R.id.tv_search)
    AppCompatEditText tvSearch;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.rv_restarant)
    ShimmerRecyclerView rvRestarant;
    @BindView(R.id.parant_layout)
    LinearLayout parantLayout;
    @BindView(R.id.tv_no_items)
    AppCompatTextView tvNoItems;
    private ArrayList<RestarantDetailsList> restarantLists = new ArrayList<>();
    private RestarantAdapter adapter;
    String foodName;
    private ArrayList<ProviderListDetails> providerList;
    private ArrayList<SubServiceList> subServiceList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restarant);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        presenter.attachView(this);

        if (getIntent() != null) {
            foodName = getIntent().getStringExtra(Global.FOOD_NAME);
        }

        //call corresponding food and mart api need to write this condition

        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("1")) {
            presenter.RestarantList(PrefConnect.readString(this, Global.FOOD_STYLE_ID, ""), PrefConnect.readString(this, Global.CURRENT_LAT, ""), PrefConnect.readString(this, Global.CURRENT_LANG, ""), "0");
            tvSearch.setHint(getResources().getString(R.string.like_to_eat));
        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("2")) {
            presenter.ProviderList(PrefConnect.readString(this, Global.FOOD_STYLE_ID, ""), PrefConnect.readString(this, Global.CURRENT_LAT, ""), PrefConnect.readString(this, Global.CURRENT_LANG, ""), "0");
            tvSearch.setHint(getResources().getString(R.string.like_to_grocery));
        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("3")) {
            presenter.getSubServiceList(PrefConnect.readString(this, Global.FOOD_STYLE_ID, ""));
            tvSearch.setHint(getResources().getString(R.string.like_to_service));
        }
        if (PrefConnect.readString(this, Global.FOOD_NAME, "") != null) {
            tvFoodItem.setText(PrefConnect.readString(this, Global.FOOD_NAME, ""));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        App.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        if (isConnected) {
            message = getResources().getString(R.string.connected_to_internet);
        } else {
            message = getResources().getString(R.string.not_connected_to_internet);
        }
        Global.snack_bar(parantLayout, message, Snackbar.LENGTH_LONG, RestarantActivity.this);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void getRestarantLists(RestarantResponse body) {
        if (body.getData() != null) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvRestarant.setLayoutManager(layoutManager);
            restarantLists = body.getData();

            Log.e("Nive ", "getRestarantLists: " + restarantLists.size());

            if (restarantLists.size() > 0) {
                rvRestarant.setVisibility(View.VISIBLE);
                tvNoItems.setVisibility(View.GONE);
                adapter = new RestarantAdapter(this, restarantLists, tvFoodItem.getText().toString(), "1");
                rvRestarant.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
                rvRestarant.setVisibility(View.GONE);
                tvNoItems.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void sucess(String msg) {

    }

    @Override
    public void getProviderList(ProviderResponse body) {
        if (body.getData() != null) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvRestarant.setLayoutManager(layoutManager);
            providerList = body.getData();

            if (providerList.size() > 0) {
                rvRestarant.setVisibility(View.VISIBLE);
                tvNoItems.setVisibility(View.GONE);
                adapter = new RestarantAdapter(this, providerList, tvFoodItem.getText().toString());
                rvRestarant.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
                rvRestarant.setVisibility(View.GONE);
                tvNoItems.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void subServicesList(SubServiceResponse body) {
        if (body.getData() != null) {

            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvRestarant.setLayoutManager(layoutManager);
            subServiceList = body.getData();

            if (subServiceList.size() > 0) {
                rvRestarant.setVisibility(View.VISIBLE);
                tvNoItems.setVisibility(View.GONE);
                adapter = new RestarantAdapter(this, subServiceList, tvFoodItem.getText().toString(), "3", "");
                rvRestarant.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
                rvRestarant.setVisibility(View.GONE);
                tvNoItems.setVisibility(View.VISIBLE);
            }
        }
    }

    @OnClick(R.id.back)
    public void onViewClicked() {
        Intent intent = new Intent(RestarantActivity.this, FoodHomeActivity.class);
        startActivity(intent);
    }
}
