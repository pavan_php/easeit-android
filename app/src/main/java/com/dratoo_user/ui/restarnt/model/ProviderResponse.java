package com.dratoo_user.ui.restarnt.model;


import java.util.ArrayList;

/**
 * Created by Admin on 3/17/2018.
 */

public class ProviderResponse {
    public ArrayList<ProviderListDetails> data;

    public ArrayList<ProviderListDetails> getData() {
        return data;
    }

    public void setData(ArrayList<ProviderListDetails> data) {
        this.data = data;
    }
}
