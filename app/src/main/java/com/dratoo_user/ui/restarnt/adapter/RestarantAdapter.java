package com.dratoo_user.ui.restarnt.adapter;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.ui.choosetimedateslot.DateTimeActivity;
import com.dratoo_user.ui.restarantdetails.RestarantDetailsActivity;
import com.dratoo_user.ui.restarnt.RestarantActivity;
import com.dratoo_user.ui.restarnt.model.ProviderListDetails;
import com.dratoo_user.ui.restarnt.model.RestarantDetailsList;
import com.dratoo_user.ui.restarnt.model.SubServiceList;

import java.util.ArrayList;

public class RestarantAdapter extends RecyclerView.Adapter<RestarantAdapter.ViewHolder> {


    private ArrayList<SubServiceList> service_list;
    ArrayList<ProviderListDetails> providerList;
    RestarantActivity restarantActivity;

    ArrayList<RestarantDetailsList> restarantDetailsLists;

    String val, service_type;
    String timing;

    public RestarantAdapter(RestarantActivity restarantActivity, ArrayList<RestarantDetailsList> restarantLists, String s, String serivce_type) {


        this.restarantActivity = restarantActivity;

        this.restarantDetailsLists = restarantLists;

        this.val = s;
        this.service_type = serivce_type;

    }

    public RestarantAdapter(RestarantActivity restarantActivity, ArrayList<ProviderListDetails> providerList, String s) {
        this.restarantActivity = restarantActivity;

        this.providerList = providerList;

        this.val = s;
    }

    public RestarantAdapter(RestarantActivity restarantActivity, ArrayList<SubServiceList> providerServiceList, String toString, String service_type, String s1) {
        this.restarantActivity = restarantActivity;

        this.service_list = providerServiceList;

        this.val = toString;

        this.service_type = service_type;
    }


    @NonNull
    @Override
    public RestarantAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.restarant_items_rv, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RestarantAdapter.ViewHolder viewHolder, final int i) {


        if (service_type != null) {

            if (service_type.equals("3")) {

                viewHolder.restarant_layout.setVisibility(View.GONE);
                viewHolder.handy_layout.setVisibility(View.VISIBLE);

                try {
                    Picasso.get().load(Global.IMG_URL + service_list.get(i).getImage()).placeholder(restarantActivity.getResources().getDrawable(R.drawable.place_holder)).fit().into(viewHolder.img_handy);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                viewHolder.tv_subservice_name.setText(service_list.get(i).getCategory_name());


//                viewHolder.tv_dish_name.setText(val);


            } else {


                viewHolder.restarant_layout.setVisibility(View.VISIBLE);
                viewHolder.handy_layout.setVisibility(View.GONE);

                String next = "<font color='#a6a6a6'>" + "(" + restarantDetailsLists.get(i).getDuration() + ")</font>";


                viewHolder.tv_rating.setText(restarantDetailsLists.get(i).getRating());

                viewHolder.tv_duration.setText(Html.fromHtml(restarantDetailsLists.get(i).getDistance() + " " + next));


                try {
                    Picasso.get().load(Global.IMG_URL + restarantDetailsLists.get(i).getProfile_pic()).placeholder(restarantActivity.getResources().getDrawable(R.drawable.place_holder)).fit().into(viewHolder.img_food);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                viewHolder.tv_restarant_name.setText(restarantDetailsLists.get(i).getName());


                viewHolder.tv_dish_name.setText(val);


            }


        } else {

            viewHolder.restarant_layout.setVisibility(View.VISIBLE);
            viewHolder.handy_layout.setVisibility(View.GONE);

            String next = "<font color='#a6a6a6'>" + "(" + providerList.get(i).getDuration() + ")</font>";


            viewHolder.tv_rating.setText(providerList.get(i).getRating());

            viewHolder.tv_duration.setText(Html.fromHtml(providerList.get(i).getDistance() + " " + next));


            try {

                Picasso.get().load(Global.IMG_URL + providerList.get(i).getProfile_pic()).placeholder(restarantActivity.getResources().getDrawable(R.drawable.place_holder)).fit().into(viewHolder.img_food);
            } catch (Exception e) {
                e.printStackTrace();
            }
            viewHolder.tv_restarant_name.setText(providerList.get(i).getName());


            viewHolder.tv_dish_name.setText(val);


        }


        viewHolder.restarant_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (service_type != null) {


                    PrefConnect.writeString(restarantActivity, Global.RESTARANT_NAME, restarantDetailsLists.get(i).getName());
                    PrefConnect.writeString(restarantActivity, Global.RESTARANT_PIC, restarantDetailsLists.get(i).getProfile_pic());
                    PrefConnect.writeString(restarantActivity, Global.RESTARANT_ID, restarantDetailsLists.get(i).getId());
                    PrefConnect.writeString(restarantActivity, Global.RESTARANT_ADDRESS, restarantDetailsLists.get(i).getAddress());
                    PrefConnect.writeString(restarantActivity, Global.RESTARANT_MOBILE, restarantDetailsLists.get(i).getPhone());
                } else {
                    PrefConnect.writeString(restarantActivity, Global.RESTARANT_NAME, providerList.get(i).getName());
                    PrefConnect.writeString(restarantActivity, Global.RESTARANT_PIC, providerList.get(i).getProfile_pic());

                    PrefConnect.writeString(restarantActivity, Global.RESTARANT_ID, providerList.get(i).getId());

                    PrefConnect.writeString(restarantActivity, Global.RESTARANT_ADDRESS, providerList.get(i).getAddress());
                    PrefConnect.writeString(restarantActivity, Global.RESTARANT_MOBILE, providerList.get(i).getPhone());


                }


                Intent intent = new Intent(restarantActivity, RestarantDetailsActivity.class);
                if (service_type != null) {
                    intent.putExtra(Global.OPENING_CLOSING_TIME, restarantDetailsLists.get(i).getClosing_time());
                    intent.putExtra(Global.OPENING_CLOSING_TIME_TWO, restarantDetailsLists.get(i).getDistance());
                    intent.putExtra(Global.LAT, restarantDetailsLists.get(i).getLat());
                    intent.putExtra(Global.LANG, restarantDetailsLists.get(i).getLng());
                } else {
                    intent.putExtra(Global.OPENING_CLOSING_TIME, providerList.get(i).getClosing_time());
                    intent.putExtra(Global.OPENING_CLOSING_TIME_TWO, providerList.get(i).getDistance());
                    intent.putExtra(Global.LAT, providerList.get(i).getLat());
                    intent.putExtra(Global.LANG, providerList.get(i).getLng());
                }

                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(restarantActivity, (View) viewHolder.property_list_item, "profile");
                restarantActivity.startActivity(intent, options.toBundle());

            }
        });

        viewHolder.handy_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PrefConnect.writeString(restarantActivity, Global.CATEGORY_ID, service_list.get(i).getCategory_id());
                Intent intent = new Intent(restarantActivity, DateTimeActivity.class);
                restarantActivity.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        if (service_type != null) {
            if (service_type.equals("3")) {
                return service_list.size();

            } else {

                return restarantDetailsLists.size();
            }
        } else {
            return providerList.size();
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        AppCompatImageView img_food, img_handy;

        AppCompatTextView tv_restarant_name;
        AppCompatTextView tv_dish_name;
        AppCompatTextView tv_duration;
        AppCompatTextView tv_doller;
        AppCompatTextView tv_subservice_name;
        AppCompatTextView tv_rating;

        LinearLayout restarant_layout, handy_layout;

        CardView property_list_item;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_food = itemView.findViewById(R.id.img_food);
            property_list_item = itemView.findViewById(R.id.property_list_item);
            img_handy = itemView.findViewById(R.id.img_handy);
            tv_dish_name = itemView.findViewById(R.id.tv_dish_name);
            tv_subservice_name = itemView.findViewById(R.id.tv_subservice_name);
            tv_duration = itemView.findViewById(R.id.tv_duration);
            tv_restarant_name = itemView.findViewById(R.id.tv_restarant_name);
            tv_doller = itemView.findViewById(R.id.tv_doller);
            restarant_layout = itemView.findViewById(R.id.restarant_layout);
            handy_layout = itemView.findViewById(R.id.handy_layout);
            tv_rating = itemView.findViewById(R.id.tv_rating);
        }
    }
}
