package com.dratoo_user.ui.main.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.main.model.RideResponse;
import com.dratoo_user.ui.updateprofile.model.ProfileUpdateResponse;

public interface MainView extends BaseView {

    void sucessGetProfile(ProfileUpdateResponse profileUpdateResponse);

    void sucessRideResponse(RideResponse rideResponse);

    void failure(String msg);

}
