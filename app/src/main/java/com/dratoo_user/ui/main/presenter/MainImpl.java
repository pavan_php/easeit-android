package com.dratoo_user.ui.main.presenter;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.main.model.RideResponse;
import com.dratoo_user.ui.updateprofile.model.ProfileUpdateResponse;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainImpl extends BasePresenter<MainView> implements MainPresenter {

    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;


    @Override
    public void attachView(MainView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void getProfile() {
        Call<ProfileUpdateResponse> call = apiInterface.getProfile();
        call.enqueue(new Callback<ProfileUpdateResponse>() {
            @Override
            public void onResponse(@NotNull Call<ProfileUpdateResponse> call, @NotNull Response<ProfileUpdateResponse> response) {
                if (response.isSuccessful()) {
                    getView().sucessGetProfile(response.body());
                }
            }

            @Override
            public void onFailure(@NotNull Call<ProfileUpdateResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void getRideStatus() {
        getView().showProgress();

        Call<RideResponse> call = apiInterface.getRideStatus();
        call.enqueue(new Callback<RideResponse>() {
            @Override
            public void onResponse(@NotNull Call<RideResponse> call, @NotNull Response<RideResponse> response) {
                getView().hideProgress();
                if (response.isSuccessful()) {
                    getView().sucessRideResponse(response.body());
                } else if (response.code() == 401) {
                    getView().failure("Unauthorize access");
                }
            }

            @Override
            public void onFailure(@NotNull Call<RideResponse> call, Throwable t) {
                getView().hideProgress();
            }
        });
    }
}
