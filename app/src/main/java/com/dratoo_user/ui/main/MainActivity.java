package com.dratoo_user.ui.main;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.PagerAdapter;

import com.dratoo_user.R;
import com.dratoo_user.app.App;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.common.ConnectivityReceiver;
import com.dratoo_user.ui.common.ProgressDialog;
import com.dratoo_user.ui.common.ServiceOnClick;
import com.dratoo_user.ui.common.ServicesBottomFragment;
import com.dratoo_user.ui.common.ServicesFragmentCallBack;
import com.dratoo_user.ui.foodhome.FoodHomeActivity;
import com.dratoo_user.ui.lookingdriverbooking.LookingDriverBookingActivity;
import com.dratoo_user.ui.main.model.RideResponse;
import com.dratoo_user.ui.main.presenter.MainPresenter;
import com.dratoo_user.ui.main.presenter.MainView;
import com.dratoo_user.ui.map.MapActivity;
import com.dratoo_user.ui.ongoingorder.OnGoingFragment;
import com.dratoo_user.ui.updateprofile.UpdateProfileFragment;
import com.dratoo_user.ui.updateprofile.model.ProfileUpdateResponse;
import com.dratoo_user.utils.AutoScrollViewPager;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements ServiceOnClick, ServicesFragmentCallBack, MainView, ConnectivityReceiver.ConnectivityReceiverListener {
    @Inject
    MainPresenter presenter;
    @BindView(R.id.editText_search)
    AppCompatEditText editTextSearch;
    @BindView(R.id.searchLay)
    LinearLayout searchLay;
    @BindView(R.id.viewpager)
    AutoScrollViewPager viewpager;
    @BindView(R.id.viewpager1)
    AutoScrollViewPager viewpager1;
    @BindView(R.id.iv_banner)
    ImageView ivBanner;
    @BindView(R.id.iv_banner1)
    ImageView ivBanner1;
    @BindView(R.id.taxi_layout)
    LinearLayout taxiLayout;
    @BindView(R.id.courier_layout)
    LinearLayout courierLayout;
    @BindView(R.id.carga_layout)
    LinearLayout cargaLayout;
    @BindView(R.id.food_layout)
    LinearLayout foodLayout;
    @BindView(R.id.mart_layout)
    LinearLayout martLayout;
    @BindView(R.id.handy_layout)
    LinearLayout handyLayout;
    @BindView(R.id.more_layout)
    LinearLayout moreLayout;
    @BindView(R.id.iv_image)
    AppCompatImageView ivImage;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;
    @BindView(R.id.normal_lay)
    LinearLayout normalLay;
    @BindView(R.id.frame)
    FrameLayout frame;
    @BindView(R.id.top_layout)
    LinearLayout topLayout;
    @BindView(R.id.bottom_layout)
    LinearLayout bottomLayout;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.moto_layout)
    LinearLayout motoLayout;
    @BindView(R.id.parant_layout)
    RelativeLayout parantLayout;
    @BindView(R.id.felht)
    AppCompatTextView felHT;
    @BindView(R.id.iv_discount)
    AppCompatImageView ivDiscount;
    @BindView(R.id.iv_cart)
    AppCompatImageView ivCart;
    private ArrayList<String> coupenList = new ArrayList<>();
    private ServicesBottomFragment servicesBottomFragment;
    private FragmentManager fragmentManager;
    private ProgressDialog progressDialog;
    private Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        presenter.attachView(this);

        progressDialog = new ProgressDialog(this);

        coupenList.add("http://php.manageprojects.in/easeit/uploads/PHOTO-1.jpg");
        coupenList.add("http://php.manageprojects.in/easeit/uploads/PHOTO-2.jpg");
        coupenList.add("http://php.manageprojects.in/easeit/uploads/PHOTO-3.jpg");

        viewpager.startAutoScroll(6000);
        viewpager.setInterval(4000);
        viewpager.setAutoScrollDurationFactor(5.0d);

        viewpager1.startAutoScroll(6000);
        viewpager1.setInterval(4000);
        viewpager1.setAutoScrollDurationFactor(5.0d);
        setViewPager(coupenList);

        editTextSearch.setOnEditorActionListener((v, id, event) -> {
            if (id == EditorInfo.IME_ACTION_SEARCH || id == EditorInfo.IME_NULL) {
                if (editTextSearch.getText().toString().contains("taxi")) {
                    PrefConnect.writeString(this, Global.SERVICE_TYPE, "4");
                    startActivity(new Intent(this, MapActivity.class));
                } else if (editTextSearch.getText().toString().contains("moto")) {
                    PrefConnect.writeString(this, Global.SERVICE_TYPE, "7");
                    startActivity(new Intent(this, MapActivity.class));
                } else if (editTextSearch.getText().toString().contains("parcel")) {
                    PrefConnect.writeString(this, Global.SERVICE_TYPE, "5");
                    startActivity(new Intent(this, MapActivity.class));
                } else if (editTextSearch.getText().toString().contains("delivery")) {
                    PrefConnect.writeString(this, Global.SERVICE_TYPE, "6");
                    startActivity(new Intent(this, MapActivity.class));
                } else if (editTextSearch.getText().toString().contains("food")) {
                    PrefConnect.writeString(this, Global.SERVICE_TYPE, "1");
                    startActivity(new Intent(this, FoodHomeActivity.class));
                } else if (editTextSearch.getText().toString().contains("grocery")) {
                    PrefConnect.writeString(this, Global.SERVICE_TYPE, "2");
                    startActivity(new Intent(this, FoodHomeActivity.class));
                } else if (editTextSearch.getText().toString().contains("service")) {
                    PrefConnect.writeString(this, Global.SERVICE_TYPE, "3");
                    startActivity(new Intent(this, FoodHomeActivity.class));
                } else if (editTextSearch.getText().toString().contains("repair")) {

                } else if (editTextSearch.getText().toString().contains("order")) {
                    fragmentManager.beginTransaction().replace(R.id.frame, new OnGoingFragment()).commit();
                } else if (editTextSearch.getText().toString().contains("help")) {
                    showDialogSoon();
                } else if (editTextSearch.getText().toString().contains("account")) {
                    fragmentManager.beginTransaction().replace(R.id.frame, new UpdateProfileFragment()).commit();
                } else if (editTextSearch.getText().toString().contains("cargo")) {
                    PrefConnect.writeString(this, Global.SERVICE_TYPE, "6");
                    startActivity(new Intent(this, MapActivity.class));
                }
                return true;
            }
            return false;
        });

        presenter.getProfile();

        // set login success local save
        PrefConnect.writeString(this, Global.LOGIN, "1");

        initBottomNavigationView();

        if (getIntent() != null) {
            if (getIntent().getStringExtra(Global.COMPLETED) != null) {
                if (getIntent().getStringExtra(Global.COMPLETED).equals("1")) {
                    frame.setVisibility(View.VISIBLE);
                    searchLay.setVisibility(View.GONE);
                    scrollView.setVisibility(View.GONE);
                    OnGoingFragment onGoingFragment = new OnGoingFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(Global.GET_VALUE, PrefConnect.readString(this, Global.SERVICE_TYPE, ""));
                    onGoingFragment.setArguments(bundle);
                    fragmentManager.beginTransaction().replace(R.id.frame, onGoingFragment).commit();
                    bottomNavigation.getMenu().findItem(R.id.action_orders).setChecked(true);

                    LinearLayout Rl = findViewById(R.id.searchLay);

                    RelativeLayout.LayoutParams layout_description = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
                    Rl.setLayoutParams(layout_description);
//                    Rl.setBackgroundColor(Color.parseColor("#FDFDFE"));

                    normalLay.setVisibility(View.VISIBLE);
                }
            } /*else if (getIntent().getStringExtra(Global.FROM_RIDE) != null) {

                if (getIntent().getStringExtra(Global.FROM_RIDE).equals("1")) {
                    presenter.getRideStatus();
                }
            }*/ else {
                // enter once we use show the animation only once
                if (!PrefConnect.readBoolean(MainActivity.this, Global.ENTERONCE, false)) {
                    animationStart();

                } else {
                    LinearLayout Rl = findViewById(R.id.searchLay);
                    RelativeLayout.LayoutParams layout_description = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
                    Rl.setLayoutParams(layout_description);
//                    Rl.setBackgroundColor(Color.parseColor("#FDFDFE"));
                    normalLay.setVisibility(View.VISIBLE);
                }
                scrollView.setVisibility(View.VISIBLE);
                searchLay.setVisibility(View.VISIBLE);
                frame.setVisibility(View.GONE);
            }
        }

        AppCompatImageView dratoHome = findViewById(R.id.dratohome);
        dratoHome.setOnClickListener(view -> {
            felHT.setText(R.string.app_name);
            scrollView.setVisibility(View.VISIBLE);
            searchLay.setVisibility(View.VISIBLE);
            frame.setVisibility(View.GONE);
        });

        AppCompatImageView dratoOrder = findViewById(R.id.dratoorders);
        dratoOrder.setOnClickListener(view -> {
            felHT.setText("ONGOING ORDERS");
            frame.setVisibility(View.VISIBLE);
            searchLay.setVisibility(View.GONE);
            scrollView.setVisibility(View.GONE);
            OnGoingFragment onGoingFragment = new OnGoingFragment();
            fragmentManager.beginTransaction().replace(R.id.frame, onGoingFragment).commit();
        });

        AppCompatImageView dratoAccount = findViewById(R.id.dratoaccount);
        dratoAccount.setOnClickListener(view -> {
            felHT.setText("ACCOUNT");
            frame.setVisibility(View.VISIBLE);
            searchLay.setVisibility(View.GONE);
            scrollView.setVisibility(View.GONE);
            UpdateProfileFragment orderTrackingMenu = new UpdateProfileFragment();
            fragmentManager.beginTransaction().replace(R.id.frame, orderTrackingMenu).commit();
        });
    }

    private void animationStart() {
        PrefConnect.writeBoolean(this, Global.ENTERONCE, true);
        CharSequence sequence = getResources().getString(R.string.welcome_sequence_drato);
        TapTargetView.showFor(this, TapTarget.forView(findViewById(R.id.editText_search), getResources().getString(R.string.welcome_drato), sequence)
                .outerCircleColor(R.color.colorPrimary)
                .targetCircleColor(R.color.colorPrimary)
                .transparentTarget(true)
                .textColor(android.R.color.black)
                .textColor(android.R.color.black), new TapTargetView.Listener() {
            @Override
            public void onTargetClick(TapTargetView view) {
                super.onTargetClick(view);

                LinearLayout Rl = findViewById(R.id.searchLay);
                RelativeLayout.LayoutParams layout_description = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                Rl.setLayoutParams(layout_description);
//                Rl.setBackgroundColor(Color.parseColor("#FDFDFE"));

                normalLay.setVisibility(View.VISIBLE);
            }

            @Override
            public void onOuterCircleClick(TapTargetView view) {
                super.onOuterCircleClick(view);
                LinearLayout Rl = findViewById(R.id.searchLay);
                RelativeLayout.LayoutParams layout_description = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                Rl.setLayoutParams(layout_description);
//                Rl.setBackgroundColor(Color.parseColor("#FDFDFE"));
                normalLay.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
                Log.d("TapTargetViewSample", "You dismissed me :(");

                LinearLayout Rl = findViewById(R.id.searchLay);
                RelativeLayout.LayoutParams layout_description = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                Rl.setLayoutParams(layout_description);
//                Rl.setBackgroundColor(Color.parseColor("#FDFDFE"));
                normalLay.setVisibility(View.VISIBLE);
            }
        });
    }

    private void initBottomNavigationView() {
        fragmentManager = getSupportFragmentManager();
        bottomNavigation.setOnNavigationItemSelectedListener(menuItem -> {
            int id = menuItem.getItemId();
            switch (id) {
                case R.id.action_home:
                    scrollView.setVisibility(View.VISIBLE);
                    searchLay.setVisibility(View.VISIBLE);
                    frame.setVisibility(View.GONE);
                    break;
                case R.id.action_orders:
                    frame.setVisibility(View.VISIBLE);
                    searchLay.setVisibility(View.GONE);
                    scrollView.setVisibility(View.GONE);
                    fragmentManager.beginTransaction().replace(R.id.frame, new OnGoingFragment()).commit();
                    break;
                case R.id.action_help:
                    showDialogSoon();
                    break;
                case R.id.action_profile:
                    frame.setVisibility(View.VISIBLE);
                    searchLay.setVisibility(View.GONE);
                    scrollView.setVisibility(View.GONE);
                    UpdateProfileFragment orderTrackingMenu = new UpdateProfileFragment();
                    fragmentManager.beginTransaction().replace(R.id.frame, new UpdateProfileFragment()).commit();
                    break;
            }
            return true;
        });
    }

    private void setViewPager(ArrayList<String> propertyImagesArrayList) {
        try {
            if (propertyImagesArrayList.size() > 0) {
                ViewPagerAdapter adapter = new ViewPagerAdapter(MainActivity.this, propertyImagesArrayList);
                viewpager.setAdapter(adapter);

                ArrayList<String> stringList = new ArrayList<>();
                stringList.add("http://php.manageprojects.in/easeit/uploads/PHOTO-4.jpg");
                stringList.add("http://php.manageprojects.in/easeit/uploads/PHOTO-5.jpg");
                stringList.add("http://php.manageprojects.in/easeit/uploads/PHOTO-6.jpg");

                ViewPagerAdapter adapter1 = new ViewPagerAdapter(MainActivity.this, stringList);
                viewpager1.setAdapter(adapter1);
                Picasso.get().load("https://readingexecutivetaxis.files.wordpress.com/2014/09/taxi-banner.png").into(ivBanner);
                Picasso.get().load("http://www.panthercab.com/uploads/1509509707.jpg").into(ivBanner1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onServicesFragmentDismiss() {
        servicesBottomFragment.dismiss();
    }

    @Override
    public void onServiceClick() {
        servicesBottomFragment = new ServicesBottomFragment(this, this);
        servicesBottomFragment.showNow(getSupportFragmentManager(), "servicesFragment");
    }

    @OnClick({R.id.iv_cart, R.id.iv_discount, R.id.taxi_layout, R.id.courier_layout, R.id.carga_layout, R.id.food_layout, R.id.mart_layout, R.id.handy_layout, R.id.moto_layout, R.id.more_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_cart:
            case R.id.iv_discount:
                Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
                break;
            case R.id.taxi_layout:
                PrefConnect.writeString(this, Global.SERVICE_TYPE, "4");
                startActivity(new Intent(this, MapActivity.class));
                break;
            case R.id.courier_layout:
                PrefConnect.writeString(this, Global.SERVICE_TYPE, "5");
                startActivity(new Intent(this, MapActivity.class));
                break;
            case R.id.carga_layout:
                PrefConnect.writeString(this, Global.SERVICE_TYPE, "6");
//                startActivity(new Intent(this, MapActivity.class));
                Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
                break;
            case R.id.food_layout:
                // set service type to detect which service you have to select because use same ui with different functionality
                PrefConnect.writeString(this, Global.SERVICE_TYPE, "1");
                startActivity(new Intent(this, FoodHomeActivity.class));
                break;
            case R.id.mart_layout:
                PrefConnect.writeString(this, Global.SERVICE_TYPE, "2");
                startActivity(new Intent(this, FoodHomeActivity.class));
                break;
            case R.id.handy_layout:
                PrefConnect.writeString(this, Global.SERVICE_TYPE, "3");
                startActivity(new Intent(this, FoodHomeActivity.class));
                break;
            case R.id.moto_layout:
                PrefConnect.writeString(this, Global.SERVICE_TYPE, "7");
                startActivity(new Intent(this, MapActivity.class));
                break;
            case R.id.more_layout:
//                onServiceClick();
                Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void showDialogSoon() {
        dialog = new Dialog(this, R.style.SlideTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.comming_soon_layout);
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        Toolbar toolbar = dialog.findViewById(R.id.toolbar);
        toolbar.setTitle("");

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.close);
        toolbar.setNavigationOnClickListener(v -> dialog.dismiss());
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (bottomNavigation.getSelectedItemId() == R.id.action_home) {
            finishAffinity();
            if (bottomNavigation.getSelectedItemId() == R.id.action_home) {
                finishAffinity();
            } else {
                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                startActivity(intent);
            }
        }
    }

    @Override
    public void hideProgress() {
        progressDialog.dismissLoader();
    }

    @Override
    public void sucessGetProfile(ProfileUpdateResponse profileUpdateResponse) {
        PrefConnect.writeString(this, Global.NAME, profileUpdateResponse.getData().getName());
        PrefConnect.writeString(this, Global.MOBILE_NBR, profileUpdateResponse.getData().getPhone());
        PrefConnect.writeString(this, Global.EMAIL, profileUpdateResponse.getData().getEmail());

        // get ride status of taxi , courier , cargo
        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("4") || PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("5") || PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("6") || PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("7")) {
            presenter.getRideStatus();
        }
    }

    @Override
    public void sucessRideResponse(RideResponse rideResponse) {
        if (rideResponse.getStatus().equals("true")) {
            if (rideResponse.getService_type().equals("4") || rideResponse.getService_type().equals("5") || rideResponse.getService_type().equals("6") || rideResponse.getService_type().equals("7")) {
                if (rideResponse.getData() != null) {
                    if (rideResponse.getData().getRequest_status().equals("1")) {
                        PrefConnect.writeString(this, Global.CURRNTRQID, rideResponse.getData().getRequest_id());
                        try {
                            Intent intent = new Intent(this, LookingDriverBookingActivity.class);
                            intent.putExtra(Global.CHECKSTATUS, "1");
                            intent.putExtra(Global.RIDE_RESPONSE, rideResponse);
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (rideResponse.getData().getRequest_status().equals("2")) {
                        PrefConnect.writeString(this, Global.CURRNTRQID, rideResponse.getData().getRequest_id());
                        try {
                            Intent intent = new Intent(this, LookingDriverBookingActivity.class);
                            intent.putExtra(Global.CHECKSTATUS, "2");
                            intent.putExtra(Global.RIDE_RESPONSE, rideResponse);
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (rideResponse.getData().getRequest_status().equals("3")) {
                        if (rideResponse.getData().getIs_paid().equals("0")) {
                            try {
                                Intent intent = new Intent(this, LookingDriverBookingActivity.class);
                                intent.putExtra(Global.CHECKSTATUS, "3");
                                intent.putExtra(Global.RIDE_RESPONSE, rideResponse);
                                startActivity(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (rideResponse.getData().getIs_paid().equals("1") || rideResponse.getData().getRate_provider().equals("0")) {
                            try {
                                Intent intent = new Intent(this, LookingDriverBookingActivity.class);
                                intent.putExtra(Global.CHECKSTATUS, "3");
                                intent.putExtra(Global.RIDE_RESPONSE, rideResponse);
                                startActivity(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void failure(String msg) {
        hideProgress();
    }

    @Override
    protected void onStart() {
        super.onStart();
        App.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        if (isConnected) {
            message = getResources().getString(R.string.connected_to_internet);
        } else {
            message = getResources().getString(R.string.not_connected_to_internet);
        }
        Global.snack_bar(parantLayout, message, Snackbar.LENGTH_LONG, MainActivity.this);
    }

    public class ViewPagerAdapter extends PagerAdapter {
        MainActivity mainActivity;
        LayoutInflater layoutInflater;
        ArrayList<String> mImagesList;

        public ViewPagerAdapter(MainActivity individualPropertyActivity, ArrayList<String> mImagesList) {
            this.mainActivity = individualPropertyActivity;
            this.mImagesList = mImagesList;
            Log.e("Nive ", "ViewPagerAdapter: " + mImagesList);
            layoutInflater = (LayoutInflater) mainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mImagesList.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, final int position) {
            View itemView = layoutInflater.inflate(R.layout.item_slider, container, false);
            AppCompatImageView imageView = itemView.findViewById(R.id.iv_image);
            try {
                if (mImagesList.get(position) != null) {
                    Picasso.get().load(mImagesList.get(position)).into(imageView);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((RelativeLayout) object);
        }
    }
}