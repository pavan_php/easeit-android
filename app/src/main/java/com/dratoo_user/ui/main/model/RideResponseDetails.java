package com.dratoo_user.ui.main.model;

import java.io.Serializable;

public class RideResponseDetails implements Serializable {


    public String request_status;
    public String user_id;
    public String provider_id;
    public String user_name;
    public String user_mobile;
    public String request_id;
    public String payment_type;
    public String price;
    public String tax;
    public String pickup_lat;
    public String pickup_long;

    public String getCreated_at1() {
        return created_at1;
    }

    public void setCreated_at1(String created_at1) {
        this.created_at1 = created_at1;
    }

    public String des_lat;
    public String rate_user;
    public String created_at1;
    public String fname;

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String lname;

    public String getRate_user() {
        return rate_user;
    }

    public void setRate_user(String rate_user) {
        this.rate_user = rate_user;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String created_at;

    public String getId_proof() {
        return id_proof;
    }

    public void setId_proof(String id_proof) {
        this.id_proof = id_proof;
    }

    public String id_proof;

    public String getPickup_lat() {
        return pickup_lat;
    }

    public void setPickup_lat(String pickup_lat) {
        this.pickup_lat = pickup_lat;
    }

    public String getPickup_long() {
        return pickup_long;
    }

    public void setPickup_long(String pickup_long) {
        this.pickup_long = pickup_long;
    }

    public String getDes_lat() {
        return des_lat;
    }

    public void setDes_lat(String des_lat) {
        this.des_lat = des_lat;
    }

    public String getDes_long() {
        return des_long;
    }

    public void setDes_long(String des_long) {
        this.des_long = des_long;
    }

    public String des_long;

    @Override
    public String toString() {
        return "MenuResponseDetails{" +
                "request_status='" + request_status + '\'' +
                ", user_id='" + user_id + '\'' +
                ", provider_id='" + provider_id + '\'' +
                ", user_name='" + user_name + '\'' +
                ", user_mobile='" + user_mobile + '\'' +
                ", request_id='" + request_id + '\'' +
                ", payment_type='" + payment_type + '\'' +
                ", price='" + price + '\'' +
                ", tax='" + tax + '\'' +
                ", bill_amount='" + bill_amount + '\'' +
                ", distance_km='" + distance_km + '\'' +
                ", pickup_address='" + pickup_address + '\'' +
                ", dest_address='" + dest_address + '\'' +
                ", rate_provider='" + rate_provider + '\'' +
                ", is_paid='" + is_paid + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", vehicle_number='" + vehicle_number + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public String bill_amount;
    public String distance_km;
    public String pickup_address;
    public String dest_address;
    public String rate_provider;
    public String is_paid;
    public String name;

    public String getRequest_status() {
        return request_status;
    }

    public void setRequest_status(String request_status) {
        this.request_status = request_status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(String provider_id) {
        this.provider_id = provider_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_mobile() {
        return user_mobile;
    }

    public void setUser_mobile(String user_mobile) {
        this.user_mobile = user_mobile;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getBill_amount() {
        return bill_amount;
    }

    public void setBill_amount(String bill_amount) {
        this.bill_amount = bill_amount;
    }

    public String getDistance_km() {
        return distance_km;
    }

    public void setDistance_km(String distance_km) {
        this.distance_km = distance_km;
    }

    public String getPickup_address() {
        return pickup_address;
    }

    public void setPickup_address(String pickup_address) {
        this.pickup_address = pickup_address;
    }

    public String getDest_address() {
        return dest_address;
    }

    public void setDest_address(String dest_address) {
        this.dest_address = dest_address;
    }

    public String getRate_provider() {
        return rate_provider;
    }

    public void setRate_provider(String rate_provider) {
        this.rate_provider = rate_provider;
    }

    public String getIs_paid() {
        return is_paid;
    }

    public void setIs_paid(String is_paid) {
        this.is_paid = is_paid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVehicle_number() {
        return vehicle_number;
    }

    public void setVehicle_number(String vehicle_number) {
        this.vehicle_number = vehicle_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String phone;
    public String vehicle_number;
    public String email;
}
