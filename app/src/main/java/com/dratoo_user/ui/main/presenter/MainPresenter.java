package com.dratoo_user.ui.main.presenter;

import com.dratoo_user.base.Presenter;

public interface MainPresenter extends Presenter<MainView> {
    void getProfile();

    void getRideStatus();
}
