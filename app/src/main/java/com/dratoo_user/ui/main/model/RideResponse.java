package com.dratoo_user.ui.main.model;

import java.io.Serializable;

public class RideResponse implements Serializable {

    public RideResponseDetails getData() {
        return data;
    }

    public void setData(RideResponseDetails data) {
        this.data = data;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    @Override
    public String toString() {
        return "MenuListResponse{" +
                "data=" + data +
                ", service_type='" + service_type + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public RideResponseDetails data;
    public String service_type;
    public String status;
    public String message;
}
