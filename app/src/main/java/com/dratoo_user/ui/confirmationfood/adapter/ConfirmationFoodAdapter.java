package com.dratoo_user.ui.confirmationfood.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.ui.confirmationfood.ConfirmationDetailsActivity;
import com.dratoo_user.ui.confirmationfood.model.FinalOrderItems;

import java.util.ArrayList;

public class ConfirmationFoodAdapter extends RecyclerView.Adapter<ConfirmationFoodAdapter.ViewHolder> {


    ArrayList<FinalOrderItems> finalOrderItems;

    ConfirmationDetailsActivity confirmationDetailsActivity;

    public ConfirmationFoodAdapter(ConfirmationDetailsActivity confirmationDetailsActivity, ArrayList<FinalOrderItems> finalOrderItems) {

        this.confirmationDetailsActivity = confirmationDetailsActivity;
        this.finalOrderItems = finalOrderItems;

        Log.e("Nive ", "ConfirmationFoodAdapter: " + finalOrderItems.size());

    }

    @NonNull
    @Override
    public ConfirmationFoodAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.restarant_individual_content_items, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ConfirmationFoodAdapter.ViewHolder viewHolder, int position) {

        viewHolder.btn_add.setVisibility(View.GONE);
        viewHolder.add_card_view_items.setVisibility(View.VISIBLE);

        try {
            Picasso.get().load(Global.IMG_URL + finalOrderItems.get(position).getImage()).placeholder(confirmationDetailsActivity.getResources().getDrawable(R.drawable.place_holder)).fit().into(viewHolder.img_food);
        } catch (Exception e) {
            e.printStackTrace();
        }


        viewHolder.tv_restarant_name.setText(finalOrderItems.get(position).getItem_name());

        viewHolder.tv_description.setText(finalOrderItems.get(position).getDescription());

        viewHolder.tv_price.setText(PrefConnect.readString(confirmationDetailsActivity, Global.CURRENCY, "") + " " + finalOrderItems.get(position).getPrice());
        viewHolder.tv_cart_count.setText(finalOrderItems.get(position).getQuantity());


    }

    @Override
    public int getItemCount() {
        return finalOrderItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView add_card_view_items;

        @Nullable
        AppCompatButton btn_add;

        @Nullable
        AppCompatImageView img_food;

        @Nullable
        AppCompatTextView tv_restarant_name;

        @Nullable
        AppCompatTextView tv_description;

        @Nullable
        AppCompatTextView tv_price;

        @Nullable
        AppCompatTextView tv_cart_count;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btn_add = itemView.findViewById(R.id.btn_add);
            add_card_view_items = itemView.findViewById(R.id.add_card_view_items);

            img_food = itemView.findViewById(R.id.img_food);
            tv_restarant_name = itemView.findViewById(R.id.tv_restarant_name);
            tv_description = itemView.findViewById(R.id.tv_description);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_cart_count = itemView.findViewById(R.id.tv_cart_count);

        }
    }
}
