package com.dratoo_user.ui.confirmationfood.model;

import java.util.ArrayList;

public class MartCartResponse {

    public String tax;
    public String mart_fare;
    public String delivery_fare;
    public String bill_amount;
    public String request_id;

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getMart_fare() {
        return mart_fare;
    }

    public void setMart_fare(String mart_fare) {
        this.mart_fare = mart_fare;
    }

    public String getDelivery_fare() {
        return delivery_fare;
    }

    public void setDelivery_fare(String delivery_fare) {
        this.delivery_fare = delivery_fare;
    }

    public String getBill_amount() {
        return bill_amount;
    }

    public void setBill_amount(String bill_amount) {
        this.bill_amount = bill_amount;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public ArrayList<FinalOrderItems> data;

    public ArrayList<FinalOrderItems> getData() {
        return data;
    }

    public void setData(ArrayList<FinalOrderItems> data) {
        this.data = data;
    }
}
