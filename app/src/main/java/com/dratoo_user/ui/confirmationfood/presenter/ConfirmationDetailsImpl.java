package com.dratoo_user.ui.confirmationfood.presenter;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentScucessResponse;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentSucessRequest;
import com.dratoo_user.ui.confirmationfood.model.FoodConfirmationResponse;
import com.dratoo_user.ui.confirmationfood.model.MartCartResponse;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodConfirmModel;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodConfirmResponse;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodModel;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodResponse;
import com.dratoo_user.ui.restarantdetails.model.FoodModelRequest;


import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmationDetailsImpl extends BasePresenter<ConfirmationDetailsView> implements ConfirmationDetailsPresenter {


    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;

    @Override
    public void attachView(ConfirmationDetailsView view) {
        super.attachView(view);

        DaggerAppComponent.create().inject(this);
    }


    @Override
    public void sendSucessRequest(PaymentSucessRequest paymentSucessRequest) {
        Call<PaymentScucessResponse> call = apiInterface.getSucessResultPayment(paymentSucessRequest);
        call.enqueue(new Callback<PaymentScucessResponse>() {
            @Override
            public void onResponse(Call<PaymentScucessResponse> call, Response<PaymentScucessResponse> response) {
                if (response.isSuccessful()) {
                    getView().paynowSucess();
                }
            }

            @Override
            public void onFailure(Call<PaymentScucessResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void foodDetailsConfirmation(FoodModelRequest foodModelRequest, String service_type) {

        getView().showProgress();

        if (service_type.equals("1")) {
            Call<FoodConfirmationResponse> call = apiInterface.getConfirmationDetails(foodModelRequest);
            call.enqueue(new Callback<FoodConfirmationResponse>() {
                @Override
                public void onResponse(Call<FoodConfirmationResponse> call, Response<FoodConfirmationResponse> response) {
                    getView().hideProgress();
                    if (response.isSuccessful()) {
                        getView().finalConfirmation(response.body());
                    }
                }

                @Override
                public void onFailure(Call<FoodConfirmationResponse> call, Throwable t) {

                    getView().hideProgress();

                }
            });
        } else {

            Call<MartCartResponse> call = apiInterface.getMartList(foodModelRequest);
            call.enqueue(new Callback<MartCartResponse>() {
                @Override
                public void onResponse(Call<MartCartResponse> call, Response<MartCartResponse> response) {
                    getView().hideProgress();
                    if (response.isSuccessful()) {
                        getView().finalConfirmationMart(response.body());
                    }
                }

                @Override
                public void onFailure(Call<MartCartResponse> call, Throwable t) {

                    getView().hideProgress();

                }
            });

        }


    }

    @Override
    public void payment(PaymentFoodModel paymentFoodModel, String service_type) {
        getView().showProgress();

        Call<PaymentFoodResponse> call = null;

        if (service_type.equals("1")) {

            call = apiInterface.getPaymentDetails(paymentFoodModel);
        } else {
            call = apiInterface.getMarkPaymentDetails(paymentFoodModel);

        }


        call.enqueue(new Callback<PaymentFoodResponse>() {
            @Override
            public void onResponse(Call<PaymentFoodResponse> call, Response<PaymentFoodResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().sucessPaymet(response.body());
                }
            }

            @Override
            public void onFailure(Call<PaymentFoodResponse> call, Throwable t) {
                getView().hideProgress();

            }
        });
    }

    @Override
    public void PaymentDone(PaymentFoodConfirmModel paymentFoodConfirmModel, String service_type) {

        Call<PaymentFoodConfirmResponse> call = null;

        if (service_type.equals("1")) {

            call = apiInterface.getPaymentConfirm(paymentFoodConfirmModel);
        } else {
            call = apiInterface.getMartPayment(paymentFoodConfirmModel);

        }

        call.enqueue(new Callback<PaymentFoodConfirmResponse>() {
            @Override
            public void onResponse(Call<PaymentFoodConfirmResponse> call, Response<PaymentFoodConfirmResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().confirmPayment(response.body());
                }
            }

            @Override
            public void onFailure(Call<PaymentFoodConfirmResponse> call, Throwable t) {
                getView().hideProgress();
                getView().failurePayment("Response Failure");

            }
        });


    }
}
