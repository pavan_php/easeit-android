package com.dratoo_user.ui.confirmationfood;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.Constants;
import com.midtrans.sdk.corekit.core.LocalDataHandler;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme;
import com.midtrans.sdk.corekit.models.BankType;
import com.midtrans.sdk.corekit.models.TransactionResponse;
import com.midtrans.sdk.corekit.models.UserAddress;
import com.midtrans.sdk.corekit.models.UserDetail;
import com.midtrans.sdk.corekit.models.snap.CreditCard;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;
import com.dratoo_user.R;
import com.dratoo_user.app.App;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentSucessRequest;
import com.dratoo_user.ui.common.ConnectivityReceiver;
import com.dratoo_user.ui.common.ProgressDialog;
import com.dratoo_user.ui.confirmationfood.adapter.ConfirmationFoodAdapter;
import com.dratoo_user.ui.confirmationfood.model.FinalOrderItems;
import com.dratoo_user.ui.confirmationfood.model.FoodConfirmationResponse;
import com.dratoo_user.ui.confirmationfood.model.MartCartResponse;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodConfirmModel;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodConfirmResponse;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodModel;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodResponse;
import com.dratoo_user.ui.confirmationfood.presenter.ConfirmationDetailsPresenter;
import com.dratoo_user.ui.confirmationfood.presenter.ConfirmationDetailsView;
import com.dratoo_user.ui.ordertracking.OrderTrackingActivity;
import com.dratoo_user.ui.restarantdetails.RestarantDetailsActivity;
import com.dratoo_user.ui.restarantdetails.model.FoodModelRequest;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmationDetailsActivity extends AppCompatActivity implements ConfirmationDetailsView, TransactionFinishedCallback, ConnectivityReceiver.ConnectivityReceiverListener {


    @Inject
    ConfirmationDetailsPresenter presenter;
    @BindView(R.id.back)
    AppCompatImageView back;
    @BindView(R.id.tv_title)
    AppCompatTextView tvTitle;
    @BindView(R.id.top_layout)
    LinearLayout topLayout;
    @BindView(R.id.tv_delivery_location)
    AppCompatTextView tvDeliveryLocation;
    @BindView(R.id.rv_cart_lists)
    RecyclerView rvCartLists;
    @BindView(R.id.iv_cash)
    AppCompatImageView ivCash;
    @BindView(R.id.tv_vertical_dot)
    AppCompatTextView tvVerticalDot;
    @BindView(R.id.tv_pay)
    AppCompatTextView tvPay;
    @BindView(R.id.tv_price_estimated_layout)
    AppCompatTextView tvPriceEstimatedLayout;
    @BindView(R.id.tv_payment_mode)
    AppCompatTextView tvPaymentMode;
    @BindView(R.id.bottom_card_view)
    CardView bottomCardView;
    @BindView(R.id.btn_place_order)
    AppCompatButton btnPlaceOrder;
    @BindView(R.id.tv_estimated_price)
    AppCompatTextView tvEstimatedPrice;
    @BindView(R.id.tv_delivery_fare)
    AppCompatTextView tvDeliveryFare;
    @BindView(R.id.tv_total_payment)
    AppCompatTextView tvTotalPayment;
    @BindView(R.id.tv_final_cost)
    AppCompatTextView tvFinalCost;
    @BindView(R.id.tv_name)
    AppCompatTextView tvName;
    @BindView(R.id.tv_mbo_nbr)
    AppCompatTextView tvMboNbr;
    @BindView(R.id.tv_tax)
    AppCompatTextView tvTax;
    @BindView(R.id.tv_payment_type)
    AppCompatTextView tvPaymentType;
    @BindView(R.id.select_payment_type)
    LinearLayout selectPaymentType;
    @BindView(R.id.tv_pay_type)
    AppCompatButton tvPayType;
    @BindView(R.id.tv_total_amt)
    AppCompatTextView tvTotalAmt;
    @BindView(R.id.parant_layout)
    RelativeLayout parantLayout;
    @BindView(R.id.tv_add_more)
    AppCompatTextView tvAddMore;
    private ConfirmationFoodAdapter adapter;
    private Dialog dialog;

    private FoodModelRequest foodModelRequest;

    ProgressDialog progressDialog;
    private PaymentFoodResponse paymentFoodResponse;
    private ArrayList<FinalOrderItems> finalCartList;
    private BottomSheetDialog mBottomSheetDialog;
    private View bottomSheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation2);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        presenter.attachView(this);

        progressDialog = new ProgressDialog(this);


        // initally payment type is cash

        PrefConnect.writeString(ConfirmationDetailsActivity.this, Global.PAYMENT_TYPE, "1");

        if (getIntent() != null) {
            Intent intent = getIntent();
            foodModelRequest = (FoodModelRequest) intent.getSerializableExtra(Global.SENDMODEL);

        }

        if (foodModelRequest != null) {


            presenter.foodDetailsConfirmation(foodModelRequest, PrefConnect.readString(this, Global.SERVICE_TYPE, ""));

        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmationDetailsActivity.this, RestarantDetailsActivity.class);

                startActivity(intent);
            }
        });

        //initialize payment
        initMidtransSdk();


    }


    private void initMidtransSdk() {


        SdkUIFlowBuilder.init()
                .setClientKey(Global.client_key) // client_key is mandatory
                .setContext(this) // context is mandatory
                .setTransactionFinishedCallback(this) // set transaction finish callback (sdk callback)
                .setMerchantBaseUrl(Global.base_url_payment) //set merchant url
                .enableLog(true) // enable sdk log
                .setColorTheme(new CustomColorTheme("#eea500", "#e5e6ea", "#4c64b5")) // will replace theme on snap theme on MAP
                .buildSDK();
    }


    private TransactionRequest initTransactionRequest() {
        // Create new Transaction Request
        TransactionRequest transactionRequestNew = new
                TransactionRequest(System.currentTimeMillis() + "", Double.parseDouble(PrefConnect.readString(this, Global.FINAL_AMT, "")));

        Log.e("Nive ", "initTransactionRequest:FinalAmt " + Double.parseDouble(PrefConnect.readString(this, Global.FINAL_AMT, "")));
        Log.e("Nive ", "initTransactionRequest:FinalAmt " + PrefConnect.readString(this, Global.EMAIL, ""));

        // Set user details
        UserDetail userDetail = new UserDetail();
        userDetail.setUserFullName(PrefConnect.readString(ConfirmationDetailsActivity.this, Global.NAME, ""));
        userDetail.setEmail(PrefConnect.readString(ConfirmationDetailsActivity.this, Global.EMAIL, ""));
        userDetail.setPhoneNumber(PrefConnect.readString(ConfirmationDetailsActivity.this, Global.MOBILE_NBR, ""));
        userDetail.setUserId(PrefConnect.readString(this, Global.AUTH_ID, ""));

        ArrayList<UserAddress> userAddresses = new ArrayList<>();
        UserAddress userAddress = new UserAddress();
        userAddress.setAddress("indonesia");
        userAddress.setCity("jakarta");
        userAddress.setCountry("IDN");
        userAddress.setZipcode("86554");
        userAddress.setAddressType(Constants.ADDRESS_TYPE_BOTH);
        userAddresses.add(userAddress);

        userDetail.setUserAddresses(userAddresses);
        LocalDataHandler.saveObject("user_details", userDetail);

        Log.e("Nive ", "initTransactionRequest:userDetail " + new Gson().toJson(userDetail));


        // Create creditcard options for payment
        CreditCard creditCard = new CreditCard();
        creditCard.setSaveCard(false); // when using one/two click set to true and if normal set to false
        creditCard.setAuthentication(CreditCard.AUTHENTICATION_TYPE_3DS);
        creditCard.setBank(BankType.BCA); //set spesific acquiring bank
        transactionRequestNew.setCreditCard(creditCard);

        return transactionRequestNew;
    }


    private void callApi() {
        PaymentFoodModel paymentFoodModel = new PaymentFoodModel();
        paymentFoodModel.setName(tvName.getText().toString());
        paymentFoodModel.setPhone(tvMboNbr.getText().toString());
        paymentFoodModel.setDelivery_location(tvDeliveryLocation.getText().toString());
        paymentFoodModel.setRequest_id(PrefConnect.readString(ConfirmationDetailsActivity.this, Global.REQUESTID, ""));
        presenter.payment(paymentFoodModel, PrefConnect.readString(this, Global.SERVICE_TYPE, ""));
    }


    public void initBottomSheet() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        bottomSheet = this.getLayoutInflater().inflate(R.layout.bottomsheet_payments, null);
        mBottomSheetDialog.setContentView(bottomSheet);

        AppCompatTextView title = (AppCompatTextView) mBottomSheetDialog.findViewById(R.id.title);
        AppCompatTextView tv_header = (AppCompatTextView) mBottomSheetDialog.findViewById(R.id.tv_header);
        AppCompatImageView img_close = mBottomSheetDialog.findViewById(R.id.img_close);
        LinearLayout halo_pay_layout = mBottomSheetDialog.findViewById(R.id.halo_pay_layout);
        LinearLayout cash_layout = mBottomSheetDialog.findViewById(R.id.cash_layout);

        tv_header.setText(getResources().getString(R.string.select_payment));

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        cash_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefConnect.writeString(ConfirmationDetailsActivity.this, Global.PAYMENT_TYPE, "1");
                mBottomSheetDialog.dismiss();
                tvPaymentType.setText(getResources().getString(R.string.cash));
                tvPayType.setText(getResources().getString(R.string.cash));
            }
        });


        halo_pay_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  INFO: TODO

                tvPaymentType.setText(getResources().getString(R.string.card));
                tvPayType.setText("CARD");
                PrefConnect.writeString(ConfirmationDetailsActivity.this, Global.PAYMENT_TYPE, "2");
                mBottomSheetDialog.dismiss();


                /*// initiate payment

                MidtransSDK.getInstance().setTransactionRequest(initTransactionRequest());
                MidtransSDK.getInstance().startPaymentUiFlow(ConfirmationDetailsActivity.this);
*/
            }
        });


        mBottomSheetDialog.show();

    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {

        String message;

        if (isConnected) {
            message = getResources().getString(R.string.connected_to_internet);

        } else {
            message = getResources().getString(R.string.not_connected_to_internet);

        }

        Global.snack_bar(parantLayout, message, Snackbar.LENGTH_LONG, ConfirmationDetailsActivity.this);

    }


    @OnClick({R.id.tv_payment_mode, R.id.btn_place_order, R.id.select_payment_type, R.id.tv_add_more})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_payment_mode:
                break;
            case R.id.tv_add_more:
                Intent intent = new Intent(ConfirmationDetailsActivity.this, RestarantDetailsActivity.class);

                if (PrefConnect.readString(ConfirmationDetailsActivity.this, Global.SERVICE_TYPE, "").equals("1")) {

                    intent.putExtra(Global.ADDITEMS, "1");
                } else {
                    intent.putExtra(Global.ADDITEMS, "2");

                }
                startActivity(intent);
                break;
            case R.id.select_payment_type:

//                initBottomSheet();
                break;

            case R.id.btn_place_order:

                if (paymentFoodResponse != null) {
                    if (PrefConnect.readString(ConfirmationDetailsActivity.this, Global.PAYMENT_TYPE, "").equals("1")) {
                        PaymentFoodConfirmModel paymentFoodConfirmModel = new PaymentFoodConfirmModel();
                        paymentFoodConfirmModel.setPayment_type("1");
                        paymentFoodConfirmModel.setRequest_id(paymentFoodResponse.getRequest_id());

                        presenter.PaymentDone(paymentFoodConfirmModel, PrefConnect.readString(this, Global.SERVICE_TYPE, ""));
                    } else {

                        showDialogConfirmation();

                    }
                } else {

                }


                break;
        }
    }

    private void showDialogConfirmation() {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirmation);
        Window window = dialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);

        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        AppCompatButton btn_done = dialog.findViewById(R.id.btn_done);

        AppCompatImageView iv_pic = dialog.findViewById(R.id.iv_pic);

        if (PrefConnect.readString(this, Global.SERVICE_TYPE, " ").equals("1")) {
            iv_pic.setImageDrawable(getResources().getDrawable(R.drawable.dialog_food));
        } else {
            iv_pic.setImageDrawable(getResources().getDrawable(R.drawable.dialog_grocery));

        }

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ConfirmationDetailsActivity.this, OrderTrackingActivity.class);
                intent.putExtra(Global.REQUESTID_TRACKING, PrefConnect.readString(ConfirmationDetailsActivity.this, Global.REQUESTID, ""));
                startActivity(intent);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);

        dialog.show();
    }

    @Override
    public void showProgress() {
        progressDialog.show();

    }

    @Override
    public void hideProgress() {

        progressDialog.dismissLoader();

    }

    @Override
    public void finalConfirmation(FoodConfirmationResponse body) {

        if (body != null) {


            LinearLayoutManager layoutManager = new LinearLayoutManager(ConfirmationDetailsActivity.this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvCartLists.setLayoutManager(layoutManager);

            finalCartList = body.getData();


            Log.e("Nive ", "finalConfirmation: " + finalCartList.size());

            adapter = new ConfirmationFoodAdapter(this, finalCartList);
            rvCartLists.setAdapter(adapter);
            adapter.notifyDataSetChanged();


            tvDeliveryLocation.setText(PrefConnect.readString(this, Global.CURRENT_ADDRESS, ""));
            tvName.setText(PrefConnect.readString(this, Global.NAME, ""));
            tvMboNbr.setText(PrefConnect.readString(this, Global.MOBILE_NBR, ""));

            tvEstimatedPrice.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getFood_fare());
            tvDeliveryFare.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getDelivery_fare());
            tvTotalPayment.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getBill_amount());
            tvFinalCost.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getBill_amount());
            tvTotalAmt.setText(getResources().getString(R.string.tot_fare) + " : " + PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getBill_amount());

            PrefConnect.writeString(this, Global.FINAL_AMT, body.getBill_amount());

            tvTax.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getTax());

            PrefConnect.writeString(ConfirmationDetailsActivity.this, Global.REQUESTID, body.getRequest_id());


            callApi();
        }


    }

    @Override
    public void finalConfirmationMart(MartCartResponse body) {

        if (body != null) {


            LinearLayoutManager layoutManager = new LinearLayoutManager(ConfirmationDetailsActivity.this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvCartLists.setLayoutManager(layoutManager);

            finalCartList = body.getData();


            adapter = new ConfirmationFoodAdapter(this, finalCartList);
            rvCartLists.setAdapter(adapter);
            adapter.notifyDataSetChanged();


            tvDeliveryLocation.setText(PrefConnect.readString(this, Global.CURRENT_ADDRESS, ""));
            tvName.setText(PrefConnect.readString(this, Global.NAME, ""));
            tvMboNbr.setText(PrefConnect.readString(this, Global.MOBILE_NBR, ""));

            tvEstimatedPrice.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getMart_fare());
            tvDeliveryFare.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getDelivery_fare());
            tvTotalPayment.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getBill_amount());
            tvFinalCost.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getBill_amount());
            tvTotalAmt.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getBill_amount());

            tvTax.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + body.getTax());

            PrefConnect.writeString(this, Global.FINAL_AMT, body.getBill_amount());

            PrefConnect.writeString(ConfirmationDetailsActivity.this, Global.REQUESTID, body.getRequest_id());


            callApi();
        }


    }

    @Override
    public void confirmationFailed(String message) {

    }

    @Override
    public void sucessPaymet(PaymentFoodResponse body) {

        this.paymentFoodResponse = body;

    }

    @Override
    public void failurePayment(String message) {

    }

    @Override
    public void confirmPayment(PaymentFoodConfirmResponse body) {

        if (body.getStatus().equals("true")) {
            showDialogConfirmation();
        }


    }

    @Override
    public void paynowSucess() {

        selectPaymentType.setClickable(false);

    }


    private void callPaymentSucessApi(TransactionResponse response) {

        PaymentSucessRequest paymentSucessRequest = new PaymentSucessRequest();
        paymentSucessRequest.setApproval_code(response.getApprovalCode());
        paymentSucessRequest.setBank(response.getBank());
        paymentSucessRequest.setCurrency(response.getCurrency());
        paymentSucessRequest.setEci(response.getEci());
        paymentSucessRequest.setFinish_redirect_url(response.getFinishRedirectUrl());
        paymentSucessRequest.setFraud_status(response.getFraudStatus());
        paymentSucessRequest.setPayment_type(response.getPaymentType());
        paymentSucessRequest.setGross_amount(response.getGrossAmount());
        paymentSucessRequest.setOrder_id(response.getOrderId());
        paymentSucessRequest.setMasked_card(response.getMaskedCard());
        paymentSucessRequest.setPoint_redeem_amount(response.getPointBalanceAmount());
        paymentSucessRequest.setPayment_type(response.getPaymentType());
        paymentSucessRequest.setStatus_message(response.getStatusMessage());
        paymentSucessRequest.setTransaction_id(response.getTransactionId());
        paymentSucessRequest.setRequest_id(PrefConnect.readString(ConfirmationDetailsActivity.this, Global.REQUESTID, ""));
        paymentSucessRequest.setPay_type("2");
        paymentSucessRequest.setService_type(PrefConnect.readString(this, Global.SERVICE_TYPE, ""));


        presenter.sendSucessRequest(paymentSucessRequest);
    }

    @Override
    public void onTransactionFinished(TransactionResult result) {
        if (result.getResponse() != null) {
            switch (result.getStatus()) {
                case TransactionResult.STATUS_SUCCESS:

                    Log.e("Nive ", "onTransactionFinished: " + new Gson().toJson(result.getResponse()));
                    callPaymentSucessApi(result.getResponse());
                    break;
                case TransactionResult.STATUS_PENDING:
                    Toast.makeText(this, "Transaction Pending. ID: " + result.getResponse().getTransactionId(), Toast.LENGTH_LONG).show();
                    break;
                case TransactionResult.STATUS_FAILED:
                    Toast.makeText(this, "Transaction Failed. ID: " + result.getResponse().getTransactionId() + ". Message: " + result.getResponse().getStatusMessage(), Toast.LENGTH_LONG).show();
                    break;
            }
            result.getResponse().getValidationMessages();
        } else if (result.isTransactionCanceled()) {
            Toast.makeText(this, "Transaction Canceled", Toast.LENGTH_LONG).show();
        } else {
            if (result.getStatus().equalsIgnoreCase(TransactionResult.STATUS_INVALID)) {
                Toast.makeText(this, "Transaction Invalid", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Transaction Finished with failure.", Toast.LENGTH_LONG).show();
            }
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        App.getInstance().setConnectivityListener(this);


    }
}
