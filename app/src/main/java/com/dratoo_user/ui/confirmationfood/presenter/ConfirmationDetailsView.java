package com.dratoo_user.ui.confirmationfood.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.confirmationfood.model.FoodConfirmationResponse;
import com.dratoo_user.ui.confirmationfood.model.MartCartResponse;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodConfirmResponse;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodResponse;

public interface ConfirmationDetailsView extends BaseView {

    void finalConfirmation(FoodConfirmationResponse body);

    void finalConfirmationMart(MartCartResponse body);

    void confirmationFailed(String message);

    void sucessPaymet(PaymentFoodResponse body);

    void failurePayment(String message);

    void confirmPayment(PaymentFoodConfirmResponse body);

    void paynowSucess();
}
