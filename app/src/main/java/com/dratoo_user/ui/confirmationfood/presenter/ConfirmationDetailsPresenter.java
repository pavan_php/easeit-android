package com.dratoo_user.ui.confirmationfood.presenter;

import com.dratoo_user.base.Presenter;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentSucessRequest;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodConfirmModel;
import com.dratoo_user.ui.confirmationfood.model.PaymentFoodModel;
import com.dratoo_user.ui.restarantdetails.model.FoodModelRequest;


public interface ConfirmationDetailsPresenter extends Presenter<ConfirmationDetailsView> {

    void foodDetailsConfirmation(FoodModelRequest foodModelRequest, String service_type);

    void payment(PaymentFoodModel paymentFoodModel,String service_type);

    void PaymentDone(PaymentFoodConfirmModel paymentFoodConfirmModel,String service_type);

    void sendSucessRequest(PaymentSucessRequest paymentSucessRequest);
}
