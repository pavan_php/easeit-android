package com.dratoo_user.ui.confirmationfood.model;

/**
 * Created by ${Krishnaprakash} on 25-03-2018.
 */

public class PaymentFoodConfirmResponse {


    public String status;
    public String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
