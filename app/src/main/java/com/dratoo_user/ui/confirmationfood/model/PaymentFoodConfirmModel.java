package com.dratoo_user.ui.confirmationfood.model;

/**
 * Created by ${Krishnaprakash} on 25-03-2018.
 */

public class PaymentFoodConfirmModel {

    public String request_id;

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String payment_type;
}
