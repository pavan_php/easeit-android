package com.dratoo_user.ui.confirmationfood.model;

/**
 * Created by ${Krishnaprakash} on 25-03-2018.
 */

public class PaymentFoodModel {

    public String request_id;

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getDelivery_location() {
        return delivery_location;
    }

    public void setDelivery_location(String delivery_location) {
        this.delivery_location = delivery_location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String delivery_location;
    public String name;
    public String phone;

}
