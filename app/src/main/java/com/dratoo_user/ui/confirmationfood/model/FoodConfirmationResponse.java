package com.dratoo_user.ui.confirmationfood.model;

import java.util.ArrayList;

/**
 * Created by ${Krishnaprakash} on 24-03-2018.
 */

public class FoodConfirmationResponse {
    public String tax;
    public String food_fare;

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getFood_fare() {
        return food_fare;
    }

    public void setFood_fare(String food_fare) {
        this.food_fare = food_fare;
    }

    public String getDelivery_fare() {
        return delivery_fare;
    }

    public void setDelivery_fare(String delivery_fare) {
        this.delivery_fare = delivery_fare;
    }

    public String getBill_amount() {
        return bill_amount;
    }

    public void setBill_amount(String bill_amount) {
        this.bill_amount = bill_amount;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String delivery_fare;
    public String bill_amount;
    public String request_id;


    public ArrayList<FinalOrderItems> data;

    public ArrayList<FinalOrderItems> getData() {
        return data;
    }

    public void setData(ArrayList<FinalOrderItems> data) {
        this.data = data;
    }
}
