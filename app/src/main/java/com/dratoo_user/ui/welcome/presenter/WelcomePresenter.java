package com.dratoo_user.ui.welcome.presenter;

import com.dratoo_user.base.Presenter;
import com.dratoo_user.ui.login.model.RegisterRequest;

public interface WelcomePresenter extends Presenter<WelcomeView> {

    void loginOrRegister(RegisterRequest registerRequest);
}
