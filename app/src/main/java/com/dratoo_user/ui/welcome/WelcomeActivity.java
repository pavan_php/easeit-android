package com.dratoo_user.ui.welcome;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;

import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.data.ProgressUtils;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.login.LoginActivity;
import com.dratoo_user.ui.login.model.RegisterRequest;
import com.dratoo_user.ui.login.model.RegisterResponse;
import com.dratoo_user.ui.main.MainActivity;
import com.dratoo_user.ui.profile.ProfileActivity;
import com.dratoo_user.ui.welcome.presenter.WelcomePresenter;
import com.dratoo_user.ui.welcome.presenter.WelcomeView;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeActivity extends AppCompatActivity implements WelcomeView {
    private final int RC_SIGN_IN = 898;
    @Inject
    WelcomePresenter presenter;
    @BindView(R.id.btn_login)
    AppCompatButton btnLogin;
    @BindView(R.id.btn_register)
    AppCompatButton btnRegister;
    @BindView(R.id.btn_gplus)
    AppCompatButton btnGplus;
    @BindView(R.id.btn_with_facebook)
    AppCompatButton btnWithFacebook;
    @BindView(R.id.tv_terms)
    AppCompatTextView tvTerms;
    private CallbackManager callbackManager;
    private ProgressUtils progressUtils;
    private GoogleSignInClient mGoogleSignInClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welocme);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);

        presenter.attachView(this);
        progressUtils = new ProgressUtils(this);

        // generate key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        //Facebook integration
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.e("Nive ", "onSuccess: ");
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                (object, response) -> {
                                    Log.e("Main", response.toString());
                                    fetchUserDataFb(object);
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.e("Nive ", "onCancel: ");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.e("Nive ", "onError: " + error.toString());
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
            System.out.println(account.getDisplayName());
            RegisterRequest registerRequest = new RegisterRequest();
            registerRequest.setType("2");
            registerRequest.setEmail(account.getEmail());
            registerRequest.setDevice_token(PrefConnect.readString(this, FirebaseInstanceId.getInstance().getToken(), ""));
            presenter.loginOrRegister(registerRequest);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            System.out.println("signInResult:failed code=" + e.getStatusCode());
        }
    }

    private void fetchUserDataFb(JSONObject jsonObject) {
        try {
            String email = (jsonObject.getString("email"));
            String name = jsonObject.getString("name");

            System.out.println("fetchUserDataFb: " + email);

            RegisterRequest registerRequest = new RegisterRequest();
            registerRequest.setType("2");
            registerRequest.setEmail(email);
            registerRequest.setDevice_token(PrefConnect.readString(this, FirebaseInstanceId.getInstance().getToken(), ""));
            presenter.loginOrRegister(registerRequest);
            fbLogout();

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("Nive ", "fetchUserDataFb: " + e.toString());
        }
    }

    public void fbLogout() {
        LoginManager.getInstance().logOut();
        Log.e("Nive ", "onLogoutSucessFul");
    }

    @OnClick({R.id.btn_login, R.id.btn_register, R.id.btn_gplus, R.id.btn_with_facebook, R.id.tv_terms})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.btn_register:
                startActivity(new Intent(this, ProfileActivity.class));
                break;
            case R.id.btn_gplus:
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.btn_with_facebook:
                LoginManager.getInstance()
                        .logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
                break;
            case R.id.tv_terms:
                showTermsDialog();
                break;
        }
    }

    private void showTermsDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_terms);

        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    @Override
    public void showProgress() {
        progressUtils.show();
    }

    @Override
    public void hideProgress() {
        progressUtils.dismiss();
    }

    @Override
    public void sucess(RegisterResponse registerResponse) {
        PrefConnect.writeString(WelcomeActivity.this, Global.AUTH_ID, registerResponse.getData().getAuthId());
        PrefConnect.writeString(WelcomeActivity.this, Global.CURRENCY, registerResponse.getData().getCurrency());
        PrefConnect.writeString(WelcomeActivity.this, Global.ORDER_PREFIX, registerResponse.getData().getOrder_prefix());
        PrefConnect.writeString(WelcomeActivity.this, Global.TYPE_OF_REGISTER, registerResponse.getData().getType());
        PrefConnect.writeString(WelcomeActivity.this, Global.AUTH_TOKEN, registerResponse.getData().getAuthToken());

        if (registerResponse.getData().getIs_user_exist().equals("0")) {
            Log.e("Nive ", "sucess:exist 0 ");
            Intent intent = new Intent(WelcomeActivity.this, ProfileActivity.class);
            intent.putExtra(Global.TYPE, registerResponse.getData().getType());
            intent.putExtra(Global.FB_EMAIL, registerResponse.getData().getEmail());
            startActivity(intent);
        } else {
            Log.e("Nive ", "sucess:already Exist ");
            Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void failure(String msg) {

    }
}