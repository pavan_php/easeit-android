package com.dratoo_user.ui.welcome.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.login.model.RegisterResponse;

public interface WelcomeView extends BaseView {

    void sucess(RegisterResponse registerResponse);

    void failure(String msg);
}
