package com.dratoo_user.ui.ordertracking;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.data.ToastMaker;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.main.MainActivity;
import com.dratoo_user.ui.ordertracking.adapter.OrderTrackingItemsAdapter;
import com.dratoo_user.ui.ordertracking.model.HandyManRating;
import com.dratoo_user.ui.ordertracking.model.OrderTrackingResponse;
import com.dratoo_user.ui.ordertracking.model.OrderedItems;
import com.dratoo_user.ui.ordertracking.presenter.JobDetailsResponse;
import com.dratoo_user.ui.ordertracking.presenter.OrderTrackingPresenter;
import com.dratoo_user.ui.ordertracking.presenter.OrderTrackingView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class OrderTrackingActivity extends AppCompatActivity implements OnMapReadyCallback, OrderTrackingView {


    @Inject
    OrderTrackingPresenter presenter;

    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.top_layout)
    FrameLayout topLayout;
    @BindView(R.id.top_layout_sliding)
    LinearLayout topLayoutSliding;
    @BindView(R.id.img_food)
    CircleImageView imgFood;
    @BindView(R.id.tv_status_food)
    AppCompatTextView tvStatusFood;
    @BindView(R.id.tv_pickup_man_name)
    AppCompatTextView tvPickupManName;
    @BindView(R.id.img_profile)
    CircleImageView imgProfile;
    @BindView(R.id.tv_name)
    AppCompatTextView tvName;
    @BindView(R.id.iv_yellow_chat)
    AppCompatImageView ivYellowChat;
    @BindView(R.id.right_layout)
    LinearLayout rightLayout;
    @BindView(R.id.layout_info_delivery_boy)
    RelativeLayout layoutInfoDeliveryBoy;
    @BindView(R.id.tv_short_address_des)
    AppCompatTextView tvShortAddressDes;
    @BindView(R.id.tv_drop_address)
    AppCompatTextView tvDropAddress;
    @BindView(R.id.select_destination_layout)
    LinearLayout selectDestinationLayout;
    @BindView(R.id.tv_order_id)
    AppCompatTextView tvOrderId;
    @BindView(R.id.tv_order_value)
    AppCompatTextView tvOrderValue;
    @BindView(R.id.tv_date)
    AppCompatTextView tvDate;
    @BindView(R.id.tv_date_value)
    AppCompatTextView tvDateValue;
    @BindView(R.id.rv_order_item)
    RecyclerView rvOrderItem;
    @BindView(R.id.tv_price_estimated_layout)
    AppCompatTextView tvPriceEstimatedLayout;
    @BindView(R.id.tv_estimated_price)
    AppCompatTextView tvEstimatedPrice;
    @BindView(R.id.tv_delivery_fare)
    AppCompatTextView tvDeliveryFare;
    @BindView(R.id.tv_total_payment)
    AppCompatTextView tvTotalPayment;
    @BindView(R.id.tv_payment_mode)
    AppCompatTextView tvPaymentMode;
    @BindView(R.id.tv_final_cost)
    AppCompatTextView tvFinalCost;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.dragView)
    FrameLayout dragView;
    @BindView(R.id.sliding_layout)
    SlidingUpPanelLayout slidingLayout;
    @BindView(R.id.parant_layout)
    RelativeLayout parantLayout;
    @BindView(R.id.iv_yellow_call)
    AppCompatImageView ivYellowCall;
    @BindView(R.id.date_layout)
    LinearLayout dateLayout;
    @BindView(R.id.tv_tax)
    AppCompatTextView tvTax;
    @BindView(R.id.btn_payment_type)
    AppCompatButton btnPaymentType;
    @BindView(R.id.delivery_fee_layout)
    LinearLayout deliveryFeeLayout;
    @BindView(R.id.view_delivery)
    View viewDelivery;
    @BindView(R.id.tv_items_heading)
    AppCompatTextView tvItemsHeading;
    @BindView(R.id.tv_name_handy)
    AppCompatTextView tvNameHandy;
    @BindView(R.id.iv_yellow_call_handy)
    AppCompatImageView ivYellowCallHandy;
    @BindView(R.id.iv_yellow_chat_handy)
    AppCompatImageView ivYellowChatHandy;
    @BindView(R.id.right_layout_handy)
    LinearLayout rightLayoutHandy;
    @BindView(R.id.layout_info_delivery_boy_handy)
    RelativeLayout layoutInfoDeliveryBoyHandy;
    private GoogleMap mMap;

    private Marker mCurrLocationMarker;

    String requestId;
    private Runnable runnable;
    private Handler handler = new Handler();
    private OrderTrackingResponse orderTrackingResponse;
    private ArrayList<OrderedItems> orderedItems;
    private OrderTrackingItemsAdapter orderTrackingAdapter;
    private BottomSheetDialog mBottomSheetDialog, mBottomSheetDialogOtherService;
    private View bottomSheet, bottomSheetOtherService;


    AppCompatTextView tv_date;
    AppCompatTextView tv_order_id_complete;
    RatingBar driverRating;
    AppCompatImageView img_before_service;
    AppCompatImageView img_after_service;
    AppCompatEditText et_comments;
    AppCompatButton btn_submit, btn_submit_other_service;
    private String date;
    private int rating = 0;
    private String date1;
    private String dateOtherService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_tracking);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        presenter.attachView(this);


        if (getIntent() != null) {

            if (getIntent().getStringExtra(Global.REQUESTID_TRACKING) != null) {
                requestId = getIntent().getStringExtra(Global.REQUESTID_TRACKING);
            }

        }

        setUpToolbar();

        SupportMapFragment HomemapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        HomemapFragment.getMapAsync(this);


        dragView.setVisibility(View.VISIBLE);


        callRunnableTracking();


    }

    private void callRunnableTracking() {

        runnable = new Runnable() {


            @Override
            public void run() {

                if (requestId != null) {
                    callTrackingApi();
                }

                handler.postDelayed(runnable, 2000);
            }
        };
        handler.postDelayed(runnable, 100);


    }

    private void callTrackingApi() {

        presenter.getStatus(PrefConnect.readString(this, Global.SERVICE_TYPE, ""), requestId);


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);


       //Double.parseDouble( orerTrackingResponse.getData().lat), Double.parseDouble(orderTrackingResponse.getData().getLng())

        /*
        mCurrLocationMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble( orderTrackingResponse.getData().lat),Double.parseDouble( orderTrackingResponse.getData().lng)))
                //.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_location_food))
                .draggable(false));

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble( orderTrackingResponse.getData().lat),Double.parseDouble( orderTrackingResponse.getData().lng)),
                16));

          */


    }

    private void setUpToolbar() {
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.map_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderTrackingActivity.this, MainActivity.class);
                intent.putExtra(Global.COMPLETED, "1");
                startActivity(intent);
                handler.removeCallbacks(runnable);

            }
        });
    }

    @Override
    public void sucessStatus(OrderTrackingResponse orderTrackingResponse) {
        this.orderTrackingResponse = orderTrackingResponse;

        if (orderTrackingResponse.getData1() != null) {

            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvOrderItem.setLayoutManager(layoutManager);
            orderedItems = orderTrackingResponse.getData1();
            Log.e("ResponseArray", " : " + orderedItems);
            orderTrackingAdapter = new OrderTrackingItemsAdapter(this, orderedItems, PrefConnect.readString(this, Global.SERVICE_TYPE, ""));
            rvOrderItem.setAdapter(orderTrackingAdapter);
            orderTrackingAdapter.notifyDataSetChanged();

            Log.e("Nive ", "sucessStatus:Status " + orderTrackingResponse.getData().getService_status());


            if (orderTrackingResponse.getData().getCreated_at() != null) {

                if (orderTrackingResponse.getData().getService_status().equals("10")) {
                    layoutInfoDeliveryBoy.setVisibility(View.GONE);
                } else {
                    layoutInfoDeliveryBoy.setVisibility(View.VISIBLE);
                }

                SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM , dd yyyy");

                try {

                    date1 = sdf.format(new SimpleDateFormat("yyyy-M-dd").parse(orderTrackingResponse.getData().getUpdated_at()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                tvDateValue.setText(date1);


                dateLayout.setVisibility(View.VISIBLE);
            }


            if (orderTrackingResponse.getData().getPayment_type().equals("1")) {

                btnPaymentType.setText(getResources().getString(R.string.cash));
            } else {

                btnPaymentType.setText(getResources().getString(R.string.card));
            }


            tvName.setText(orderTrackingResponse.getData().getFname() + " " + orderTrackingResponse.getData().getLname());
            tvEstimatedPrice.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + orderTrackingResponse.getData().getPrice());
            tvTotalPayment.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + orderTrackingResponse.getData().getBill_amount());
            tvFinalCost.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + orderTrackingResponse.getData().getBill_amount());
            tvTax.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + orderTrackingResponse.getData().getTax() + " ");
            tvDeliveryFare.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + orderTrackingResponse.getData().getDelivery_charge());


            tvDropAddress.setText(orderTrackingResponse.getData().getAddress());


            Log.e("Nive ", "sucessStatus: " + orderTrackingResponse.getData().getAddress());

            Log.e("Nive ", "sucessStatus:SERVICE_TYPE " + PrefConnect.readString(this, Global.SERVICE_TYPE, ""));


            if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("3")) {

                layoutInfoDeliveryBoyHandy.setVisibility(View.VISIBLE);
                layoutInfoDeliveryBoy.setVisibility(View.GONE);

                tvNameHandy.setText("Order Id" + " : " + PrefConnect.readString(this, Global.ORDER_PREFIX, "") + orderTrackingResponse.getData().getId());
                tvOrderValue.setText(" " + PrefConnect.readString(this, Global.ORDER_PREFIX, "") + orderTrackingResponse.getData().getId());
                deliveryFeeLayout.setVisibility(View.GONE);
                viewDelivery.setVisibility(View.GONE);

                imgProfile.setVisibility(View.INVISIBLE);
                tvItemsHeading.setText("Worker Details");

                tvStatusFood.setVisibility(View.VISIBLE);
                tvPickupManName.setVisibility(View.GONE);

                tvOrderId.setText(PrefConnect.readString(this, Global.ORDER_PREFIX, "") + orderTrackingResponse.getData().getId());


                SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd , yyyy");

                try {

                    date1 = sdf.format(new SimpleDateFormat("yyyy-M-dd").parse(orderTrackingResponse.getData().getStart_date()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                tvDateValue.setText(date1);


                if (orderTrackingResponse.getData().getService_status().equals("1")) {

                    tvStatusFood.setText(getResources().getString(R.string.order_placed));

                } else if (orderTrackingResponse.getData().getService_status().equals("2")) {
                    tvStatusFood.setText(getResources().getString(R.string.booking_accepted));

                } else if (orderTrackingResponse.getData().getService_status().equals("3")) {

                    tvStatusFood.setText(getResources().getString(R.string.completed));

                    if (orderTrackingResponse.getData().getWorker_rating().equals("0")) {
                        reviewBottomSheet(orderTrackingResponse.getData().getCreated_at(), orderTrackingResponse.getData().getId());
                    } else {
                        tvStatusFood.setText(getResources().getString(R.string.reviwed));

                    }

                } else if (orderTrackingResponse.getData().getService_status().equals("4")) {

                    tvStatusFood.setText(getResources().getString(R.string.reject_by_restarant));
                } else if (orderTrackingResponse.getData().getService_status().equals("5")) {

                    tvStatusFood.setText(getResources().getString(R.string.started_job));
                } else if (orderTrackingResponse.getData().getService_status().equals("6")) {

                    tvStatusFood.setText(getResources().getString(R.string.worker_on_the_way));
                } else if (orderTrackingResponse.getData().getService_status().equals("7")) {

                    tvStatusFood.setText(getResources().getString(R.string.job_completed));
                } else if (orderTrackingResponse.getData().getService_status().equals("10")) {
                    tvStatusFood.setText(getResources().getString(R.string.order_placed));
                } else if (orderTrackingResponse.getData().getService_status().equals("3") && !orderTrackingResponse.getData().getWorker_rating().equals("0")) {
                    tvStatusFood.setText(getResources().getString(R.string.reviwed));
                }


            } else {


                if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("2")) {
                    tvStatusFood.setVisibility(View.VISIBLE);
                    tvPickupManName.setVisibility(View.GONE);

                    Log.e("Nive ", "sucessStatus:Nivi ");

                    tvOrderId.setText(PrefConnect.readString(this, Global.ORDER_PREFIX, "") + orderTrackingResponse.getData().getCid());

                    if (orderTrackingResponse.getData().getService_status().equals("0")) {

                        tvStatusFood.setText(getResources().getString(R.string.order_placed));

                    } else if (orderTrackingResponse.getData().getService_status().equals("10")) {
                        tvStatusFood.setText(getResources().getString(R.string.order_placed));
                    } else if (orderTrackingResponse.getData().getService_status().equals("1")) {

                        tvStatusFood.setText(getResources().getString(R.string.booking_accepted));
                    } else if (orderTrackingResponse.getData().getService_status().equals("2")) {

                        tvStatusFood.setText(getResources().getString(R.string.waiting_delivery));
                    } else if (orderTrackingResponse.getData().getService_status().equals("3")) {

                        tvStatusFood.setText(getResources().getString(R.string.completed));

                        if (orderTrackingResponse.getData().getRate_provider().equals("0")) {
                            reviewBottomSheetOtherService(orderTrackingResponse.getData().getCreated_at(), orderTrackingResponse.getData().getCid());
                        } else {
                            tvStatusFood.setText(getResources().getString(R.string.reviwed));

                        }

                    } else if (orderTrackingResponse.getData().getService_status().equals("4")) {

                        tvStatusFood.setText(getResources().getString(R.string.reject_by_shop));
                    } else if (orderTrackingResponse.getData().getService_status().equals("5")) {

                        tvStatusFood.setText(getResources().getString(R.string.started_job));
                    }


                } else {
                    tvStatusFood.setVisibility(View.VISIBLE);
                    tvPickupManName.setVisibility(View.VISIBLE);

                    tvOrderId.setText(PrefConnect.readString(this, Global.ORDER_PREFIX, "") + orderTrackingResponse.getData().getCid());


                    if (orderTrackingResponse.getData().getService_status().equals("0")) {

                        tvPickupManName.setText(getResources().getString(R.string.order_placed));

                    } else if (orderTrackingResponse.getData().getService_status().equals("10")) {
                        tvPickupManName.setText(getResources().getString(R.string.order_placed));
                    } else if (orderTrackingResponse.getData().getService_status().equals("1")) {

                        tvPickupManName.setText(getResources().getString(R.string.booking_accepted));
                    } else if (orderTrackingResponse.getData().getService_status().equals("2")) {

                        tvPickupManName.setText(getResources().getString(R.string.waiting_delivery));
                    } else if (orderTrackingResponse.getData().getService_status().equals("3")) {

                        tvPickupManName.setText(getResources().getString(R.string.completed));

                        if (orderTrackingResponse.getData().getRate_provider().equals("0")) {
                            reviewBottomSheetOtherService(orderTrackingResponse.getData().getCreated_at(), orderTrackingResponse.getData().getCid());
                        } else {
                            tvStatusFood.setText(getResources().getString(R.string.reviwed));

                        }

                    } else if (orderTrackingResponse.getData().getService_status().equals("4")) {

                        tvPickupManName.setText(getResources().getString(R.string.reject_by_restarant));
                    } else if (orderTrackingResponse.getData().getService_status().equals("5")) {

                        tvPickupManName.setText(getResources().getString(R.string.started_job));
                    }


                }


                tvItemsHeading.setText("order item {s)");


                imgProfile.setVisibility(View.VISIBLE);


                try {
                    Picasso.get().load(Global.IMG_URL + orderTrackingResponse.getData().getProfile_pic()).fit().into(imgProfile);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                deliveryFeeLayout.setVisibility(View.VISIBLE);
                viewDelivery.setVisibility(View.VISIBLE);
                tvOrderValue.setText(" " + PrefConnect.readString(this, Global.ORDER_PREFIX, "") + orderTrackingResponse.getData().getCid());


            }

        }


    }

    @Override
    public void getPictures(JobDetailsResponse body) {

        Picasso.get().load(Global.IMG_URL + body.getStart_job_pic()).fit().into(img_before_service);
        Picasso.get().load(Global.IMG_URL + body.getEnd_job_pic()).fit().into(img_after_service);

    }

    @Override
    public void submitRatingSucess() {

        ToastMaker.makeToast(this, "Rating Successfully Submitted!!!");

        if (mBottomSheetDialog != null) {
            mBottomSheetDialog.dismiss();
        }


        if (mBottomSheetDialogOtherService != null) {
            mBottomSheetDialogOtherService.dismiss();
        }

        callRunnableTracking();

    }


    public void reviewBottomSheetOtherService(String created_at, final String id) {
        handler.removeCallbacks(runnable);
        mBottomSheetDialogOtherService = new BottomSheetDialog(this);
        bottomSheetOtherService = this.getLayoutInflater().inflate(R.layout.handyman_rating_user_otherservice, null);
        mBottomSheetDialogOtherService.setContentView(bottomSheetOtherService);
        mBottomSheetDialogOtherService.setCancelable(false);


        tv_date = mBottomSheetDialogOtherService.findViewById(R.id.tv_date);
        tv_order_id_complete = mBottomSheetDialogOtherService.findViewById(R.id.tv_order_id_complete);
        driverRating = mBottomSheetDialogOtherService.findViewById(R.id.driverRating);
        et_comments = mBottomSheetDialogOtherService.findViewById(R.id.et_comments);
        btn_submit_other_service = mBottomSheetDialogOtherService.findViewById(R.id.btn_submit_other_service);


        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM , dd ");

        try {

            dateOtherService = sdf.format(new SimpleDateFormat("yyyy-M-dd").parse(created_at));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        tv_date.setText(dateOtherService);
        tv_order_id_complete.setText(PrefConnect.readString(this, Global.ORDER_PREFIX, "") + id);


        btn_submit_other_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Nive ", "onClick:UserRating ");

                rating = (int) driverRating.getRating();

                Log.e("Nive ", "onClick:rating " + rating);

                if (rating == 0) {
                    ToastMaker.makeToast(OrderTrackingActivity.this, "Please Give Rating!!!");
                } else if (et_comments.getText().toString().isEmpty()) {
                    ToastMaker.makeToast(OrderTrackingActivity.this, "Please Add Comments!!!");
                } else {

                    HandyManRating handyManRating = new HandyManRating();

                    handyManRating.setComments(et_comments.getText().toString());
                    handyManRating.setRating(String.valueOf(rating));
                    handyManRating.setRequest_id(id);
                    handyManRating.setRate_type("2");
                    handyManRating.setService_type(PrefConnect.readString(OrderTrackingActivity.this, Global.SERVICE_TYPE, ""));
                    presenter.sumbitRating(handyManRating);
                }
            }
        });


        mBottomSheetDialogOtherService.show();


    }

    private void reviewBottomSheet(String created_at, final String id) {

        handler.removeCallbacks(runnable);
        mBottomSheetDialog = new BottomSheetDialog(this);
        bottomSheet = this.getLayoutInflater().inflate(R.layout.handyman_rating_user, null);
        mBottomSheetDialog.setContentView(bottomSheet);
        mBottomSheetDialog.setCancelable(false);

        tv_date = mBottomSheetDialog.findViewById(R.id.tv_date);
        tv_order_id_complete = mBottomSheetDialog.findViewById(R.id.tv_order_id_complete);
        driverRating = mBottomSheetDialog.findViewById(R.id.driverRating);
        img_before_service = mBottomSheetDialog.findViewById(R.id.img_before_service);
        img_after_service = mBottomSheetDialog.findViewById(R.id.img_after_service);
        et_comments = mBottomSheetDialog.findViewById(R.id.et_comments);
        btn_submit = mBottomSheetDialog.findViewById(R.id.btn_submit);
        showDetailsApi(id);

        // convert date into corresponding formate

        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM , dd ");

        try {

            date = sdf.format(new SimpleDateFormat("yyyy-M-dd").parse(created_at));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        tv_date.setText(date);
        tv_order_id_complete.setText(PrefConnect.readString(this, Global.ORDER_PREFIX, "") + id);

        mBottomSheetDialog.show();

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Nive ", "onClick:UserRating ");

                rating = (int) driverRating.getRating();

                Log.e("Nive ", "onClick:rating " + rating);

                if (rating == 0) {
                    ToastMaker.makeToast(OrderTrackingActivity.this, "Please Give Rating!!!");
                } else if (et_comments.getText().toString().isEmpty()) {
                    ToastMaker.makeToast(OrderTrackingActivity.this, "Please Add Comments!!!");


                } else {

                    HandyManRating handyManRating = new HandyManRating();

                    handyManRating.setComments(et_comments.getText().toString());
                    handyManRating.setRating(String.valueOf(rating));
                    handyManRating.setRequest_id(id);
                    handyManRating.setRate_type("2");
                    handyManRating.setService_type(PrefConnect.readString(OrderTrackingActivity.this, Global.SERVICE_TYPE, ""));
                    presenter.sumbitRating(handyManRating);
                }
            }
        });


    }

    private void showDetailsApi(String id) {

        presenter.getPictures(id);

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }


    @OnClick({R.id.top_layout_sliding, R.id.iv_yellow_call, R.id.iv_yellow_call_handy})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.top_layout_sliding:
                if (slidingLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.COLLAPSED)) {
                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                } else {
                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

                }
                break;
            case R.id.iv_yellow_call:


                if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("3")) {
                    Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                    dialIntent.setData(Uri.parse("tel:" + orderTrackingResponse.getData1().get(0).getAdmin_phone()));
                    startActivity(dialIntent);
                } else {
                    if (orderTrackingResponse.getData().getPhone() != null) {
                        Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                        dialIntent.setData(Uri.parse("tel:" + orderTrackingResponse.getData().getPhone()));
                        startActivity(dialIntent);
                    }
                }

                break;
            case R.id.iv_yellow_call_handy:

                Log.e("Nive ", "onViewClicked: ");
                Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                dialIntent.setData(Uri.parse("tel:" + orderTrackingResponse.getData1().get(0).getAdmin_phone()));
                startActivity(dialIntent);


                break;
        }
    }
}
