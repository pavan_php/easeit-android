package com.dratoo_user.ui.ordertracking.adapter;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.ui.ordertracking.OrderTrackingActivity;
import com.dratoo_user.ui.ordertracking.model.OrderedItems;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class OrderTrackingItemsAdapter extends RecyclerView.Adapter<OrderTrackingItemsAdapter.ViewHolder> {

    OrderTrackingActivity orderTrackingActivity;

    ArrayList<OrderedItems> orderedItemsArrayList;

    String type;

    public OrderTrackingItemsAdapter(OrderTrackingActivity orderTrackingActivity, ArrayList<OrderedItems> orderedItems, String string) {

        this.orderTrackingActivity = orderTrackingActivity;

        this.orderedItemsArrayList = orderedItems;

        this.type = string;

    }

    @NonNull
    @Override
    public OrderTrackingItemsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.order_tracking_items, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull OrderTrackingItemsAdapter.ViewHolder viewHolder, int position) {

        if (type.equals("3")) {

            viewHolder.tv_rating.setText(orderedItemsArrayList.get(position).getRating());
            viewHolder.technician_layout.setVisibility(View.VISIBLE);
            viewHolder.food_layout.setVisibility(View.GONE);
            viewHolder.tv_provider_name.setText(orderedItemsArrayList.get(position).getAdmin_name());
            viewHolder.tv_distance.setText("Experience" + " : " + orderedItemsArrayList.get(position).getExperiance() + " " + "Year(s)");

            Picasso.get().load(Global.IMG_URL + orderedItemsArrayList.get(position).getProfile_image()).fit().into(viewHolder.img_provider);

        } else {
            viewHolder.technician_layout.setVisibility(View.GONE);
            viewHolder.food_layout.setVisibility(View.VISIBLE);

            viewHolder.tv_order_name.setText(orderedItemsArrayList.get(position).getItem_name());
            viewHolder.tv_order_quantity.setText(orderedItemsArrayList.get(position).getQuantity());

        }


    }

    @Override
    public int getItemCount() {
        return orderedItemsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        AppCompatTextView tv_order_name, tv_order_quantity, tv_provider_name, tv_distance, tv_rating;

        LinearLayout food_layout, technician_layout;

        CircleImageView img_provider;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_order_name = itemView.findViewById(R.id.tv_order_name);
            img_provider = itemView.findViewById(R.id.img_provider);
            tv_provider_name = itemView.findViewById(R.id.tv_provider_name);
            tv_distance = itemView.findViewById(R.id.tv_distance);
            tv_rating = itemView.findViewById(R.id.tv_rating);
            food_layout = itemView.findViewById(R.id.food_layout);
            technician_layout = itemView.findViewById(R.id.technician_layout);
            tv_order_quantity = itemView.findViewById(R.id.tv_order_quantity);
        }
    }
}
