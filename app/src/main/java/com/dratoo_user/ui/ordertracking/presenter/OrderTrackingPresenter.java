package com.dratoo_user.ui.ordertracking.presenter;

import com.dratoo_user.base.Presenter;
import com.dratoo_user.ui.ordertracking.model.HandyManRating;

public interface OrderTrackingPresenter extends Presenter<OrderTrackingView> {

    void getStatus(String typeId,String requestId);

    void getPictures(String request_id);

    void sumbitRating(HandyManRating handyManRating);


}
