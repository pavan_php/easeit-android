package com.dratoo_user.ui.ordertracking.model;

import java.util.ArrayList;

public class OrderTrackingResponse {


    public UserDetails data;
    public ArrayList<OrderedItems> data1;

    public UserDetails getData() {
        return data;
    }

    public ArrayList<OrderedItems> getData1() {
        return data1;
    }
}
