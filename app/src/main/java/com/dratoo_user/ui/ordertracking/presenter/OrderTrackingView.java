package com.dratoo_user.ui.ordertracking.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.ordertracking.model.OrderTrackingResponse;

public interface OrderTrackingView extends BaseView {

    void sucessStatus(OrderTrackingResponse orderTrackingResponse);

    void getPictures(JobDetailsResponse body);

    void submitRatingSucess();
}
