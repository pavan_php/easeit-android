package com.dratoo_user.ui.ordertracking.model;

public class UserDetails {

    public String id;
    public String cid;
    public String user_id;
    public String name;
    public String fname;

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String lname;
    public String phone;
    public String provider_admin_id;
    public String message;
    public String provider_id;
    public String address;
    public String lat;
    public String lng;
    public String price;
    public String tax;
    public String bill_amount;
    public String payment_type;

    public String getRate_provider() {
        return rate_provider;
    }

    public void setRate_provider(String rate_provider) {
        this.rate_provider = rate_provider;
    }

    public String getDeliveryboy_comment() {
        return deliveryboy_comment;
    }

    public void setDeliveryboy_comment(String deliveryboy_comment) {
        this.deliveryboy_comment = deliveryboy_comment;
    }

    public String status;
    public String rate_provider;
    public String deliveryboy_comment;


    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String start_date;

    public String worker_rating;


    public String getWorker_rating() {
        return worker_rating;
    }

    public void setWorker_rating(String worker_rating) {
        this.worker_rating = worker_rating;
    }

    public String getUser_comments() {
        return user_comments;
    }

    public void setUser_comments(String user_comments) {
        this.user_comments = user_comments;
    }

    public String user_comments;

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String created_at;
    public String updated_at;
    public String admin_name;
    public String delivery_charge;

    public String getDelivery_charge() {
        return delivery_charge;
    }

    public void setDelivery_charge(String delivery_charge) {
        this.delivery_charge = delivery_charge;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProvider_admin_id() {
        return provider_admin_id;
    }

    public void setProvider_admin_id(String provider_admin_id) {
        this.provider_admin_id = provider_admin_id;
    }

    public String getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(String provider_id) {
        this.provider_id = provider_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getBill_amount() {
        return bill_amount;
    }

    public void setBill_amount(String bill_amount) {
        this.bill_amount = bill_amount;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getAdmin_name() {
        return admin_name;
    }

    public void setAdmin_name(String admin_name) {
        this.admin_name = admin_name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String profile_pic;

    public String getService_status() {
        return service_status;
    }

    public void setService_status(String service_status) {
        this.service_status = service_status;
    }

    public String service_status;
}
