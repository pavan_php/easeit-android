package com.dratoo_user.ui.ordertracking.presenter;

import android.util.Log;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.lookingdriverbooking.model.CommonResponse;
import com.dratoo_user.ui.ordertracking.model.HandyManRating;
import com.dratoo_user.ui.ordertracking.model.OrderTrackingResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class OrderTrackingImpl extends BasePresenter<OrderTrackingView> implements OrderTrackingPresenter {


    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;

    @Override
    public void attachView(OrderTrackingView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void getStatus(String typeId, String requestId) {
        Call<OrderTrackingResponse> call = apiInterface.getStatus(typeId,requestId);
        call.enqueue(new Callback<OrderTrackingResponse>() {
            @Override
            public void onResponse(Call<OrderTrackingResponse> call, Response<OrderTrackingResponse> response) {
                if (response.isSuccessful()) {
                    //   getView().hideProgress();
                    getView().sucessStatus(response.body());
                }
            }

            @Override
            public void onFailure(Call<OrderTrackingResponse> call, Throwable t) {
                // getView().hideProgress();
                Log.e(TAG, "onFailure: "+t.toString() );

            }
        });

    }


    @Override
    public void getPictures(String request_id) {
        getView().showProgress();

        Call<JobDetailsResponse> call = apiInterface.getPictures(request_id);

        call.enqueue(new Callback<JobDetailsResponse>() {
            @Override
            public void onResponse(Call<JobDetailsResponse> call, Response<JobDetailsResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().getPictures(response.body());
                }
            }

            @Override
            public void onFailure(Call<JobDetailsResponse> call, Throwable t) {
                getView().hideProgress();
            }
        });
    }

    @Override
    public void sumbitRating(HandyManRating handyManRating) {

        getView().showProgress();

        Call<CommonResponse> call = apiInterface.sendRating(handyManRating);

        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().submitRatingSucess();
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                getView().hideProgress();
            }
        });


    }
}
