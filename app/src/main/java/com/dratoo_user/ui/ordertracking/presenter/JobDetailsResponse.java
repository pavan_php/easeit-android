package com.dratoo_user.ui.ordertracking.presenter;

public class JobDetailsResponse {

    public String start_job_pic;
    public String end_job_pic;

    public String getStart_job_pic() {
        return start_job_pic;
    }

    public void setStart_job_pic(String start_job_pic) {
        this.start_job_pic = start_job_pic;
    }

    public String getEnd_job_pic() {
        return end_job_pic;
    }

    public void setEnd_job_pic(String end_job_pic) {
        this.end_job_pic = end_job_pic;
    }
}
