package com.dratoo_user.ui.history.presenter;

import android.util.Log;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.history.model.HistoryResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class HistoryImpl extends BasePresenter<HistoryView> implements HistoryPresenter {

    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;


    @Override
    public void attachView(HistoryView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }


    @Override
    public void getStatus(String typeId, String requestId) {
        getView().showProgress();
        Call<HistoryResponse> call = apiInterface.getHistory(typeId, requestId);
        call.enqueue(new Callback<HistoryResponse>() {
            @Override
            public void onResponse(Call<HistoryResponse> call, Response<HistoryResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    getView().sucessStatus(response.body());
                }
            }

            @Override
            public void onFailure(Call<HistoryResponse> call, Throwable t) {
                getView().hideProgress();
                Log.e(TAG, "onFailure: " + t.toString());

            }
        });

    }
}
