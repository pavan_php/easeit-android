package com.dratoo_user.ui.history.presenter;

import com.dratoo_user.base.Presenter;

public interface HistoryPresenter extends Presenter<HistoryView> {

    void getStatus(String typeId,String requestId);

}
