package com.dratoo_user.ui.history.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.history.model.HistoryResponse;

public interface HistoryView extends BaseView {

    void sucessStatus(HistoryResponse orderTrackingResponse);

}
