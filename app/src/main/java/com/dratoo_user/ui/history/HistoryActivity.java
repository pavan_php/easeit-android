package com.dratoo_user.ui.history;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.common.ProgressDialog;
import com.dratoo_user.ui.history.model.HistoryResponse;
import com.dratoo_user.ui.history.presenter.HistoryPresenter;
import com.dratoo_user.ui.history.presenter.HistoryView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryActivity extends AppCompatActivity implements HistoryView {


    @Inject
    HistoryPresenter presenter;
    @BindView(R.id.tv_order_id_complete)
    AppCompatTextView tvOrderIdComplete;
    @BindView(R.id.tv_total_distance)
    AppCompatTextView tvTotalDistance;
    @BindView(R.id.tv_short_address_pickup)
    AppCompatTextView tvShortAddressPickup;
    @BindView(R.id.tv_drop_address_pickup)
    AppCompatTextView tvDropAddressPickup;
    @BindView(R.id.select_pickup_layout)
    LinearLayout selectPickupLayout;
    @BindView(R.id.tv_short_address_des)
    AppCompatTextView tvShortAddressDes;
    @BindView(R.id.tv_drop_address_des)
    AppCompatTextView tvDropAddressDes;
    @BindView(R.id.select_destination_layout)
    LinearLayout selectDestinationLayout;
    @BindView(R.id.txtRideFare)
    AppCompatTextView txtRideFare;
    @BindView(R.id.txtTax)
    AppCompatTextView txtTax;
    @BindView(R.id.txtTotal)
    AppCompatTextView txtTotal;
    @BindView(R.id.calculation_layout)
    LinearLayout calculationLayout;
    @BindView(R.id.tv_date)
    AppCompatTextView tvDate;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_status)
    AppCompatTextView tvStatus;

    private String requestId;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        presenter.attachView(this);

        progressDialog = new ProgressDialog(this);

        if (getIntent() != null) {

            if (getIntent().getStringExtra(Global.REQUESTID_TRACKING) != null) {
                requestId = getIntent().getStringExtra(Global.REQUESTID_TRACKING);
            }

        }

        setUpToolbar();

        callHistoryApi();
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
    }

    @Override
    public void showProgress() {
        progressDialog.show();

    }

    @Override
    public void hideProgress() {

        progressDialog.dismissLoader();

    }

    private void callHistoryApi() {

        presenter.getStatus(PrefConnect.readString(this, Global.SERVICE_TYPE, ""), requestId);


    }

    @Override
    protected void onStart() {
        super.onStart();
//        App.getInstance().setConnectivityListener(this);

    }

    @Override
    public void sucessStatus(HistoryResponse historyResponse) {


        if (historyResponse != null) {

            if (historyResponse.getData().getRide_status().equals("1")) {
                tvStatus.setText(getResources().getString(R.string.trip_accept));

            } else if (historyResponse.getData().getRide_status().equals("2")) {
                tvStatus.setText(getResources().getString(R.string.trip_started));

            } else if (historyResponse.getData().getRide_status().equals("3")) {

                tvStatus.setText(getResources().getString(R.string.trip_completed));
            } else if (historyResponse.getData().getRide_status().equals("4")) {

                tvStatus.setText(getResources().getString(R.string.trip_canelled));
            } else {
                tvStatus.setVisibility(View.GONE);
            }
            tvTotalDistance.setText(historyResponse.getData().getDistance_km() + getResources().getString(R.string.km));
            tvDropAddressPickup.setText(historyResponse.getData().getPickup_address());
            tvDropAddressDes.setText(historyResponse.getData().getDest_address());
            txtRideFare.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + historyResponse.getData().getPrice());
            txtTax.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + historyResponse.getData().getTax());
            txtTotal.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + historyResponse.getData().getBill_amount());
            tvOrderIdComplete.setText(PrefConnect.readString(this, Global.ORDER_PREFIX, "")+historyResponse.getData().getRequest_id());
            tvDate.setText(historyResponse.getData().getCreated_at());

        }


    }
}
