package com.dratoo_user.ui.history.model;

public class HistoryResponse {

    public HistoryModel getData() {
        return data;
    }

    public void setData(HistoryModel data) {
        this.data = data;
    }

    public HistoryModel data;

}
