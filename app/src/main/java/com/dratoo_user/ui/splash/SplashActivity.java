package com.dratoo_user.ui.splash;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.ui.main.MainActivity;
import com.dratoo_user.ui.welcome.WelcomeActivity;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Fabric.with(this, new Crashlytics());

        VideoView view = findViewById(R.id.videoView);
        String path = "android.resource://" + getPackageName() + "/" + R.raw.splash;
        view.setVideoURI(Uri.parse(path));
        view.start();
        view.setOnCompletionListener(mp -> {
            if (PrefConnect.readString(getBaseContext(), Global.LOGIN, "").isEmpty()) {
                startActivity(new Intent(getBaseContext(), WelcomeActivity.class));
            } else {
                startActivity(new Intent(getBaseContext(), MainActivity.class));
            }
            finish();
        });
    }
}