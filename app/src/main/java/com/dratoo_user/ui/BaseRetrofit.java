package com.dratoo_user.ui;

import android.content.Context;

import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;
import com.dratoo_user.BuildConfig;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseRetrofit {


    public static String REQUEST = "REQUEST";
    public static String RESPONSE = "RESPONSE";

    public static String GOOGLE_BASE_URL = "https://maps.googleapis.com/maps/api/";


    public static Retrofit retrofit = null;

    public static Context context;


    public BaseRetrofit(Context context) {

        this.context = context;

    }



    public static Retrofit getGoogleApi() {


        LoggingInterceptor loggingInterceptor = new LoggingInterceptor.Builder()
                .loggable(BuildConfig.DEBUG)
                .setLevel(Level.BASIC)
                .request(REQUEST)
                .response(RESPONSE)
                .build();

        OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build();

        retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(GOOGLE_BASE_URL).client(okHttpClient).build();

        return retrofit;


    }



}
