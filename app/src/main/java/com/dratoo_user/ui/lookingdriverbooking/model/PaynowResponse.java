package com.dratoo_user.ui.lookingdriverbooking.model;

public class PaynowResponse {


    public PaynowData getData() {
        return data;
    }

    public void setData(PaynowData data) {
        this.data = data;
    }

    public PaynowData data;
}
