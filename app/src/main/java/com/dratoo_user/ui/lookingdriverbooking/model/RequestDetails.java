package com.dratoo_user.ui.lookingdriverbooking.model;

import java.io.Serializable;

public class RequestDetails implements Serializable {


    public String user_id;
    public String provider_id;
    public String user_name;
    public String user_mobile;
    public String status;
    public String request_id;
    public String payment_type;
    public String price;
    public String tax;
    public String bill_amount;
    public String distance_km;

    public RequestDetailsCommon taxi_request_detail;
    public RequestDetailsCommon courier_request_detail;
    public RequestDetailsCommon cargo_request_detail;
    public RequestDetailsCommon moto_request_detail;

    public RequestDetailsCommon getMoto_request_detail() {
        return moto_request_detail;
    }

    public void setMoto_request_detail(RequestDetailsCommon moto_request_detail) {
        this.moto_request_detail = moto_request_detail;
    }

    public RequestDetailsCommon getCargo_request_detail() {
        return cargo_request_detail;
    }

    public void setCargo_request_detail(RequestDetailsCommon cargo_request_detail) {
        this.cargo_request_detail = cargo_request_detail;
    }

    public RequestDetailsCommon getCourier_request_detail() {
        return courier_request_detail;
    }

    public void setCourier_request_detail(RequestDetailsCommon courier_request_detail) {
        this.courier_request_detail = courier_request_detail;
    }

    public RequestDetailsCommon getTaxi_request_detail() {
        return taxi_request_detail;
    }

    public void setTaxi_request_detail(RequestDetailsCommon taxi_request_detail) {
        this.taxi_request_detail = taxi_request_detail;
    }

    @Override
    public String toString() {
        return "RequestDetails{" +
                "user_id='" + user_id + '\'' +
                ", provider_id='" + provider_id + '\'' +
                ", user_name='" + user_name + '\'' +
                ", user_mobile='" + user_mobile + '\'' +
                ", status='" + status + '\'' +
                ", request_id='" + request_id + '\'' +
                ", payment_type='" + payment_type + '\'' +
                ", price='" + price + '\'' +
                ", tax='" + tax + '\'' +
                ", bill_amount='" + bill_amount + '\'' +
                ", distance_km='" + distance_km + '\'' +
                ", pickup_address='" + pickup_address + '\'' +
                ", dest_address='" + dest_address + '\'' +
                '}';
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(String provider_id) {
        this.provider_id = provider_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_mobile() {
        return user_mobile;
    }

    public void setUser_mobile(String user_mobile) {
        this.user_mobile = user_mobile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getBill_amount() {
        return bill_amount;
    }

    public void setBill_amount(String bill_amount) {
        this.bill_amount = bill_amount;
    }

    public String getDistance_km() {
        return distance_km;
    }

    public void setDistance_km(String distance_km) {
        this.distance_km = distance_km;
    }

    public String getPickup_address() {
        return pickup_address;
    }

    public void setPickup_address(String pickup_address) {
        this.pickup_address = pickup_address;
    }

    public String getDest_address() {
        return dest_address;
    }

    public void setDest_address(String dest_address) {
        this.dest_address = dest_address;
    }

    public String pickup_address;
    public String dest_address;

}
