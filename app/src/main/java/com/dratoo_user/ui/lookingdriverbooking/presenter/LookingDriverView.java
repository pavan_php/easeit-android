package com.dratoo_user.ui.lookingdriverbooking.presenter;

import com.dratoo_user.api.FirebaseDetails;
import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.lookingdriverbooking.model.CommonResponse;

public interface LookingDriverView extends BaseView {

    void showDriverDetails(FirebaseDetails body);

    void notificationSucess();

    void rideComplete(CommonResponse commonResponse);

    void paynowSucess();

    void ratingResponse();

    void rideCanceled();


}
