package com.dratoo_user.ui.lookingdriverbooking.presenter;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.api.FirebaseDetails;
import com.dratoo_user.api.RatingResponse;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentScucessResponse;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentSucessRequest;
import com.dratoo_user.ui.lookingdriverbooking.model.CommonResponse;
import com.dratoo_user.ui.lookingdriverbooking.model.PaynowResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LookingDriverImpl extends BasePresenter<LookingDriverView> implements LookingDriverPresenter {

    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;


    @Override
    public void attachView(LookingDriverView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }


    @Override
    public void setDriverDetails(String accessBy, String reqId, String service_type) {

        Call<FirebaseDetails> call = null;


        if (service_type.equals("4")) {
            call = apiInterface.getDriversDetails(accessBy, reqId);
        } else if (service_type.equals("5")) {
            call = apiInterface.getDriversDetailsCourier(accessBy, reqId);
        } else if (service_type.equals("6")) {
            call = apiInterface.getDriversDetailsCargo(accessBy, reqId);
        } else if (service_type.equals("7")) {
            call = apiInterface.getDriversDetailsMoto(accessBy, reqId);
        }

        call.enqueue(new Callback<FirebaseDetails>() {
            @Override
            public void onResponse(Call<FirebaseDetails> call, Response<FirebaseDetails> response) {

                if (response.isSuccessful()) {

                    getView().showDriverDetails(response.body());

                }
            }

            @Override
            public void onFailure(Call<FirebaseDetails> call, Throwable t) {

            }
        });


    }

    @Override
    public void getNotification(String requestId, String service_type) {

        Call<NotificationStartTripResponse> call = null;

        if (service_type.equals("4")) {
            call = apiInterface.getNotification(requestId);
        } else if (service_type.equals("5")) {
            call = apiInterface.getNotificationCourier(requestId);
        } else if (service_type.equals("6")) {
            call = apiInterface.getNotificationCargo(requestId);
        } else if (service_type.equals("7")) {
            call = apiInterface.getNotificationMoto(requestId);
        }

        call.enqueue(new Callback<NotificationStartTripResponse>() {
            @Override
            public void onResponse(Call<NotificationStartTripResponse> call, Response<NotificationStartTripResponse> response) {
                if (response.isSuccessful()) {
                    getView().notificationSucess();
                }
            }

            @Override
            public void onFailure(Call<NotificationStartTripResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void rideComplete(String reqId, String status, String service_type) {

        getView().showProgress();
        Call<CommonResponse> call = null;

        if (service_type.equals("4")) {
            call = apiInterface.rideCompleted(reqId, status);
        } else if (service_type.equals("5")) {
            call = apiInterface.rideCompletedCourier(reqId, status);
        } else if (service_type.equals("6")) {
            call = apiInterface.rideCompletedCargo(reqId, status);
        } else if (service_type.equals("7")) {
            call = apiInterface.rideCompletedMoto(reqId, status);
        }


        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body().getMessage().equals("Ride Cancelled Successfully")) {
                        getView().rideCanceled();
                        getView().hideProgress();
                    } else {
                        getView().rideComplete(response.body());
                        getView().hideProgress();
                    }

                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                getView().hideProgress();
            }
        });

    }

    @Override
    public void getPaynow(String id, String service_type) {

        Call<PaynowResponse> call = null;

        if (service_type.equals("4")) {
            call = apiInterface.getPaynowDetails(id);
        } else if (service_type.equals("5")) {
            call = apiInterface.getPaynowCourierDetails(id);
        } else if (service_type.equals("6")) {
            call = apiInterface.getPaynowCargoDetails(id);
        } else if (service_type.equals("7")) {
            call = apiInterface.getPaynowMotoDetails(id);
        }

        call.enqueue(new Callback<PaynowResponse>() {
            @Override
            public void onResponse(Call<PaynowResponse> call, Response<PaynowResponse> response) {
                if (response.isSuccessful()) {

                    getView().paynowSucess();
                }
            }

            @Override
            public void onFailure(Call<PaynowResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void sendSucessRequest(PaymentSucessRequest paymentSucessRequest) {
        Call<PaymentScucessResponse> call = apiInterface.getSucessResultPayment(paymentSucessRequest);
        call.enqueue(new Callback<PaymentScucessResponse>() {
            @Override
            public void onResponse(Call<PaymentScucessResponse> call, Response<PaymentScucessResponse> response) {
                if (response.isSuccessful()) {
                    getView().paynowSucess();
                }
            }

            @Override
            public void onFailure(Call<PaymentScucessResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void getRating(String reqId, String rating, String service_type, String comments) {


        getView().showProgress();
        Call<RatingResponse> call = null;

        if (service_type.equals("4")) {
            call = apiInterface.getRatingResponse(reqId, rating, comments);
        } else if (service_type.equals("5")) {
            call = apiInterface.getRatingResponseCourier(reqId, rating, comments);
        } else if (service_type.equals("6")) {
            call = apiInterface.getRatingResponseCargo(reqId, rating, comments);
        } else if (service_type.equals("7")) {
            call = apiInterface.getRatingResponseMoto(reqId, rating, comments);
        }

        call.enqueue(new Callback<RatingResponse>() {
            @Override
            public void onResponse(Call<RatingResponse> call, Response<RatingResponse> response) {
                if (response.isSuccessful()) {

                    getView().hideProgress();

//                    if(response.body().getData().getStatus().equals(""))
                    getView().ratingResponse();
                } else {
                    getView().hideProgress();
                }
            }

            @Override
            public void onFailure(Call<RatingResponse> call, Throwable t) {
                getView().hideProgress();
            }
        });

    }


}
