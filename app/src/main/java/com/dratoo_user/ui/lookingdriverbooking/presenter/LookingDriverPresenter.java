package com.dratoo_user.ui.lookingdriverbooking.presenter;

import com.dratoo_user.base.Presenter;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentSucessRequest;

public interface LookingDriverPresenter extends Presenter<LookingDriverView> {

    void setDriverDetails(String accessBy, String reqId, String service_type);

    void getNotification(String requestId, String service_type);

    void rideComplete(String reqId, String status, String service_type);

    void getPaynow(String id, String service_type);

    void getRating(String reqId, String rating, String service_type, String string);

    void sendSucessRequest(PaymentSucessRequest paymentSucessRequest);


}
