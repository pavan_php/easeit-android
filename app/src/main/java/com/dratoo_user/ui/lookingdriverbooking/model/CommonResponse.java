package com.dratoo_user.ui.lookingdriverbooking.model;

public class CommonResponse {


    public String message;

    public  RequestDetails request_details;

    public RequestDetails getRequest_details() {
        return request_details;
    }

    @Override
    public String toString() {
        return "CompleteOrCancelRideResponse{" +
                "message='" + message + '\'' +
                ", request_details=" + request_details +
                '}';
    }

    public void setRequest_details(RequestDetails request_details) {
        this.request_details = request_details;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
