package com.dratoo_user.ui.lookingdriverbooking;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.maps.android.PolyUtil;
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.Constants;
import com.midtrans.sdk.corekit.core.LocalDataHandler;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme;
import com.midtrans.sdk.corekit.models.BankType;
import com.midtrans.sdk.corekit.models.TransactionResponse;
import com.midtrans.sdk.corekit.models.UserAddress;
import com.midtrans.sdk.corekit.models.UserDetail;
import com.midtrans.sdk.corekit.models.snap.CreditCard;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.api.FirebaseDetails;
import com.dratoo_user.app.App;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.BaseRetrofit;
import com.dratoo_user.ui.ConfirmationRide.model.PaymentSucessRequest;
import com.dratoo_user.ui.ConfirmationRide.model.RouteMap;
import com.dratoo_user.ui.common.ConnectivityReceiver;
import com.dratoo_user.ui.common.ProgressDialog;
import com.dratoo_user.ui.common.caranimation.AnimateMarker;
import com.dratoo_user.ui.lookingdriverbooking.model.CommonResponse;
import com.dratoo_user.ui.lookingdriverbooking.model.DriverTracking;
import com.dratoo_user.ui.lookingdriverbooking.model.FirebaseUser;
import com.dratoo_user.ui.lookingdriverbooking.presenter.LookingDriverPresenter;
import com.dratoo_user.ui.lookingdriverbooking.presenter.LookingDriverView;
import com.dratoo_user.ui.main.MainActivity;
import com.dratoo_user.ui.main.model.RideResponse;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LookingDriverBookingActivity extends AppCompatActivity implements OnMapReadyCallback, LookingDriverView, TransactionFinishedCallback, ConnectivityReceiver.ConnectivityReceiverListener {


    @Inject
    LookingDriverPresenter lookingDriverPresenter;

    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.linear_layout)
    LinearLayout linear_layout;
    @BindView(R.id.sliding_layout)
    SlidingUpPanelLayout slidingLayout;
    @BindView(R.id.tv_rating)
    AppCompatTextView tvRating;
    @BindView(R.id.img_layout)
    LinearLayout imgLayout;
    @BindView(R.id.tv_referece_nbr)
    AppCompatTextView tvRefereceNbr;
    @BindView(R.id.tv_pickup_man_name)
    AppCompatTextView tvPickupManName;
    @BindView(R.id.call_layout)
    LinearLayout callLayout;
    @BindView(R.id.chat_layout)
    LinearLayout chatLayout;
    @BindView(R.id.tv_short_address_pickup)
    AppCompatTextView tvShortAddressPickup;
    @BindView(R.id.tv_drop_address_pickup)
    AppCompatTextView tvDropAddressPickup;
    @BindView(R.id.select_pickup_layout)
    LinearLayout selectPickupLayout;
    @BindView(R.id.tv_short_address_des)
    AppCompatTextView tvShortAddressDes;
    @BindView(R.id.tv_drop_address_des)
    AppCompatTextView tvDropAddressDes;
    @BindView(R.id.select_destination_layout)
    LinearLayout selectDestinationLayout;
    @BindView(R.id.img_place_holder_cash)
    AppCompatImageView imgPlaceHolderCash;
    @BindView(R.id.select_cash_layout)
    LinearLayout selectCashLayout;
    @BindView(R.id.tv_amt)
    AppCompatTextView tvAmt;
    @BindView(R.id.cash_layout)
    RelativeLayout cashLayout;
    @BindView(R.id.parant_layout)
    CoordinatorLayout parantLayout;
    @BindView(R.id.driverRating)
    RatingBar driverRating;
    @BindView(R.id.btn_submit)
    AppCompatButton btn_submit;
    @BindView(R.id.rating_layout)
    LinearLayout ratingLayout;
    @BindView(R.id.accept_layout)
    LinearLayout acceptLayout;
    @BindView(R.id.tv_cancel_order_inside)
    AppCompatTextView tvCancelOrderInside;
    @BindView(R.id.tv_status_ride)
    AppCompatTextView tvStatusRide;
    @BindView(R.id.img_driver)
    CircleImageView imgDriver;
    @BindView(R.id.tv_total_distance)
    AppCompatTextView total_distance;
    @BindView(R.id.txtRideFare)
    AppCompatTextView txtRideFare;
    @BindView(R.id.txtTax)
    AppCompatTextView txtTax;
    @BindView(R.id.txtTotal)
    AppCompatTextView txtTotal;
    @BindView(R.id.rating_layout_view)
    LinearLayout ratingLayoutView;
    @BindView(R.id.rpl_submit)
    MaterialRippleLayout rplSubmit;
    @BindView(R.id.btn_pay)
    AppCompatButton btnPay;
    @BindView(R.id.rpl_pay)
    MaterialRippleLayout rplPay;
    @BindView(R.id.calculation_layout)
    LinearLayout calculationLayout;
    @BindView(R.id.tv_order_id)
    AppCompatTextView tvOrderId;
    @BindView(R.id.tv_order_id_complete)
    AppCompatTextView tvOrderIdComplete;
    @BindView(R.id.tv_date)
    AppCompatTextView tvDate;
    @BindView(R.id.frame_layout)
    FrameLayout frameLayout;
    @BindView(R.id.accept_layout_details)
    LinearLayout acceptLayoutDetails;
    @BindView(R.id.click_layout)
    LinearLayout clickLayout;
    @BindView(R.id.et_comments)
    AppCompatEditText etComments;

    private GoogleMap mMap;
    private BottomSheetDialog mBottomSheetDialog, mBottomsheetDialogLooking;

    View bottomSheet, bottomSheetLooking;

    String picLat, picLang, ride_status, dropLat, dropLang;
    private Marker mCurrLocationMarker;
    private Runnable runnable;
    private Handler handler;
    private String reqId;

    int rating = 0;

    ProgressDialog progressDialog;
    private Dialog dialog;
    private RideResponse ride_response;

    FirebaseUser firebaseUserResponseStatus;
    private String newOrigin, newDes;

    private ApiInterface apiInterface;

    Polyline line;
    List<RouteMap.Routes> routesDetails = new ArrayList<RouteMap.Routes>();
    List<LatLng> route = new ArrayList<>();
    private Marker mDeliveryMarker;
    private LatLng picLatLang;
    private LatLng dropLatLang;
    private String enterRideStatus = "";
    private DatabaseReference reference;
    private Marker driverMarker;

    private float v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_looking_driver_booking);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        lookingDriverPresenter.attachView(this);

        apiInterface = BaseRetrofit.getGoogleApi().create(ApiInterface.class);

        progressDialog = new ProgressDialog(this);
        handler = new Handler();


        // sliding layout initaillay set false because rating accept layout click don't change their state
        slidingLayout.setTouchEnabled(false);

        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

        if (getIntent() != null) {
            Intent intent = getIntent();
            picLat = intent.getStringExtra(Global.PIC_LAT);
            picLang = intent.getStringExtra(Global.PIC_LANG);
            dropLat = intent.getStringExtra(Global.DROP_LAT);
            dropLang = intent.getStringExtra(Global.DROP_LANG);
            ride_status = intent.getStringExtra(Global.CHECKSTATUS);
            ride_response = (RideResponse) intent.getSerializableExtra(Global.RIDE_RESPONSE);

        }

        Log.e("Nive ", "onCreate:looking ");

        PrefConnect.writeString(this, Global.REFERENCE, "");
        PrefConnect.writeString(this, Global.REFERENCE_END_TRIP, "");
        PrefConnect.writeString(this, Global.REFERENCE_START_TRIP, "");


        // below condition is redirect to corresponding trip status

        if (ride_status != null && ride_response != null) {

            picLat = ride_response.getData().getPickup_lat();
            picLang = ride_response.getData().getPickup_long();
            dropLat = ride_response.getData().getDes_lat();
            dropLang = ride_response.getData().getDes_long();


            reqId = ride_response.getData().getRequest_id();
            tvStatusRide.setText("Driver Accept The Ride");

            tvRefereceNbr.setText(PrefConnect.readString(this, Global.ORDER_PREFIX, "") + ride_response.getData().getRequest_id());
            tvOrderId.setText(PrefConnect.readString(this, Global.ORDER_PREFIX, "") + ride_response.getData().getRequest_id());
            tvOrderIdComplete.setText(PrefConnect.readString(this, Global.ORDER_PREFIX, "") + ride_response.getData().getRequest_id());
            tvDate.setText(ride_response.getData().getCreated_at1());

            total_distance.setText(ride_response.getData().getDistance_km() + " " + "Km");
            tvDropAddressPickup.setText(ride_response.getData().getPickup_address());
            tvDropAddressDes.setText(ride_response.getData().getDest_address());
            txtRideFare.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + ride_response.getData().getPrice());
            txtTax.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + ride_response.getData().getTax());
            txtTotal.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + ride_response.getData().getBill_amount());
            PrefConnect.writeString(this, Global.FINAL_AMT, ride_response.getData().getBill_amount());

            if (ride_response.getData().getId_proof() != null) {

                try {
                    Picasso.get().load(Global.IMG_URL + ride_response.getData().getId_proof()).placeholder(R.drawable.default_profile).fit().into(imgDriver);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (ride_response.getData().getName() != null) {
                tvPickupManName.setText(ride_response.getData().getFname() + " " + ride_response.getData().getLname() + ". Picking You up");
            }


            if (ride_response.getData().getPhone() != null) {

                PrefConnect.writeString(this, Global.MOBILE_DRIVER, ride_response.getData().getPhone());
            }


            if (ride_status.equals("1") || ride_status.equals("2")) {

                Log.e("Nive ", "onCreate:ride_status " + ride_status);
                initialize();

            } else if (ride_response.getData().getIs_paid().equals("0")) {


                enterRideStatus = ride_response.getData().getRequest_status();

                handler.removeCallbacks(runnable);

                Log.e("Nive ", "onCreate:Paid ");

                slidingLayout.setVisibility(View.VISIBLE);
                acceptLayoutDetails.setVisibility(View.GONE);

                calculationLayout.setVisibility(View.VISIBLE);
                ratingLayoutView.setVisibility(View.GONE);
                rplPay.setVisibility(View.VISIBLE);
                rplSubmit.setVisibility(View.GONE);

                ratingLayout.setVisibility(View.VISIBLE);

            } else if (ride_response.getData().getIs_paid().equals("1") || ride_response.getData().getRate_provider().equals("0")) {


                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);


                handler.removeCallbacks(runnable);


                slidingLayout.setVisibility(View.VISIBLE);


                acceptLayoutDetails.setVisibility(View.GONE);
                ratingLayout.setVisibility(View.VISIBLE);
                calculationLayout.setVisibility(View.GONE);
                ratingLayoutView.setVisibility(View.VISIBLE);

                rplPay.setVisibility(View.GONE);
                rplSubmit.setVisibility(View.VISIBLE);


                Log.e("Nive ", "onCreate:Rate ");

            }

        } else {

            initialize();
            initLookingBottomSheet();
        }


        setUpToolbar();


        initMap();


        //initialize payment
        initMidtransSdk();


    }

    public void initialize() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        Retrofit retrofit = null;

        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("4")) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://dratoo-services.firebaseio.com/current_request/")
                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the
                    .client(httpClient.build())
                    .build();
        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("5")) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://dratoo-services.firebaseio.com/courier_current_request/")
                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the
                    .client(httpClient.build())
                    .build();
        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("6")) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://dratoo-services.firebaseio.com/cargo_current_request/")
                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the
                    .client(httpClient.build())
                    .build();
        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("7")) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://dratoo-services.firebaseio.com/moto_current_request/")
                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the
                    .client(httpClient.build())
                    .build();
        }


        //creating the api interface
        final ApiInterface api = retrofit.create(ApiInterface.class);

        if (handler != null) {


            if (PrefConnect.readString(LookingDriverBookingActivity.this, Global.CURRNTRQID, "") != null) {


                runnable = new Runnable() {
                    public void run() {
                        Call<FirebaseUser> call = api.getStatusRide(PrefConnect.readString(LookingDriverBookingActivity.this, Global.CURRNTRQID, ""));


                        call.enqueue(new Callback<FirebaseUser>() {
                            @Override
                            public void onResponse(Call<FirebaseUser> call, Response<FirebaseUser> response) {
                                if (response.isSuccessful()) {
                                    firebaseUserResponseStatus = response.body();
                                    sucessResponse(response.body());
                                }
                            }

                            @Override
                            public void onFailure(Call<FirebaseUser> call, Throwable t) {

                            }
                        });


                        handler.postDelayed(runnable, 1000);

                    }
                };

                Log.e("Nive ", "handler: ");
                handler.postDelayed(runnable, 100);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cancelRequest(firebaseUserResponseStatus);
                    }
                }, 40000);


            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {

        String message;

        if (isConnected) {
            message = getResources().getString(R.string.connected_to_internet);

        } else {
            message = getResources().getString(R.string.not_connected_to_internet);

        }

        Global.snack_bar(parantLayout, message, Snackbar.LENGTH_LONG, LookingDriverBookingActivity.this);

    }


    private void callPaymentSucessApi(TransactionResponse response) {

        PaymentSucessRequest paymentSucessRequest = new PaymentSucessRequest();
        paymentSucessRequest.setApproval_code(response.getApprovalCode());
        paymentSucessRequest.setBank(response.getBank());
        paymentSucessRequest.setCurrency(response.getCurrency());
        paymentSucessRequest.setEci(response.getEci());
        paymentSucessRequest.setFinish_redirect_url(response.getFinishRedirectUrl());
        paymentSucessRequest.setFraud_status(response.getFraudStatus());
        paymentSucessRequest.setPayment_type(response.getPaymentType());
        paymentSucessRequest.setGross_amount(response.getGrossAmount());
        paymentSucessRequest.setOrder_id(response.getOrderId());
        paymentSucessRequest.setMasked_card(response.getMaskedCard());
        paymentSucessRequest.setPoint_redeem_amount(response.getPointBalanceAmount());
        paymentSucessRequest.setPayment_type(response.getPaymentType());
        paymentSucessRequest.setStatus_message(response.getStatusMessage());
        paymentSucessRequest.setTransaction_id(response.getTransactionId());
        paymentSucessRequest.setRequest_id(reqId);
        paymentSucessRequest.setPay_type("2");
        paymentSucessRequest.setService_type(PrefConnect.readString(this, Global.SERVICE_TYPE, ""));


        lookingDriverPresenter.sendSucessRequest(paymentSucessRequest);
    }


    private TransactionRequest initTransactionRequest() {
        // Create new Transaction Request
        TransactionRequest transactionRequestNew = new
                TransactionRequest(System.currentTimeMillis() + "", Double.parseDouble(PrefConnect.readString(this, Global.FINAL_AMT, "")));

        Log.e("Nive ", "initTransactionRequest:FinalAmt " + Double.parseDouble(PrefConnect.readString(this, Global.FINAL_AMT, "")));
        Log.e("Nive ", "initTransactionRequest:FinalAmt " + PrefConnect.readString(this, Global.EMAIL, ""));

        // Set user details
        UserDetail userDetail = new UserDetail();
        userDetail.setUserFullName(PrefConnect.readString(LookingDriverBookingActivity.this, Global.NAME, ""));
        userDetail.setEmail(PrefConnect.readString(LookingDriverBookingActivity.this, Global.EMAIL, ""));
        userDetail.setPhoneNumber(PrefConnect.readString(LookingDriverBookingActivity.this, Global.MOBILE_NBR, ""));
        userDetail.setUserId(PrefConnect.readString(this, Global.AUTH_ID, ""));

        ArrayList<UserAddress> userAddresses = new ArrayList<>();
        UserAddress userAddress = new UserAddress();
        userAddress.setAddress("indonesia");
        userAddress.setCity("jakarta");
        userAddress.setCountry("IDN");
        userAddress.setZipcode("86554");
        userAddress.setAddressType(Constants.ADDRESS_TYPE_BOTH);
        userAddresses.add(userAddress);

        userDetail.setUserAddresses(userAddresses);
        LocalDataHandler.saveObject("user_details", userDetail);

        Log.e("Nive ", "initTransactionRequest:userDetail " + new Gson().toJson(userDetail));


        // Create creditcard options for payment
        CreditCard creditCard = new CreditCard();
        creditCard.setSaveCard(false); // when using one/two click set to true and if normal set to false
        creditCard.setAuthentication(CreditCard.AUTHENTICATION_TYPE_3DS);
        creditCard.setBank(BankType.BCA); //set spesific acquiring bank
        transactionRequestNew.setCreditCard(creditCard);

        return transactionRequestNew;
    }


    private void initMidtransSdk() {


        SdkUIFlowBuilder.init()
                .setClientKey(Global.client_key) // client_key is mandatory
                .setContext(this) // context is mandatory
                .setTransactionFinishedCallback(this) // set transaction finish callback (sdk callback)
                .setMerchantBaseUrl(Global.base_url_payment) //set merchant url
                .enableLog(true) // enable sdk log
                .setColorTheme(new CustomColorTheme("#eea500", "#e5e6ea", "#4c64b5")) // will replace theme on snap theme on MAP
                .buildSDK();
    }


    @Override
    protected void onStart() {
        super.onStart();
        App.getInstance().setConnectivityListener(this);

        Log.e("Nive ", "onStart: ");

    }

    private void cancelRequest(FirebaseUser body) {

        if (body != null) {
            if (body.getStatus().equals("1") || body.getStatus().equals("2") || body.getStatus().equals("3") || body.getStatus().equals("4")) {

            } else {
                lookingDriverPresenter.rideComplete(PrefConnect.readString(LookingDriverBookingActivity.this, Global.CURRNTRQID, ""), "4", PrefConnect.readString(LookingDriverBookingActivity.this, Global.SERVICE_TYPE, ""));
            }
        }
    }


    private void sucessResponse(FirebaseUser body) {

        if (body != null) {
            if (body.getStatus().equals("1") || body.getStatus().equals("2") || body.getStatus().equals("3") || body.getStatus().equals("4")) {


                if (body.getStatus().equals("1")) {

                    if (mBottomsheetDialogLooking != null) {
                        Log.e("Nive", "sucessResponse:dismissSucess ");
                        mBottomsheetDialogLooking.dismiss();
                    }


                    if (PrefConnect.readString(this, Global.REFERENCE, "").isEmpty()) {
                        PrefConnect.writeString(LookingDriverBookingActivity.this, Global.REFERENCE, "SET");

                        setDriverDeatails();

                    } else {


                        Log.e("Nive ", "sucessResponse:2 Time ");
                    }

                } else if (body.getStatus().equals("2")) {

                    if (PrefConnect.readString(this, Global.REFERENCE_START_TRIP, "").isEmpty()) {

                        Log.e("Nive", "sucessResponse:MotoNotification ");

                        PrefConnect.writeString(this, Global.REFERENCE_START_TRIP, "SET");

                        tvCancelOrderInside.setVisibility(View.GONE);
                        tvStatusRide.setText("Driver Start The Trip");
                        lookingDriverPresenter.getNotification(PrefConnect.readString(LookingDriverBookingActivity.this, Global.CURRNTRQID, ""), PrefConnect.readString(LookingDriverBookingActivity.this, Global.SERVICE_TYPE, ""));
                    } else {
                        Log.e("Nive ", "sucessResponse:Start Trip 2 Time ");
                    }

                } else if (body.getStatus().equals("3")) {


                    Log.e("Nive ", "sucessResponse:rideComplete " + PrefConnect.readString(LookingDriverBookingActivity.this, Global.CURRNTRQID, ""));

                    if (PrefConnect.readString(this, Global.REFERENCE_END_TRIP, "").isEmpty()) {
                        PrefConnect.writeString(this, Global.REFERENCE_END_TRIP, "SET");
                        lookingDriverPresenter.rideComplete(PrefConnect.readString(LookingDriverBookingActivity.this, Global.CURRNTRQID, ""), "3", PrefConnect.readString(LookingDriverBookingActivity.this, Global.SERVICE_TYPE, ""));

                    } else {
                        Log.e("Nive ", "sucessResponse:End Trip 2 Time  ");
                    }

                }
            }
        }

    }

    private void setDriverDeatails() {

        lookingDriverPresenter.setDriverDetails("2", PrefConnect.readString(LookingDriverBookingActivity.this, Global.CURRNTRQID, ""), PrefConnect.readString(LookingDriverBookingActivity.this, Global.SERVICE_TYPE, ""));
    }

    private void initLookingBottomSheet() {

        mBottomsheetDialogLooking = new BottomSheetDialog(this);
        bottomSheetLooking = this.getLayoutInflater().inflate(R.layout.looking_driver_bottom_sheet, null);
        mBottomsheetDialogLooking.setContentView(bottomSheetLooking);

        mBottomsheetDialogLooking.setCancelable(false);
        AppCompatTextView tv_cancel_order = (AppCompatTextView) mBottomsheetDialogLooking.findViewById(R.id.tv_cancel_order);


        tv_cancel_order.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                showDialogConfirmation();
//                initCancelSheet();
                return false;
            }
        });

        mBottomsheetDialogLooking.show();
    }


    private void showDialogConfirmation() {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_cancel_sheet);
        Window window = dialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);

        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        AppCompatButton btn_done = dialog.findViewById(R.id.btn_done);
        AppCompatButton btn_no = dialog.findViewById(R.id.btn_no);

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lookingDriverPresenter.rideComplete(PrefConnect.readString(LookingDriverBookingActivity.this, Global.CURRNTRQID, ""), "4", PrefConnect.readString(LookingDriverBookingActivity.this, Global.SERVICE_TYPE, ""));

                dialog.dismiss();
            }
        });

        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);

        dialog.show();
    }


    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    private void setUpToolbar() {
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.map_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(runnable);
                Intent intent = new Intent(LookingDriverBookingActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


        if (picLat != null && picLang != null && dropLat != null && dropLang != null) {


            picLatLang = new LatLng(Double.parseDouble(picLat), Double.parseDouble(picLang));
            dropLatLang = new LatLng(Double.parseDouble(dropLat), Double.parseDouble(dropLang));

            newOrigin = picLat + "," + picLang;
            newDes = dropLat + "," + dropLang;


            if (PrefConnect.readString(this, Global.REFERENCE_END_TRIP, "") != null) {
                if (PrefConnect.readString(this, Global.REFERENCE_END_TRIP, "").equals("SET") || enterRideStatus.equals("3")) {

                } else {

                    drawRoute(newOrigin, newDes);

                }
            } else {

                drawRoute(newOrigin, newDes);
            }


        }


    }

    private void updateDriverLocation() {


        if (PrefConnect.readString(LookingDriverBookingActivity.this, Global.SERVICE_TYPE, "").equals("4")) {

            Log.e("Nive ", "updateDriverLocation:Taxi ");
            reference = FirebaseDatabase.getInstance().getReference(Global.TAXI_AVAILABLE_DRIVERS).child(PrefConnect.readString(this, Global.DRIVER_ID, ""));

        } else if (PrefConnect.readString(LookingDriverBookingActivity.this, Global.SERVICE_TYPE, "").equals("5")) {
            reference = FirebaseDatabase.getInstance().getReference(Global.COURIER_AVAILABE_DRIVERS).child(PrefConnect.readString(this, Global.DRIVER_ID, ""));

        } else if (PrefConnect.readString(LookingDriverBookingActivity.this, Global.SERVICE_TYPE, "").equals("6")) {
            reference = FirebaseDatabase.getInstance().getReference(Global.CARGO_AVAILABE_DRIVERS).child(PrefConnect.readString(this, Global.DRIVER_ID, ""));

        } else if (PrefConnect.readString(LookingDriverBookingActivity.this, Global.SERVICE_TYPE, "").equals("7")) {
            reference = FirebaseDatabase.getInstance().getReference(Global.MOTO_AVAILABE_DRIVERS).child(PrefConnect.readString(this, Global.DRIVER_ID, ""));

        }


        Log.e("Nive ", "updateDriverLocation:DriverId " + PrefConnect.readString(this, Global.DRIVER_ID, ""));

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {

                    Log.e("Nive", "onDataChange: " + dataSnapshot.getValue().toString());

                    DriverTracking user = dataSnapshot.getValue(DriverTracking.class);
                    assert user != null;
                    LatLng latLng = new LatLng(Double.parseDouble(user.getLat()), Double.parseDouble(user.getLng()));

                    if (driverMarker != null) {
                        driverMarker.remove();
                    }


                    driverMarker = mMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_map)));

                    double bearing = bearingBetweenLocations(driverMarker.getPosition(), latLng);
                    AnimateMarker.animateMarker(LookingDriverBookingActivity.this, latLng, driverMarker, mMap, bearing);
                } else {

                }

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    private double bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }


    private void drawRoute(String newOrigin, String newDes) {

        Call<RouteMap> call = apiInterface.getRoute(newOrigin, newDes);
        call.enqueue(new Callback<RouteMap>() {
            @Override
            public void onResponse(Call<RouteMap> call, Response<RouteMap> response) {
                if (response.body().getStatus().equalsIgnoreCase("OK")) {


                    if (line != null) {
                        line.remove();
                    }

                    if (mCurrLocationMarker != null) {
                        mCurrLocationMarker.remove();
                    }

                    if (mDeliveryMarker != null) {
                        mDeliveryMarker.remove();
                    }

                    mCurrLocationMarker = mMap.addMarker(new MarkerOptions().position(picLatLang)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.pick_pin))
                            .draggable(false));

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(picLatLang,
                            16));


                    mDeliveryMarker = mMap.addMarker(new MarkerOptions().position(dropLatLang)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.drop_location))
                            .draggable(false));

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dropLatLang,
                            16));

                    routesDetails.clear();
                    route.clear();

                    routesDetails.addAll(response.body().getRoutes());
                    route.addAll(PolyUtil.decode(routesDetails.get(0).getOverview_polyline().getPoints()));
                    line = mMap.addPolyline(new PolylineOptions()
                            .width(8)
                            .color(getResources().getColor(R.color.splash_bg)));
                    line.setPoints(route);

                }
            }

            @Override
            public void onFailure(Call<RouteMap> call, Throwable t) {

            }
        });
    }


    @OnClick({R.id.btn_submit, R.id.click_layout, R.id.tv_cancel_order_inside, R.id.call_layout, R.id.btn_pay})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.tv_cancel_order_inside:
                showDialogConfirmation();
//                initCancelSheet();
                break;
            case R.id.click_layout:
                slidingLayout.setTouchEnabled(false);

                Log.e("Nive ", "onViewClicked:state " + slidingLayout.getPanelState());

                if (slidingLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

                } else {

                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                }
                break;
            case R.id.btn_pay:


                Log.e("Nive ", "onViewClicked: " + PrefConnect.readString(LookingDriverBookingActivity.this, Global.PAYMENT_TYPE, ""));

                if (PrefConnect.readString(LookingDriverBookingActivity.this, Global.PAYMENT_TYPE, "").equals("2")) {

                    Log.e("Nive ", "onViewClicked: Card");

                    MidtransSDK.getInstance().setTransactionRequest(initTransactionRequest());
                    MidtransSDK.getInstance().startPaymentUiFlow(LookingDriverBookingActivity.this);
                } else {


                    Log.e("Nive ", "onViewClicked: Cash");

                    lookingDriverPresenter.getPaynow(reqId, PrefConnect.readString(LookingDriverBookingActivity.this, Global.SERVICE_TYPE, ""));
                }
                break;
            case R.id.call_layout:
                Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                dialIntent.setData(Uri.parse("tel:" + PrefConnect.readString(this, Global.MOBILE_DRIVER, "")));
                startActivity(dialIntent);
                break;

            case R.id.btn_submit:
                rating = (int) driverRating.getRating();

                Log.e("Nive ", "onViewClicked:DriverRating " + driverRating.getRating());

                PrefConnect.writeString(this, Global.REFERENCE, "");
                PrefConnect.writeString(this, Global.REFERENCE_END_TRIP, "");
                PrefConnect.writeString(this, Global.REFERENCE_START_TRIP, "");


                if (ride_status != null) {

                } else {
                    handler.removeCallbacks(runnable);
                }


                if (rating == 0.0) {

                    Log.e("Nive ", "onViewClicked:Rating");

                    Global.snack_bar(parantLayout, getResources().getString(R.string.rate_provider), Snackbar.LENGTH_LONG, LookingDriverBookingActivity.this);

                } else if (etComments.getText().toString().isEmpty()) {

                    Log.e("Nive ", "onViewClicked:etComments ");

                    Global.snack_bar(parantLayout, getResources().getString(R.string.add_comments), Snackbar.LENGTH_LONG, LookingDriverBookingActivity.this);

                } else {

                    Log.e("Nive ", "onViewClicked:Api ");

                    lookingDriverPresenter.getRating(reqId, String.valueOf(rating), PrefConnect.readString(LookingDriverBookingActivity.this, Global.SERVICE_TYPE, ""), etComments.getText().toString());
                }


                break;
        }
    }


    private void initCancelSheet() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        bottomSheet = this.getLayoutInflater().inflate(R.layout.bottomsheet_cancel_order, null);
        mBottomSheetDialog.setContentView(bottomSheet);

        RadioButton radioButtonYes = mBottomSheetDialog.findViewById(R.id.radio_yes);
        RadioButton radioButtonNo = mBottomSheetDialog.findViewById(R.id.radio_no);


        radioButtonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mBottomSheetDialog.dismiss();
                        Intent intentYes = new Intent(LookingDriverBookingActivity.this, MainActivity.class);
                        startActivity(intentYes);
                    }
                }, 1000);

            }
        });
        radioButtonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        AppCompatImageView img_close = (AppCompatImageView) mBottomSheetDialog.findViewById(R.id.img_close);

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });


        mBottomSheetDialog.show();
    }

    @Override
    public void showDriverDetails(FirebaseDetails body) {


        Log.e("Nive ", "showDriverDetails:driverId " + body.getRequest_data().getLname());


        Log.e("Nive ", "showDriverDetails:driverId : " + slidingLayout.getVisibility());
        Log.e("Nive ", "showDriverDetails:driverId : " + acceptLayoutDetails.getVisibility());
        Log.e("Nive ", "showDriverDetails:driverId : " + ratingLayout.getVisibility());


        slidingLayout.setVisibility(View.VISIBLE);
        acceptLayoutDetails.setVisibility(View.VISIBLE);
        ratingLayout.setVisibility(View.GONE);
        PrefConnect.writeString(this, Global.DRIVER_ID, body.getRequest_data().getProvider_id());
        updateDriverLocation();


        tvStatusRide.setText("Driver Accept The Ride");

        tvRefereceNbr.setText(PrefConnect.readString(this, Global.ORDER_PREFIX, "") + body.getRequest_data().getRequest_id());
        tvOrderId.setText(PrefConnect.readString(this, Global.ORDER_PREFIX, "") + body.getRequest_data().getRequest_id());
        tvOrderIdComplete.setText(PrefConnect.readString(this, Global.ORDER_PREFIX, "") + body.getRequest_data().getRequest_id());
        tvDate.setText(body.getRequest_data().getCreated_at());


        tvDropAddressPickup.setText(body.getRequest_data().getSource_address());
        tvDropAddressDes.setText(body.getRequest_data().getDest_address());

        if (body.getRequest_data().getId_proof() != null) {

            try {
                Picasso.get().load(Global.IMG_URL + body.getRequest_data().getId_proof()).placeholder(R.drawable.default_profile).fit().into(imgDriver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (body.getRequest_data().getFname() != null) {
            tvPickupManName.setText(body.getRequest_data().getFname() + " " + body.getRequest_data().getLname() + ". Picking You up");
        }


        if (body.getRequest_data().getPhone() != null) {

            PrefConnect.writeString(this, Global.MOBILE_DRIVER, body.getRequest_data().getPhone());
        }
    }

    @Override
    public void notificationSucess() {

        slidingLayout.setVisibility(View.VISIBLE);
        acceptLayoutDetails.setVisibility(View.VISIBLE);
        ratingLayout.setVisibility(View.GONE);

        updateDriverLocation();

    }

    @Override
    public void rideComplete(CommonResponse commonResponse) {

        mMap.clear();

        slidingLayout.setVisibility(View.VISIBLE);
        acceptLayoutDetails.setVisibility(View.GONE);
        ratingLayout.setVisibility(View.VISIBLE);
        rplPay.setVisibility(View.VISIBLE);
        rplSubmit.setVisibility(View.GONE);
        ratingLayoutView.setVisibility(View.GONE);
        calculationLayout.setVisibility(View.VISIBLE);

        Global.snack_bar(parantLayout, "Ride Completed Sucessfully", Snackbar.LENGTH_LONG, LookingDriverBookingActivity.this);


        if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("4")) {
            reqId = commonResponse.getRequest_details().getTaxi_request_detail().getRequest_id();
            total_distance.setText(commonResponse.getRequest_details().getTaxi_request_detail().getDistance_km() + " " + "Km");
            txtRideFare.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + commonResponse.getRequest_details().getTaxi_request_detail().getPrice());
            txtTotal.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + commonResponse.getRequest_details().getTaxi_request_detail().getBill_amount());
            txtTax.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + commonResponse.getRequest_details().getTaxi_request_detail().getTax());

        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("5")) {
            reqId = commonResponse.getRequest_details().getCourier_request_detail().getRequest_id();
            total_distance.setText(commonResponse.getRequest_details().getCourier_request_detail().getDistance_km() + " " + "Km");
            txtRideFare.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + commonResponse.getRequest_details().getCourier_request_detail().getPrice());
            txtTotal.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + commonResponse.getRequest_details().getCourier_request_detail().getBill_amount());
            txtTax.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + commonResponse.getRequest_details().getCourier_request_detail().getTax());

        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("6")) {
            reqId = commonResponse.getRequest_details().getCargo_request_detail().getRequest_id();
            total_distance.setText(commonResponse.getRequest_details().getCargo_request_detail().getDistance_km() + " " + "Km");
            txtRideFare.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + commonResponse.getRequest_details().getCargo_request_detail().getPrice());
            txtTotal.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + commonResponse.getRequest_details().getCargo_request_detail().getBill_amount());
            txtTax.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + commonResponse.getRequest_details().getCargo_request_detail().getTax());

        } else if (PrefConnect.readString(this, Global.SERVICE_TYPE, "").equals("7")) {
            reqId = commonResponse.getRequest_details().getMoto_request_detail().getRequest_id();
            total_distance.setText(commonResponse.getRequest_details().getMoto_request_detail().getDistance_km() + " " + "Km");
            txtRideFare.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + commonResponse.getRequest_details().getMoto_request_detail().getPrice());
            txtTotal.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + commonResponse.getRequest_details().getMoto_request_detail().getBill_amount());
            txtTax.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + commonResponse.getRequest_details().getMoto_request_detail().getTax());

        }


    }

    @Override
    public void paynowSucess() {


        slidingLayout.setVisibility(View.VISIBLE);
        acceptLayoutDetails.setVisibility(View.GONE);
        ratingLayout.setVisibility(View.VISIBLE);


        rplPay.setVisibility(View.GONE);
        rplSubmit.setVisibility(View.VISIBLE);
        ratingLayoutView.setVisibility(View.VISIBLE);
        calculationLayout.setVisibility(View.GONE);
        Global.snack_bar(parantLayout, "Payment Completed Sucessfully", Snackbar.LENGTH_LONG, LookingDriverBookingActivity.this);

    }

    @Override
    public void ratingResponse() {

        Intent intent = new Intent(LookingDriverBookingActivity.this, MainActivity.class);
        startActivity(intent);
      /*  new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        }, 2000);
*/

    }

    @Override
    public void rideCanceled() {

        Log.e("Nive ", "rideCanceled: ");

        handler.removeCallbacks(runnable);

        Intent intent = new Intent(LookingDriverBookingActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);


    }

    @Override
    public void showProgress() {

        progressDialog.show();

    }

    @Override
    public void hideProgress() {

        progressDialog.dismissLoader();

    }

    @Override
    public void onTransactionFinished(TransactionResult result) {

        if (result.getResponse() != null) {
            switch (result.getStatus()) {
                case TransactionResult.STATUS_SUCCESS:

                    Log.e("Nive ", "onTransactionFinished: " + new Gson().toJson(result.getResponse()));
                    callPaymentSucessApi(result.getResponse());
                    break;
                case TransactionResult.STATUS_PENDING:
                    Toast.makeText(this, "Transaction Pending. ID: " + result.getResponse().getTransactionId(), Toast.LENGTH_LONG).show();
                    break;
                case TransactionResult.STATUS_FAILED:
                    Toast.makeText(this, "Transaction Failed. ID: " + result.getResponse().getTransactionId() + ". Message: " + result.getResponse().getStatusMessage(), Toast.LENGTH_LONG).show();
                    break;
            }
            result.getResponse().getValidationMessages();
        } else if (result.isTransactionCanceled()) {
            Toast.makeText(this, "Transaction Canceled", Toast.LENGTH_LONG).show();
        } else {
            if (result.getStatus().equalsIgnoreCase(TransactionResult.STATUS_INVALID)) {
                Toast.makeText(this, "Transaction Invalid", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Transaction Finished with failure.", Toast.LENGTH_LONG).show();
            }
        }

    }
}
