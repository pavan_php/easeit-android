package com.dratoo_user.ui.lookingdriverbooking.model;

public class CommonRequest {

    public String access_by;
    public String request_id;

    public String status;
    public String rating;

    public String getAccess_by() {
        return access_by;
    }

    public void setAccess_by(String access_by) {
        this.access_by = access_by;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
