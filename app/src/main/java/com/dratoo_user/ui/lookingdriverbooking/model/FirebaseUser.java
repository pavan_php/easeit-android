package com.dratoo_user.ui.lookingdriverbooking.model;

/**
 * Created by ${Krishnaprakash} on 03-04-2018.
 */

public class FirebaseUser {

    @Override
    public String toString() {
        return "FirebaseUser{" +
                "driver_id='" + driver_id + '\'' +
                ", status='" + status + '\'' +
                ", user_id='" + user_id + '\'' +
                '}';
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String driver_id;
    public String status;
    public String user_id;
}
