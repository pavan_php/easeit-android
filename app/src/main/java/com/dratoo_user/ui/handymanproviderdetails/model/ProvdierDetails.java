package com.dratoo_user.ui.handymanproviderdetails.model;

public class ProvdierDetails {

    public String name;
    public String fname;
    public String lname;
    public String experiance;
    public String profile_image;
    public String rate_per_hour;
    public String rating;

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExperiance() {
        return experiance;
    }

    public void setExperiance(String experiance) {
        this.experiance = experiance;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getRate_per_hour() {
        return rate_per_hour;
    }

    public void setRate_per_hour(String rate_per_hour) {
        this.rate_per_hour = rate_per_hour;
    }
}
