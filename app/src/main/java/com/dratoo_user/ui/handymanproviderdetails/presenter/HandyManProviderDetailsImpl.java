package com.dratoo_user.ui.handymanproviderdetails.presenter;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;

import javax.inject.Inject;
import javax.inject.Named;

public class HandyManProviderDetailsImpl extends BasePresenter<HandyManProviderDetailsView> implements HandyManProviderDetailsPresenter {


    @Inject
    @Named(ApiModule.AUTH)
    ApiInterface apiInterface;



    @Override
    public void attachView(HandyManProviderDetailsView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }



}
