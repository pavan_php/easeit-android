package com.dratoo_user.ui.handymanproviderdetails;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.data.PrefConnect;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.common.ProgressDialog;
import com.dratoo_user.ui.confirmationHandyMan.ConfirmationHandyManActivity;
import com.dratoo_user.ui.handymanmaplocation.HandyManMapLocationActivity;
import com.dratoo_user.ui.handymanproviderdetails.adapter.RatingAdapter;
import com.dratoo_user.ui.handymanproviderdetails.presenter.HandyManProviderDetailsPresenter;
import com.dratoo_user.ui.handymanproviderdetails.presenter.HandyManProviderDetailsView;
import com.dratoo_user.ui.restarnt.model.ServiceProviderDetails;

import java.text.DecimalFormat;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class HandyManProviderDetailsActivity extends AppCompatActivity implements HandyManProviderDetailsView {


    @Inject
    HandyManProviderDetailsPresenter providerDetailsPresenter;
    ProgressDialog progressDialog;
    @BindView(R.id.back)
    AppCompatImageView back;
    @BindView(R.id.search)
    LinearLayout search;
    @BindView(R.id.img_provider)
    CircleImageView imgProvider;
    @BindView(R.id.tv_distance)
    AppCompatTextView tvDistance;
    @BindView(R.id.tv_amt_hr)
    AppCompatTextView tvAmtHr;
    @BindView(R.id.tv_rating)
    AppCompatTextView tvRating;
    @BindView(R.id.other_layout)
    LinearLayout otherLayout;
    @BindView(R.id.restarant_layout)
    LinearLayout restarantLayout;
    @BindView(R.id.call_layout)
    LinearLayout callLayout;
    @BindView(R.id.chat_layout)
    LinearLayout chatLayout;
    @BindView(R.id.tv_no_reviews_found)
    AppCompatTextView tvNoReviewsFound;
    @BindView(R.id.rv_reviews)
    RecyclerView rvReviews;
    @BindView(R.id.btn_next)
    AppCompatButton btnNext;

    @BindView(R.id.tv_provider_name)
    AppCompatTextView tvProviderName;
    @BindView(R.id.tv_summary)
    AppCompatTextView tvSummary;
    @BindView(R.id.tv_see_all)
    AppCompatTextView tvSeeAll;
    private ServiceProviderDetails list;
    private RatingAdapter adapter;
    private Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handy_man_provider_details);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        progressDialog = new ProgressDialog(this);
        providerDetailsPresenter.attachView(this);

        tvNoReviewsFound.setVisibility(View.VISIBLE);

        if (getIntent() != null) {


            list = (ServiceProviderDetails) getIntent().getSerializableExtra(Global.PROVIDER_DETAILS);

        }

        if (list != null) {

            tvSummary.setText(list.getSummary());

            DecimalFormat precision = new DecimalFormat("0.00");

            double val = Double.parseDouble(list.getRating());

            String conversion = precision.format(val);

            tvRating.setText(conversion);
            tvProviderName.setText(list.getFname()+" "+list.getLname());
            tvDistance.setText("Experience " + " : " + list.getExperiance() + "Year(s)");
            tvAmtHr.setText(PrefConnect.readString(this, Global.CURRENCY, "") + " " + list.getRate_per_hour() + " " + "/" + " " + "hr");

            try {
                Picasso.get().load(Global.IMG_URL + list.getProfile_image()).fit().into(imgProvider);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (list.getReviews().size() > 0) {

                rvReviews.setVisibility(View.VISIBLE);
                tvNoReviewsFound.setVisibility(View.GONE);

                LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                rvReviews.setLayoutManager(layoutManager);

                adapter = new RatingAdapter(this, list.getReviews(), "normal");
                rvReviews.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            } else {
                rvReviews.setVisibility(View.GONE);
                tvNoReviewsFound.setVisibility(View.VISIBLE);

            }


        }

    }


    @Override
    public void showProgress() {
        progressDialog.show();

    }

    @Override
    public void hideProgress() {
        progressDialog.dismissLoader();


    }


    @OnClick({R.id.call_layout, R.id.chat_layout, R.id.btn_next, R.id.back, R.id.tv_see_all})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.call_layout:
                if (list != null) {
                    Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                    dialIntent.setData(Uri.parse("tel:" + list.getPhone()));
                    startActivity(dialIntent);
                }

                break;
            case R.id.chat_layout:
                break;
            case R.id.tv_see_all:
                showDialog();
                break;
            case R.id.btn_next:
                Intent intent = new Intent(HandyManProviderDetailsActivity.this, ConfirmationHandyManActivity.class);
                intent.putExtra(Global.SEND_TIMING, list.getId());
                startActivity(intent);
                break;
            case R.id.back:
                Intent intentBack = new Intent(HandyManProviderDetailsActivity.this, HandyManMapLocationActivity.class);
                startActivity(intentBack);
                break;
        }
    }


    public void showDialog() {
        dialog = new Dialog(this, R.style.SlideTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.rating_dialog_seeall);
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        AppCompatImageView back = dialog.findViewById(R.id.back);
        RecyclerView recyclerView = dialog.findViewById(R.id.rv_rating);
        AppCompatTextView tv_no_reviews_found = dialog.findViewById(R.id.tv_no_reviews_found);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if (list.getReviews().size() > 0) {

            recyclerView.setVisibility(View.VISIBLE);
            tv_no_reviews_found.setVisibility(View.GONE);

            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(layoutManager);

            adapter = new RatingAdapter(this, list.getReviews(), "dialog");
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        } else {
            recyclerView.setVisibility(View.GONE);
            tv_no_reviews_found.setVisibility(View.VISIBLE);

        }

        dialog.show();

    }
}
