package com.dratoo_user.ui.handymanproviderdetails.adapter;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.ui.handymanproviderdetails.HandyManProviderDetailsActivity;
import com.dratoo_user.ui.restarnt.model.ReviewsList;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RatingAdapter extends RecyclerView.Adapter<RatingAdapter.ViewHolder> {

    HandyManProviderDetailsActivity handyManProviderDetailsActivity;

    ArrayList<ReviewsList> reviewsListArrayList;

    String type;


    public RatingAdapter(HandyManProviderDetailsActivity handyManProviderDetailsActivity, ArrayList<ReviewsList> reviews, String dialog) {


        this.handyManProviderDetailsActivity = handyManProviderDetailsActivity;

        this.reviewsListArrayList = reviews;
        this.type = dialog;

    }

    @NonNull
    @Override
    public RatingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.reviews_layout_items, viewGroup, false);
        return new ViewHolder(view);
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(@NonNull RatingAdapter.ViewHolder viewHolder, int position) {

        viewHolder.setIsRecyclable(false);

        try {
            Picasso.get().load(Global.IMG_URL + reviewsListArrayList.get(position).getProfile_pic()).fit().into(viewHolder.img_user_profile);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.e("Nive ", "onBindViewHolder:Image " + Global.IMG_URL + reviewsListArrayList.get(position).getProfile_pic());

        viewHolder.tv_rating_val.setText(reviewsListArrayList.get(position).getWorker_rating());

        viewHolder.tv_user_name.setText(reviewsListArrayList.get(position).getName());

        if (reviewsListArrayList.get(position).getUser_comments() != null) {

            viewHolder.tv_comments.setText(reviewsListArrayList.get(position).getUser_comments());
        } else {
            viewHolder.tv_comments.setText("No Comments");

        }

    }

    @Override
    public int getItemCount() {


        if (type.equals("dialog")) {

            return reviewsListArrayList.size();
        } else {
            if (reviewsListArrayList.size() > 5) {

                return 5;
            } else {

                return reviewsListArrayList.size();
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView img_user_profile;

        AppCompatTextView tv_user_name, tv_rating_val, tv_comments;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_user_profile = itemView.findViewById(R.id.img_user_profile);
            tv_user_name = itemView.findViewById(R.id.tv_user_name);
            tv_rating_val = itemView.findViewById(R.id.tv_rating_val);
            tv_comments = itemView.findViewById(R.id.tv_comments);
        }
    }
}
