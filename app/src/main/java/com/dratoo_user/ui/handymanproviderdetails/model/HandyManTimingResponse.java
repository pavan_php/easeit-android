package com.dratoo_user.ui.handymanproviderdetails.model;

public class HandyManTimingResponse {

    public String tax;
    public String service_fare;
    public String bill_amount;
    public String request_id;
    public String start_date;
    public String working_hour;

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getWorking_hour() {
        return working_hour;
    }

    public void setWorking_hour(String working_hour) {
        this.working_hour = working_hour;
    }

    public ProvdierDetails data;

    public ProvdierDetails getData() {
        return data;
    }

    public void setData(ProvdierDetails data) {
        this.data = data;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getService_fare() {
        return service_fare;
    }

    public void setService_fare(String service_fare) {
        this.service_fare = service_fare;
    }

    public String getBill_amount() {
        return bill_amount;
    }

    public void setBill_amount(String bill_amount) {
        this.bill_amount = bill_amount;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }
}
