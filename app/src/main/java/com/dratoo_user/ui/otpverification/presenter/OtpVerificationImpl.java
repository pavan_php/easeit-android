package com.dratoo_user.ui.otpverification.presenter;

import com.dratoo_user.api.ApiInterface;
import com.dratoo_user.base.BasePresenter;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.injection.module.ApiModule;
import com.dratoo_user.ui.otpverification.model.OtpVerificationResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpVerificationImpl extends BasePresenter<OtpVerificationView> implements OtpVerificationPresenter {

    @Inject
    @Named(ApiModule.DEFAULT)
    ApiInterface apiInterface;


    @Override
    public void attachView(OtpVerificationView view) {
        super.attachView(view);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void otpVerifity(String mob, String otp) {
        getView().showProgress();

        Call<OtpVerificationResponse> call = apiInterface.otpVerifiy(mob, otp);
        call.enqueue(new Callback<OtpVerificationResponse>() {
            @Override
            public void onResponse(Call<OtpVerificationResponse> call, Response<OtpVerificationResponse> response) {
                if (response.isSuccessful()) {
                    getView().hideProgress();
                    if (response.body().getData().getStatusCode().equals("1")) {
                        getView().sucess(response.body());

                    } else {
                        getView().hideProgress();
                        getView().failure(response.body().getData().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<OtpVerificationResponse> call, Throwable t) {
                getView().hideProgress();

            }
        });
    }
}
