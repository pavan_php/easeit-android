package com.dratoo_user.ui.otpverification;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.chaos.view.PinView;
import com.dratoo_user.R;
import com.dratoo_user.data.Global;
import com.dratoo_user.injection.component.DaggerAppComponent;
import com.dratoo_user.ui.common.ProgressDialog;
import com.dratoo_user.ui.main.MainActivity;
import com.dratoo_user.ui.otpverification.model.OtpVerificationResponse;
import com.dratoo_user.ui.otpverification.presenter.OtpVerificationPresenter;
import com.dratoo_user.ui.otpverification.presenter.OtpVerificationView;
import com.dratoo_user.ui.profile.ProfileActivity;
import com.google.android.material.snackbar.Snackbar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OtpVerificationActivity extends AppCompatActivity implements OtpVerificationView {
    @Inject
    OtpVerificationPresenter otpVerificationPresenter;
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.password_field)
    PinView passwordField;
    @BindView(R.id.btn_confirm)
    AppCompatButton btnConfirm;
    @BindView(R.id.tv_header)
    AppCompatTextView tvHeader;
    @BindView(R.id.parant_layout)

    RelativeLayout parantLayout;
    private MenuItem item;
    String otp, mobNbr;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        ButterKnife.bind(this);
        setUpToolbar();
        DaggerAppComponent.create().inject(this);
        otpVerificationPresenter.attachView(this);

        progressDialog = new ProgressDialog(this);

        if (getIntent().getExtras() != null) {
            otp = getIntent().getStringExtra(Global.OTP);
            mobNbr = getIntent().getStringExtra(Global.MOBILE);
        }

        if (otp != null && mobNbr != null) {
//            passwordField.setText(otp);
            tvHeader.setText(getResources().getString(R.string.enter_the_code_we_sent_via_sms) + " " + mobNbr);
        }

        if (!passwordField.getText().toString().isEmpty()) {
            btnConfirm.setBackgroundColor(getResources().getColor(R.color.dark_yellow));
        }
    }

    private void setUpToolbar() {
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.back_arrow);
        mToolbar.setNavigationOnClickListener(v -> finish());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.common, menu);
        item = menu.findItem(R.id.action_help);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @OnClick(R.id.btn_confirm)
    public void onViewClicked() {
        if (mobNbr != null && otp != null) {
            otpVerificationPresenter.otpVerifity(mobNbr, passwordField.getText().toString());
        }
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismissLoader();
    }

    @Override
    public void sucess(OtpVerificationResponse otpVerificationResponse) {
        if (otpVerificationResponse.getData().getIs_user_exist().equals("0")) {
            Intent intent = new Intent(OtpVerificationActivity.this, ProfileActivity.class);
            intent.putExtra(Global.TYPE, otpVerificationResponse.getData().getType());
            intent.putExtra(Global.MOBILE, mobNbr);
            startActivity(intent);
        } else {
            Intent intent = new Intent(OtpVerificationActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    @Override
    public void failure(String msg) {
        Global.snack_bar(parantLayout, msg, Snackbar.LENGTH_LONG, OtpVerificationActivity.this);
    }
}