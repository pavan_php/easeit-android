package com.dratoo_user.ui.otpverification.model;

public class OtpVerificationDetails {

    public String statusCode;
    public String message;
    public String type;
    public String authToken;
    public String authId;
    public String is_user_exist;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

    public String getIs_user_exist() {
        return is_user_exist;
    }

    public void setIs_user_exist(String is_user_exist) {
        this.is_user_exist = is_user_exist;
    }
}
