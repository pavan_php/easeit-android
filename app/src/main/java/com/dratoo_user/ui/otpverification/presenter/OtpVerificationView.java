package com.dratoo_user.ui.otpverification.presenter;

import com.dratoo_user.base.BaseView;
import com.dratoo_user.ui.otpverification.model.OtpVerificationResponse;

public interface OtpVerificationView extends BaseView {

    void sucess(OtpVerificationResponse otpVerificationResponse);

    void failure(String msg);
}
