package com.dratoo_user.ui.otpverification.model;

public class OtpVerificationResponse {


    public OtpVerificationDetails getData() {
        return data;
    }

    public void setData(OtpVerificationDetails data) {
        this.data = data;
    }

    public OtpVerificationDetails data;


}
