package com.dratoo_user.ui.otpverification.presenter;

import com.dratoo_user.base.Presenter;

public interface OtpVerificationPresenter extends Presenter<OtpVerificationView> {

    void  otpVerifity(String mob,String otp);
}
